#include "window.h"
#include <QDebug>
#include <QString>
#include <mesh.h>
#include <shaderprogram.h>

Window::Window(QWidget *parent)
    : QOpenGLWidget(parent)
{
}

void Window::initializeGL()
{
    QObject::connect(this, SIGNAL(frameSwapped()), this, SLOT(update()));

    auto f = QOpenGLContext::currentContext()->functions();
    f->glEnable(GL_CULL_FACE);
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_LINE_SMOOTH);
    f->glClearColor(0.9f, 0.9f, 0.9f, 1.0f);

    auto phongProgram = ShaderProgramPtr::create(":/shaders/phong.%1");
    m_meshFactory.setProgram(phongProgram);

    m_builder.setMainView(View::XZ);

    auto projMatrix = m_builder.getProjMatrix();

    for (const auto camera : ThirdAngleBuilder::Cameras)
    {
        auto viewProj = projMatrix * m_builder.getCameraMatrix(camera);
        TraceData traceData;
        traceData.viewProj = QSharedPointer<QMatrix4x4>::create(viewProj);
        m_tracingData[0].append(traceData);
    }
    m_tracingData[1] = m_tracingData[2] = m_tracingData[0];

    auto addUniforms  = [this](CameraType type, const MeshPtr &mesh)
    {
        MeshesUniforms uniforms;
        m_cameraTypeToMeshesUniforms.insert(type, uniforms);
        mesh->addUniform(QByteArrayLiteral("mvp"), QVariant::fromValue(uniforms.mvp));
        mesh->addUniform(QByteArrayLiteral("modelView"), QVariant::fromValue(uniforms.modelView));
        mesh->addUniform(QByteArrayLiteral("modelViewNormal"), QVariant::fromValue(uniforms.modelViewNormal));

        static const auto zeroVector = QVector3D{0.0f, 0.0f, 0.0f};
        auto camera = m_builder.getCameraMatrix(type);
        auto lightPosition = camera.map(zeroVector);
        lightPosition += QVector3D(0.0f, 0.0f, 10.0f);

        mesh->addUniform(QByteArrayLiteral("lightPosition"), QVariant::fromValue(lightPosition));
    };

    auto meshMain = m_meshFactory.createCornerXYZ();
    auto meshTop = MeshPtr::create(*meshMain);
    auto meshRight = MeshPtr::create(*meshMain);

    addUniforms(CameraType::MainCamera, meshMain);
    m_meshesArray.insert(meshMain);

    addUniforms(CameraType::TopCamera, meshTop);
    m_meshesArray.insert(meshTop);

    addUniforms(CameraType::RightCamera, meshRight);
    m_meshesArray.insert(meshRight);

    auto simpleProgram = ShaderProgramPtr::create(":/shaders/simple.%1");
    m_meshFactory.setProgram(simpleProgram);
    m_meshFactory.setColor({0.5f, 0.5f, 0.5f});
    const auto bordersData = m_builder.getBordersData();

    for (int i = 0; i < 2; i++)
    {
        auto borderLine = m_meshFactory.createLine(bordersData.at(i).at(0), bordersData.at(i).at(1));
        borderLine->addUniform(QByteArrayLiteral("mvp"), QVariant::fromValue(m_builder.getProjMatrix()));
        m_meshesArray.insert(borderLine);
    }
}

void Window::resizeGL(int width, int height)
{
    Q_UNUSED(width);
    Q_UNUSED(height);
    m_builder.adjustGeometry(this);
}

void Window::paintGL()
{
    auto f = QOpenGLContext::currentContext()->functions();
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (auto &mesh : m_meshesArray)
    {
        mesh->render();
    }
}

void Window::update()
{
    makeCurrent();
    static const QVector<QVector3D> unitVectors{{1.0f, 0.0f, 0.0f},
                                                {0.0f, 1.0f, 0.0f},
                                                {0.0f, 0.0f, 1.0f}};

    auto prevModel = m_model;
    m_model.rotate(3.0f, QVector3D(0.5f, 0.5f, 0.5f));
    recalculateUniforms();

    for (int i = 0; i < unitVectors.size(); i++)
    {
        const auto &vector = unitVectors.at(i);
        const QVector3D begin = prevModel.mapVector(vector);
        const QVector3D end = m_model.mapVector(vector);
        m_meshFactory.setColor(vector);
        const auto mesh = m_meshFactory.createLine(begin, end);

        for (auto &traceData : m_tracingData[i])
        {
            auto copyMesh = MeshPtr::create(*mesh);
            copyMesh->addUniform(QByteArrayLiteral("mvp"), QVariant::fromValue(traceData.viewProj));
            m_meshesArray.insert(copyMesh);

            auto &tracingLines = traceData.tracingLines;
            tracingLines.enqueue(copyMesh);
            if (tracingLines.size() > traceSize)
            {
                const auto removedMesh = tracingLines.dequeue();
                auto it = m_meshesArray.find(removedMesh);
                Q_ASSERT(it != m_meshesArray.end());
                m_meshesArray.erase(it);
            }
        }
    }

    QOpenGLWidget::update();
}

void Window::recalculateUniforms()
{
    for (auto it = m_cameraTypeToMeshesUniforms.cbegin(); it != m_cameraTypeToMeshesUniforms.cend(); ++it)
    {
        auto &cameraType = it.key();
        auto &meshesUniforms = it.value();

        auto modelView = m_builder.getCameraMatrix(cameraType) * m_model;
        *meshesUniforms.modelView = modelView;
        *meshesUniforms.modelViewNormal = modelView.normalMatrix();
        *meshesUniforms.mvp = m_builder.getProjMatrix() * modelView;
    }
}
