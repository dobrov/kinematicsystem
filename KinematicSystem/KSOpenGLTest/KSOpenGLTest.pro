#-------------------------------------------------
#
# Project created by QtCreator 2016-02-09T03:16:34
#
#-------------------------------------------------

QT += core gui
CONFIG += c++14

include(../KSOpenGL/KSOpenGL.pri)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KSOpenGLTest
TEMPLATE = app


SOURCES += main.cpp \
    window.cpp

HEADERS  += \
    window.h

RESOURCES += \
    resource.qrc
