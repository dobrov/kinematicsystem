#version 330

in vec4 gColor;
in vec3 gDist;
out vec4 fColor;

void main()
{
   float nearD = min(min(gDist[0], gDist[1]), gDist[2]);
   float edgeIntensity = exp2(-1.0*nearD*nearD);
   fColor = (1 - edgeIntensity) * gColor;
}
