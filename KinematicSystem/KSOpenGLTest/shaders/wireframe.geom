#version 330

layout(triangles) in;
layout(triangle_strip, max_vertices=3) out;

in vec4 vColor[];
out vec4 gColor;
noperspective out vec3 gDist;

uniform vec2 WIN_SCALE;

 void main()
{
     vec2 p0 = WIN_SCALE * gl_in[0].gl_Position.xy / gl_in[0].gl_Position.w;
     vec2 p1 = WIN_SCALE * gl_in[1].gl_Position.xy / gl_in[1].gl_Position.w;
     vec2 p2 = WIN_SCALE * gl_in[2].gl_Position.xy / gl_in[2].gl_Position.w;
     vec2 v0 = p2-p1;
     vec2 v1 = p2-p0;
     vec2 v2 = p1-p0;

     // shoelace formula
     float area = abs(v1.x*v2.y - v1.y*v2.x);

     // barycentric coordinate
     // height for every side
     gDist = vec3(area/length(v0),0,0);
     gColor = vColor[0];
     gl_Position = gl_in[0].gl_Position;
     EmitVertex();
     gDist = vec3(0,area/length(v1),0);
     gColor = vColor[1];
     gl_Position = gl_in[1].gl_Position;
     EmitVertex();
     gDist = vec3(0,0,area/length(v2));
     gColor = vColor[2];
     gl_Position = gl_in[2].gl_Position;
     EmitVertex();
}
