#version 130

in vec3 position;
in vec3 color;
in vec3 normal;

out vec3 vColor;
out vec3 vNormal;
out vec3 vPosition;

uniform mat4 modelView;
uniform mat3 modelViewNormal;
uniform mat4 mvp;

void main()
{
  gl_Position = mvp * vec4(position, 1.0);
  vPosition = vec3(modelView * vec4(position, 1.0));
  vNormal = modelViewNormal * normal;
  vColor = color;
}
