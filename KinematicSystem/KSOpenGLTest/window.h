#ifndef WINDOW_H
#define WINDOW_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QMatrix4x4>
#include <QQueue>
#include <transform3d.h>
#include <camera3d.h>
#include <meshfactory.h>
#include <thirdanglebuilder.h>


class Window : public QOpenGLWidget
{
    Q_OBJECT

    typedef QQueue<QSharedPointer<Mesh>> TracingLines;

    struct MeshesUniforms
    {
        QSharedPointer<QMatrix4x4> modelView = QSharedPointer<QMatrix4x4>::create();
        QSharedPointer<QMatrix3x3> modelViewNormal = QSharedPointer<QMatrix3x3>::create();
        QSharedPointer<QMatrix4x4> mvp = QSharedPointer<QMatrix4x4>::create();
    };

    struct TraceData
    {
        QSharedPointer<QMatrix4x4> viewProj;
        TracingLines tracingLines;
    };

public:
    explicit Window(QWidget *parent = nullptr);
    void initializeGL();
    void resizeGL(int width, int height);
    void paintGL();

protected slots:
    void update();

private:     
    void recalculateUniforms();

    MeshFactory m_meshFactory;
    QSet<MeshPtr> m_meshesArray;
    ThirdAngleBuilder m_builder{-1.5, 1.5, -1.5, 1.5, -1.5, 1.5};

    QMatrix4x4 m_model;
    std::array<QVector<TraceData>, 3> m_tracingData;
    const int traceSize = 25;

    QMap<CameraType, MeshesUniforms> m_cameraTypeToMeshesUniforms;
};

#endif // WINDOW_H
