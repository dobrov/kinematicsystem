#ifndef ANIMATIONWIDGET_H
#define ANIMATIONWIDGET_H

#include <QOpenGLFunctions>
#include <QOpenGLWidget>
#include <kstypes.h>
#include <meshfactory.h>
#include <camera3d.h>
#include <abstractpassingdata.h>
#include <thirdanglebuilder.h>
#include <QQueue>

class AnimationSettings;

class AnimationWidget : public QOpenGLWidget
{
    Q_OBJECT

    //Глобальные переменные для мешей сегментов.
    //Сохраняем указатели на них, чтобы потом можно было обновить их, когда придет новый kinSample
    //Их нужно пересчитывать потому что меняется model матрица
    struct MeshesUniforms
    {
        QSharedPointer<QMatrix4x4> modelView = QSharedPointer<QMatrix4x4>::create();
        QSharedPointer<QMatrix3x3> modelViewNormal = QSharedPointer<QMatrix3x3>::create();
        QSharedPointer<QMatrix4x4> mvp = QSharedPointer<QMatrix4x4>::create();
    };

    //Для каждого меша создается DynamicDrawData, которая хранит указатели на глобальные переменные (DynamicDrawData)
    //Сохраняется тип камеры, чтобы можно было получить ViewMatrix
    //Сохраняется указатель на model матрицу, который шарится с BodyTransforms
    struct DynamicDrawData
    {
        DynamicDrawData(CameraType type, const MeshPtr &mesh, const TransformPtr &transform);
        DynamicDrawData();
        MeshesUniforms meshesUniforms;
        TransformPtr modelTransform;
        CameraType cameraType;
    };

    typedef QPair<BodyPart, int> SegmentKey;
    template <typename T>
    using SegmentsMap = QMap<SegmentKey, T>;

    //Для каждого сегмента содержит model матрицу (Обновляем через handleKinData)
    typedef SegmentsMap<TransformPtr> BodyTransforms;

    //Линии траекторий, полученные из одного PosSample (Мы их сохраняем указатели на меши линий, с меткой времени для того, чтобы потом удалить
    //траектории по истечении заданного времени.
    struct TracingLines
    {
        TimeSample time;
        QVector<MeshPtr> meshes;
    };
    typedef QQueue<TracingLines> TracingLinesQueue;

    //Вспомогательная структура, которая нужна для построения линии траектории (начальная точка, цвет). конечную точку берем из PosSample
    struct TraceData
    {
        QVector3D prevPos;
        QVector3D color;
    };
    typedef SegmentsMap<TraceData> BodyTraceData;

public:
    explicit AnimationWidget(QWidget *parent = nullptr);

    virtual void initializeGL() override;
    virtual void resizeGL(int width, int height) override;
    virtual void paintGL() override;

    virtual QSize sizeHint() const override;

public slots:
    void initBody(const AnimationSettings &animSettings);
    void handleNewData(const AbstractPassingDataPointer &passingData);

private:
    void handleKinData(const DataVector<KinSample> &kinData);
    void handlePosData(const AbstractPassingDataPointer &posPassingData);

    void createTracingLine(TraceData &traceData, const QVector3D& newPos, TracingLines& tSample);
    void addThirdAnglesMeshes(const MeshPtr &meshPtr, Transform3D transform = Transform3D());
    void refreshDynamicData();
    void clear();

    MeshFactory m_meshFactory;
    ThirdAngleBuilder m_builder{-25.0f, 25.0f, -20.0f, 20.0f, -33.0f, 1.0f};

    QSet<MeshPtr> m_meshesArray;

    QVector<DynamicDrawData> m_dynamicData;
    BodyTransforms m_bodyTransforms;

    //тип камеры - viewPresenterMatrix (Нужно для создания линий траекторий)
    QMap<CameraType, QSharedPointer<QMatrix4x4>> m_vpByCameraType;

    BodyTraceData m_bodyTraceData;
    TracingLinesQueue m_tracingLines;
    TimeSample m_traceDuration;

    ShaderProgramPtr m_phongProgram;
    ShaderProgramPtr m_simpleProgram;
};

#endif // ANIMATIONWIDGET_H
