#ifndef SENSORCALIBRATIONDIALOG_H
#define SENSORCALIBRATIONDIALOG_H

#include <QDialog>
#include <kstypes.h>
#include <QSet>
#include <QString>

namespace Ui {
class SensorCalibrationDialog;
}

class SensorCalibrationDialog : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(QString calDirectory READ calDirectory WRITE setCalDirectory USER true)
    Q_PROPERTY(QString offsetFilePath READ offsetFilePath WRITE setOffsetFilePath USER true)

public:
    explicit SensorCalibrationDialog(QWidget *parent = 0);
    ~SensorCalibrationDialog();

    QString calDirectory() const;
    void setCalDirectory(const QString &calDirectory);

    QString offsetFilePath() const;
    void setOffsetFilePath(const QString &offsetFilePath);

signals:
    void sendCalDataMap(const CalDataMap &calDataMap);
    void sendOffsetData(const OffsetData &bodyOffset);

private slots:
    void on_chooseCalDirectory_clicked();
    void on_chooseOffsetFile_clicked();
    void tryReadOffsetFile(const QString &filePath);
    void tryReadCalFiles(const QString &dirPath);

    void on_refreshButton_clicked();

private:
    QString m_calDirectory;
    QString m_offsetFilePath;

    Ui::SensorCalibrationDialog *ui;
};

#endif // SENSORCALIBRATIONDIALOG_H
