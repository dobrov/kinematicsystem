#ifndef SEMAPHORELABEL_H
#define SEMAPHORELABEL_H

#include <QWidget>
#include <QColor>

class SemaphoreLabel : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QColor color  READ color WRITE setColor)
public:
    explicit SemaphoreLabel(QWidget *parent = 0);

    QColor color() const;

protected:
    virtual void paintEvent(QPaintEvent *) override;

public slots:
    void setColor(const QColor &color);
    void setAnimation(const QColor &bColor, const QColor &eColor, int msecs);

private:
    QColor m_color;
};

#endif // SEMAPHORELABEL_H
