#version 130

in vec3 vColor;
in vec3 vNormal;
in vec3 vPosition;
out vec4 fColor;

uniform vec3 lightPosition = vec3(0.0, 0.0, 0.0);
uniform vec3 lightIntensity = vec3(1.0, 1.0, 1.0);

uniform vec3 ka = vec3(0.1, 0.1, 0.1); // Ambient reflectivity
//uniform vec3 kd = vec3(0.8, 0.8, 0.8); // Diffuse reflectivity
uniform vec3 ks = vec3(0.7, 0.7, 0.7); // Specular reflectivity
uniform float shininess = 5; // Specular shininess factor

void main()
{
    //all vec in view frame (camera space)
    vec3 l = normalize(lightPosition - vPosition);
    vec3 v = normalize(-vPosition);
    vec3 n = normalize(vNormal);
    vec3 r = reflect(-l, n);

    // Calculate the diffuse component
    float diffuse = max(dot(l, n), 0.0);

    // Calculate the specular component
    float specular = 0.0;
    if (diffuse != 0.0)
        specular = pow(max(dot(r, v), 0.0), shininess);

    vec3 ambientColor = ka * vColor;
    vec3 diffuseColor = vColor;
    vec3 color = lightIntensity * (ambientColor + diffuseColor * diffuse + ks * specular);

    fColor = vec4(color, 1.0);
}
