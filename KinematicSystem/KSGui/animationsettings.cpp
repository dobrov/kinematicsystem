#include "animationsettings.h"

AnimationSettings::AnimationSettings()
{
    m_builder.setMainView(View::XZ);
    m_builder.setAxisDirection(Axis::X, AxisDirection::Reverse);
    m_builder.setAxisDirection(Axis::Y, AxisDirection::Reverse);
    m_builder.setAxisDirection(Axis::Z, AxisDirection::Normal);
}

ThirdAngleBuilder AnimationSettings::builder() const
{
    return m_builder;
}

void AnimationSettings::setBuilder(const ThirdAngleBuilder &builder)
{
    m_builder = builder;
}

EnabledSegmentsMap AnimationSettings::enabledSegments() const
{
    return m_enabledSegmentsMap;
}

void AnimationSettings::setEnabledSegments(const EnabledSegmentsMap &enabledSegmentsMap)
{
    m_enabledSegmentsMap = enabledSegmentsMap;
}

float AnimationSettings::traceDuration() const
{
    return m_traceDuration;
}

void AnimationSettings::setTraceDuration(float traceDuration)
{
    m_traceDuration = traceDuration;
}

void AnimationSettings::setOffsetData(const OffsetData &offsetData)
{
    m_bodyInit = offsetData.m_bodyInit;
    m_hipLength = offsetData.m_hipLength;

    const auto &bodyDevList = offsetData.m_deviceList;
    EnabledSegmentsMap newSegmentsMap;
    for (auto it = bodyDevList.cbegin(); it != bodyDevList.cend(); ++it)
    {
        QVector<bool> oldSegments;
        auto nOffset = it.value().size();
        auto findIt = m_enabledSegmentsMap.find(it.key());
        if (findIt != m_enabledSegmentsMap.cend())
        {
            oldSegments = findIt.value();
        }
        else
        {
            if (!m_enabledSegmentsMap.empty())
            {
                auto anotherBodyPart = m_enabledSegmentsMap.cbegin();
                Q_ASSERT(anotherBodyPart.key() != it.key());
                oldSegments = anotherBodyPart.value();
            }
            else
            {
                oldSegments = {true};
            }
        }
        auto nSettings = oldSegments.size();

        QVector<bool> newSegments;
        auto nDiff = nOffset - nSettings;
        if (nDiff >= 0)
        {
            newSegments = oldSegments;
        }
        else if (nDiff < 0)
        {
            newSegments = oldSegments.mid(-nDiff); //truncate beginning
        }
        newSegmentsMap.insert(it.key(), newSegments);
    }
    m_enabledSegmentsMap = newSegmentsMap;
}

BodyParam AnimationSettings::bodyInit() const
{
    return m_bodyInit;
}

QVector3D AnimationSettings::hipLength() const
{
    return m_hipLength;
}

bool AnimationSettings::isEnabled() const
{
    return m_isEnabled;
}

void AnimationSettings::setIsEnabled(bool isEnabled)
{
    m_isEnabled = isEnabled;
}

QDataStream& operator <<(QDataStream& ds, const AnimationSettings &settings)
{
    ds << settings.m_isEnabled;
    ds << settings.m_builder;
    ds << settings.m_enabledSegmentsMap;
    ds << settings.m_traceDuration;
    return ds;
}

QDataStream& operator >>(QDataStream& ds, AnimationSettings &settings)
{
    ds >> settings.m_isEnabled;
    ds >> settings.m_builder;
    ds >> settings.m_enabledSegmentsMap;
    ds >> settings.m_traceDuration;
    return ds;
}
