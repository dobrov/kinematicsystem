#-------------------------------------------------
#
# Project created by QtCreator 2015-08-19T18:55:54
#
#-------------------------------------------------

QT       += core gui printsupport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KinematicSystemGUI
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qcustomplot.cpp \
    choosedevicedialog.cpp \
    initdevicedialog.cpp

HEADERS  += mainwindow.h \
    qcustomplot.h \
    choosedevicedialog.h \
    initdevicedialog.h

FORMS    += mainwindow.ui \
    choosedevicedialog.ui \
    initdevicedialog.ui

Debug:LIBS += ../../KinematicSystem/libs/Debug/ftd2xx.lib\
           ../../KinematicSystem/Debug/Core.lib

Release:LIBS += ../../KinematicSystem/libs/Release/ftd2xx.lib\
             ../../KinematicSystem/Release/Core.lib

INCLUDEPATH += ../../KinematicSystem/includes\
            ../../KinematicSystem/Core

RESOURCES += \
    resource.qrc
