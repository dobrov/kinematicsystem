#include "abstactplot.h"
#include <qwt_plot_canvas.h>
#include <qwt_plot_glcanvas.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_grid.h>
#include <qwt_legend.h>
#include "circularbuffer.h"
#include <concretepassingdata.h>
#include <QDebug>

AbstractPlot::AbstractPlot(QWidget *parent)
    : QwtPlot(parent)
{
    QwtPlotGLCanvas *canvas = new QwtPlotGLCanvas;
    setCanvas(canvas);
    setCanvasBackground(Qt::white);
    insertLegend(new QwtLegend);

    QwtPlotGrid *grid = new QwtPlotGrid;
    grid->setMajorPen(Qt::gray);
    grid->attach(this);
}

void AbstractPlot::setBufferSize(int bufferSize)
{
    setAxisScale(QwtPlot::xBottom, 0, bufferSize * 1.01);

    for (const auto dataBuffer : m_dataBufferArray)
    {
        dataBuffer->setSize(bufferSize);
    }
    replot();
}

void AbstractPlot::setYRange(float min, float max)
{
    setAxisScale(QwtPlot::yLeft, min * 1.1f, max * 1.1f);
    replot();
}

void AbstractPlot::clear()
{
    for (const auto dataBuffer : m_dataBufferArray)
    {
        dataBuffer->clear();
    }
    replot();
}

void AbstractPlot::fillPlotList(const QVector<AbstractPlot::PlotParameters> plotParametersVector, int bufferSize)
{
    for (const auto &param : plotParametersVector)
    {
        auto plot = new QwtPlotCurve(std::get<Title>(param));
        plot->setPen(std::get<PenColor>(param), 2);
        plot->setRenderHint(QwtPlotItem::RenderAntialiased);
        auto dataBuffer = new CircularBuffer(bufferSize);
        plot->setData(dataBuffer);
        m_dataBufferArray.append(dataBuffer);
        plot->attach(this);
    }
}

void AbstractPlot::addDataBuffer(CircularBuffer * const dataBuffer)
{
    m_dataBufferArray.append(dataBuffer);
}

QSize AbstractPlot::sizeHint() const
{
    return QSize(100, 50);
}

template <typename T>
void AbstractPlot::handleNewDataImpl(const QSharedPointer<ConcretePassingData<T>> &passingData,
                                     std::enable_if_t<!std::is_scalar<T>::value>*)
{
    const auto &data = passingData->getData();
    Q_ASSERT(m_dataBufferArray.size() >= data.first().size());

    for (const auto &sample : data)
    {
        for (int i = 0; i < sample.size(); i++)
        {
            m_dataBufferArray.at(i)->addData(sample.at(i));
        }
    }
}

template <typename T>
void AbstractPlot::handleNewDataImpl(const QSharedPointer<ConcretePassingData<T> > &passingData,
                                     std::enable_if_t<std::is_scalar<T>::value>*)
{
    const auto &data = passingData->getData();
    Q_ASSERT(m_dataBufferArray.size() == 1);

    for (const auto sample : data)
        m_dataBufferArray.first()->addData(sample);
}

SensorDataPlot::SensorDataPlot(QWidget *parent, bool plotMagnitude, int bufferSize)
    : AbstractPlot(parent), m_plotMagnitude(plotMagnitude)
{
    static const QVector<PlotParameters> params{std::make_tuple(QStringLiteral("X"), Qt::red),
                                                std::make_tuple(QStringLiteral("Y"), Qt::green),
                                                std::make_tuple(QStringLiteral("Z"), Qt::blue)};
    fillPlotList(params, bufferSize);

    if (m_plotMagnitude)
    {
        auto plot = new QwtPlotCurve(QStringLiteral("M"));
        plot->setPen(Qt::black, 2);
        plot->setRenderHint(QwtPlotItem::RenderAntialiased);
        m_magnitudeBuffer = new CircularBuffer(bufferSize);
        plot->setData(m_magnitudeBuffer);
        plot->attach(this);
        addDataBuffer(m_magnitudeBuffer);
    }
}

SensorDataPlot::~SensorDataPlot()
{
}

template <typename T>
void SensorDataPlot::addMagnitudeData(const QSharedPointer<ConcretePassingData<T>> &passingData)
{
    const auto &data = passingData->getData();
    for (const auto &sample : data)
    {
        float x = std::get<0>(sample);
        float y = std::get<1>(sample);
        float z = std::get<2>(sample);

        float magnitude = sqrt(x*x + y*y + z*z);
        m_magnitudeBuffer->addData(magnitude);
    }
}

RawDataPlot::RawDataPlot(QWidget *parent, bool plotMagnitude, int bufferSize)
    : SensorDataPlot(parent, plotMagnitude, bufferSize)
{
}

void RawDataPlot::handleNewData(const AbstractPassingDataPointer &passingData)
{
    const auto &rawPassingData = qSharedPointerCast<RawPassingData>(passingData);
    handleNewDataImpl<RawSample>(rawPassingData);
    if (m_plotMagnitude)
        addMagnitudeData<RawSample>(rawPassingData);
    replot();
}

CalDataPlot::CalDataPlot(QWidget *parent, bool plotMagnitude, int bufferSize)
    : SensorDataPlot(parent, plotMagnitude, bufferSize)
{
}

void CalDataPlot::handleNewData(const AbstractPassingDataPointer &passingData)
{
    const auto calPassingData = qSharedPointerCast<CalPassingData>(passingData);
    handleNewDataImpl<CalSample>(calPassingData);
    if (m_plotMagnitude)
        addMagnitudeData<CalSample>(calPassingData);
    replot();
}

QuatDataPlot::QuatDataPlot(QWidget *parent, int bufferSize)
    : AbstractPlot(parent)
{
    static const QVector<PlotParameters> params{std::make_tuple(QStringLiteral("W"), Qt::black),
                                                std::make_tuple(QStringLiteral("X"), Qt::red),
                                                std::make_tuple(QStringLiteral("Y"), Qt::green),
                                                std::make_tuple(QStringLiteral("Z"), Qt::blue)};
    fillPlotList(params, bufferSize);
}

void QuatDataPlot::handleNewData(const AbstractPassingDataPointer &passingData)
{
    handleNewDataImpl<QuatSample>(qSharedPointerCast<QuatPassingData>(passingData));
    replot();
}

DipDataPlot::DipDataPlot(QWidget *parent, int bufferSize)
    : AbstractPlot(parent)
{
    static const QVector<PlotParameters> params{std::make_tuple(QStringLiteral("A"), Qt::blue)};
    fillPlotList(params, bufferSize);
}

void DipDataPlot::handleNewData(const AbstractPassingDataPointer &passingData)
{
    handleNewDataImpl<DipSample>(qSharedPointerCast<DipPassingData>(passingData));
    replot();
}
