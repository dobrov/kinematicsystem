#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "plottingwidget.h"

#include <QMainWindow>
#include <QString>
#include <QMap>
#include <kstypes.h>
#include <abstractfusionfiltersettings.h>

namespace Ui {
class MainWindow;
}

class AbstractWorker;
class InitDeviceDialog;
class FilterSettingsDialog;
class PlottingSettingsDialog;
class SensorCalibrationDialog;
class AnimationSettingsDialog;
class SemaphoreLabel;
class Frame;
class QThread;
class QLabel;

class MainWindow : public QMainWindow
{
    Q_OBJECT
    Q_PROPERTY(QString fileToSave READ fileToSave WRITE setFileToSave USER true)

    struct WorkerData
    {
        WorkerData ();
        WorkerData (QThread *thread, AbstractWorker *worker);

        QThread *thread;
        AbstractWorker *worker;
    };

    enum class DeviceCondition
    {
        NotConnected,
        NotInitialized,
        Ready,
        Writing
    };

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    QString fileToSave() const;
    void setFileToSave(const QString &fileToSave);

    QString calDirectory() const;
    void setCalDirectory(const QString &dirName);

signals:
    void writeToPort(const Frame &frame, bool isPeriodic);
    void startRecording();
    void forceStop();
    void stopRecording();
    void saveToFile(const QString &fileName);
    void sensitivity(float accelSens, float gyroSens, float magSens);
    void dataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate);
    void fullScale(quint16 accelFullScale, quint16 gyroFullScale, quint16 magFullScale);
    void sendCalDirectory(const QString &dirName);
    void chosenDevices(const DeviceList &deviceList);
    void filterSettings(const AbstractFilterSettingsPointer &settings);
    void stopAllWorkers();

private slots:
    void updateStatusBar(const QString &msg);
    void handleOpenPort();
    void handleClosePort();
    void handleConnectedDevice(const DeviceList &data);
    void handleAck();
    void handleSyncTime();

    void on_actionInitDevice_triggered();
    void on_actionStartStop_triggered();
    void on_actionSave_triggered();
    void on_actionChoosePlot_triggered();
    void on_actionChooseCalDir_triggered();
    void on_actionFilterSetttings_triggered();
    void on_actionAnimationSettings_triggered();

private:
    void setupComPort(QThread *thread);
    void setupFrameDecoder(QThread *thread);
    void setupFlowQueue(QThread *thread);
    void setupInterpolator(QThread *thread);
    void setupMagImpulseNoiseFilter(QThread *thread);
    void setupFlowSynchronizer(QThread *thread);
    void setupCalibrator(QThread *thread);
    void setupSensorFusion(QThread *thread);
    void setupDataSystem(QThread *thread);
    void setupKinematicsCalculator(QThread *thread);
    void setupDipAngleCalculator(QThread *thread);
    void setupLogger(QThread *thread);

    void setupInterconnections();

    void createStatusBar();
    void setupWidgets();
    void createInitDeviceDialog(const DeviceList &deviceList);

    Ui::MainWindow *ui;
    InitDeviceDialog *m_initDialog;
    FilterSettingsDialog *m_filterSettingsDialog;
    PlottingSettingsDialog *m_plottingSettingsDialog;
    SensorCalibrationDialog *m_sensorCalibrationDialog;
    AnimationSettingsDialog *m_animationSettingsDialog;

    QScopedPointer<PlottingWidget> m_plottingWidget;

    DeviceCondition m_deviceCondition;
    SemaphoreLabel *m_conditionLabel, *m_syncLabel;
    QLabel *m_statusLabel;

    QMap<QString, WorkerData> m_workersMap;
    qint8 m_waitingAckCounter;

    QString m_fileToSave;

protected:
    virtual void closeEvent(QCloseEvent *event);
};

#endif // MAINWINDOW_H
