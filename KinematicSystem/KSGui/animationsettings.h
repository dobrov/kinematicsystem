#ifndef ANIMATIONSETTINGS_H
#define ANIMATIONSETTINGS_H

#include <thirdanglebuilder.h>
#include "kstypes.h"

//Если сегмент содержится в векторе, то он отображается в анимации (true - показывается траектория, false - не показывается)
typedef BodyPartMap<QVector<bool>> EnabledSegmentsMap;

class AnimationSettings
{
public:
    AnimationSettings();

    ThirdAngleBuilder builder() const;
    void setBuilder(const ThirdAngleBuilder &builder);

    EnabledSegmentsMap enabledSegments() const;
    void setEnabledSegments(const EnabledSegmentsMap &enabledSegments);

    float traceDuration() const;
    void setTraceDuration(float traceDuration);

    void setOffsetData(const OffsetData &offsetData);

    BodyParam bodyInit() const;
    QVector3D hipLength() const;

    bool isEnabled() const;
    void setIsEnabled(bool isEnabled);

private:
    ThirdAngleBuilder m_builder{-20.0, 20.0, -20.0, 20.0, -20.0, 20.0};
    EnabledSegmentsMap m_enabledSegmentsMap{{BodyPart::LHL, {false, false, true}}};
    float m_traceDuration = 5.0f; //seconds
    BodyParam m_bodyInit;
    QVector3D m_hipLength;
    bool m_isEnabled;

    friend QDataStream& operator <<(QDataStream& ds, const AnimationSettings &bodyPart);
    friend QDataStream& operator >>(QDataStream& ds, AnimationSettings &bodyPart);
};

Q_DECLARE_METATYPE(AnimationSettings)
QDataStream& operator <<(QDataStream& ds, const AnimationSettings &bodyPart);
QDataStream& operator >>(QDataStream& ds, AnimationSettings &bodyPart);

#endif // ANIMATIONSETTINGS_H
