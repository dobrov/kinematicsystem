#ifndef FILTERSETTINGSDIALOG_H
#define FILTERSETTINGSDIALOG_H

#include <QDialog>
#include <abstractfusionfiltersettings.h>

namespace Ui {
class FilterSettingsDialog;
}

class FilterSettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit FilterSettingsDialog(QWidget *parent = 0);
    ~FilterSettingsDialog();

    AbstractFilterSettingsPointer getSettings();

public slots:
    virtual int exec() override;

private:
    void restoreDlgState();
    void loadDlgState();

    AbstractFilterSettingsPointer m_currentSettings;
    Ui::FilterSettingsDialog *ui;

};

#endif // FILTERSETTINGSDIALOG_H
