#include "plottingsettingsdialog.h"
#include "ui_plottingsettingsdialog.h"
#include "settingssaver.h"
#include <algorithm>
#include <QListWidgetItem>
#include <QMenu>
#include <QMetaEnum>

PlottingSettingsDialog::PlottingSettingsDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::PlottingSettingsDialog), m_currentDeviceId(0), m_isDirty(true),
    m_showMagnitudeAccel(false), m_showMagnitudeMag(false)
{
    ui->setupUi(this);
    SettingsSaver::readSettings(this);
    auto checkBoxList = this->ui->groupBox->findChildren<QCheckBox*>();
    for (const auto checkBox : checkBoxList)
    {
        QObject::connect(checkBox, &QCheckBox::stateChanged, this,
                         &PlottingSettingsDialog::handleStateChanged);
    }
    ui->chooseFlowQueue->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(ui->chooseFlowQueue, &QWidget::customContextMenuRequested,
                     this, &PlottingSettingsDialog::eraseSelectedItems);
}

PlottingSettingsDialog::~PlottingSettingsDialog()
{
    SettingsSaver::writeSettings(this);
    delete ui;
}

FlowIdVector PlottingSettingsDialog::currentFlowId() const
{
    return m_currentFlowId;
}

void PlottingSettingsDialog::setCurrentFlowId(const FlowIdVector &existFlowId)
{
    m_currentFlowId = existFlowId;
}

void PlottingSettingsDialog::setDeviceList(const DeviceList &deviceList)
{
    ui->comboBox->clear();
    static QString nameTemplate = tr("Датчик %1");
    for (const auto deviceId : deviceList)
    {
        ui->comboBox->addItem(nameTemplate.arg(deviceId));
    }

    m_currentFlowId.erase(std::remove_if(m_currentFlowId.begin(), m_currentFlowId.end(),
                          [&deviceList](const FlowId &value)
                          {
                            return !deviceList.contains(std::get<FDeviceId>(value));
                          }),
                          m_currentFlowId.end());

    emit sendFlowIdVector(m_currentFlowId, m_showMagnitudeAccel, m_showMagnitudeMag);
    m_isDirty = true;
}

void PlottingSettingsDialog::uncheckAllCheckBox()
{
    auto checkBoxList = ui->groupBox->findChildren<QCheckBox *>();
    for (const auto checkBox : checkBoxList)
    {
        checkBox->setChecked(false);
    }
}

void PlottingSettingsDialog::checkCheckBox(const PassingDataType type)
{
    if (type == PassingDataType::RawAccel)
        ui->rawAccelBox->setChecked(true);
    else if (type == PassingDataType::RawGyro)
        ui->rawGyroBox->setChecked(true);
    else if (type == PassingDataType::RawMag)
        ui->rawMagBox->setChecked(true);
    else if (type == PassingDataType::CalAccel)
        ui->calAccelBox->setChecked(true);
    else if (type == PassingDataType::CalGyro)
        ui->calGyroBox->setChecked(true);
    else if (type == PassingDataType::CalMag)
        ui->calMagBox->setChecked(true);
    else if (type == PassingDataType::Quat)
        ui->quatBox->setChecked(true);
    else if (type == PassingDataType::DipAngle)
        ui->dipAngleBox->setChecked(true);
    else
        Q_ASSERT(false);
}

void PlottingSettingsDialog::refreshFlowQueueList()
{
    ui->chooseFlowQueue->clear();
    static const QString nameTemplate = tr("Датчик: %1, Тип: %2");
    static const auto metaEnum = QMetaEnum::fromType<PassingDataType>();
    for (const auto &flowId : m_unsavedFlowId)
    {
        ui->chooseFlowQueue->addItem(nameTemplate.arg(std::get<FDeviceId>(flowId))
                                     .arg(metaEnum.key(static_cast<int>(std::get<FPassingDataType>(flowId)))));
    }
}

bool PlottingSettingsDialog::showMagnitudeMag() const
{
    return m_showMagnitudeMag;
}

void PlottingSettingsDialog::setShowMagnitudeMag(bool showMagnitudeMag)
{
    m_showMagnitudeMag = showMagnitudeMag;
}

bool PlottingSettingsDialog::showMagnitudeAccel() const
{
    return m_showMagnitudeAccel;
}

void PlottingSettingsDialog::setShowMagnitudeAccel(bool showMagnitudeAccel)
{
    m_showMagnitudeAccel = showMagnitudeAccel;
}

void PlottingSettingsDialog::showEvent(QShowEvent *)
{
    m_unsavedFlowId = m_currentFlowId;
    ui->showMagnitudeAccelBox->setChecked(m_showMagnitudeAccel);
    ui->showMagnitudeMagBox->setChecked(m_showMagnitudeMag);
    m_isDirty = true;
}

void PlottingSettingsDialog::accept()
{
    m_currentFlowId = m_unsavedFlowId;
    m_showMagnitudeAccel = ui->showMagnitudeAccelBox->isChecked();
    m_showMagnitudeMag = ui->showMagnitudeMagBox->isChecked();
    emit sendFlowIdVector(m_currentFlowId, m_showMagnitudeAccel, m_showMagnitudeMag);
    done(QDialog::Accepted);
}

void PlottingSettingsDialog::paintEvent(QPaintEvent *)
{
    if (m_isDirty)
    {
        uncheckAllCheckBox();
        static auto predicate = [this](const FlowId &flowId)
        {
            return (std::get<FDeviceId>(flowId) == m_currentDeviceId) ? true : false;
        };

        auto iter = std::find_if(std::begin(m_unsavedFlowId), std::end(m_unsavedFlowId), predicate);
        while (iter != std::end(m_unsavedFlowId))
        {
            checkCheckBox(std::get<FPassingDataType>(*iter));
            iter = std::find_if(iter + 1, std::end(m_unsavedFlowId), predicate);
        }
        refreshFlowQueueList();
        m_isDirty = false;
    }
}

void PlottingSettingsDialog::on_comboBox_currentIndexChanged(const QString &deviceName)
{
    if (deviceName.isEmpty())
        return;

    int idIndex = deviceName.indexOf(QStringLiteral(" ")) + 1;
    Q_ASSERT(idIndex != 0);
    m_currentDeviceId = deviceName.mid(idIndex).toUShort();
    m_isDirty = true;
}

void PlottingSettingsDialog::handleStateChanged(int state)
{
    if (m_isDirty)
        return;

    PassingDataType type;
    auto checkBoxName = QObject::sender()->objectName();
    if (checkBoxName == QStringLiteral("rawAccelBox"))
        type = PassingDataType::RawAccel;
    else if (checkBoxName == QStringLiteral("rawGyroBox"))
        type = PassingDataType::RawGyro;
    else if (checkBoxName == QStringLiteral("rawMagBox"))
        type = PassingDataType::RawMag;
    else if (checkBoxName == QStringLiteral("calAccelBox"))
        type = PassingDataType::CalAccel;
    else if (checkBoxName == QStringLiteral("calGyroBox"))
        type = PassingDataType::CalGyro;
    else if (checkBoxName == QStringLiteral("calMagBox"))
        type = PassingDataType::CalMag;
    else if (checkBoxName == QStringLiteral("quatBox"))
        type = PassingDataType::Quat;
    else if (checkBoxName == QStringLiteral("dipAngleBox"))
        type = PassingDataType::DipAngle;
    else
        Q_ASSERT(false);

    auto flowId = std::make_tuple(this->m_currentDeviceId, type);

    if (state == Qt::Checked)
    {
        m_unsavedFlowId.append(flowId);
        if (m_unsavedFlowId.size() > m_maxSize)
            m_unsavedFlowId.removeFirst();
    }
    else if (state == Qt::Unchecked)
    {
#ifdef QT_DEBUG
        bool result = m_unsavedFlowId.removeOne(flowId);
        Q_ASSERT(result);
#else
        m_unsavedFlowId.removeOne(flowId);
#endif
    }
    else
        Q_ASSERT(false);

    m_isDirty = true;
}

void PlottingSettingsDialog::eraseSelectedItems()
{
    const auto selectedItems = ui->chooseFlowQueue->selectedItems();
    if (selectedItems.empty())
        return;
    Q_ASSERT(selectedItems.size() == 1);
    const auto item = selectedItems.first();
    auto index = ui->chooseFlowQueue->row(item);
    m_unsavedFlowId.removeAt(index);
    m_isDirty = true;
    update();
}
