#include "semaphorelabel.h"
#include <QPainter>
#include <QPropertyAnimation>

SemaphoreLabel::SemaphoreLabel(QWidget *parent)
    : QWidget(parent), m_color(Qt::black)
{
    setFixedSize(15, 15);
}

QColor SemaphoreLabel::color() const
{
    return m_color;
}

void SemaphoreLabel::setColor(const QColor &color)
{
    m_color = color;
    update();
}

void SemaphoreLabel::setAnimation(const QColor &bColor, const QColor &eColor, int msecs)
{
    auto animation = new QPropertyAnimation(this, QByteArrayLiteral("color"));
    animation->setDuration(msecs);
    animation->setStartValue(bColor);
    animation->setEndValue(eColor);
    animation->start(QAbstractAnimation::DeleteWhenStopped);
}

void SemaphoreLabel::paintEvent(QPaintEvent *)
{
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    painter.setBrush(m_color);
    auto widgetRect = rect();
    widgetRect -= QMargins(1, 1, 1, 1);
    painter.drawEllipse(widgetRect);
}
