#ifndef INITDEVICEDIALOG_H
#define INITDEVICEDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QMap>
#include <framedecoder.h>
#include "ui_initdevicedialog.h"
#include <frame.h>
#include <kstypes.h>

namespace Ui {
class InitDeviceDialog;
}

class ConfigureAllDevicePacket;

class InitDeviceDialog : public QDialog
{
    Q_OBJECT

public:
    explicit InitDeviceDialog(const DeviceList &list, QWidget *parent = 0);
    ~InitDeviceDialog();

signals:
    void sensitivity(float accelSens, float gyroSens, float magSens) const;
    void dataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate) const;
    void fullScale(quint16 accelFullScale, quint16 gyroFullScale, quint16 magFullScale) const;
    void chosenDevices(const DeviceList &deviceList);

public slots:
    virtual void accept() override;
    virtual int exec() override;
    void setExistingDevice(const DeviceList &list);
    Frame getInitFrame();

private:
    void buildConfigureDevicePacket(ConfigureAllDevicePacket &packet) const;

    Ui::InitDeviceDialog *ui;

    static const QMap<int, float> m_sensitivityAccel;
    static const QMap<int, float> m_sensitivityGyro;
    static const QMap<int, float> m_sensitivityMag;

    static const QMap<int, quint16> m_dataRateAccel;
    static const QMap<int, quint16> m_dataRateGyro;
    static const QMap<int, quint16> m_dataRateMag;

    static const QMap<int, quint16> m_fullScaleAccel;
    static const QMap<int, quint16> m_fullScaleGyro;
    static const QMap<int, quint16> m_fullScaleMag;

public:
    static const SensOrientArray m_signMap;
    static const SensOrientArray m_axisMap;

};

#endif // INITDEVICEDIALOG_H
