#ifndef ANIMATIONSETTINGSDIALOG_H
#define ANIMATIONSETTINGSDIALOG_H

#include <QDialog>
#include "animationsettings.h"

namespace Ui {
class AnimationSettingsDialog;
}

class QLayout;
class QComboBox;
class QCheckBox;

class AnimationSettingsDialog : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(AnimationSettings animationSettings READ animationSettings WRITE setAnimationSettings USER true)

public:
    explicit AnimationSettingsDialog(QWidget *parent);
    ~AnimationSettingsDialog();

    AnimationSettings animationSettings() const;
    void setAnimationSettings(const AnimationSettings &animationSettings);

public slots:
    virtual int exec() override;
    void setOffsetData(const OffsetData &offsetData);

signals:
    void sendAnimSettings(const AnimationSettings &animSettings);

private slots:
    void handleEnabledBodyPart(bool isEnabled);
    void handleChangeSegmentsComboBox(int index);
    void on_enableAnimCheckBox_toggled(bool checked);

private:
    void fillMainViewComboBox();
    void fillAxisDirectionGroupBox();
    void fillSegmentsGroupBox(const BodyDevList &bodyDevList);
    void fillTraceGroupBox(const BodyDevList &bodyDevList);
    void deleteLayout(QLayout *const layout);

    struct BodyPartWidgetPtr
    {
        BodyPartWidgetPtr()
            : bodyPartComboBox(nullptr), bodyPartCheckBox(nullptr)
        {}
        inline bool isNull() const
        {
            return bodyPartComboBox == nullptr;
        }

        QComboBox* bodyPartComboBox; //Количество сегментов у части тела
        QCheckBox* bodyPartCheckBox; //Включена/выключена часть тела
        QMap<int, QCheckBox*> tracesCheckBoxes; //Показ траектории для каждого сегмента(сустава)
    };

    BodyPartWidgetPtr getBodyPartWidgets(const char *bodyPartKey);

    void restoreDlgState();
    void saveDlgState();

    Ui::AnimationSettingsDialog *ui;
    AnimationSettings m_animationSettings;
};

#endif // ANIMATIONSETTINGSDIALOG_H
