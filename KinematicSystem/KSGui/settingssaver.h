#ifndef SETTINGSSAVER_H
#define SETTINGSSAVER_H

#include <QWidget>
#include <QSettings>

class SettingsSaver
{
public:
    static void readSettings(QWidget* window);
    static void writeSettings(QWidget* window);

private:
    SettingsSaver() {}
    static void recurseRead(const QSettings& settings, QObject* object);
    static void recurseWrite(QSettings &settings, QObject *object);
};

#endif // SETTINGSSAVER_H
