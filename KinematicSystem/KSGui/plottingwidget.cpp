#include "plottingwidget.h"
#include <limits>
#include <QLayout>
#include <QVBoxLayout>
#include <QLayoutItem>
#include <QMainWindow>

PlottingWidget::PlottingWidget(QWidget *parent) :
    QWidget(parent, Qt::Window), m_accelDataRate(0), m_gyroDataRate(0), m_magDataRate(0),
    m_accelFullScale(0), m_gyroFullScale(0), m_magFullScale(0)
{
    setLayout(new QVBoxLayout);
    setMinimumSize(640, 480);
}

void PlottingWidget::clear()
{
    for (const auto &plot : m_plotMap)
    {
        Q_ASSERT(plot);
        plot->clear();
    }
}

void PlottingWidget::clearLayout()
{
    QLayoutItem *item;
    while((item = layout()->takeAt(0)) != 0)
    {
        delete item->widget();
        delete item;
    }
}

void PlottingWidget::setYRange(const QPointer<AbstractPlot> &plot, PassingDataType type)
{
    Q_ASSERT(!plot.isNull());
    switch (type)
    {
    case PassingDataType::RawAccel:
    case PassingDataType::RawGyro:
    case PassingDataType::RawMag:
        plot->setYRange(std::numeric_limits<qint16>::min(),
                        std::numeric_limits<qint16>::max());
        break;
    case PassingDataType::CalAccel:
        plot->setYRange(-m_accelFullScale * 1.1f, m_accelFullScale * 1.1f);
        break;
    case PassingDataType::CalGyro:
        plot->setYRange(-m_gyroFullScale * 1.1f, m_gyroFullScale * 1.1f);
        break;
    case PassingDataType::CalMag:
        plot->setYRange(-m_magFullScale * 1.1f, m_magFullScale * 1.1f);
        break;
    case PassingDataType::Quat:
    case PassingDataType::DipAngle:
        plot->setYRange(-1.2f, 1.2f);
        break;
    default:
        Q_ASSERT(false);
    }
}

void PlottingWidget::setBufferSize(const QPointer<AbstractPlot> &plot, PassingDataType type)
{
    switch (type)
    {
    case PassingDataType::CalAccel:
    case PassingDataType::RawAccel:
        plot->setBufferSize(m_duration * m_accelDataRate);
        break;
    case PassingDataType::CalGyro:
    case PassingDataType::RawGyro:
    case PassingDataType::Quat:
        plot->setBufferSize(m_duration * m_gyroDataRate);
        break;
    case PassingDataType::RawMag:
    case PassingDataType::CalMag:
    case PassingDataType::DipAngle:
        plot->setBufferSize(m_duration * m_magDataRate);
        break;
    default:
        Q_ASSERT(false);
    }
}

QString PlottingWidget::getTitle(const FlowId &flowId)
{
    static QString titleTemplate(tr("Датчик %1, %2"));
    QString sensorName;
    switch (std::get<FPassingDataType>(flowId))
    {
    case PassingDataType::RawAccel:
        sensorName = tr("Сырой Акселерометр");
        break;
    case PassingDataType::RawGyro:
        sensorName = tr("Сырой Гироскоп");
        break;
    case PassingDataType::RawMag:
        sensorName = tr("Сырой Магнитометр");
        break;
    case PassingDataType::CalAccel:
        sensorName = tr("Калиброванный Акселерометр");
        break;
    case PassingDataType::CalGyro:
        sensorName = tr("Калиброванный Гироскоп");
        break;
    case PassingDataType::CalMag:
        sensorName = tr("Калиброванный Магнитометр");
        break;
    case PassingDataType::Quat:
        sensorName = tr("Кватернион");
        break;
    case PassingDataType::DipAngle:
        sensorName = tr("Магнитное наклонение");
        break;
    default:
        Q_ASSERT(false);
    }
    return titleTemplate.arg(std::get<FDeviceId>(flowId)).arg(sensorName);
}

void PlottingWidget::handleNewData(const AbstractPassingDataPointer &passingData)
{
    auto it = m_plotMap.find(std::make_tuple(passingData->getDeviceId(), passingData->getDataType()));
    if(it != m_plotMap.end())
        it.value()->handleNewData(passingData);
}

void PlottingWidget::setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate)
{
    m_accelDataRate = accelDataRate;
    m_gyroDataRate = gyroDataRate;
    m_magDataRate = magDataRate;

    for (auto it = m_plotMap.cbegin(); it != m_plotMap.cend(); ++it)
        setBufferSize(it.value(), std::get<FPassingDataType>(it.key()));
}

void PlottingWidget::setFullScale(quint16 accelFullScale, quint16 gyroFullScale, quint16 magFullScale)
{
    m_accelFullScale = accelFullScale;
    m_gyroFullScale = gyroFullScale;
    m_magFullScale = magFullScale;

    for (auto it = m_plotMap.cbegin(); it != m_plotMap.cend(); ++it)
        setYRange(it.value(), std::get<FPassingDataType>(it.key()));
}

void PlottingWidget::setFlowList(const FlowIdVector &flowIdList, bool showMagnitudeAccel, bool showMagnitudeMag)
{
    clearLayout();
    m_plotMap.clear();

    if (flowIdList.isEmpty())
    {
        this->hide();
        return;
    }

    for (const auto &flowId : flowIdList)
    {
        auto type = std::get<FPassingDataType>(flowId);

        AbstractPlot *plot = nullptr;
        switch (type)
        {
        case PassingDataType::RawAccel:
            plot = new RawDataPlot(this, showMagnitudeAccel);
            break;
        case PassingDataType::RawMag:
            plot = new RawDataPlot(this, showMagnitudeMag);
            break;
        case PassingDataType::RawGyro:
            plot = new RawDataPlot(this);
            break;
        case PassingDataType::CalAccel:
            plot = new CalDataPlot(this, showMagnitudeAccel);
            break;
        case PassingDataType::CalMag:
            plot = new CalDataPlot(this, showMagnitudeMag);
            break;
        case PassingDataType::CalGyro:
            plot = new CalDataPlot(this);
            break;
        case PassingDataType::Quat:
            plot = new QuatDataPlot(this);
            break;
        case PassingDataType::DipAngle:
            plot = new DipDataPlot(this);
            break;
        default:
            Q_ASSERT(false);
        }
        plot->setTitle(getTitle(flowId));
        setBufferSize(plot, type);
        setYRange(plot, type);
        layout()->addWidget(plot);
        m_plotMap.insert(flowId, plot);
    }

    this->show();
}
