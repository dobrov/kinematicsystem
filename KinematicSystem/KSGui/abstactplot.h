#ifndef SENSORPLOT_H
#define SENSORPLOT_H


#include <qwt_plot.h>
#include <concretepassingdata.h>
#include <QSharedPointer>

class QwtPlotCurve;
class CircularBuffer;

class AbstractPlot : public QwtPlot
{
   Q_OBJECT

public:
    typedef std::tuple<QString, Qt::GlobalColor> PlotParameters;
    enum PlotParametersIndex
    {
        Title,
        PenColor
    };

    AbstractPlot(QWidget *parent);

public slots:
    void setBufferSize(int bufferSize);
    void setYRange(float min, float max);
    virtual void handleNewData(const AbstractPassingDataPointer &passingData)  = 0;
    void clear();

protected:
    void fillPlotList(const QVector<PlotParameters> plotParametersVector, int bufferSize);
    void addDataBuffer(CircularBuffer *const dataBuffer);
    template <typename T>
    void handleNewDataImpl(const QSharedPointer<ConcretePassingData<T>> &data,
                           std::enable_if_t<!std::is_scalar<T>::value>* = nullptr);
    template <typename T>
    void handleNewDataImpl(const QSharedPointer<ConcretePassingData<T>> &data,
                           std::enable_if_t<std::is_scalar<T>::value>* = nullptr);

private:
    QVector<CircularBuffer*> m_dataBufferArray;

public:
    virtual QSize sizeHint() const override;
};

class SensorDataPlot : public AbstractPlot
{
public:
    SensorDataPlot(QWidget *parent, bool plotMagnitude, int bufferSize);
    virtual ~SensorDataPlot() = 0;

protected:
    template <typename T>
    void addMagnitudeData(const QSharedPointer<ConcretePassingData<T>> &data);

    bool m_plotMagnitude;

private:
    CircularBuffer *m_magnitudeBuffer;
};

class RawDataPlot : public SensorDataPlot
{
public:
    explicit RawDataPlot(QWidget *parent = nullptr, bool plotMagnitude = false, int bufferSize = 3000);
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
};

class CalDataPlot : public SensorDataPlot
{
public:
    explicit CalDataPlot(QWidget *parent = nullptr, bool plotMagnitude = false, int bufferSize = 3000);
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
};

class QuatDataPlot : public AbstractPlot
{
public:
    explicit QuatDataPlot(QWidget *parent = nullptr, int bufferSize = 3000);
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
};

class DipDataPlot : public AbstractPlot
{
public:
    explicit DipDataPlot(QWidget *parent = nullptr, int bufferSize = 3000);
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
};

#endif // SENSORPLOT_H
