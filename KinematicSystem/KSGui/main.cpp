#include "mainwindow.h"
#include <QApplication>
#include <QStyleFactory>
#include <QFile>
#include <abstractpassingdata.h>
#include <abstractfusionfiltersettings.h>
#include <animationsettings.h>
#include <frame.h>
#include <QSettings>
#include <QSurfaceFormat>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setOrganizationName("SUAI");
    a.setApplicationName("Kinematic System");
    a.setStyle(QStyleFactory::create("Fusion"));

//    QSettings settings;
//    auto allKeys = settings.allKeys();
//    for (const auto & key : allKeys)
//    {
//        settings.remove(key);
//    }

    qRegisterMetaType<DeviceList>("DeviceList");
    qRegisterMetaType<AbstractPassingDataPointer>("AbstractPassingDataPointer");
    qRegisterMetaType<AbstractFilterSettingsPointer>("AbstractFilterSettingsPointer");
    qRegisterMetaType<Frame>("Frame");
    qRegisterMetaType<SensOrientArray>("SensOrientArray");
    qRegisterMetaType<CalDataMap>("CalDataMap");
    qRegisterMetaType<OffsetData>("OffsetData");
    qRegisterMetaType<TimeSample>("TimeSample");
    qRegisterMetaTypeStreamOperators<FlowIdVector>("FlowIdVector");
    qRegisterMetaType<FlowIdVector>("FlowIdVector");
    qRegisterMetaTypeStreamOperators<AnimationSettings>("AnimationSettings");
    qRegisterMetaType<AnimationSettings>("AnimationSettings");


    QSurfaceFormat format;
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setProfile(QSurfaceFormat::CoreProfile);
    format.setVersion(3, 3);
    format.setSamples(16);
    QSurfaceFormat::setDefaultFormat(format);

    MainWindow w;

    QFile file(":/qss/default.qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    w.setStyleSheet(styleSheet);

    w.show();

    return a.exec();
}
