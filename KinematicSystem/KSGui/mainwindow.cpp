#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "initdevicedialog.h"
#include "plottingsettingsdialog.h"
#include "sensorcalibrationdialog.h"
#include "filtersettingsdialog.h"
#include "animationsettingsdialog.h"
#include "animationwidget.h"
#include "semaphorelabel.h"

#include <QStatusBar>
#include <QMessageBox>
#include <QFileDialog>
#include <QDir>
#include <QRegExp>
#include <QLabel>
#include <QElapsedTimer>
#include <QThread>
#include <QCloseEvent>
#include <algorithm>
#include <settingssaver.h>
#include <frame.h>
#include <devicelistpacket.h>
#include <requestdatapacket.h>
#include <comportworker.h>
#include <framedecoder.h>
#include <flowsynchronizer.h>
#include <flowqueue.h>
#include <logger.h>
#include <linearinterpolator.h>
#include <magimpulsenoisefilter.h>
#include <calibrator.h>
#include <sensorfusion.h>
#include <datasystem.h>
#include <kinematicscalculator.h>
#include <dipanglecalculator.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), ui(new Ui::MainWindow), m_initDialog(nullptr),
    m_filterSettingsDialog(new FilterSettingsDialog(this)),
    m_plottingSettingsDialog(new PlottingSettingsDialog(this)),
    m_sensorCalibrationDialog(new SensorCalibrationDialog(this)),
    m_animationSettingsDialog(new AnimationSettingsDialog(this)),
    m_deviceCondition(DeviceCondition::NotConnected),
    m_waitingAckCounter(0), m_fileToSave("test.json")
{
    ui->setupUi(this);
    ui->animationWidget->hide();
    createStatusBar();
    setupWidgets();

    QThread *comThread = new QThread(this);
    setupComPort(comThread);
    setupFrameDecoder(comThread);
    setupMagImpulseNoiseFilter(comThread);
    setupInterpolator(comThread);
    setupFlowSynchronizer(comThread);
    setupCalibrator(comThread);
    setupSensorFusion(comThread);
    setupKinematicsCalculator(comThread);
    setupDipAngleCalculator(comThread);

    QThread *dataThread = new QThread(this);
    setupDataSystem(dataThread);

    setupFlowQueue(thread());

    QThread *loggerThread = new QThread(this);
    setupLogger(loggerThread);

    setupInterconnections();

    comThread->start(QThread::HighPriority);
    loggerThread->start(QThread::LowPriority);
    dataThread->start(QThread::LowPriority);

    SettingsSaver::readSettings(this);
}

#define CREATE_WORKER(name) \
    Q_CHECK_PTR(thread); \
    Q_ASSERT(!m_workersMap.contains(QStringLiteral(#name))); \
    name *worker = new name; \
    m_workersMap.insert(QStringLiteral(#name), {thread, worker})

void MainWindow::setupComPort(QThread *thread)
{
    CREATE_WORKER(ComPortWorker);
    worker->setMessageError(false);
    worker->setMessageRawData(true);
    QObject::connect(this, &MainWindow::writeToPort, worker, &ComPortWorker::write);
    QObject::connect(worker, &ComPortWorker::opened, this, &MainWindow::handleOpenPort);
    QObject::connect(worker, &ComPortWorker::closed, this, &MainWindow::handleClosePort);
}

void MainWindow::setupFrameDecoder(QThread *thread)
{
    CREATE_WORKER(FrameDecoder);
    QObject::connect(worker, &FrameDecoder::ackReceived, this, &MainWindow::handleAck);
    QObject::connect(worker, &FrameDecoder::connectedDeviceData, this, &MainWindow::handleConnectedDevice);
    QObject::connect(worker, &FrameDecoder::syncTime, this, &MainWindow::handleSyncTime);
}

void MainWindow::setupFlowQueue(QThread *thread)
{
    CREATE_WORKER(FlowQueue);
    worker->setRefreshInterval(8);
    worker->setDelay(20000);

    QObject::connect(worker, &FlowQueue::sendData, m_plottingWidget.data(), &PlottingWidget::handleNewData);
    QObject::connect(worker, &FlowQueue::sendData, ui->animationWidget, &AnimationWidget::handleNewData);
    QObject::connect(m_plottingSettingsDialog, &PlottingSettingsDialog::sendFlowIdVector,
                     worker, &FlowQueue::setFlowList);
}

void MainWindow::setupInterpolator(QThread *thread)
{
    CREATE_WORKER(LinearInterpolator);
    QObject::connect(this, &MainWindow::dataRate, worker, &AbstractInterpolator::setDataRate);
}

void MainWindow::setupMagImpulseNoiseFilter(QThread *thread)
{
    CREATE_WORKER(MagImpulseNoiseFilter);
}

void MainWindow::setupFlowSynchronizer(QThread *thread)
{
    CREATE_WORKER(FlowSynchronizer);
    QObject::connect(this, &MainWindow::stopRecording, worker, &FlowSynchronizer::handleStopRecording);
    QObject::connect(this, &MainWindow::forceStop, worker, &FlowSynchronizer::forceStopRecording);
    QObject::connect(this, &MainWindow::startRecording, worker, &FlowSynchronizer::handleStartRecording);
    QObject::connect(this, &MainWindow::dataRate, worker, &FlowSynchronizer::setDataRate);
    QObject::connect(this, &MainWindow::chosenDevices, [worker](const DeviceList &deviceList)
    {
        FlowIdVector flowVector;
        for (const auto id : deviceList)
        {
            flowVector.append(std::make_tuple(id, PassingDataType::RawAccel));
            flowVector.append(std::make_tuple(id, PassingDataType::RawGyro));
            flowVector.append(std::make_tuple(id, PassingDataType::RawMag));
        }
        QMetaObject::invokeMethod(worker, "setSynchronizingFlows", Q_ARG(FlowIdVector, flowVector));
    });
}

void MainWindow::setupCalibrator(QThread *thread)
{
    CREATE_WORKER(Calibrator);
    QObject::connect(this, &MainWindow::sensitivity, worker, &Calibrator::setSensitivity);
    QObject::connect(this, &MainWindow::fullScale, worker, &Calibrator::setFullScale);
    QObject::connect(m_sensorCalibrationDialog, &SensorCalibrationDialog::sendCalDataMap,
                     worker, &Calibrator::setCalDataMap);
    QMetaObject::invokeMethod(worker, "setAxisMap", Q_ARG(SensOrientArray, InitDeviceDialog::m_axisMap));
    QMetaObject::invokeMethod(worker, "setSignMap", Q_ARG(SensOrientArray, InitDeviceDialog::m_signMap));
}

void MainWindow::setupSensorFusion(QThread *thread)
{
    CREATE_WORKER(SensorFusion);
    QObject::connect(this, &MainWindow::dataRate, worker, &SensorFusion::setDataRate);
    QObject::connect(this, &MainWindow::filterSettings, worker, &SensorFusion::setFilterSettings);
    QMetaObject::invokeMethod(worker, "setFilterSettings",
        Q_ARG(AbstractFilterSettingsPointer, m_filterSettingsDialog->getSettings()));
}

void MainWindow::setupDataSystem(QThread *thread)
{
    CREATE_WORKER(DataSystem);
    QObject::connect(this, &MainWindow::dataRate, worker, &DataSystem::setDataRate);
    QObject::connect(this, &MainWindow::fullScale, worker, &DataSystem::setFullScale);
    QObject::connect(this, &MainWindow::saveToFile, worker, &DataSystem::saveToFile);
    QObject::connect(this, &MainWindow::startRecording, worker, &DataSystem::clear);
    QObject::connect(worker, &DataSystem::tempFileFound, this, &MainWindow::on_actionSave_triggered);
}

void MainWindow::setupKinematicsCalculator(QThread *thread)
{
    CREATE_WORKER(KinematicsCalculator);
    QObject::connect(this, &MainWindow::dataRate, worker, &KinematicsCalculator::setDataRate);
    QObject::connect(m_sensorCalibrationDialog, &SensorCalibrationDialog::sendOffsetData,
                     worker, &KinematicsCalculator::setOffsetData);
}

void MainWindow::setupDipAngleCalculator(QThread *thread)
{
    CREATE_WORKER(DipAngleCalculator);
    QObject::connect(this, &MainWindow::dataRate, worker, &DipAngleCalculator::setDataRate);
}

void MainWindow::setupLogger(QThread *thread)
{
    CREATE_WORKER(Logger);
}
#undef CREATE_WORKER

void MainWindow::createInitDeviceDialog(const DeviceList &deviceList)
{
    m_initDialog = new InitDeviceDialog(deviceList, this);
    QObject::connect(m_initDialog, &InitDeviceDialog::dataRate, this, &MainWindow::dataRate);
    QObject::connect(m_initDialog, &InitDeviceDialog::sensitivity, this, &MainWindow::sensitivity);
    QObject::connect(m_initDialog, &InitDeviceDialog::fullScale, this, &MainWindow::fullScale);
    QObject::connect(m_initDialog, &InitDeviceDialog::chosenDevices, this, &MainWindow::chosenDevices);
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    emit stopAllWorkers();
    m_plottingWidget->close();
    SettingsSaver::writeSettings(this);
    event->accept();
}

void MainWindow::setupInterconnections()
{    
#define CHECK_EXIST(worker) Q_ASSERT(m_workersMap.contains(QStringLiteral(#worker)))
    CHECK_EXIST(ComPortWorker);
    CHECK_EXIST(FrameDecoder);
    CHECK_EXIST(FlowQueue);
    CHECK_EXIST(FlowSynchronizer);
    CHECK_EXIST(LinearInterpolator);
    CHECK_EXIST(MagImpulseNoiseFilter);
    CHECK_EXIST(Logger);
    CHECK_EXIST(Calibrator);
    CHECK_EXIST(DataSystem);
    CHECK_EXIST(KinematicsCalculator);
    CHECK_EXIST(DipAngleCalculator);
#undef CHECK_EXIST

#define WORKER(name) qobject_cast<name*>(m_workersMap[QStringLiteral(#name)].worker)
    QObject::connect(WORKER(ComPortWorker), &ComPortWorker::readDataAvailable,
                     WORKER(FrameDecoder), &FrameDecoder::parseFrame);
    QObject::connect(WORKER(FrameDecoder), &FrameDecoder::sendData,
                     WORKER(MagImpulseNoiseFilter), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(MagImpulseNoiseFilter), &AbstractPassingDataHandler::sendData,
                     WORKER(LinearInterpolator), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(LinearInterpolator), &AbstractPassingDataHandler::sendData,
                     WORKER(FlowSynchronizer), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(FlowSynchronizer), &AbstractPassingDataHandler::sendData,
                     WORKER(DataSystem), &DataSystem::handleNewData);
    QObject::connect(WORKER(FlowSynchronizer), &AbstractPassingDataHandler::sendData,
                     WORKER(FlowQueue), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(FlowSynchronizer), &AbstractPassingDataHandler::sendData,
                     WORKER(Calibrator), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(Calibrator), &AbstractPassingDataHandler::sendData,
                     WORKER(FlowQueue), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(Calibrator), &AbstractPassingDataHandler::sendData,
                     WORKER(SensorFusion), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(Calibrator), &AbstractPassingDataHandler::sendData,
                     WORKER(DipAngleCalculator), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(SensorFusion), &AbstractPassingDataHandler::sendData,
                     WORKER(FlowQueue), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(SensorFusion), &AbstractPassingDataHandler::sendData,
                     WORKER(KinematicsCalculator), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(KinematicsCalculator), &AbstractPassingDataHandler::sendData,
                     WORKER(FlowQueue), &AbstractPassingDataHandler::handleNewData);
    QObject::connect(WORKER(DipAngleCalculator), &AbstractPassingDataHandler::sendData,
                     WORKER(FlowQueue), &AbstractPassingDataHandler::handleNewData);
//    QObject::connect(WORKER(KinematicsCalculator), &AbstractPassingDataHandler::sendData,
//                     WORKER(DataSystem), &DataSystem::handleNewData);
    QObject::connect(WORKER(FrameDecoder), &FrameDecoder::syncTime,
                     WORKER(DataSystem), &DataSystem::handleNewData);

    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::stopRecording,
                     WORKER(ComPortWorker), &ComPortWorker::handleStopPeriodic);
    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::stopRecording,
                     WORKER(MagImpulseNoiseFilter), &AbstractPassingDataHandler::clear);
    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::stopRecording,
                     WORKER(LinearInterpolator), &AbstractPassingDataHandler::clear);
    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::stopRecording,
                     WORKER(FlowQueue), &AbstractPassingDataHandler::clear);
    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::stopRecording,
                     WORKER(SensorFusion), &AbstractPassingDataHandler::clear);
    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::stopRecording,
                     WORKER(DipAngleCalculator), &AbstractPassingDataHandler::clear);
    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::stopRecording,
                     WORKER(KinematicsCalculator), &AbstractPassingDataHandler::clear);
    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::stopRecording,
                     WORKER(DataSystem), &DataSystem::stopTimer);
    QObject::connect(WORKER(FlowSynchronizer), &FlowSynchronizer::beginTime,
                     WORKER(FrameDecoder), &FrameDecoder::setBeginTime);

    for (const auto &workerData: m_workersMap)
    {
        QObject::connect(workerData.thread, &QThread::started, workerData.worker, &AbstractWorker::startWork);
        QObject::connect(this, &MainWindow::stopAllWorkers, workerData.worker, &AbstractWorker::finishWork);
        QObject::connect(workerData.worker, &AbstractWorker::message, WORKER(Logger), &Logger::handleMessage);
    }
#undef WORKER
}

MainWindow::~MainWindow()
{  
    for (const auto &workerData : m_workersMap)
    {
        if(workerData.thread != this->thread() && workerData.thread->isRunning())
            workerData.thread->wait();
    }
    delete ui;
}

void MainWindow::updateStatusBar(const QString &msg)
{
    Q_CHECK_PTR(m_conditionLabel);
    Q_CHECK_PTR(m_statusLabel);

    switch (m_deviceCondition)
    {
    case DeviceCondition::NotConnected:
        m_conditionLabel->setColor(Qt::red);
        break;
    case DeviceCondition::NotInitialized:
        m_conditionLabel->setColor(Qt::yellow);
        break;
    case DeviceCondition::Writing:
    case DeviceCondition::Ready:
        m_conditionLabel->setColor(Qt::green);
        break;
    }

    m_statusLabel->setText(msg);
}

void MainWindow::handleOpenPort()
{
    Q_ASSERT(m_deviceCondition != DeviceCondition::Ready &&
             m_deviceCondition != DeviceCondition::NotInitialized);

    m_deviceCondition = DeviceCondition::NotInitialized;
    Frame frame;
    frame.addPacket(DeviceListPacket());
    emit writeToPort(frame, false);
    updateStatusBar(tr("Устройство подключено"));
}

void MainWindow::handleClosePort()
{
    if (m_deviceCondition == DeviceCondition::Writing)
    {
        ui->actionSave->setEnabled(true);
        emit forceStop();
    }

    m_deviceCondition = DeviceCondition::NotConnected;
    ui->actionInitDevice->setDisabled(true);
    ui->actionStartStop->setDisabled(true);
    ui->actionChoosePlot->setDisabled(true);
    ui->actionChooseCalDir->setEnabled(true);
    ui->actionFilterSetttings->setEnabled(true);
    updateStatusBar(tr("Устройство отключено"));
}

void MainWindow::handleConnectedDevice(const DeviceList &data)
{
    Q_ASSERT(m_deviceCondition == DeviceCondition::NotInitialized);

    if (!m_initDialog)
        createInitDeviceDialog(data);
    else
        m_initDialog->setExistingDevice(data);

    ui->actionInitDevice->setEnabled(true);
    updateStatusBar(tr("Получен список датчиков"));
}

void MainWindow::handleAck()
{
    Q_ASSERT(m_deviceCondition == DeviceCondition::NotInitialized);
    Q_ASSERT(m_waitingAckCounter >= 0);
    Q_ASSERT(!ui->actionStartStop->isEnabled());

    if(--m_waitingAckCounter == 0)
    {
        //wait for gyro firing
        QTimer::singleShot(500, [this]()
        {
            if (m_deviceCondition == DeviceCondition::NotInitialized)
            {
                m_deviceCondition = DeviceCondition::Ready;
                updateStatusBar(tr("Устройство готово к началу записи"));
                ui->actionStartStop->setEnabled(true);
            }
        });
        ui->actionChooseCalDir->setEnabled(true);
        ui->actionChoosePlot->setEnabled(true);
    }
}

void MainWindow::handleSyncTime()
{
    Q_CHECK_PTR(m_syncLabel);
    m_syncLabel->setColor(Qt::green);
    QTimer::singleShot(1500, [this]()
    {
        m_syncLabel->setAnimation(Qt::green, Qt::black, 2000);
    });
}

void MainWindow::createStatusBar()
{
    statusBar()->addWidget(m_conditionLabel = new SemaphoreLabel);
    statusBar()->addWidget(m_statusLabel = new QLabel);
    m_statusLabel->setIndent(3);

    statusBar()->addWidget(m_syncLabel = new SemaphoreLabel);
    m_syncLabel->hide();
    auto syncText = new QLabel(tr("Синхросигнал"));
    syncText->setObjectName(QStringLiteral("syncText"));
    syncText->hide();
    statusBar()->addWidget(syncText);

    updateStatusBar(tr("Устройство отключено"));
}

void MainWindow::setupWidgets()
{
    m_plottingWidget.reset(new PlottingWidget);
    m_plottingWidget->hide();
    QObject::connect(this, &MainWindow::dataRate, m_plottingWidget.data(), &PlottingWidget::setDataRate);
    QObject::connect(this, &MainWindow::fullScale,m_plottingWidget.data(), &PlottingWidget::setFullScale);
    QObject::connect(this, &MainWindow::chosenDevices, m_plottingSettingsDialog, &PlottingSettingsDialog::setDeviceList);
    QObject::connect(m_plottingSettingsDialog, &PlottingSettingsDialog::sendFlowIdVector,
                     m_plottingWidget.data(), &PlottingWidget::setFlowList);
    QObject::connect(m_sensorCalibrationDialog, &SensorCalibrationDialog::sendOffsetData,
                     [this](const auto &offsetData)
                     {
                        m_animationSettingsDialog->setOffsetData(offsetData);
                        ui->actionAnimationSettings->setEnabled(true);
                     });
    QObject::connect(m_animationSettingsDialog, &AnimationSettingsDialog::sendAnimSettings, ui->animationWidget, &AnimationWidget::initBody);
}

QString MainWindow::fileToSave() const
{
    return m_fileToSave;
}

void MainWindow::setFileToSave(const QString &fileToSave)
{
    m_fileToSave = fileToSave;
}

void MainWindow::on_actionInitDevice_triggered()
{
    Q_CHECK_PTR(m_initDialog);
    Q_ASSERT(!m_waitingAckCounter);

    DeviceCondition lastCondition = m_deviceCondition;
    m_deviceCondition = DeviceCondition::NotInitialized;
    updateStatusBar(tr("Инициализация устройства"));
    if(m_initDialog->exec())
    {
        ui->actionStartStop->setDisabled(true);

        Frame initFrame = m_initDialog->getInitFrame();
        m_waitingAckCounter = initFrame.getNumberOfPackets();
        emit writeToPort(initFrame, false);
    }
    else
    {
        m_deviceCondition = lastCondition;
        if (lastCondition == DeviceCondition::NotInitialized)
            updateStatusBar(tr("Устройство не инициализировано"));
        else
            updateStatusBar(tr("Устройство готово к началу записи"));
    }
}

void MainWindow::on_actionStartStop_triggered()
{
    Q_ASSERT(m_deviceCondition == DeviceCondition::Ready ||
             m_deviceCondition == DeviceCondition::Writing);

    auto syncText = statusBar()->findChild<QWidget*>(QStringLiteral("syncText"));
    Q_CHECK_PTR(syncText);

    static QElapsedTimer elapsedTimer;
    static QTimer recordingTimer;
    static QIcon stopIcon(QStringLiteral(":/icons/cancel.svg"));
    static QIcon startIcon(QStringLiteral(":/icons/start.svg"));

    static auto refreshLabel = [this]()
    {
        if (m_deviceCondition != DeviceCondition::Writing)
        {
            recordingTimer.stop();
            ui->actionStartStop->setIcon(startIcon);
            ui->actionStartStop->setText(tr("Старт"));
            ui->actionStartStop->setToolTip(tr("Старт"));
            return;
        }

        auto secs = elapsedTimer.elapsed() / 1000;
        auto mins = (secs / 60);
        secs %= 60;
        updateStatusBar(tr("Идет запись: %1:%2")
                        .arg(mins, 2, 10, QLatin1Char('0'))
                        .arg(secs, 2, 10, QLatin1Char('0')));


    };
    QObject::connect(&recordingTimer, &QTimer::timeout, refreshLabel);

    if (m_deviceCondition == DeviceCondition::Ready)
    {
        m_plottingWidget->clear();

        Frame frame;
        frame.addPacket(RequestDataPacket());
        writeToPort(frame, true);
        emit startRecording();

        m_deviceCondition = DeviceCondition::Writing;
        ui->actionInitDevice->setDisabled(true);
        ui->actionSave->setDisabled(true);
        ui->actionChooseCalDir->setDisabled(true);
        ui->actionFilterSetttings->setDisabled(true);
        ui->actionChoosePlot->setDisabled(true);

        ui->actionStartStop->setIcon(stopIcon);
        ui->actionStartStop->setText(tr("Стоп"));
        ui->actionStartStop->setToolTip(tr("Стоп"));
        updateStatusBar(tr("Идет запись: 00:00"));

        syncText->show();
        m_syncLabel->show();

        recordingTimer.start(200);
        elapsedTimer.start();

    }
    else
    {
        emit stopRecording();

        m_deviceCondition = DeviceCondition::Ready;
        ui->actionInitDevice->setEnabled(true);
        ui->actionSave->setEnabled(true);
        ui->actionFilterSetttings->setEnabled(true);
        ui->actionChooseCalDir->setEnabled(true);
        ui->actionChoosePlot->setEnabled(true);

        syncText->hide();
        m_syncLabel->hide();
        auto endTime = m_statusLabel->text().split(" ").last();
        updateStatusBar(tr("Запись остановлена: %1").arg(endTime));
    }

}

void MainWindow::on_actionSave_triggered()
{
    if (QFile::exists(m_fileToSave))
    {
        QRegExp rx(QStringLiteral("\\d+(?=\\..+$)"));
        int iter;
        int number;
        if((iter = rx.indexIn(m_fileToSave)) != -1)
        {
            QStringList strList = rx.capturedTexts();
            Q_ASSERT(strList.size() == 1);
            number = strList.back().toInt() + 1;
            m_fileToSave.replace(iter, rx.matchedLength(), QString::number(number));
        }
        else
        {
            number = 0;
            iter = m_fileToSave.lastIndexOf(QStringLiteral("."));
            m_fileToSave.insert(iter, QString::number(number));
        }

        while(QFile::exists(m_fileToSave))
        {
            number++;
            m_fileToSave = m_fileToSave.replace(iter, QString::number(number - 1).size(),
                                                QString::number(number));
        }
    }

    QString fileToSave = QFileDialog::getSaveFileName(this, tr("Выберите имя файла для сохранения"),
                                                (m_fileToSave.isEmpty()) ? QDir::currentPath() : m_fileToSave,
                                                QStringLiteral("JSON files (*.json)"));

    if (!fileToSave.isEmpty())
    {
        m_fileToSave = fileToSave;
        emit saveToFile(m_fileToSave);
        updateStatusBar(tr("Лог сохранен в файл %1").arg(m_fileToSave));
    }
}

void MainWindow::on_actionChoosePlot_triggered()
{
    m_plottingSettingsDialog->show();
    m_plottingSettingsDialog->raise();
    m_plottingSettingsDialog->activateWindow();
}

void MainWindow::on_actionChooseCalDir_triggered()
{
    m_sensorCalibrationDialog->show();
    m_sensorCalibrationDialog->raise();
    m_sensorCalibrationDialog->activateWindow();
}

void MainWindow::on_actionFilterSetttings_triggered()
{
    if (m_filterSettingsDialog->exec())
    {
        emit  filterSettings(m_filterSettingsDialog->getSettings());
    }
}

void MainWindow::on_actionAnimationSettings_triggered()
{
    m_animationSettingsDialog->exec();
}

MainWindow::WorkerData::WorkerData() : thread(nullptr), worker(nullptr)
{}

MainWindow::WorkerData::WorkerData(QThread *thread, AbstractWorker *worker)
    : thread(thread), worker(worker)
{
    worker->moveToThread(thread);
}
