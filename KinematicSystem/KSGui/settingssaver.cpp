#include "settingssaver.h"

#include <QCheckBox>
#include <QComboBox>
#include <QRadioButton>
#include <QVariant>
#include <QListWidget>
#include <QDialog>
#include <qwt_plot.h>
#include <QMetaEnum>
#include <QGroupBox>
#include <QDoubleSpinBox>

void SettingsSaver::readSettings(QWidget *window)
{
    QSettings settings;

    settings.beginGroup(window->objectName());
    QVariant pos = settings.value(QStringLiteral("geometry"));
    if (!pos.isNull())
    {
        const QMetaObject *metaobject = window->metaObject();
        int count = metaobject->propertyCount();
        for (int i = 0; i < count; ++i)
        {
            QMetaProperty metaproperty = metaobject->property(i);
            if (metaproperty.isWritable() && metaproperty.isReadable() && metaproperty.isUser())
            {
                const char *name = metaproperty.name();
                window->setProperty(name, settings.value(QString(name)));
            }
        }

        window->restoreGeometry(settings.value(QStringLiteral("geometry")).toByteArray());
        recurseRead(settings, window);
    }
    settings.endGroup();
}

void SettingsSaver::writeSettings(QWidget *window)
{
    QSettings settings;

    settings.beginGroup(window->objectName());
    settings.setValue(QStringLiteral("geometry"), window->saveGeometry());

    const QMetaObject *metaobject = window->metaObject();
    int count = metaobject->propertyCount();
    for (int i = 0; i < count; ++i)
    {
        QMetaProperty metaproperty = metaobject->property(i);
        if(metaproperty.isWritable() && metaproperty.isReadable() && metaproperty.isUser())
        {
            const char *name = metaproperty.name();
            QVariant value = window->property(name);
            settings.setValue(QString(name), value);
        }
    }

    recurseWrite(settings, window);
    settings.endGroup();
}

void SettingsSaver::recurseRead(const QSettings &settings, QObject *object)
{
    QCheckBox *checkBox = dynamic_cast<QCheckBox*>(object);
    if (nullptr != checkBox)
    {
        checkBox->setChecked(settings.value(checkBox->objectName()).toBool());
    }

    QComboBox *comboBox = dynamic_cast<QComboBox*>(object);
    if (nullptr != comboBox)
    {
        comboBox->setCurrentIndex(settings.value(comboBox->objectName()).toInt());
    }

    QRadioButton *radioButton = dynamic_cast<QRadioButton*>(object);
    if (nullptr != radioButton)
    {
        radioButton->setChecked(settings.value(radioButton->objectName()).toBool());
    }

    QListWidget *listWidget = dynamic_cast<QListWidget*>(object);
    if (nullptr != listWidget)
    {
        QStringList selectedItems = settings.value(listWidget->objectName()).toStringList();

        for (const QString &item : selectedItems)
        {
            QList<QListWidgetItem *> found = listWidget->findItems(item, Qt::MatchExactly);
            if (!found.isEmpty())
            {
                Q_ASSERT(found.size() == 1);
                found[0]->setSelected(true);
            }
        }
    }

    QGroupBox *groupBox = dynamic_cast<QGroupBox*>(object);
    if (nullptr != groupBox)
    {
        groupBox->setChecked(settings.value(groupBox->objectName()).toBool());
    }

    QDoubleSpinBox *doubleSpinBox = dynamic_cast<QDoubleSpinBox*>(object);
    if (nullptr != doubleSpinBox)
    {
        doubleSpinBox->setValue(settings.value(doubleSpinBox->objectName()).toDouble());
    }

    for(QObject *child : object->children())
    {
        recurseRead(settings, child);
    }
}

void SettingsSaver::recurseWrite(QSettings &settings, QObject *object)
{
    QCheckBox *checkBox = dynamic_cast<QCheckBox*>(object);
    if (nullptr != checkBox)
    {
        settings.setValue(checkBox->objectName(), checkBox->isChecked());
    }

    QComboBox *comboBox = dynamic_cast<QComboBox*>(object);
    if (nullptr != comboBox)
    {
        settings.setValue(comboBox->objectName(), comboBox->currentIndex());
    }

    QRadioButton *radioButton = dynamic_cast<QRadioButton*>(object);
    if (nullptr != radioButton)
    {
        settings.setValue(radioButton->objectName(), radioButton->isChecked());
    }

    QListWidget *listWidget = dynamic_cast<QListWidget*>(object);
    if (nullptr != listWidget)
    {
        QStringList selectedItems;
        for (QListWidgetItem *item : listWidget->selectedItems())
        {
            selectedItems.append(item->text());
        }
        settings.setValue(listWidget->objectName(), selectedItems);
    }

    QGroupBox *groupBox = dynamic_cast<QGroupBox*>(object);
    if (nullptr != groupBox)
    {
        settings.setValue(groupBox->objectName(), groupBox->isChecked());
    }

    QDoubleSpinBox *doubleSpinBox = dynamic_cast<QDoubleSpinBox*>(object);
    if (nullptr != doubleSpinBox)
    {
        settings.setValue(doubleSpinBox->objectName(), doubleSpinBox->value());
    }

    for(QObject *child : object->children())
    {
        recurseWrite(settings, child);
    }
}
