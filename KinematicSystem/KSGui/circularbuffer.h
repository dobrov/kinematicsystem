#ifndef CIRCULARBUFFER_H
#define CIRCULARBUFFER_H
#include <qwt_series_data.h>
#include <boost/circular_buffer.hpp>

typedef boost::circular_buffer<float> PlotBuffer;

class CircularBuffer : public QwtSeriesData<QPointF>
{
public:
    explicit CircularBuffer(size_t bufferSize);
    void setSize(size_t size);
    virtual size_t size() const override;
    virtual QPointF sample(size_t i) const override;
    virtual QRectF boundingRect() const override;
    void addData(float data);
    void clear();

private:
    PlotBuffer m_dataBuffer;
    QRectF m_boundingRect;
};

#endif // CIRCULARBUFFER_H
