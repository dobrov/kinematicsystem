#ifndef PLOTTINGSETTINGSDIALOG_H
#define PLOTTINGSETTINGSDIALOG_H

#include <QDialog>
#include "kstypes.h"

namespace Ui {
class PlottingSettingsDialog;
}

class PlottingSettingsDialog : public QDialog
{
    Q_OBJECT
    Q_PROPERTY(FlowIdVector currentFlowId READ currentFlowId WRITE setCurrentFlowId USER true)
    Q_PROPERTY(bool showMagnitudeMag READ showMagnitudeMag WRITE setShowMagnitudeMag USER true)
    Q_PROPERTY(bool showMagnitudeAccel READ showMagnitudeAccel WRITE setShowMagnitudeAccel USER true)

public:
    explicit PlottingSettingsDialog(QWidget *parent = 0);
    virtual ~PlottingSettingsDialog();

public slots:
    void setDeviceList(const DeviceList &deviceList);
    virtual void accept() override;

private slots:
    void on_comboBox_currentIndexChanged(const QString &deviceName);
    void handleStateChanged(int state);
    void eraseSelectedItems();

protected:
    virtual void showEvent(QShowEvent *) override;
    virtual void paintEvent(QPaintEvent *) override;

signals:
    void sendFlowIdVector(const FlowIdVector &flowIdVector, bool showMagnitudeAccel,
                          bool showMagnitudeMag);

private:
    FlowIdVector currentFlowId() const;
    void setCurrentFlowId(const FlowIdVector &currentFlowId);
    bool showMagnitudeAccel() const;
    void setShowMagnitudeAccel(bool showMagnitudeAccel);
    bool showMagnitudeMag() const;
    void setShowMagnitudeMag(bool showMagnitudeMag);

    void uncheckAllCheckBox();
    void checkCheckBox(const PassingDataType type);
    void refreshFlowQueueList();

    Ui::PlottingSettingsDialog *ui;
    FlowIdVector m_currentFlowId;
    FlowIdVector m_unsavedFlowId;
    quint8 m_currentDeviceId;
    bool m_isDirty;
    bool m_showMagnitudeAccel;
    bool m_showMagnitudeMag;

    static const int m_maxSize = 3;
};

#endif // PLOTTINGSETTINGSDIALOG_H
