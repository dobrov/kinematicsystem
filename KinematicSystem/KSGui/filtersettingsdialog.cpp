#include "filtersettingsdialog.h"
#include "ui_filtersettingsdialog.h"
#include "settingssaver.h"
#include <complementaryfusionfiltersettings.h>

FilterSettingsDialog::FilterSettingsDialog(QWidget *parent) :
    QDialog(parent), m_currentSettings(new ComplementaryFusionFilterSettings),
    ui(new Ui::FilterSettingsDialog)
{
    ui->setupUi(this);
    SettingsSaver::readSettings(this);
    layout()->setSizeConstraint(QLayout::SetFixedSize);
    loadDlgState();
}

FilterSettingsDialog::~FilterSettingsDialog()
{
    SettingsSaver::writeSettings(this);
    delete ui;
}

AbstractFilterSettingsPointer FilterSettingsDialog::getSettings()
{
    return m_currentSettings;
}

int FilterSettingsDialog::exec()
{
    if (!QDialog::exec())
    {
        restoreDlgState();
        return QDialog::Rejected;
    }
    else
    {
        loadDlgState();
        return QDialog::Accepted;
    }
}

void FilterSettingsDialog::restoreDlgState()
{
    auto complFilterSettings = qSharedPointerCast<ComplementaryFusionFilterSettings>(m_currentSettings);
    ui->accelGainSpinBox->setValue(complFilterSettings->accelGain());
    ui->magGainSpinBox->setValue(complFilterSettings->magGain());

    ui->thresholdingBox->setChecked(m_currentSettings->doThresholding());
    ui->magnAccelSpinBox->setValue(m_currentSettings->accelThrAlpha());
    ui->magnMagSpinBox->setValue(m_currentSettings->magThrAlpha());
    ui->dipAngleSpinBox->setValue(m_currentSettings->dipThrAlpha());

    ui->gyroBiasBox->setChecked(m_currentSettings->doBiasEstimation());
    ui->gyroBiasAlphaSpinBox->setValue(m_currentSettings->gyroBiasAlpha());
    ui->minSteadyDurationSpinBox->setValue(m_currentSettings->minSteadyDuration());
}

void FilterSettingsDialog::loadDlgState()
{
    auto complFilterSettings = qSharedPointerCast<ComplementaryFusionFilterSettings>(m_currentSettings);
    complFilterSettings->setAccelGain(ui->accelGainSpinBox->value());
    complFilterSettings->setMagGain(ui->magGainSpinBox->value());

    m_currentSettings->setDoThresholding(ui->thresholdingBox->isChecked());
    m_currentSettings->setAccelThrAlpha(ui->magnAccelSpinBox->value());
    m_currentSettings->setMagThrAlpha(ui->magGainSpinBox->value());
    m_currentSettings->setDipThrAlpha(ui->dipAngleSpinBox->value());

    m_currentSettings->setDoBiasEstimation(ui->gyroBiasBox->isChecked());
    m_currentSettings->setGyroBiasAlpha(ui->gyroBiasAlphaSpinBox->value());
    m_currentSettings->setMinSteadyDuration(ui->minSteadyDurationSpinBox->value());
}
