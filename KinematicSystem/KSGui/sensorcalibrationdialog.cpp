#include "sensorcalibrationdialog.h"
#include "ui_sensorcalibrationdialog.h"
#include <QFileDialog>
#include <QDir>
#include <QMetaEnum>
#include <QTimer>
#include <QMessageBox>
#include "settingssaver.h"

SensorCalibrationDialog::SensorCalibrationDialog(QWidget *parent) :
    QDialog(parent), ui(new Ui::SensorCalibrationDialog)
{
    ui->setupUi(this);
    SettingsSaver::readSettings(this);
}

SensorCalibrationDialog::~SensorCalibrationDialog()
{
    SettingsSaver::writeSettings(this);
    delete ui;
}

void SensorCalibrationDialog::on_chooseCalDirectory_clicked()
{
    auto calDirectory = QFileDialog::getExistingDirectory(this, tr("Выберите папку с калибровочными данными"),
        (m_calDirectory.isEmpty()) ? QDir::currentPath() : m_calDirectory);
    if (!calDirectory.isEmpty())
        setCalDirectory(calDirectory);
}

void SensorCalibrationDialog::tryReadOffsetFile(const QString &filePath)
{
    static const OffsetData emptyData;
    auto offsetData = readOffsetFile(filePath);
    if (offsetData == emptyData)
    {
        QMessageBox::warning(this, tr("Предупреждение"),
                             tr("Невозможно загрузить файл оффсета!\n"
                                "Путь к файлу: %1.").arg(filePath));
        return;
    }

    ui->currentOffsetLabel->setText(tr("Загружен файл оффсета: %1").arg(filePath));
    m_offsetFilePath = filePath;

    emit sendOffsetData(offsetData);
}

void SensorCalibrationDialog::tryReadCalFiles(const QString &dirPath)
{
    static const QString stringTemplate("Датчик: %1, Сенсор: %2, Предел измерений: %3");
    static const auto metaEnum = QMetaEnum::fromType<PassingDataType>();
    static const CalData emptyData;
    static CalIdFileNamesMap curCalId;

    ui->calFilesList->clear();

    auto calIdMap = findCalFiles(dirPath);
    CalDataMap calDataMap;
    for (auto it = calIdMap.begin(); it != calIdMap.end();)
    {
        const auto &fileName = it.value();
        const auto calData = readCalFile(fileName);
        if (calData == emptyData)
        {
            QMessageBox::warning(this, tr("Предупреждение"),
                                 tr("Невозможно прочитать файл калибровки %1").arg(fileName));
            it = calIdMap.erase(it);
            continue;
        }

        const auto &calId = it.key();
        ui->calFilesList->addItem(stringTemplate.arg(std::get<0>(calId)).
                                  arg(metaEnum.key(static_cast<int>(std::get<1>(calId)))).
                                  arg(std::get<2>(calId)));

        calDataMap.insert(calId, calData);
        ++it;
    }

    if (!calIdMap.empty())
    {
        curCalId = calIdMap;
        emit sendCalDataMap(calDataMap);
        m_calDirectory = dirPath;
    }

    ui->currentDirectoryLabel->setText(tr("Текущая папка: %1").arg(m_calDirectory));
    ui->totalCalFilesLabel->setText(tr("Всего загружено калибровок: %1").arg(calDataMap.size()));
}

QString SensorCalibrationDialog::calDirectory() const
{
    return m_calDirectory;
}

void SensorCalibrationDialog::setCalDirectory(const QString &calDirectory)
{
    if (!calDirectory.isEmpty())
        QTimer::singleShot(100, [this, calDirectory]()
        {
            tryReadCalFiles(calDirectory);
        });
}

void SensorCalibrationDialog::on_chooseOffsetFile_clicked()
{
    auto filePath = QFileDialog::getOpenFileName(this, tr("Выберите файл оффсета"), (m_offsetFilePath.isEmpty()) ?
        QDir::currentPath() : QFileInfo(m_offsetFilePath).dir().absolutePath(), QStringLiteral("Offset files (*.offset)"));
    if (!filePath.isEmpty())
        tryReadOffsetFile(filePath);
}

QString SensorCalibrationDialog::offsetFilePath() const
{
    return m_offsetFilePath;
}

void SensorCalibrationDialog::setOffsetFilePath(const QString &offsetFilePath)
{
    if (!offsetFilePath.isEmpty())
        QTimer::singleShot(100, [this, offsetFilePath]()
        {
            tryReadOffsetFile(offsetFilePath);
        });
}

void SensorCalibrationDialog::on_refreshButton_clicked()
{
    tryReadOffsetFile(m_offsetFilePath);
    tryReadCalFiles(m_calDirectory);
}
