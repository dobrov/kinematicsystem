#-------------------------------------------------
#
# Project created by QtCreator 2015-08-27T01:55:13
#
#-------------------------------------------------

QT += gui svg opengl

include(../KSCore/KSCore.pri)
include(../KSOpenGL/KSOpenGL.pri)
include (C:/Qwt-6.1.3/features/qwt.prf)

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = KSGui
TEMPLATE = app
CONFIG += c++14 qwt

INCLUDEPATH += $$PWD/../include

SOURCES += main.cpp\
           mainwindow.cpp \
           initdevicedialog.cpp \
           circularbuffer.cpp \
           plottingwidget.cpp \
           plottingsettingsdialog.cpp \
           settingssaver.cpp \
    sensorcalibrationdialog.cpp \
    abstractplot.cpp \
    animationwidget.cpp \
    semaphorelabel.cpp \
    filtersettingsdialog.cpp \
    animationsettingsdialog.cpp \
    animationsettings.cpp

HEADERS += mainwindow.h \
           initdevicedialog.h \
           circularbuffer.h \
           plottingwidget.h \
           plottingsettingsdialog.h \
           settingssaver.h \
    sensorcalibrationdialog.h \
    abstactplot.h \
    animationwidget.h \
    semaphorelabel.h \
    filtersettingsdialog.h \
    animationsettingsdialog.h \
    animationsettings.h

FORMS += mainwindow.ui \
         initdevicedialog.ui \
         plottingsettingsdialog.ui \
    sensorcalibrationdialog.ui \
    filtersettingsdialog.ui \
    animationsettingsdialog.ui

RESOURCES += resource.qrc

win32 {
RC_FILE += app.rc
}

DISTFILES +=
