#ifndef PLOTTINGWIDGET_H
#define PLOTTINGWIDGET_H

#include <QPointer>
#include <QWidget>
#include <abstactplot.h>

class PlottingWidget : public QWidget
{
    Q_OBJECT

public:
    explicit PlottingWidget(QWidget *parent = 0);

public slots:
    void handleNewData(const AbstractPassingDataPointer &passingData);
    void setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate);
    void setFullScale(quint16 accelFullScale, quint16 gyroFullScale, quint16 magFullScale);
    void setFlowList(const FlowIdVector &flowIdList, bool showMagnitudeAccel, bool showMagnitudeMag);
    void clear();

private:
    void clearLayout();
    void setYRange(const QPointer<AbstractPlot> &plot, PassingDataType type);
    void setBufferSize(const QPointer<AbstractPlot> &plot, PassingDataType type);

    static QString getTitle(const FlowId &flowId);

    quint16 m_accelDataRate;
    quint16 m_gyroDataRate;
    quint16 m_magDataRate;
    quint16 m_accelFullScale;
    quint16 m_gyroFullScale;
    quint16 m_magFullScale;

    QMap<FlowId, QPointer<AbstractPlot>> m_plotMap;
    static const uint m_duration = 15;
};

#endif // PLOTTINGWIDGET_H
