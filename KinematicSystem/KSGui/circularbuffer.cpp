#include "circularbuffer.h"
#include <limits>
#include <QDebug>

CircularBuffer::CircularBuffer(size_t bufferSize)
    : m_dataBuffer{bufferSize}
{
}

void CircularBuffer::setSize(size_t size)
{
    clear();
    m_dataBuffer.set_capacity(size);
}

size_t CircularBuffer::size() const
{
    return m_dataBuffer.size();
}

QPointF CircularBuffer::sample(size_t i) const
{
    return QPointF(i, m_dataBuffer.at(i));
}

QRectF CircularBuffer::boundingRect() const
{
    return QRectF(0 , std::numeric_limits<float>::min(),
                  m_dataBuffer.capacity(), std::numeric_limits<float>::max());
}

void CircularBuffer::addData(float data)
{
    m_dataBuffer.push_back(data);
}

void CircularBuffer::clear()
{
    m_dataBuffer.clear();
}
