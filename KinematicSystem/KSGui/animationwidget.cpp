#include "animationsettings.h"
#include "animationwidget.h"

#include <QOpenGLShaderProgram>
#include <QDateTime>
#include <meshfactory.h>
#include <mesh.h>
#include <concretepassingdata.h>

namespace
{
    QVector3D getColor(const BodyPart &bodyPart)
    {
        static const QVector<QVector3D> boneColors{{1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}};
        return boneColors.at(static_cast<int>(bodyPart) % boneColors.size());
    }
}

AnimationWidget::DynamicDrawData::DynamicDrawData(CameraType type,
                                                  const MeshPtr &mesh, const TransformPtr &transform) :
    modelTransform(transform), cameraType(type)
{
    mesh->addUniform(QByteArrayLiteral("mvp"), QVariant::fromValue(meshesUniforms.mvp));
    mesh->addUniform(QByteArrayLiteral("modelView"), QVariant::fromValue(meshesUniforms.modelView));
    mesh->addUniform(QByteArrayLiteral("modelViewNormal"), QVariant::fromValue(meshesUniforms.modelViewNormal));
}

AnimationWidget::DynamicDrawData::DynamicDrawData()
{
}

AnimationWidget::AnimationWidget(QWidget *parent)
    : QOpenGLWidget(parent)
{
}

void AnimationWidget::initializeGL()
{
    setUpdateBehavior(QOpenGLWidget::PartialUpdate);

    m_phongProgram = ShaderProgramPtr::create(":/shaders/phong.%1");
    m_simpleProgram = ShaderProgramPtr::create(":/shaders/simple.%1");

    auto f = QOpenGLContext::currentContext()->functions();

    f->glEnable(GL_CULL_FACE);
    f->glEnable(GL_DEPTH_TEST);
    f->glClearColor(0.9f, 0.9f, 0.9f, 1.0f);
}

void AnimationWidget::resizeGL(int width, int height)
{
    Q_UNUSED(width);
    Q_UNUSED(height);
    m_builder.adjustGeometry(this);
}

void AnimationWidget::paintGL()
{
    auto f = QOpenGLContext::currentContext()->functions();

    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (const auto &mesh : m_meshesArray)
    {
        mesh->render();
    }
}

void AnimationWidget::handleNewData(const AbstractPassingDataPointer &passingData)
{
    auto dataType = passingData->getDataType();
    switch(dataType)
    {
    case PassingDataType::Kin:
        handleKinData(qSharedPointerCast<KinPassingData>(passingData)->getData());
        break;
    case PassingDataType::Pos:
        handlePosData(passingData);
        break;
    default:
        return;
    }

    update();
}

void AnimationWidget::handleKinData(const DataVector<KinSample> &kinData)
{
    const auto &lastSample = kinData.last();

    for (auto it = m_bodyTransforms.begin(); it != m_bodyTransforms.end(); ++it)
    {
        const auto bodyPart = it.key().first;
        const auto segment = it.key().second;
        auto bodyPartIt = lastSample.constFind(bodyPart);
        Q_ASSERT(bodyPartIt != lastSample.cend());
        Q_ASSERT(bodyPartIt->size() > segment);
        const auto &segmentData = bodyPartIt->at(segment);
        it.value()->setTranslation(std::get<KSegTranslation>(segmentData));
        it.value()->setRotation(std::get<KSegRotation>(segmentData));
    }
    refreshDynamicData();
}

void AnimationWidget::handlePosData(const AbstractPassingDataPointer &posPassingData)
{
    makeCurrent();

    const auto data = qSharedPointerCast<PosPassingData>(posPassingData)->getData();
    const auto time = posPassingData->getTime();
    Q_ASSERT(time.size() == data.size());

    for (int i = 0; i < data.size(); i++)
    {
        TracingLines tLinesSample;
        tLinesSample.time = time.at(i);
        const auto &posSample = data.at(i);
        //Откидываем для каждого семпла данные, которые мы не показываем.      
        for(auto traceIt = m_bodyTraceData.begin(); traceIt != m_bodyTraceData.end(); ++traceIt)
        {
            static const auto nullPos = QVector3D();

            const auto bodyPart = traceIt.key().first;
            const auto segment = traceIt.key().second;
            auto posIt = posSample.constFind(bodyPart);
            Q_ASSERT(posIt != posSample.cend());
            Q_ASSERT(posIt->size() > segment + 1);
            const auto &segmentPos = posIt->at(segment + 1);
            auto &segmentTrace = traceIt.value();

            if (segmentTrace.prevPos != nullPos)
                createTracingLine(segmentTrace, segmentPos, tLinesSample);
            else
                segmentTrace.prevPos = segmentPos;
        }

        if (!tLinesSample.meshes.isEmpty())
        {
            m_tracingLines.enqueue(tLinesSample);
        }
    }

    auto currentTime = time.last();

    //Удаляем меши траекторий за пределами показываемого временного интервала
    while(!m_tracingLines.isEmpty() && (currentTime - m_tracingLines.head().time) > m_traceDuration)
    {
        const auto tLineSample = m_tracingLines.dequeue().meshes;
        for (const auto tLine : tLineSample)
        {
            auto it = m_meshesArray.find(tLine);
            Q_ASSERT(it != m_meshesArray.end());
            m_meshesArray.erase(it);
        }
    }
}

void AnimationWidget::createTracingLine(AnimationWidget::TraceData &traceData, const QVector3D &newPos,
                                        AnimationWidget::TracingLines &tSample)
{
    constexpr auto minLineDistance = 0.5f;
    if (traceData.prevPos.distanceToPoint(newPos) > minLineDistance)
    {
        m_meshFactory.setColor(traceData.color);
        const auto mesh = m_meshFactory.createLine(traceData.prevPos, newPos);

        for (const auto &vp : m_vpByCameraType)
        {
            auto copyMesh = MeshPtr::create(*mesh);
            copyMesh->addUniform(QByteArrayLiteral("mvp"), QVariant::fromValue(vp));
            m_meshesArray.insert(copyMesh);
            tSample.meshes.append(copyMesh);
        }
        traceData.prevPos = newPos;
    }
}

void AnimationWidget::addThirdAnglesMeshes(const MeshPtr &meshPtr, Transform3D transform)
{
    const auto model = transform.toMatrix();
    for (const auto camera : ThirdAngleBuilder::Cameras)
    {
        const auto meshCopy = MeshPtr::create(*meshPtr);
        const auto modelView = m_builder.getCameraMatrix(camera) * model;
        meshCopy->addUniform(QByteArrayLiteral("modelView"), QVariant::fromValue(modelView));
        meshCopy->addUniform(QByteArrayLiteral("modelViewNormal"), QVariant::fromValue(modelView.normalMatrix()));
        const auto mvp = m_builder.getProjMatrix() * modelView;
        meshCopy->addUniform(QByteArrayLiteral("mvp"), QVariant::fromValue(mvp));

        static const auto zeroVector = QVector3D{0.0f, 0.0f, 0.0f};
        auto cameraMatrix = m_builder.getCameraMatrix(camera);
        auto lightPosition = cameraMatrix.map(zeroVector);
        lightPosition += QVector3D(5.0f, 5.0f, 5.0f);

        meshCopy->addUniform(QByteArrayLiteral("lightPosition"), QVariant::fromValue(lightPosition));

        m_meshesArray.insert(meshCopy);
    }
}

void AnimationWidget::refreshDynamicData()
{
    for (const auto &data : m_dynamicData)
    {
        const auto modelView = m_builder.getCameraMatrix(data.cameraType) * data.modelTransform->toMatrix();
        *data.meshesUniforms.modelView = modelView;
        *data.meshesUniforms.modelViewNormal = modelView.normalMatrix();
        const auto mvp = m_builder.getProjMatrix() * modelView;
        *data.meshesUniforms.mvp = mvp;
    }
}

void AnimationWidget::clear()
{
    m_meshesArray.clear();
    m_dynamicData.clear();
    m_bodyTransforms.clear();
    m_tracingLines.clear();
    m_bodyTraceData.clear();
    m_vpByCameraType.clear();
}

void AnimationWidget::initBody(const AnimationSettings &animSettings)
{
    constexpr qreal radius = 0.2;

    makeCurrent();
    clear();

    if (isHidden())
        setVisible(true);

    if (!animSettings.isEnabled())
        return;

    //Переводим в микросекунды
    m_traceDuration = static_cast<TimeSample>(animSettings.traceDuration() * 1000000);

    m_builder = animSettings.builder();
    for (const auto camera : ThirdAngleBuilder::Cameras)
    {
        const auto vp = QSharedPointer<QMatrix4x4>::create(m_builder.getProjMatrix() *
                                                           m_builder.getCameraMatrix(camera));
        m_vpByCameraType.insert(camera, vp);
    }

    //Добавляет оси координат
    m_meshFactory.setProgram(m_simpleProgram);
    auto mesh = m_meshFactory.createCornerXYZ(4.0f);
    Transform3D transform;
    transform.setTranslation(1.0f, 1.0f, 1.0f);
    addThirdAnglesMeshes(mesh, transform);

    const auto enabledSegments = animSettings.enabledSegments();
    const auto hipLength = animSettings.hipLength();
    //Если есть 2 ноги рисуем hip
    if (enabledSegments.contains(BodyPart::LHL) && enabledSegments.contains(BodyPart::RHL))
    {
        m_meshFactory.setColor(QVector3D(0.5f, 0.5f, 0.5f));
        mesh = m_meshFactory.createBone(hipLength.length(), radius, false);
        Transform3D transform;
        transform.setRotation(QQuaternion::rotationTo(QVector3D(1.0f, 0.0f, 0.0f),
                                                      hipLength.normalized()));
        addThirdAnglesMeshes(mesh, transform);
    }
    //Если есть правая/левая нога, рисуем тазобедренный сустав
    if (enabledSegments.contains(BodyPart::LHL))
    {
        m_meshFactory.setColor(QVector3D(0.0f, 0.0f, 0.0f));
        mesh = m_meshFactory.createIcoSphere(radius * 2);
        addThirdAnglesMeshes(mesh);
    }
    if (enabledSegments.contains(BodyPart::RHL))
    {
        m_meshFactory.setColor(QVector3D(0.0f, 0.0f, 0.0f));
        mesh = m_meshFactory.createIcoSphere(radius * 2);
        Transform3D transform;
        transform.setTranslation(hipLength);
        addThirdAnglesMeshes(mesh, transform);
    }

    //Рисуем сегменты
    const auto bodyInit = animSettings.bodyInit();
    for (auto it = enabledSegments.cbegin(); it != enabledSegments.cend(); ++it)
    {
        const auto bodyPart = it.key();
        const auto bodyPartEnabledSegments = it.value();
        const auto bodyPartInit = bodyInit.find(bodyPart);
        Q_ASSERT(bodyPartInit != bodyInit.end());

        QVector3D translation;
        if (bodyPart == BodyPart::RHL)
            translation = hipLength;
        const QVector3D bodyPartColor = getColor(bodyPart);
        m_meshFactory.setColor(bodyPartColor);
        for (int i = 0; i < bodyPartEnabledSegments.size(); i++)
        {
            const auto segmentInit = bodyInit.find(bodyPart).value().at(i);
            const auto length = std::get<BLength>(segmentInit);
            const auto &quat = std::get<BQuat>(segmentInit);

            auto bodyPartTransform = TransformPtr::create();
            bodyPartTransform->setTranslation(translation);
            bodyPartTransform->setRotation(quat);
            m_bodyTransforms.insert(qMakePair(bodyPart, i), bodyPartTransform);

            mesh = m_meshFactory.createBone(length, radius);
            for (const auto camera : ThirdAngleBuilder::Cameras)
            {
                auto copyMesh = MeshPtr::create(*mesh);
                DynamicDrawData dynDrawData(camera, copyMesh, bodyPartTransform);
                m_meshesArray.insert(copyMesh);
                m_dynamicData.append(dynDrawData);
            }
            translation += quat.rotatedVector(QVector3D(length, 0.0f, 0.0f));

            if (bodyPartEnabledSegments.at(i) == true)
            {
                TraceData segmentTraceData;
                segmentTraceData.color = bodyPartColor;
                m_bodyTraceData.insert(qMakePair(bodyPart, i), segmentTraceData);
            }
        }
    }
    refreshDynamicData();

    //Рисуем рамки, разделяющие три вида.
    m_meshFactory.setProgram(m_simpleProgram);
    m_meshFactory.setColor(QVector3D(0.5f, 0.5f, 0.5f));
    const auto bordersData = m_builder.getBordersData();

    for (int i = 0; i < 2; i++)
    {
        auto borderLine = m_meshFactory.createLine(bordersData.at(i).at(0), bordersData.at(i).at(1));
        borderLine->addUniform(QByteArrayLiteral("mvp"), QVariant::fromValue(m_builder.getProjMatrix()));
        m_meshesArray.insert(borderLine);
    }

    updateGeometry();
}

QSize AnimationWidget::sizeHint() const
{
    m_builder.adjustGeometry(const_cast<QWidget*>(static_cast<const QWidget*>(this)));
    return QWidget::sizeHint();
}
