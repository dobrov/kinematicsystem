#include "animationsettingsdialog.h"
#include "ui_animationsettingsdialog.h"
#include <QGroupBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QMetaEnum>
#include "settingssaver.h"

AnimationSettingsDialog::AnimationSettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AnimationSettingsDialog)
{
    ui->setupUi(this);
    layout()->setSizeConstraint(QLayout::SetFixedSize);

    fillMainViewComboBox();
    fillAxisDirectionGroupBox();

    SettingsSaver::readSettings(this);
}

void AnimationSettingsDialog::fillMainViewComboBox()
{
    const auto metaEnum = QMetaEnum::fromType<View>();

    auto size = metaEnum.keyCount();
    for (int i = 0; i < size; i++)
    {
        ui->mainViewComboBox->addItem(metaEnum.key(i));
    }
    ui->mainViewComboBox->setCurrentIndex(0);
}

void AnimationSettingsDialog::fillAxisDirectionGroupBox()
{
    const auto axisMetaEnum = QMetaEnum::fromType<Axis>();
    const auto axisDirMetaEnum = QMetaEnum::fromType<AxisDirection>();

    QVBoxLayout* mainLayout = new QVBoxLayout;

    auto nAxis = axisMetaEnum.keyCount();
    for (int i = 0; i < nAxis; i++)
    {
        QHBoxLayout* rowLayout = new QHBoxLayout;
        rowLayout->addStretch();
        rowLayout->addWidget(new QLabel(tr("Ось %1:").arg(axisMetaEnum.key(i))));

        QComboBox* rowComboBox = new QComboBox;
        rowComboBox->setObjectName(QString("axisComboBox%1").arg(axisMetaEnum.key(i)));
        for (int j = 0; j < axisDirMetaEnum.keyCount(); j++)
        {
            rowComboBox->addItem(axisDirMetaEnum.key(j));
        }
        rowLayout->addWidget(rowComboBox);
        mainLayout->addLayout(rowLayout);
    }
    ui->axisDirectionGroupBox->setLayout(mainLayout);
}

AnimationSettingsDialog::~AnimationSettingsDialog()
{
    SettingsSaver::writeSettings(this);
    delete ui;
}

void AnimationSettingsDialog::fillSegmentsGroupBox(const BodyDevList &bodyDevList)
{
    deleteLayout(ui->segmentsGroupBox->layout());

    const auto bodyPartMetaEnum = QMetaEnum::fromType<BodyPart>();

    QVBoxLayout *layout = new QVBoxLayout;
    for (auto it = bodyDevList.cbegin(); it != bodyDevList.cend(); ++it)
    {
        auto bodyPart = it.key();

        QCheckBox *bodyPartCheckBox = new QCheckBox;
        bodyPartCheckBox->setObjectName(bodyPartMetaEnum.key(static_cast<int>(bodyPart)));
        QObject::connect(bodyPartCheckBox, &QCheckBox::toggled, this, &AnimationSettingsDialog::handleEnabledBodyPart);
        QComboBox *comboBox = new QComboBox;
        comboBox->setObjectName(bodyPartMetaEnum.key(static_cast<int>(bodyPart)));

        switch (bodyPart)
        {
        case BodyPart::RHL:
            bodyPartCheckBox->setText(tr("Правая нога (Число сегментов):"));
            break;
        case BodyPart::LHL:
            bodyPartCheckBox->setText(tr("Левая нога (Число сегментов):"));
            break;
        default:
            Q_ASSERT(false);
        }

        const auto bodyPartSize = it.value().size();
        for (int i = 0; i < bodyPartSize; i++)
        {
            comboBox->addItem(QString::number(i + 1));
        }
        QObject::connect(comboBox, static_cast<void(QComboBox::*)(int)>(&QComboBox::currentIndexChanged),
                         this, &AnimationSettingsDialog::handleChangeSegmentsComboBox);
        comboBox->setCurrentIndex(bodyPartSize - 1);

        QHBoxLayout *rowLayout = new QHBoxLayout;
        rowLayout->addStretch();
        rowLayout->addWidget(bodyPartCheckBox);
        rowLayout->addWidget(comboBox);
        layout->addLayout(rowLayout);
    }

    ui->segmentsGroupBox->setLayout(layout);
}

void AnimationSettingsDialog::fillTraceGroupBox(const BodyDevList &bodyDevList)
{
    //удаление старых чекбоксов
    const auto bodyPartMetaEnum = QMetaEnum::fromType<BodyPart>();

    auto layout = qobject_cast<QVBoxLayout*>(ui->traceGroupBox->layout());
    Q_ASSERT(layout != nullptr);
    for (int i = 0; i < bodyPartMetaEnum.keyCount(); i++)
    {
        auto oldLayout = ui->traceGroupBox->findChild<QHBoxLayout*>(bodyPartMetaEnum.key(i));
        if (oldLayout != nullptr)
        {
            deleteLayout(oldLayout);
        }
    }

    for (auto it = bodyDevList.cbegin(); it != bodyDevList.cend(); ++it)
    {
        auto bodyPart = it.key();
        QHBoxLayout *bodyPartLayout = new QHBoxLayout;
        bodyPartLayout->setObjectName(bodyPartMetaEnum.key(static_cast<int>(bodyPart)));

        QLabel *label = new QLabel;

        switch (bodyPart)
        {
        case BodyPart::RHL:
            label->setText(tr("Правая нога:"));
            break;
        case BodyPart::LHL:
            label->setText(tr("Левая нога:"));
            break;
        default:
            Q_ASSERT(false);
        }
        bodyPartLayout->addStretch();
        bodyPartLayout->addWidget(label);
        const auto bodyPartSize = it.value().size();
        for (int i = 0; i < bodyPartSize; i++)
        {
            QCheckBox *checkBox = new QCheckBox;
            checkBox->setObjectName(QString::number(i+1));
            checkBox->setText(QString::number(i+1));
            bodyPartLayout->addWidget(checkBox);
        }

        layout->addLayout(bodyPartLayout);
    }
}

void AnimationSettingsDialog::deleteLayout(QLayout * const layout)
{
    if (layout != nullptr)
    {
        QLayoutItem *child;
        while ((child = layout->takeAt(0)) != nullptr)
        {
            if(child->layout() != nullptr)
            {
                deleteLayout(child->layout());
            }
            else if(child->widget() != nullptr)
            {
                delete child->widget();
                delete child;
            }
        }
        delete layout;
    }
}

AnimationSettingsDialog::BodyPartWidgetPtr AnimationSettingsDialog::getBodyPartWidgets(const char *bodyPartKey)
{
    BodyPartWidgetPtr widgetPtr;

    //Если нет части тела, то пропускаем
    auto bodyPartCheckBox = ui->segmentsGroupBox->findChild<QCheckBox*>(bodyPartKey);
    if (bodyPartCheckBox == nullptr)
        return widgetPtr;

    widgetPtr.bodyPartCheckBox = bodyPartCheckBox;
    widgetPtr.bodyPartComboBox = ui->segmentsGroupBox->findChild<QComboBox*>(bodyPartKey);
    Q_ASSERT(widgetPtr.bodyPartComboBox != nullptr);

    auto traceLayout = ui->traceGroupBox->findChild<QHBoxLayout*>(bodyPartKey);
    Q_ASSERT(traceLayout != nullptr);
    for (int i = 0; i < traceLayout->count(); i++)
    {
        auto checkBox = qobject_cast<QCheckBox*>(traceLayout->itemAt(i)->widget());
        if (checkBox == nullptr)
            continue;
        bool ok;
        int segmentNumber = checkBox->objectName().toInt(&ok);
        Q_ASSERT(ok);
        widgetPtr.tracesCheckBoxes.insert(segmentNumber, checkBox);
    }

    return widgetPtr;
}

void AnimationSettingsDialog::restoreDlgState()
{
    ui->enableAnimCheckBox->setChecked(m_animationSettings.isEnabled());

    const auto &builder = m_animationSettings.builder();
    builder.restoreMainView(ui->mainViewComboBox);

    const auto axisMetaEnum = QMetaEnum::fromType<Axis>();
    for (int i = 0; i < axisMetaEnum.keyCount(); i++)
    {
        auto comboBox = ui->axisDirectionGroupBox->findChild<QComboBox*>(
                    QString("axisComboBox%1").arg(axisMetaEnum.key(i)));
        Q_ASSERT(comboBox != nullptr);
        builder.restoreAxisDirection(comboBox, static_cast<Axis>(axisMetaEnum.value(i)));
    }

    builder.restoreConstraint(ui->xMaxSpinBox, ThirdAngleBuilder::Constraint::xMax);
    builder.restoreConstraint(ui->xMinSpinBox, ThirdAngleBuilder::Constraint::xMin);
    builder.restoreConstraint(ui->yMaxSpinBox, ThirdAngleBuilder::Constraint::yMax);
    builder.restoreConstraint(ui->yMinSpinBox, ThirdAngleBuilder::Constraint::yMin);
    builder.restoreConstraint(ui->zMaxSpinBox, ThirdAngleBuilder::Constraint::zMax);
    builder.restoreConstraint(ui->zMinSpinBox, ThirdAngleBuilder::Constraint::zMin);

    const auto &segmentsMap = m_animationSettings.enabledSegments();
    static const auto bodyPartMetaEnum = QMetaEnum::fromType<BodyPart>();
    for (auto it = segmentsMap.cbegin(); it != segmentsMap.cend(); ++it)
    {
        const auto &bodyPartSegments = it.value();
        const auto bodyPartWidgets = getBodyPartWidgets(bodyPartMetaEnum.valueToKey(static_cast<int>(it.key())));
        Q_ASSERT(!bodyPartWidgets.isNull()); //Если в настройках есть часть тела, она должна быть и в виджетах
        bool isEnabled = !bodyPartSegments.isEmpty();
        bodyPartWidgets.bodyPartCheckBox->setChecked(isEnabled);
        bodyPartWidgets.bodyPartCheckBox->toggled(isEnabled);
        if (!isEnabled)
            continue;

        //Почему не работает сигнал?
        bodyPartWidgets.bodyPartComboBox->setCurrentIndex(bodyPartSegments.size() - 1);
        bodyPartWidgets.bodyPartComboBox->currentIndexChanged(bodyPartSegments.size() - 1);
        //Учитываем тот факт, что индекс = номеру сегмента-1, иначе нужен доп мап

        for (int i = 0; i < bodyPartSegments.size(); i++)
        {
            auto traceCheckBox = bodyPartWidgets.tracesCheckBoxes.value(i+1, nullptr);
            Q_ASSERT(traceCheckBox != nullptr);

            if (i < bodyPartSegments.size())
                traceCheckBox->setChecked(bodyPartSegments.at(i));
        }
    }

    ui->traceDurationSpinBox->setValue(m_animationSettings.traceDuration());
}

void AnimationSettingsDialog::saveDlgState()
{
    m_animationSettings.setIsEnabled(ui->enableAnimCheckBox->isChecked());
    ThirdAngleBuilder newBuilder(ui->xMinSpinBox->value(), ui->xMaxSpinBox->value(),
                                 ui->yMinSpinBox->value(), ui->yMaxSpinBox->value(),
                                 ui->zMinSpinBox->value(), ui->zMaxSpinBox->value());
    Q_ASSERT(ui->mainViewComboBox->currentIndex() != -1);
    newBuilder.setMainView(static_cast<View>(ui->mainViewComboBox->currentIndex()));

    const auto axisMetaEnum = QMetaEnum::fromType<Axis>();
    for (int i = 0; i < axisMetaEnum.keyCount(); i++)
    {
        auto comboBox = ui->axisDirectionGroupBox->findChild<QComboBox*>(
                    QString("axisComboBox%1").arg(axisMetaEnum.key(i)));
        Q_ASSERT(comboBox != nullptr);
        newBuilder.setAxisDirection(static_cast<Axis>(i),
                                    static_cast<AxisDirection>(comboBox->currentIndex()));
    }
    m_animationSettings.setBuilder(newBuilder);

    const auto bodyPartMetaEnum = QMetaEnum::fromType<BodyPart>();
    EnabledSegmentsMap newSegmentsMap;
    //Для всех возможных частей тела
    for (int i = 0; i < bodyPartMetaEnum.keyCount(); i++)
    {
        const auto bodyPartWidgets = getBodyPartWidgets(bodyPartMetaEnum.key(i));
        if (bodyPartWidgets.isNull())
            continue;

        QVector<bool> tracesVector;
        if (bodyPartWidgets.bodyPartCheckBox->isChecked())
        {
            int nSegments = bodyPartWidgets.bodyPartComboBox->currentIndex() + 1;
            for (int i = 1; i <= nSegments; i++)
            {
                auto segmentCheckBox = bodyPartWidgets.tracesCheckBoxes.value(i, nullptr);
                Q_ASSERT(segmentCheckBox != nullptr);
                tracesVector.append(segmentCheckBox->isChecked());
            }
        }
        if (!tracesVector.isEmpty())
        {
            newSegmentsMap.insert(static_cast<BodyPart>(bodyPartMetaEnum.value(i)), tracesVector);
        }
    }
    m_animationSettings.setEnabledSegments(newSegmentsMap);

    m_animationSettings.setTraceDuration(static_cast<float>(ui->traceDurationSpinBox->value()));
}

void AnimationSettingsDialog::setOffsetData(const OffsetData &offsetData)
{
    const auto &devList = offsetData.m_deviceList;
    m_animationSettings.setOffsetData(offsetData);
    fillTraceGroupBox(devList);
    fillSegmentsGroupBox(devList);
    restoreDlgState();

    emit sendAnimSettings(m_animationSettings);
}

void AnimationSettingsDialog::handleEnabledBodyPart(bool isEnabled)
{
    auto checkBox = qobject_cast<QCheckBox*>(QObject::sender());
    Q_ASSERT(checkBox != nullptr);
    auto checkBoxName = checkBox->objectName();
    auto layout = this->findChild<QHBoxLayout*>(checkBoxName);
    Q_ASSERT(layout != nullptr);

    for(int j = 0; j < layout->count(); j++)
    {
        auto widget = layout->itemAt(j)->widget();
        if (widget != nullptr)
        {
            widget->setEnabled(isEnabled);
        }
    }

    auto comboBox = this->findChild<QComboBox*>(checkBoxName);
    Q_ASSERT(comboBox != nullptr);
    comboBox->setEnabled(isEnabled);
}

void AnimationSettingsDialog::handleChangeSegmentsComboBox(int index)
{
    auto comboBox = qobject_cast<QComboBox*>(QObject::sender());
    Q_ASSERT(comboBox != nullptr);
    auto comboBoxName = comboBox->objectName();
    QHBoxLayout *layout = this->findChild<QHBoxLayout*>(comboBoxName);
    Q_ASSERT(layout != nullptr);
    for(int i = 0; i < layout->count(); i++)
    {
        auto checkBox = qobject_cast<QCheckBox*>(layout->itemAt(i)->widget());
        if(checkBox == nullptr)
            continue;

        if (checkBox->objectName().toInt() <= index + 1)
            checkBox->setVisible(true);
        else
            checkBox->setVisible(false);
    }
}

AnimationSettings AnimationSettingsDialog::animationSettings() const
{
    return m_animationSettings;
}

void AnimationSettingsDialog::setAnimationSettings(const AnimationSettings &animationSettings)
{
    m_animationSettings = animationSettings;
}

int AnimationSettingsDialog::exec()
{
    if (!QDialog::exec())
    {
        restoreDlgState();
        return QDialog::Rejected;
    }
    else
    {
        saveDlgState();
        emit sendAnimSettings(m_animationSettings);
        return QDialog::Accepted;
    }
}

void AnimationSettingsDialog::on_enableAnimCheckBox_toggled(bool checked)
{
    ui->animSettingsGroupBox->setEnabled(checked);
}
