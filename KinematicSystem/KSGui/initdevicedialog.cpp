#include "initdevicedialog.h"
#include "settingssaver.h"
#include <configurealldevicepacket.h>
#include <configuresingledevicepacket.h>
#include <configuretimerpacket.h>
#include <configuresyncpacket.h>
#include <QString>
#include <QMessageBox>
#include <QList>
#include <QListWidgetItem>
#include <QListWidget>

const QMap<int, float> InitDeviceDialog::m_sensitivityAccel{{0, 0.000061f}, {1, 0.000122f},
                                                             {2, 0.000244f}, {3, 0.000732f}}; // g force
const QMap<int, float> InitDeviceDialog::m_sensitivityGyro{{0, 0.00875f}, {1, 0.0175f}, {2, 0.07f}}; //dps
const QMap<int, float> InitDeviceDialog::m_sensitivityMag{{0, 0.00008f}, {1, 0.00016f},
														  {2, 0.00032f}, {3, 0.000479f}}; // gauss

const QMap<int, quint16> InitDeviceDialog::m_fullScaleAccel{{0, 2}, {1, 4}, {2, 8}, {3, 16}};
const QMap<int, quint16> InitDeviceDialog::m_fullScaleGyro{{0, 250}, {1, 500}, {2, 2000}};
const QMap<int, quint16> InitDeviceDialog::m_fullScaleMag{{0, 2}, {1, 4}, {2, 8}, {3, 16}};

const QMap<int, quint16> InitDeviceDialog::m_dataRateAccel{{0, 100}, {1, 200}};
const QMap<int, quint16> InitDeviceDialog::m_dataRateGyro{{0, 100}, {1, 200}};
const QMap<int, quint16> InitDeviceDialog::m_dataRateMag{{0, 50}, {1, 100}};

const SensOrientArray InitDeviceDialog::m_signMap{{{1, 1, 1}, {-1, -1, -1}, {1, 1, 1}}}; //sign for accel, gyro, mag
const SensOrientArray InitDeviceDialog::m_axisMap{{{RValueX, RValueY, RValueZ}, {RValueY, RValueX, RValueZ},
                                                  {RValueX, RValueY, RValueZ}}};

InitDeviceDialog::InitDeviceDialog(const DeviceList &list, QWidget *parent)
    : QDialog(parent), ui(new Ui::InitDeviceDialog)
{
    ui->setupUi(this);

    setExistingDevice(list);
    SettingsSaver::readSettings(this);
    layout()->setSizeConstraint(QLayout::SetFixedSize);
    if (ui->allDevice->isChecked())
        ui->listWidget->hide();
}

InitDeviceDialog::~InitDeviceDialog()
{
    SettingsSaver::writeSettings(this);
    delete ui;
}

void InitDeviceDialog::setExistingDevice(const DeviceList &list)
{
    ui->listWidget->clear();
    static const QString templateName = tr("Датчик %1");

    for (auto id : list)
    {
        ui->listWidget->addItem(templateName.arg(id));
    }

    auto nDevices = ui->listWidget->count();
    auto frameWidth = ui->listWidget->frameWidth();
    ui->listWidget->setFixedHeight(ui->listWidget->sizeHintForRow(0) * nDevices + 2 * frameWidth);
}

Frame InitDeviceDialog::getInitFrame()
{
    Frame frame;

    ConfigureTimerPacket timerPacket;
    timerPacket.setEnable(1);
    if(ui->timer->isChecked())
        timerPacket.setReset(1);
    frame.addPacket(timerPacket);

    ConfigureSyncPacket syncPacket;
    syncPacket.setEnable(1);
    frame.addPacket(syncPacket);

    DeviceList chosenDevice;
    if (ui->allDevice->isChecked())
    {
        ConfigureAllDevicePacket confPacket;
        buildConfigureDevicePacket(confPacket);
        frame.addPacket(confPacket);

        for (int i = 0; i < ui->listWidget->count(); i++)
        {
            quint8 id = ui->listWidget->item(i)->text().
                    section(QStringLiteral(" "), -1).toUShort();
            Q_ASSERT(id);
            chosenDevice.push_back(id);
        }
    }
    else
    {
        //reset prev settings on device
        ConfigureAllDevicePacket disablePacket;
        disablePacket.setEnable(0);
        frame.addPacket(disablePacket);

        ConfigureSingleDevicePacket confPacket;
        buildConfigureDevicePacket(confPacket);

        auto selectedItems = ui->listWidget->selectedItems();
        for (auto item : selectedItems)
        {
            quint8 id = item->text().section(QStringLiteral(" "), -1).toUShort();
            Q_ASSERT(id);
            chosenDevice.push_back(id);
            quint8 bus = (id - 1) / 5 + 1;
            quint8 device = id % 5 ? id % 5 : 5;
            confPacket.setBus(bus);
            confPacket.setDevice(device);
            frame.addPacket(confPacket);
        }
    }

    std::sort(chosenDevice.begin(), chosenDevice.end());
    emit chosenDevices(chosenDevice);
    return frame;
}

void InitDeviceDialog::buildConfigureDevicePacket(ConfigureAllDevicePacket &packet) const
{
    packet.setEnable(1);

    int drAccelIndex = ui->dataRateAcc->currentIndex();
    packet.setDataRateAcc(static_cast<quint8>(drAccelIndex));
    int drGyroIndex = ui->dataRateGyro->currentIndex();
    packet.setDataRateGyro(static_cast<quint8>(drGyroIndex));
    int drMagIndex = ui->dataRateMag->currentIndex();
    packet.setDataRateMag(static_cast<quint8>(drMagIndex));
    emit dataRate(InitDeviceDialog::m_dataRateAccel.value(drAccelIndex),
                  InitDeviceDialog::m_dataRateGyro.value(drGyroIndex),
                  InitDeviceDialog::m_dataRateMag.value(drMagIndex));

    int fsAccelIndex = ui->fullScaleAcc->currentIndex();
    packet.setFullScaleAcc(static_cast<quint8>(fsAccelIndex));
    int fsGyroIndex = ui->fullScaleGyro->currentIndex();
    packet.setFullScaleGyro(static_cast<quint8>(fsGyroIndex));
    int fsMagIndex = ui->fullScaleMag->currentIndex();
    packet.setFullScaleMag(static_cast<quint8>(fsMagIndex));
    emit sensitivity(InitDeviceDialog::m_sensitivityAccel.value(fsAccelIndex),
                     InitDeviceDialog::m_sensitivityGyro.value(fsGyroIndex),
                     InitDeviceDialog::m_sensitivityMag.value(fsMagIndex));

    emit fullScale(InitDeviceDialog::m_fullScaleAccel.value(fsAccelIndex),
                   InitDeviceDialog::m_fullScaleGyro.value(fsGyroIndex),
                   InitDeviceDialog::m_fullScaleMag.value(fsMagIndex));
}

void InitDeviceDialog::accept()
{
    if (ui->concreteDevice->isChecked() &&
        ui->listWidget->selectedItems().isEmpty())
    {
        QMessageBox::warning(this, tr("Предупреждение"),
                             tr("Не было выбрано ни одного датчика!\n"
                                "Пожалуйста, выберите из списка хотя бы один."));
        return;
    }

    done(QDialog::Accepted);
}

int InitDeviceDialog::exec()
{
    auto selectedItemsPrev = ui->listWidget->selectedItems();
    bool isAllDevicePrev = ui->allDevice->isChecked();
    if (!QDialog::exec())
    {
        auto selectedItems = ui->listWidget->selectedItems();
        for (auto item : selectedItems)
            item->setSelected(false);
        for (auto item : selectedItemsPrev)
            item->setSelected(true);

        if (isAllDevicePrev)
        {
            ui->allDevice->setChecked(true);
            ui->listWidget->hide();
        }
        else
        {
            ui->concreteDevice->setChecked(true);
            ui->listWidget->show();
        }

        return QDialog::Rejected;
    }
    return QDialog::Accepted;
}
