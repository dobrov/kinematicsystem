#include "abstractinterpolator.h"

AbstractInterpolator::AbstractInterpolator(QObject *parent) :
    AbstractPassingDataHandler(parent), m_accelPeriod(0), m_gyroPeriod(0), m_magPeriod(0)
{
}

void AbstractInterpolator::setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate)
{
    m_accelPeriod = 1000000 / accelDataRate;
    m_gyroPeriod = 1000000 / gyroDataRate;
    m_magPeriod = 1000000 / magDataRate;
}

void AbstractInterpolator::clear()
{  
    m_interpolatorMap.clear();
    sendMessage(QStringLiteral("Interpolator has been cleared"));
}
