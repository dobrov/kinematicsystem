#ifndef ABSTRACTPASSINGDATA_H
#define ABSTRACTPASSINGDATA_H

#include <QtGlobal>
#include <QVector>
#include <QSharedPointer>
#include "kstypes.h"

class AbstractPassingData;

typedef QSharedPointer<AbstractPassingData> AbstractPassingDataPointer;

class AbstractPassingData
{
public:
    enum class EraseType
    {
        Head,
        Tail
    };

    AbstractPassingData();
    AbstractPassingData(quint8 deviceId, PassingDataType dataType);
    AbstractPassingData(quint8 deviceId, PassingDataType dataType, const TimeDataVector &time);
    virtual ~AbstractPassingData() = 0;
    AbstractPassingDataPointer clone() const;

    inline quint8 getDeviceId() const
    {
        return m_deviceId;
    }

    inline PassingDataType getDataType() const
    {
        return m_dataType;
    }

    inline FlowId getFlowId() const
    {
        return std::make_tuple(m_deviceId, m_dataType);
    }

    inline const TimeDataVector &getTime() const
    {
        return m_time;
    }

    inline int size() const
    {
        return m_time.size();
    }

    inline bool isEmpty() const
    {
        return m_time.isEmpty();
    }

    //split passingData into 2 object - current object: [i len), returning object: [1 i)
    AbstractPassingDataPointer split(int i);
    void append(const AbstractPassingDataPointer &appendingData);
    void erase(int n, EraseType eraseType);

protected:
    quint8 m_deviceId;
    PassingDataType m_dataType;

private:
    virtual AbstractPassingData *doClone() const = 0;
    virtual AbstractPassingDataPointer splitTemplateMethod(int i, const TimeDataVector &time) = 0;
    virtual void appendTemplateMethod(const AbstractPassingDataPointer &appendingData);
    virtual void eraseTemplateMethod(int n, EraseType eraseType);

    TimeDataVector m_time;
};

template <typename T>
const DataVector<T> &getData(const AbstractPassingDataPointer &passingData);

#endif // ABSTRACTPASSINGDATA_H
