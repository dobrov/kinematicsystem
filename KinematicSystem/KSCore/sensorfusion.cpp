#include "sensorfusion.h"
#include "complementaryfusionfilter.h"
#include "concretepassingdata.h"

SensorFusion::DeviceData::DeviceData(TimeSample gyroPeriod, TimeSample beginTime)
    : filter(new ComplementaryFusionFilter(gyroPeriod / 1000000.0)), isMagSample(true), currentTime(beginTime)
{
}

SensorFusion::DeviceData::DeviceData(const AbstractFilterSettingsPointer &settings,
                                     TimeSample gyroPeriod, TimeSample beginTime)
    : DeviceData(gyroPeriod, beginTime)
{
    filter->setSettings(settings);
}

SensorFusion::SensorFusion(QObject *parent)
    : AbstractPassingDataHandler(parent), m_gyroPeriod(0)
{
}

void SensorFusion::setDataRate(quint16, quint16 gyroDataRate, quint16)
{
    m_gyroPeriod = 1000000 / gyroDataRate;
}

void SensorFusion::setFilterSettings(const AbstractFilterSettingsPointer &settings)
{
    m_settings = settings;
}

void SensorFusion::clear()
{
    m_deviceDataMap.clear();
}

void SensorFusion::handleNewData(const AbstractPassingDataPointer &passingData)
{
    Q_ASSERT(m_gyroPeriod);

    auto id = passingData->getDeviceId();
    auto type = passingData->getDataType();
    Q_ASSERT(type == PassingDataType::CalAccel || type == PassingDataType::CalGyro ||
             type == PassingDataType::CalMag);

    DeviceData &deviceData = m_deviceDataMap.contains(id) ? m_deviceDataMap[id] :
                             m_deviceDataMap.insert(id, DeviceData(m_settings, m_gyroPeriod,
                             passingData->getTime().first())).value();

    auto newData = qSharedPointerCast<CalPassingData>(passingData)->getData();

    if (type == PassingDataType::CalAccel)
        deviceData.accelData.append(newData);
    else if (type == PassingDataType::CalGyro)
        deviceData.gyroData.append(newData);
    else
        deviceData.magData.append(newData);

    QuatDataVector outData;
    TimeDataVector outTime;
    while (!deviceData.accelData.isEmpty() && !deviceData.gyroData.isEmpty())
    {
        if (deviceData.isMagSample && deviceData.magData.isEmpty())
            break;
        static const QString stringTemplate("Current quaternion time : %1");
        sendMessage(stringTemplate.arg(deviceData.currentTime));

        const auto accelSample = deviceData.accelData.takeFirst();
        auto ax = std::get<CValueX>(accelSample);
        auto ay = std::get<CValueY>(accelSample);
        auto az = std::get<CValueZ>(accelSample);
        const auto gyroSample = deviceData.gyroData.takeFirst();
        auto wx = std::get<CValueX>(gyroSample) * deg2rad();
        auto wy = std::get<CValueY>(gyroSample) * deg2rad();
        auto wz = std::get<CValueZ>(gyroSample) * deg2rad();

        if (deviceData.isMagSample)
        {
            const auto magSample = deviceData.magData.takeFirst();
            auto mx = std::get<CValueX>(magSample);
            auto my = std::get<CValueY>(magSample);
            auto mz = std::get<CValueZ>(magSample);
            deviceData.filter->updateMARG(ax, ay, az, wx, wy, wz, mx, my, mz);
        }
        else
            deviceData.filter->updateIMU(ax, ay, az, wx, wy, wz);

        outData.append(deviceData.filter->getOrientation());
        outTime.append(deviceData.currentTime);

        deviceData.isMagSample = !deviceData.isMagSample;
        deviceData.currentTime += m_gyroPeriod;
    }
    if (!outData.isEmpty())
        emit sendData(QSharedPointer<QuatPassingData>::create(id, PassingDataType::Quat, outTime, outData));
}
