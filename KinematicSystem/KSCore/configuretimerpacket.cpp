#include "configuretimerpacket.h"

ConfigureTimerPacket::ConfigureTimerPacket()
    : Packet(0x10, 2), m_enable(0), m_reset(0)
{
}

ConfigureTimerPacket::ConfigureTimerPacket(quint8 enable, quint8 reset)
    : Packet(0x10, 2), m_enable(enable), m_reset(reset)
{
}

void ConfigureTimerPacket::setEnable(const quint8 &enable)
{
    m_enable = enable;
}

void ConfigureTimerPacket::setReset(const quint8 &reset)
{
    m_reset = reset;
}

void ConfigureTimerPacket::addPacketData(QByteArray &packet) const
{
    packet[3] = m_enable;
    packet[4] = m_reset;
}
