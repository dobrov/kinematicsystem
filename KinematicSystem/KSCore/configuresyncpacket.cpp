#include "configuresyncpacket.h"

ConfigureSyncPacket::ConfigureSyncPacket()
    : Packet(0x11, 2), m_enable(0), m_lev(0)
{
}

ConfigureSyncPacket::ConfigureSyncPacket(quint8 enable, quint8 lev)
    : Packet(0x11, 2), m_enable(enable), m_lev(lev)
{
}

void ConfigureSyncPacket::setEnable(const quint8 &enable)
{
    m_enable = enable;
}

void ConfigureSyncPacket::setLev(const quint8 &lev)
{
    m_lev = lev;
}

void ConfigureSyncPacket::addPacketData(QByteArray &packet) const
{
    packet[3] = m_enable;
    packet[4] = m_lev;
}
