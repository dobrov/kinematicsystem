#include "abstractworker.h"
#include <QThread>
#include <QString>

AbstractWorker::AbstractWorker(QObject *parent)
    : QObject(parent)
{
}

void AbstractWorker::startWork()
{
    QObject::connect(this, SIGNAL(finished()), QObject::thread(), SLOT(quit()));
    QObject::connect(this, SIGNAL(finished()), this, SLOT(deleteLater()));
    QObject::connect(QObject::thread(), SIGNAL(finished()), QObject::thread(), SLOT(deleteLater()));
    QString t;
    t.sprintf("%8p", QObject::thread());
    emit message(QString("%1 has started its work in thread %2").arg(metaObject()->className()).
                 arg(t));
    startWorkImpl();
}

void AbstractWorker::finishWork()
{
    emit message(QString("%1 has ended its work").arg(metaObject()->className()));
    emit finished();
    finishWorkImpl();
}

void AbstractWorker::sendMessage(const QString &msg) const
{
    emit message(QString("%1: %2").arg(metaObject()->className()).arg(msg));
}

void AbstractWorker::startWorkImpl()
{
}

void AbstractWorker::finishWorkImpl()
{
}

AbstractWorker::~AbstractWorker()
{
}
