#include "complementaryfusionfiltersettings.h"

ComplementaryFusionFilterSettings::ComplementaryFusionFilterSettings()
{
}

float ComplementaryFusionFilterSettings::magGain() const
{
    return m_magGain;
}

void ComplementaryFusionFilterSettings::setMagGain(float magGain)
{
    m_magGain = magGain;
}

float ComplementaryFusionFilterSettings::accelGain() const
{
    return m_accelGain;
}

void ComplementaryFusionFilterSettings::setAccelGain(float accelGain)
{
    m_accelGain = accelGain;
}
