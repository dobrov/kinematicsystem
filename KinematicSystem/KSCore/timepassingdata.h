#ifndef TIMEPASSINGDATA_H
#define TIMEPASSINGDATA_H

#include "abstractpassingdata.h"

class TimePassingData : public AbstractPassingData
{
public:
    explicit TimePassingData(PassingDataType dataType, TimeSample time);
    explicit TimePassingData(PassingDataType dataType, const TimeDataVector &time);
    inline const TimeDataVector &getData() const;

private:
    virtual TimePassingData *doClone() const override;
    virtual AbstractPassingDataPointer splitTemplateMethod(int i, const TimeDataVector &time) override;
};

inline const TimeDataVector &TimePassingData::getData() const
{
    return getTime();
}

template <>
inline const DataVector<TimeSample> &getData(const AbstractPassingDataPointer &passingData)
{
    return qSharedPointerCast<TimePassingData>(passingData)->getData();
}

#endif // TIMEPASSINGDATA_H
