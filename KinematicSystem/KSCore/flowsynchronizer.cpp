#include "flowsynchronizer.h"
#include <QTimer>
#include <QMetaEnum>

FlowSynchronizer::FlowSynchronizer(QObject *parent)
    : AbstractPassingDataHandler(parent), m_cutTime(0), m_lcmPeriod(0),
      m_condition(Condition::Idle), m_isFindCutTime(false)
{
}

void FlowSynchronizer::setSynchronizingFlows(const FlowIdVector &flowVector)
{
    Q_ASSERT(m_flowMap.empty());
    Q_ASSERT(m_condition == Condition::Idle);
    m_synchronizingFlows = flowVector;
}

void FlowSynchronizer::setDataRate(float accelDataRate, float gyroDataRate,
                                   float magDataRate)
{
    TimeSample accelPeriod = 1000000 / accelDataRate;
    TimeSample gyroPeriod = 1000000 / gyroDataRate;
    TimeSample magPeriod = 1000000 / magDataRate;
    m_lcmPeriod = lcm<TimeSample>(lcm<TimeSample>(accelPeriod, gyroPeriod), magPeriod);
}

void FlowSynchronizer::handleStartRecording()
{
    Q_ASSERT(m_condition == Condition::Idle);
    setNewState(Condition::Ready);
}

void FlowSynchronizer::forceStopRecording()
{
    QTimer::singleShot(10, this, &FlowSynchronizer::stopRecording);
    setNewState(Condition::Idle);
}

void FlowSynchronizer::handleNewData(const AbstractPassingDataPointer &passingData)
{
    Q_ASSERT(m_lcmPeriod != 0);
    Q_ASSERT(!m_synchronizingFlows.isEmpty());

    switch (m_condition)
    {
    case Condition::Idle:
        return;
    case Condition::Record:
        sendData(passingData);
        return;
    case Condition::Ready:
        setNewState(Condition::Launch);
        initFlowMap();
    case Condition::Launch:
    case Condition::End:
        if (!m_isFindCutTime)
            findPhase(passingData);
        else
            cutPhase(passingData);
        return;
    default:
        Q_ASSERT(false);
    }
}

void FlowSynchronizer::clear()
{
    m_flowMap.clear();
    m_cutTime = 0;
    setNewState(Condition::Idle);
    m_isFindCutTime = false;
}

void FlowSynchronizer::findPhase(const AbstractPassingDataPointer &passingData)
{
    Q_ASSERT(!m_flowMap.isEmpty());
    Q_ASSERT(m_condition == Condition::Launch ||
             m_condition == Condition::End);

    const auto flowId = getFlowId(passingData);
    Q_ASSERT(m_flowMap.contains(flowId));
    auto &flowData = m_flowMap[flowId];

    if (flowData == nullptr)
    {
        if (passingData->getTime().first() > m_cutTime)
            m_cutTime = passingData->getTime().first();
        flowData = passingData->clone();
    }
    else
    {
        flowData->append(passingData);
    }

    if (!checkIsFullAllFlow())
        return;

    m_isFindCutTime = true;
    if (m_cutTime % m_lcmPeriod)
        m_cutTime = (m_cutTime / m_lcmPeriod + 1) * m_lcmPeriod;
    switch (m_condition)
    {
    case Condition::End:
        sendMessage(QString("Dropped all samples after %1").arg(m_cutTime));
        break;
    case Condition::Launch:
        sendMessage(QString("Dropped all samples below %1").arg(m_cutTime));
        emit beginTime(m_cutTime);
        break;
    default:
        Q_ASSERT(false);
    }
    dropSamples();
    checkIsCutPhase();
}

void FlowSynchronizer::cutPhase(const AbstractPassingDataPointer &passingData)
{
    const auto flowId = getFlowId(passingData);

    auto it = m_flowMap.find(flowId);
    if (it == m_flowMap.end())
    {
        if (m_condition == Condition::Launch)
            sendData(passingData);
        return;
    }

    const auto &time = passingData->getTime();
    int dropIter = time.indexOf(m_cutTime);
    if (dropIter != -1)
    {
        if (m_condition == Condition::Launch)
            passingData->erase(dropIter, AbstractPassingData::EraseType::Head);
        else
            passingData->erase(time.size() - dropIter - 1, AbstractPassingData::EraseType::Tail);
        if (!passingData->isEmpty())
            sendData(passingData);
        m_flowMap.erase(it);
    }

    checkIsCutPhase();
}

void FlowSynchronizer::initFlowMap()
{
    Q_ASSERT(m_flowMap.isEmpty());
    for (auto flow : m_synchronizingFlows)
    {
        m_flowMap.insert(flow, AbstractPassingDataPointer());
    }
}

bool FlowSynchronizer::checkIsFullAllFlow() const
{
    for (const auto &flow : m_flowMap)
    {
        if (flow == nullptr)
            return false;
    }
    return true;
}

void FlowSynchronizer::checkIsCutPhase()
{
    if (m_flowMap.isEmpty())
    {
        if (m_condition == Condition::Launch)
            setNewState(Condition::Record);
        else
            forceStopRecording();
        m_isFindCutTime = false;
        m_cutTime = 0;
    }
}

void FlowSynchronizer::dropSamples()
{
    for (auto it = m_flowMap.begin(); it != m_flowMap.end();)
    {
        auto &flow = it.value();
        const auto &time = flow->getTime();

        int dropIter = time.indexOf(m_cutTime);
        if (dropIter != -1)
        {
            if (m_condition == Condition::Launch)
                flow->erase(dropIter, AbstractPassingData::EraseType::Head);
            else
                flow->erase(time.size() - dropIter - 1, AbstractPassingData::EraseType::Tail);
            if (!flow->isEmpty())
                sendData(flow);
            it = m_flowMap.erase(it);
        }
        else
        {
            flow->erase(time.size(), AbstractPassingData::EraseType::Tail);
            ++it;
        }
    }
}

void FlowSynchronizer::setNewState(Condition &&newState)
{
    static const auto conditionMetaEnum = QMetaEnum::fromType<Condition>();
    sendMessage(QString("Switch state from %1 to %2")
                .arg(conditionMetaEnum.key(static_cast<int>(m_condition)))
                .arg(conditionMetaEnum.key(static_cast<int>(newState))));
    m_condition = newState;
}

void FlowSynchronizer::handleStopRecording()
{
    Q_ASSERT(m_condition != Condition::End || m_condition != Condition::Idle);
    if (m_condition == Condition::Launch)
        clear();
    else
    {
        initFlowMap();
        setNewState(Condition::End);
    }
}
