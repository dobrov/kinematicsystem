#include "linearinterpolator.h"
#include <QMetaEnum>

LinearInterpolator::LinearInterpolator(QObject *parent)
    : AbstractInterpolator(parent)
{
}

void LinearInterpolator::handleNewData(const AbstractPassingDataPointer &passingData)
{
    Q_ASSERT((m_accelPeriod > 0) && (m_gyroPeriod > 0) && (m_magPeriod > 0));

    PassingDataType dataType = passingData->getDataType();

    Q_ASSERT(dataType != PassingDataType::Quat && dataType != PassingDataType::CalAccel &&
             dataType != PassingDataType::CalMag);

    quint64 currentPeriod = 0;
    switch (dataType)
    {
    case PassingDataType::RawAccel:
        currentPeriod = m_accelPeriod;
        break;
    case PassingDataType::RawGyro:
        currentPeriod = m_gyroPeriod;
        break;
    case PassingDataType::RawMag:
        currentPeriod = m_magPeriod;
        break;
    default:
        Q_ASSERT(false);
    }

    quint8 deviceId = passingData->getDeviceId();
    const FlowId &flowId = getFlowId(passingData);

    InterpolatorMap::iterator currentFlow;
    if (!m_interpolatorMap.contains(flowId))
    {
        auto &&rawDataPointer = qSharedPointerCast<RawPassingData>(passingData);
        auto &firstTimeStamp = rawDataPointer->getTime().first();
        TimeSample currentInterpolateTime = (firstTimeStamp / currentPeriod + 1) * currentPeriod;
        currentFlow = m_interpolatorMap.insert(flowId, std::make_tuple(currentInterpolateTime, rawDataPointer));
    }
    else
    {
        currentFlow = m_interpolatorMap.find(flowId);
        Q_ASSERT(currentFlow != m_interpolatorMap.end());
        std::get<LastSamples>(currentFlow.value())->append(passingData);
    }

    static const auto metaEnum = QMetaEnum::fromType<PassingDataType>();
    static const QString stringTemplate("Handle new data from id: %1, sensor type: %2");
    sendMessage(stringTemplate.arg(deviceId).arg(metaEnum.key(static_cast<int>(dataType))));

    RawDataVector newData;
    TimeDataVector newTime;

    TimeSample &interpTime = std::get<CurrentInterpolateTime>(currentFlow.value());
    auto &lastSamples = std::get<LastSamples>(currentFlow.value());
    auto &lastData = lastSamples->getData();
    auto &lastTime = lastSamples->getTime();

    for (int i = 1; i < lastSamples->size(); i++)
    {
        const RawSample &currentData = lastData.at(i);
        const TimeSample &currentTime = lastTime.at(i);

        if (currentTime > interpTime)
        {
            const RawSample &prevData = lastData.at(i-1);
            const TimeSample &prevTime = lastTime.at(i-1);

            qint16 prevValueX = std::get<RValueX>(prevData);
            qint16 prevValueY = std::get<RValueY>(prevData);
            qint16 prevValueZ = std::get<RValueZ>(prevData);

            qint16 currentValueX = std::get<RValueX>(currentData);
            qint16 currentValueY = std::get<RValueY>(currentData);
            qint16 currentValueZ = std::get<RValueZ>(currentData);

            qint64 dtInterp = interpTime - prevTime;
            qint64 dt = currentTime - prevTime;
            qint16 interpX = prevValueX + (currentValueX - prevValueX) * dtInterp / dt;
            qint16 interpY = prevValueY + (currentValueY - prevValueY) * dtInterp / dt;
            qint16 interpZ = prevValueZ + (currentValueZ - prevValueZ) * dtInterp / dt;

            static const QString stringTemplate("Interpolated data: Time %1, X %2, Y %3, Z %4");
            sendMessage(stringTemplate.arg(interpTime).arg(interpX).arg(interpY).arg(interpZ));
            newData.append({interpX, interpY, interpZ});
            newTime.append(interpTime);

            interpTime += currentPeriod;
            //Декремент нужен, так как не гарантируется, что текущий currentTime будет меньше нового interpTime
            --i;
        }
    }

    if (!newData.isEmpty())
        emit sendData(QSharedPointer<RawPassingData>::create(passingData->getDeviceId(), dataType,
                                                             newTime, newData));
    lastSamples->erase(lastSamples->size() - 1, AbstractPassingData::EraseType::Head);
}
