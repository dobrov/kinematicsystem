#include "comportworker.h"
#include <QTimer>

const QStringList ComPortWorker::possibleDevice{"FTDI", "STMicroelectronics."};

ComPortWorker::ComPortWorker(QObject *parent)
    : AbstractWorker(parent), m_isAutoConnect(true), m_messageError(false), m_messageRawData(true),
      m_readInterval(50), m_isPeriodic(false), m_detectInterval(50), m_bytesWritten(0)
{

    m_settings = Settings(QSerialPortInfo(), 3000000, QSerialPort::Data8, QSerialPort::NoParity,
                          QSerialPort::OneStop, QSerialPort::HardwareControl);
}

ComPortWorker::ComPortWorker(const Settings &settings, QObject *parent)
    : AbstractWorker(parent), m_isAutoConnect(true), m_messageError(false), m_messageRawData(false),
      m_readInterval(50), m_isPeriodic(false), m_detectInterval(50), m_bytesWritten(0)
{
    setSettings(settings);
}

bool ComPortWorker::openPort()
{
    Q_CHECK_PTR(m_port);
    setupPort();
    if(m_port->open(QIODevice::ReadWrite))
    {
        sendMessage(QString("Serial port %1 is opened").arg(m_port->portName()));
        emit opened(m_port->portName());
    }
    return m_port->isOpen();
}

void ComPortWorker::closePort()
{
    Q_CHECK_PTR(m_port);
    sendMessage(QString("Serial port %1 is closed").arg(m_port->portName()));
    emit closed(m_port->portName());
    if (m_port->isOpen())
        m_port->close();
}

void ComPortWorker::tryOpenPort()
{
    Q_CHECK_PTR(m_port);
    Q_CHECK_PTR(m_detectTimer);
    if (!m_port->isOpen())
    {
        if (!m_detectTimer->isActive())
            m_detectTimer->start(m_detectInterval);
        if(detectDevice())
        {
            if(openPort())
                m_detectTimer->stop();
        }
    }
    else
        sendMessage(QString("Port %1 is already open").arg(m_port->portName()));

}

void ComPortWorker::write(const Frame &writeData, bool isPeriodic)
{
    Q_CHECK_PTR(m_readTimer);
    Q_ASSERT(!m_readTimer->isActive());
    Q_ASSERT(!m_isPeriodic);

    m_writeData = writeData.getFrame();
    if (isPeriodic)
        m_readTimer->setSingleShot(false);
    else
        m_readTimer->setSingleShot(true);

    writeImpl();
    m_isPeriodic = isPeriodic;
    m_readTimer->start(m_readInterval);
}

void ComPortWorker::writeImpl()
{
    Q_CHECK_PTR(m_port);
    Q_ASSERT(m_port->isOpen());
    Q_ASSERT(!m_writeData.isEmpty());
#ifdef QT_DEBUG
    qint64 bytesForWrite = m_port->write(m_writeData);
    Q_ASSERT(bytesForWrite == m_writeData.size());
#else
    m_port->write(m_writeData);
#endif

    sendMessage(QString("Write data to port %1. Size: %2").arg(m_port->portName()).arg(m_writeData.size()));
    if (m_messageRawData)
        sendMessage(QString("Raw data: %1").arg(QString(m_writeData.toHex())));
}

bool ComPortWorker::detectDevice()
{
    for (const QSerialPortInfo &device : QSerialPortInfo::availablePorts())
    {
        if (ComPortWorker::possibleDevice.contains(device.manufacturer(), Qt::CaseInsensitive))
        {
            m_settings.portInfo = device;
            return true;
        }
    }
    return false;
}

void ComPortWorker::startWorkImpl()
{
    m_readTimer = new QTimer(this);
    m_detectTimer = new QTimer(this);
    m_detectTimer->setSingleShot(false);
    m_port = new QSerialPort(this);
    QObject::connect(m_readTimer, &QTimer::timeout, this, &ComPortWorker::handleReadyRead);
    QObject::connect(m_detectTimer, &QTimer::timeout, this, &ComPortWorker::tryOpenPort);
    QObject::connect(m_port, &QSerialPort::bytesWritten, this, &ComPortWorker::handleBytesWritten);
    QObject::connect(m_port, SIGNAL(error(QSerialPort::SerialPortError)), this,
                     SLOT(handleError(QSerialPort::SerialPortError)));
    if (m_isAutoConnect)
        tryOpenPort();
}

void ComPortWorker::finishWorkImpl()
{
    closePort();
}

bool ComPortWorker::messageRawData() const
{
    return m_messageRawData;
}

void ComPortWorker::setMessageRawData(bool messageRawData)
{
    m_messageRawData = messageRawData;
}

bool ComPortWorker::messageError() const
{
    return m_messageError;
}

void ComPortWorker::setMessageError(bool messageError)
{
    m_messageError = messageError;
}

void ComPortWorker::handleBytesWritten(qint64 bytes)
{
    Q_ASSERT(!m_writeData.isEmpty());

    m_bytesWritten += bytes;
    if (m_writeData.size() == m_bytesWritten)
    {
        m_bytesWritten = 0;
        sendMessage(QString("Data successfully sent to port %1").arg(m_port->portName()));
    }
}

void ComPortWorker::handleReadyRead()
{
    Q_CHECK_PTR(m_port);
    Q_CHECK_PTR(m_readTimer);

    if (!m_isPeriodic && !m_readTimer->isSingleShot())
    {
        m_readTimer->stop();
        m_port->clear();
        return;
    }

    if (!m_port->isOpen())
    {
        m_readTimer->stop();
        m_isPeriodic = false;
        return;
    }

    static uint failReadCount;
    QByteArray data = m_port->readAll();
    if (data.isEmpty())
    {
        sendMessage(QString("Nothing to read: %1").arg(++failReadCount));
        if (failReadCount > nFailedRead)
        {
            failReadCount = 0;
            m_readTimer->stop();

            closePort();
            if(m_isAutoConnect)
                tryOpenPort();
        }
        if (m_readTimer->isSingleShot())
        {
            writeImpl();
            m_readTimer->start(m_readInterval);
        }
        return;
    }
    failReadCount = 0;

    sendMessage(QString("Read data from port %1. Size: %2").arg(m_port->portName()).arg(data.size()));
    if (m_messageRawData)
        sendMessage(QString("Raw data: %1").arg(QString(data.toHex())));

    emit readDataAvailable(data);

    if (!m_isPeriodic)
    {
        m_readTimer->stop();
        return;
    }

    writeImpl();
}

void ComPortWorker::handleError(QSerialPort::SerialPortError error)
{
    if (error != QSerialPort::NoError)
    {
        if (m_messageError)
        {
            const QMetaObject *mobject = m_port->metaObject();
            int enumIndex = mobject->indexOfEnumerator("SerialPortError");
            sendMessage(QString("%1 was occured").arg(
                         mobject->enumerator(enumIndex).key(static_cast<int>(error))));
        }

        if (error == QSerialPort::ResourceError)
        {
            sendMessage(QStringLiteral("Device unplugged"));
            closePort();
            if(m_isAutoConnect)
                tryOpenPort();
        }
    }
}

void ComPortWorker::handleStopPeriodic()
{
    Q_CHECK_PTR(m_readTimer);

    m_isPeriodic = false;
    sendMessage(QStringLiteral("Stopped requesting data"));
}

void ComPortWorker::setupPort()
{
    Q_CHECK_PTR(m_port);

    m_port->setPort(m_settings.portInfo);
    m_port->setBaudRate(m_settings.baudRate);
    m_port->setDataBits(m_settings.dataBits);
    m_port->setParity(m_settings.parity);
    m_port->setStopBits(m_settings.stopBits);
    m_port->setFlowControl(m_settings.flowControl);
}

bool ComPortWorker::isAutoConnect() const
{
    return m_isAutoConnect;
}

void ComPortWorker::setIsAutoConnect(bool isAutoConnect)
{
    m_isAutoConnect = isAutoConnect;
}

Settings ComPortWorker::settings() const
{
    return m_settings;
}

void ComPortWorker::setSettings(const Settings &settings)
{
    m_settings = settings;
}

int ComPortWorker::detectInterval() const
{
    return m_detectInterval;
}

void ComPortWorker::setDetectInterval(int detectInterval)
{
    m_detectInterval = detectInterval;
}

int ComPortWorker::readInterval() const
{
    return m_readInterval;
}

void ComPortWorker::setReadInterval(int readInterval)
{
    m_readInterval = readInterval;
}
