#include "complementaryfusionfilter.h"
#include "complementaryfusionfiltersettings.h"
#include <cmath>

const float ComplementaryFusionFilter::m_singularityThreshold = -0.999f;

ComplementaryFusionFilter::ComplementaryFusionFilter(float gyroPeriod)
    : AbstractFusionFilter(gyroPeriod), m_magGain(0.02f), m_accelGain(0.02f)
{
}

void ComplementaryFusionFilter::updateIMUImpl(float ax, float ay, float az,
                                              float wx, float wy, float wz)
{
    if (!m_isInit)
    {
        getMeasurement(ax, ay, az,
                       m_q0, m_q1, m_q2, m_q3);
        m_isInit = true;
        return;
    }

    float q0_pred, q1_pred, q2_pred, q3_pred;
    getPrediction(wx, wy, wz,
                  q0_pred, q1_pred, q2_pred, q3_pred);
    m_q0 = q0_pred;
    m_q1 = q1_pred;
    m_q2 = q2_pred;
    m_q3 = q3_pred;

    if (m_isValidAccel)
    {
        float dq0_acc, dq1_acc, dq2_acc, dq3_acc;
        getAccCorrection(ax, ay, az, dq0_acc, dq1_acc, dq2_acc, dq3_acc);
        scaleQuaternion(m_accelGain, dq0_acc, dq1_acc, dq2_acc, dq3_acc);

        quaternionMultiplication(m_q0, m_q1, m_q2, m_q3,
                                 dq0_acc, dq1_acc, dq2_acc, dq3_acc,
                                 m_q0, m_q1, m_q2, m_q3);
    }

    normalizeQuaternion(m_q0, m_q1, m_q2, m_q3);
}

void ComplementaryFusionFilter::updateMARGImpl(float ax, float ay, float az,
                                               float wx, float wy, float wz,
                                               float mx, float my, float mz)
{
    if (!m_isInit)
    {
        // First time - ignore prediction:
        getMeasurement(ax, ay, az,
                       mx, my, mz,
                       m_q0, m_q1, m_q2, m_q3);
        m_isInit = true;
        return;
    }

    float q0_pred, q1_pred, q2_pred, q3_pred;
    getPrediction(wx, wy, wz,
                  q0_pred, q1_pred, q2_pred, q3_pred);
    m_q0 = q0_pred;
    m_q1 = q1_pred;
    m_q2 = q2_pred;
    m_q3 = q3_pred;

    if (m_isValidAccel)
    {
        float dq0_acc, dq1_acc, dq2_acc, dq3_acc;
        getAccCorrection(ax, ay, az, dq0_acc, dq1_acc, dq2_acc, dq3_acc);
        scaleQuaternion(m_accelGain, dq0_acc, dq1_acc, dq2_acc, dq3_acc);

        quaternionMultiplication(m_q0, m_q1, m_q2, m_q3,
                                 dq0_acc, dq1_acc, dq2_acc, dq3_acc,
                                 m_q0, m_q1, m_q2, m_q3);
    }

    if (m_isValidMag)
    {
        float dq0_mag, dq1_mag, dq2_mag, dq3_mag;
        getMagCorrection(mx, my, mz, dq0_mag, dq1_mag, dq2_mag, dq3_mag);
        scaleQuaternion(m_magGain, dq0_mag, dq1_mag, dq2_mag, dq3_mag);
        quaternionMultiplication(m_q0, m_q1, m_q2, m_q3,
                                 dq0_mag, dq1_mag, dq2_mag, dq3_mag,
                                 m_q0, m_q1, m_q2, m_q3);
    }

    normalizeQuaternion(m_q0, m_q1, m_q2, m_q3);
}

void ComplementaryFusionFilter::setConcreteSettings(const AbstractFilterSettingsPointer &settings)
{
    const auto concreteSettings = qSharedPointerCast<ComplementaryFusionFilterSettings>(settings);
    m_magGain = concreteSettings->magGain();
    m_accelGain = concreteSettings->accelGain();
}

void ComplementaryFusionFilter::getPrediction(float wx, float wy, float wz,
                                              float &q0_pred, float &q1_pred, float &q2_pred, float &q3_pred) const
{
    q0_pred = m_q0 + 0.5 * m_gyroPeriod * ( wx*m_q1 + wy*m_q2 + wz*m_q3);
    q1_pred = m_q1 + 0.5 * m_gyroPeriod * (-wx*m_q0 - wy*m_q3 + wz*m_q2);
    q2_pred = m_q2 + 0.5 * m_gyroPeriod * ( wx*m_q3 - wy*m_q0 - wz*m_q1);
    q3_pred = m_q3 + 0.5 * m_gyroPeriod * (-wx*m_q2 + wy*m_q1 - wz*m_q0);

    normalizeQuaternion(q0_pred, q1_pred, q2_pred, q3_pred);
}


void ComplementaryFusionFilter::getMeasurement(float ax, float ay, float az,
                                               float &q0_meas, float &q1_meas, float &q2_meas, float &q3_meas)
{
    normalizeVector(ax, ay, az);

    if (az < m_singularityThreshold)
    {
        q0_meas = 0;
        q1_meas = 1;
        q2_meas = 0;
        q3_meas = 0;
    }
    else
    {
        q0_meas =  sqrt((az + 1) * 0.5);
        q1_meas = -ay/(2.0 * q0_meas);
        q2_meas =  ax/(2.0 * q0_meas);
        q3_meas = 0;
    }
}

void ComplementaryFusionFilter::getMeasurement(float ax, float ay, float az,
                                               float mx, float my, float mz,
                                               float &q0_meas, float &q1_meas, float &q2_meas, float &q3_meas)
{
    // q_acc is the quaternion obtained from the acceleration vector representing
    // the orientation of the Global frame wrt the Local frame with arbitrary yaw
    // (intermediary frame). q3_acc is defined as 0.
    float q0_acc, q1_acc, q2_acc, q3_acc;

    // Normalize acceleration vector.
    normalizeVector(ax, ay, az);

    // Avoid singularity
    if (az < m_singularityThreshold)
    {
        q0_acc = 0;
        q1_acc = 1;
        q2_acc = 0;
        q3_acc = 0;
    }
    else
    {
        q0_acc =  sqrt((az + 1) * 0.5);
        q1_acc = -ay/(2.0 * q0_acc);
        q2_acc =  ax/(2.0 * q0_acc);
        q3_acc = 0;
    }

    // [lx, ly, lz] is the magnetic field reading, rotated into the intermediary
    // frame by the inverse of q_acc.
    // l = R(q_acc)^-1 m
    float lx = (q0_acc*q0_acc + q1_acc*q1_acc - q2_acc*q2_acc)*mx +
                 2.0 * (q1_acc*q2_acc)*my - 2.0 * (q0_acc*q2_acc)*mz;
    float ly = 2.0 * (q1_acc*q2_acc)*mx + (q0_acc*q0_acc - q1_acc*q1_acc +
                       q2_acc*q2_acc)*my + 2.0 * (q0_acc*q1_acc)*mz;

    // q_mag is the quaternion that rotates the Global frame (North West Up) into
    // the intermediary frame. q1_mag and q2_mag are defined as 0.
    float gamma = lx*lx + ly*ly;
    float beta = sqrt(gamma + lx*sqrt(gamma));
    float q0_mag = beta / (sqrt(2.0 * gamma));
    float q3_mag = ly / (sqrt(2.0) * beta);

    // The quaternion multiplication between q_acc and q_mag represents the
    // quaternion, orientation of the Global frame wrt the local frame.
    // q = q_acc times q_mag
    quaternionMultiplication(q0_acc, q1_acc, q2_acc, q3_acc,
                             q0_mag, 0, 0, q3_mag,
                             q0_meas, q1_meas, q2_meas, q3_meas );
}

void ComplementaryFusionFilter::getAccCorrection(float ax, float ay, float az,
                                                 float &dq0, float &dq1, float &dq2, float &dq3)
{
    // Normalize acceleration vector.
    normalizeVector(ax, ay, az);

    // Acceleration reading rotated into the world frame by the inverse predicted
    // quaternion (predicted gravity):
    float gx, gy, gz;
    rotateVectorByQuaternion(ax, ay, az,
                             m_q0, -m_q1, -m_q2, -m_q3,
                             gx, gy, gz);

    // Delta quaternion that rotates the predicted gravity into the real gravity:
    dq0 =  sqrt((gz + 1) * 0.5);
    dq1 = -gy/(2.0 * dq0);
    dq2 =  gx/(2.0 * dq0);
    dq3 =  0.0;
}

void ComplementaryFusionFilter::getMagCorrection(float mx, float my, float mz,
                                                 float &dq0, float &dq1, float &dq2, float &dq3)
{
    // Magnetic reading rotated into the world frame by the inverse predicted
    // quaternion:
    float lx, ly, lz;
    rotateVectorByQuaternion(mx, my, mz,
                             m_q0, -m_q1, -m_q2, -m_q3,
                             lx, ly, lz);

    // Delta quaternion that rotates the l so that it lies in the xz-plane
    // (points north):
    float gamma = lx*lx + ly*ly;
    float beta = sqrt(gamma + lx*sqrt(gamma));
    dq0 = beta / (sqrt(2.0 * gamma));
    dq1 = 0.0;
    dq2 = 0.0;
    dq3 = ly / (sqrt(2.0) * beta);
}
