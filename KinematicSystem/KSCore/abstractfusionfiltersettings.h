#ifndef ABSTRACTFUSIONFILTERSETTINGS_H
#define ABSTRACTFUSIONFILTERSETTINGS_H
#include <QtGlobal>
#include <QSharedPointer>

class AbstractFusionFilterSettings;
typedef QSharedPointer<AbstractFusionFilterSettings> AbstractFilterSettingsPointer;

class AbstractFusionFilterSettings
{
public:
    AbstractFusionFilterSettings();
    virtual ~AbstractFusionFilterSettings() = 0;

    bool doBiasEstimation() const;
    void setDoBiasEstimation(bool doBiasEstimation);

    bool doThresholding() const;
    void setDoThresholding(bool doThresholding);

    float minSteadyDuration() const;
    void setMinSteadyDuration(float minSteadyDuration);

    float gyroBiasAlpha() const;
    void setGyroBiasAlpha(float gyroBiasAlpha);

    float accelThrAlpha() const;
    void setAccelThrAlpha(float accelThrAlpha);

    float magThrAlpha() const;
    void setMagThrAlpha(float magThrAlpha);

    float dipThrAlpha() const;
    void setDipThrAlpha(float dipThrAlpha);

private:
    bool m_doBiasEstimation = true;
    bool m_doThresholding = true;

    float m_minSteadyDuration = 1.0f;
    float m_gyroBiasAlpha = 0.05f;

    float m_accelThrAlpha = 0.01f;
    float m_magThrAlpha = 0.01f;
    float m_dipThrAlpha = 0.01f;
};

#endif // ABSTRACTFUSIONFILTERSETTINGS_H
