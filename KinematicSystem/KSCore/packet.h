#ifndef PACKET_H
#define PACKET_H

#include "kstypes.h"

class QByteArray;

class Packet
{

public:
    Packet();
    Packet(quint8 packetType, quint8 packetSize);
    virtual ~Packet() = 0;
    QByteArray getPacket() const;
    quint16 getSize() const;

protected:
    quint8 m_packetType;
    quint16 m_size;

private:
    virtual void addPacketData(QByteArray &packet) const;

};

#endif // PACKET_H
