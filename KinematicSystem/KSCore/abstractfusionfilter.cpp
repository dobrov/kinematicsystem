#include "abstractfusionfilter.h"
#include <cmath>

AbstractFusionFilter::AbstractFusionFilter(float gyroPeriod) :
    m_gyroPeriod(gyroPeriod), m_q0(1), m_q1(0), m_q2(0), m_q3(0),
    m_minSteadyLenSamples(0)
{
}

void AbstractFusionFilter::updateIMU(float ax, float ay, float az,
                                     float wx, float wy, float wz)
{
    Q_CHECK_PTR(m_settings);

    if (m_settings->doThresholding())
    {
        auto accelMagn = sqrt(ax*ax + ay*ay + az*az);
        m_isValidAccel = std::abs(accelMagn - m_accelTrueMagn) <
                m_settings->accelThrAlpha() * m_accelTrueMagn;
    }

    if (m_settings->doBiasEstimation())
    {
        updateGyroBiases(ax, ay, az, wx, wy, wz);
        wx -= m_gyroBiases.at(0);
        wy -= m_gyroBiases.at(1);
        wz -= m_gyroBiases.at(2);
    }

    updateIMUImpl(ax, ay, az, wx, wy, wz);
}

void AbstractFusionFilter::updateMARG(float ax, float ay, float az,
                                      float wx, float wy, float wz,
                                      float mx, float my, float mz)
{
    Q_CHECK_PTR(m_settings);

    if (m_settings->doThresholding())
    {
        auto accelMagn = sqrt(ax*ax + ay*ay + az*az);
        m_isValidAccel = std::abs(accelMagn - m_accelTrueMagn) <
                m_settings->accelThrAlpha() * m_accelTrueMagn;

        auto magMagn = sqrt(mx*mx + my*my + mz*mz);
        if (!m_isInit)
        {
            m_magTrueMagn = magMagn;
        }
        m_isValidMag = std::abs(magMagn - m_magTrueMagn) <
                m_settings->magThrAlpha() * m_magTrueMagn;
        if (m_isValidAccel && m_isValidMag)
        {
            QVector3D accel(ax, ay, az);
            accel.normalize();
            QVector3D mag(mx, my, mz);
            mag.normalize();
            auto dipAngle = QVector3D::dotProduct(accel, mag);
            if (m_trueDipAngle == 0.0f)
            {
                m_trueDipAngle = dipAngle;
            }
            else
            {
                m_isValidMag = std::abs(dipAngle - m_trueDipAngle) <
                        std::abs(m_settings->dipThrAlpha() * m_trueDipAngle);
            }
        }
    }

    if (m_settings->doBiasEstimation())
    {
        updateGyroBiases(ax, ay, az, wx, wy, wz);
        wx -= m_gyroBiases.at(0);
        wy -= m_gyroBiases.at(1);
        wz -= m_gyroBiases.at(2);
    }

    updateMARGImpl(ax, ay, az, wx, wy, wz, mx, my, mz);
}

AbstractFusionFilter::~AbstractFusionFilter()
{
}

void AbstractFusionFilter::clear()
{
    m_isInit = false;
    m_q0 = 1;
    m_q1 = 0;
    m_q2 = 0;
    m_q3 = 0;
}

QuatSample AbstractFusionFilter::getOrientation() const
{
    return {m_q0, -m_q1, -m_q2, -m_q3};
}

void AbstractFusionFilter::setOrientation(const QuatSample &quat)
{
    invertQuaternion(std::get<QValueS>(quat), std::get<QValueX>(quat),
                     std::get<QValueY>(quat), std::get<QValueZ>(quat),
                     m_q0, m_q1, m_q2, m_q3);
}

void AbstractFusionFilter::setGyroPeriod(float gyroPeriod)
{
    m_gyroPeriod = gyroPeriod;
    m_minSteadyLenSamples = m_settings->minSteadyDuration() / m_gyroPeriod;
}


bool AbstractFusionFilter::isSteadyState(float ax, float ay, float az, float wx, float wy, float wz) const
{
    constexpr float accelMagnThreshold = 1.01f; //g
    constexpr float deltaAccelMagnThreshold = 0.005f; //g
    constexpr float gyroThreshold = 5.0f * deg2rad(); //rad
    constexpr float deltaGyroThreshold = 1.0f * deg2rad(); //rad

    bool steadyState = true;
    float accelMagn = std::sqrt(ax*ax + ay*ay + az*az);

    if (std::abs(accelMagn) > accelMagnThreshold){
        steadyState = false;
    }
    else if (std::abs(accelMagn - m_prevAccelMagn) > deltaAccelMagnThreshold){
        steadyState = false;
    }
    else if (std::abs(wx) > gyroThreshold ||
             std::abs(wy) > gyroThreshold ||
             std::abs(wz) > gyroThreshold){
        steadyState = false;
    }
    else if (std::abs(wx - m_prevGyro.at(0)) > deltaGyroThreshold ||
             std::abs(wy - m_prevGyro.at(1)) > deltaGyroThreshold ||
             std::abs(wz - m_prevGyro.at(2)) > deltaGyroThreshold){
        steadyState = false;
    }

    m_prevAccelMagn = accelMagn;
    m_prevGyro[0] = wx;
    m_prevGyro[1] = wy;
    m_prevGyro[2] = wz;

    return steadyState;
}

void AbstractFusionFilter::updateGyroBiases(float ax, float ay, float az, float wx, float wy, float wz)
{
    Q_ASSERT(m_minSteadyLenSamples != 0);

    if (!isSteadyState(ax, ay, az, wx, wy, wz))
    {
        m_steadyStateCount = 0;
        return;
    }

    if (!m_steadyStateCount)
    {
        m_gyroMeanRT[0] = wx;
        m_gyroMeanRT[1] = wy;
        m_gyroMeanRT[2] = wz;
    }
    else
    {
        m_gyroMeanRT[0] = (m_gyroMeanRT.at(0) * m_steadyStateCount + wx) / (m_steadyStateCount + 1);
        m_gyroMeanRT[1] = (m_gyroMeanRT.at(1) * m_steadyStateCount + wy) / (m_steadyStateCount + 1);
        m_gyroMeanRT[2] = (m_gyroMeanRT.at(2) * m_steadyStateCount + wz) / (m_steadyStateCount + 1);
    }
    m_steadyStateCount++;

    if (m_steadyStateCount > m_minSteadyLenSamples)
    {
        const auto alpha = m_settings->gyroBiasAlpha();
        m_gyroBiases[0] = (1 - alpha) * m_gyroBiases.at(0) + alpha * m_gyroMeanRT.at(0);
        m_gyroBiases[1] = (1 - alpha) * m_gyroBiases.at(1) + alpha * m_gyroMeanRT.at(1);
        m_gyroBiases[2] = (1 - alpha) * m_gyroBiases.at(2) + alpha * m_gyroMeanRT.at(2);
        //qDebug() << m_gyroBiases[0] << m_gyroBiases[1] << m_gyroBiases[2];
    }
}

void AbstractFusionFilter::setSettings(const AbstractFilterSettingsPointer &settings)
{
    m_settings = settings;
    m_minSteadyLenSamples = m_settings->minSteadyDuration() / m_gyroPeriod;
    setConcreteSettings(settings);
}

void normalizeVector(float &x, float &y, float &z)
{
    float norm = sqrt(x*x + y*y + z*z);
    x /= norm;
    y /= norm;
    z /= norm;
}

void normalizeQuaternion(float &q0, float &q1, float &q2, float &q3)
{
    float norm = sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);
    q0 /= norm;
    q1 /= norm;
    q2 /= norm;
    q3 /= norm;
}

void scaleQuaternion(float gain, float &dq0, float &dq1, float &dq2, float &dq3)
{
    if (dq0 < 0.9) //Slerp threhold
    {
        // Slerp (Spherical linear interpolation):
        float angle = acos(dq0);
        float A = sin(angle*(1.0f - gain))/sin(angle);
        float B = sin(angle * gain)/sin(angle);
        dq0 = A + B * dq0;
        dq1 = B * dq1;
        dq2 = B * dq2;
        dq3 = B * dq3;
    }
    else
    {
        // Lerp (Linear interpolation):
        dq0 = (1.0f - gain) + gain * dq0;
        dq1 = gain * dq1;
        dq2 = gain * dq2;
        dq3 = gain * dq3;
    }

    normalizeQuaternion(dq0, dq1, dq2, dq3);
}

void invertQuaternion(float q0, float q1, float q2, float q3,
                      float &q0_inv, float &q1_inv, float &q2_inv, float &q3_inv)
{
    // Assumes quaternion is normalized.
    q0_inv = q0;
    q1_inv = -q1;
    q2_inv = -q2;
    q3_inv = -q3;
}

void quaternionMultiplication(float p0, float p1, float p2, float p3,
                              float q0, float q1, float q2, float q3,
                              float &r0, float &r1, float &r2, float &r3)
{
    r0 = p0*q0 - p1*q1 - p2*q2 - p3*q3;
    r1 = p0*q1 + p1*q0 + p2*q3 - p3*q2;
    r2 = p0*q2 - p1*q3 + p2*q0 + p3*q1;
    r3 = p0*q3 + p1*q2 - p2*q1 + p3*q0;
}

void rotateVectorByQuaternion(float x, float y, float z,
                              float q0, float q1, float q2, float q3,
                              float &vx, float &vy, float &vz)
{
    vx = (q0*q0 + q1*q1 - q2*q2 - q3*q3)*x + 2.0f*(q1*q2 - q0*q3)*y + 2.0f*(q1*q3 + q0*q2)*z;
    vy = 2.0f*(q1*q2 + q0*q3)*x + (q0*q0 - q1*q1 + q2*q2 - q3*q3)*y + 2.0f*(q2*q3 - q0*q1)*z;
    vz = 2.0f*(q1*q3 - q0*q2)*x + 2.0f*(q2*q3 + q0*q1)*y + (q0*q0 - q1*q1 - q2*q2 + q3*q3)*z;
}
