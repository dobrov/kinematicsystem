#ifndef DIPANGLECALCULATOR_H
#define DIPANGLECALCULATOR_H

#include <abstractpassingdatahandler.h>

class DipAngleCalculator : public AbstractPassingDataHandler
{
    Q_OBJECT
    struct DeviceData
    {
        DeviceData(TimeSample beginTime = 0);
        bool isMagSample;
        TimeSample currentTime;
        CalDataVector accelData;
        CalDataVector magData;
    };

public:
    explicit DipAngleCalculator(QObject *parent = 0);

public slots:
    void setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate);
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
    virtual void clear() override;

private:
    TimeSample m_magPeriod;

    QMap<quint8, DeviceData> m_deviceDataMap;
};

#endif // DIPANGLECALCULATOR_H
