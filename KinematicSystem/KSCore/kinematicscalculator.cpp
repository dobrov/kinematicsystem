#include "kinematicscalculator.h"
#include "concretepassingdata.h"
#include <QVector3D>

KinematicsCalculator::KinematicsCalculator(QObject *parent)
    : AbstractPassingDataHandler(parent), m_currentTime(0)
{
}

void KinematicsCalculator::handleNewData(const AbstractPassingDataPointer &passingData)
{
    Q_ASSERT(m_gyroPeriod);
    Q_ASSERT(m_bodyParam.size());
    Q_ASSERT(!m_quatEG.isIdentity());
    Q_ASSERT(passingData->getDataType() == PassingDataType::Quat);

    auto itBone = m_deviceToBoneMap.constFind(passingData->getDeviceId());
    if (itBone == m_deviceToBoneMap.constEnd())
        return;

    if (m_currentTime == 0)
        m_currentTime = passingData->getTime().first();

    const auto newData = qSharedPointerCast<QuatPassingData>(passingData)->getData();
    const auto index = itBone.value();
    m_quatSEData[std::get<BodyPartValue>(index)][std::get<BoneValue>(index)].append(newData);

    KinDataVector outKinData;
    PosDataVector outPosData;
    TimeDataVector outTime;

    while(checkIsReadyESData())
    {
        static const QString stringCurTemplate("Current kinematics time: %1");
        sendMessage(stringCurTemplate.arg(m_currentTime));
        KinSample kinSample;
        PosSample posSample;

        for (auto it = m_quatSEData.begin(); it != m_quatSEData.end(); ++it)
        {
            const auto bodyPart = it.key();
            QVector3D trans;
            if (bodyPart == BodyPart::RHL)
                trans += m_hipLength;
            QVector<SegmentData> bodyPartKinData;
            QVector<QVector3D> bodyPartPosData;
            auto &bodyPartData = it.value();
            const auto &bodyPartParam = m_bodyParam.value(bodyPart);
            for (int i = 0; i < bodyPartData.size(); i++)
            {
                auto quatSEdata = bodyPartData[i].takeFirst();
                QQuaternion quatSE(std::get<QValueS>(quatSEdata), std::get<QValueX>(quatSEdata),
                                   std::get<QValueY>(quatSEdata), std::get<QValueZ>(quatSEdata));
                const auto &boneParam = bodyPartParam.at(i);
                auto quatBG = m_quatEG * quatSE * std::get<BQuat>(boneParam);
                bodyPartKinData.append(std::make_tuple(trans, quatBG));
                bodyPartPosData.append(trans);
                trans += quatBG.rotatedVector(QVector3D(std::get<BLength>(boneParam), 0.0f, 0.0f));
            }
            bodyPartPosData.append(trans);
            posSample.insert(bodyPart, bodyPartPosData);
            kinSample.insert(bodyPart, bodyPartKinData);
        }
        outPosData.append(posSample);
        outKinData.append(kinSample);
        outTime.append(m_currentTime);
        m_currentTime += m_gyroPeriod;
    }

    if (!outTime.isEmpty())
    {
        emit sendData(QSharedPointer<KinPassingData>::create(0, PassingDataType::Kin, outTime, outKinData));
        emit sendData(QSharedPointer<PosPassingData>::create(0, PassingDataType::Pos, outTime, outPosData));
    }
}

void KinematicsCalculator::clear()
{
    m_currentTime = 0;
    for (auto &bodyPart : m_quatSEData)
    {
        for (auto &bone : bodyPart)
            bone.clear();
    }
}

void KinematicsCalculator::setOffsetData(const OffsetData &offsetData)
{
    setGlobalFrame(offsetData.m_quatEG);
    setBodyOffset(offsetData.m_bodyOffset);
    setDeviceList(offsetData.m_deviceList);
    setHipLength(offsetData.m_hipLength);
}

void KinematicsCalculator::setGlobalFrame(const QQuaternion &quatEG)
{
    m_quatEG = quatEG;
}

void KinematicsCalculator::setHipLength(const QVector3D &hipLength)
{
    m_hipLength = hipLength;
}

void KinematicsCalculator::setBodyOffset(const BodyParam &bodyOffset)
{
    m_bodyParam = bodyOffset;
    m_quatSEData.clear();
    for (auto it = m_bodyParam.cbegin(); it != m_bodyParam.cend(); ++it)
    {
        QVector<QuatDataVector> bodyPartData;
        bodyPartData.resize(it.value().size());
        m_quatSEData.insert(it.key(), bodyPartData);
    }
}

void KinematicsCalculator::setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate)
{
    Q_UNUSED(accelDataRate);
    Q_UNUSED(magDataRate);

    m_gyroPeriod = 1000000 / gyroDataRate;
}

void KinematicsCalculator::setDeviceList(const BodyDevList &bodyDevList)
{
    m_deviceToBoneMap.clear();

    for (auto it = bodyDevList.cbegin(); it != bodyDevList.cend(); ++it)
    {
        const auto &bodyPart = it.value();
        for (int i = 0; i < bodyPart.size(); i++)
            m_deviceToBoneMap.insert(bodyPart.at(i), std::make_tuple(it.key(), i));
    }
}

bool KinematicsCalculator::checkIsReadyESData()
{
    for (const auto &bodyPart : m_quatSEData)
    {
        for (const auto &quatData : bodyPart)
        {
            if (quatData.isEmpty())
                return false;
        }
    }
    return true;
}
