#include "abstractfusionfiltersettings.h"

AbstractFusionFilterSettings::AbstractFusionFilterSettings()
{
}

bool AbstractFusionFilterSettings::doBiasEstimation() const
{
    return m_doBiasEstimation;
}

void AbstractFusionFilterSettings::setDoBiasEstimation(bool doBiasEstimation)
{
    m_doBiasEstimation = doBiasEstimation;
}

bool AbstractFusionFilterSettings::doThresholding() const
{
    return m_doThresholding;
}

void AbstractFusionFilterSettings::setDoThresholding(bool doThresholding)
{
    m_doThresholding = doThresholding;
}

float AbstractFusionFilterSettings::minSteadyDuration() const
{
    return m_minSteadyDuration;
}

void AbstractFusionFilterSettings::setMinSteadyDuration(float minSteadyDuration)
{
    m_minSteadyDuration = minSteadyDuration;
}

float AbstractFusionFilterSettings::gyroBiasAlpha() const
{
    return m_gyroBiasAlpha;
}

void AbstractFusionFilterSettings::setGyroBiasAlpha(float gyroBiasAlpha)
{
    m_gyroBiasAlpha = gyroBiasAlpha;
}

float AbstractFusionFilterSettings::accelThrAlpha() const
{
    return m_accelThrAlpha;
}

void AbstractFusionFilterSettings::setAccelThrAlpha(float accelThrAlpha)
{
    m_accelThrAlpha = accelThrAlpha;
}

float AbstractFusionFilterSettings::magThrAlpha() const
{
    return m_magThrAlpha;
}

void AbstractFusionFilterSettings::setMagThrAlpha(float magThrAlpha)
{
    m_magThrAlpha = magThrAlpha;
}

float AbstractFusionFilterSettings::dipThrAlpha() const
{
    return m_dipThrAlpha;
}

void AbstractFusionFilterSettings::setDipThrAlpha(float dipThrAlpha)
{
    m_dipThrAlpha = dipThrAlpha;
}

AbstractFusionFilterSettings::~AbstractFusionFilterSettings()
{
}
