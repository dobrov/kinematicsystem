#ifndef LINEARINTERPOLATOR_H
#define LINEARINTERPOLATOR_H

#include "abstractinterpolator.h"

class LinearInterpolator : public AbstractInterpolator
{
    Q_OBJECT
public:
    explicit LinearInterpolator(QObject *parent = 0);

public slots:
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
};

#endif // LINEARINTERPOLATOR_H
