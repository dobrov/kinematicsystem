#ifndef CONCRETEPASSINGDATA_H
#define CONCRETEPASSINGDATA_H

#include "abstractpassingdata.h"
#include <type_traits>
#include <algorithm>
#include <iterator>
#include <typeinfo>

template <typename T>
class ConcretePassingData : public AbstractPassingData
{
public:
    ConcretePassingData();
    explicit ConcretePassingData(const FlowId &flowId);
    explicit ConcretePassingData(quint8 deviceId, PassingDataType dataType);
    explicit ConcretePassingData(quint8 deviceId, PassingDataType dataType,
                                 const TimeDataVector &time, const DataVector<T> &data);
    explicit ConcretePassingData(const FlowId &flowId, const TimeDataVector &time,
                                 const DataVector<T> &data);
    inline const DataVector<T> &getData() const;

private:
#ifdef QT_DEBUG
    inline void checkType(PassingDataType dataType) const;
#endif
    virtual ConcretePassingData<T> *doClone() const override;
    virtual AbstractPassingDataPointer splitTemplateMethod(int i, const TimeDataVector &time) override;
    virtual void eraseTemplateMethod(int n, EraseType eraseType) override;
    virtual void appendTemplateMethod(const AbstractPassingDataPointer &appendingData) override;
    DataVector<T> m_data;
};

typedef ConcretePassingData<QuatSample> QuatPassingData;
typedef ConcretePassingData<RawSample> RawPassingData;
typedef ConcretePassingData<CalSample> CalPassingData;
typedef ConcretePassingData<KinSample> KinPassingData;
typedef ConcretePassingData<PosSample> PosPassingData;
typedef ConcretePassingData<DipSample> DipPassingData;

template <typename T>
ConcretePassingData<T>::ConcretePassingData()
    : AbstractPassingData()
{
}

template <typename T>
ConcretePassingData<T>::ConcretePassingData(const FlowId &flowId)
    : AbstractPassingData(std::get<FDeviceId>(flowId), std::get<FPassingDataType>(flowId))
{
#ifdef QT_DEBUG
    checkType(m_dataType);
#endif
}

template <typename T>
ConcretePassingData<T>::ConcretePassingData(quint8 deviceId, PassingDataType dataType)
    : AbstractPassingData(deviceId, dataType)
{
#ifdef QT_DEBUG
    checkType(dataType);
#endif
}

template <typename T>
ConcretePassingData<T>::ConcretePassingData(quint8 deviceId, PassingDataType dataType,
                                            const TimeDataVector &time, const DataVector<T> &data)
    : AbstractPassingData(deviceId, dataType, time), m_data(data)
{
    Q_ASSERT(!m_data.isEmpty());
    Q_ASSERT(getTime().size() == m_data.size());
#ifdef QT_DEBUG
    checkType(dataType);
#endif
}

template <typename T>
ConcretePassingData<T>::ConcretePassingData(const FlowId &flowId, const TimeDataVector &time,
                                            const DataVector<T> &data)
    : ConcretePassingData(std::get<FDeviceId>(flowId), std::get<FPassingDataType>(flowId), time, data)
{
}

#ifdef QT_DEBUG
template <typename T>
void ConcretePassingData<T>::checkType(PassingDataType dataType) const
{
    if (std::is_same<T, QuatSample>::value)
    {
        Q_ASSERT(dataType == PassingDataType::Quat);
    }
    else if (std::is_same<T, CalSample>::value)
    {
        Q_ASSERT(dataType == PassingDataType::CalAccel || dataType == PassingDataType::CalMag ||
                 dataType == PassingDataType::CalGyro);
    }
    else if (std::is_same<T, RawSample>::value)
    {
        Q_ASSERT(dataType == PassingDataType::RawAccel || dataType == PassingDataType::RawGyro ||
                 dataType == PassingDataType::RawMag);
    }
    else if (std::is_same<T, KinSample>::value)
    {
        Q_ASSERT(dataType == PassingDataType::Kin);
    }
    else if (std::is_same<T, PosSample>::value)
    {
        Q_ASSERT(dataType == PassingDataType::Pos);
    }
    else if (std::is_same<T, DipSample>::value)
    {
        Q_ASSERT(dataType == PassingDataType::DipAngle);
    }
    else
        Q_ASSERT_X(false, "ConcretePassingData<T>::checkType", "Incorrect template type");
}
#endif

template <typename T>
ConcretePassingData<T> *ConcretePassingData<T>::doClone() const
{
    return new ConcretePassingData<T>(*this);
}

template <typename T>
const DataVector<T> &ConcretePassingData<T>::getData() const
{
    return m_data;
}

template <typename T>
AbstractPassingDataPointer ConcretePassingData<T>::splitTemplateMethod(int i, const TimeDataVector &time)
{
    DataVector<T> data;
    data.reserve(i);

    auto iter = m_data.begin();
    std::move(iter, iter + i, std::back_inserter(data));
    m_data.erase(iter, iter + i);

    return QSharedPointer<ConcretePassingData<T>>::create(m_deviceId, m_dataType, time, data);
}

template <typename T>
void ConcretePassingData<T>::appendTemplateMethod(const AbstractPassingDataPointer &appendingData)
{
    Q_ASSERT(typeid(*this) == typeid(*appendingData));
    m_data.append(qSharedPointerCast<ConcretePassingData<T>>(appendingData)->getData());
}

template <typename T>
void ConcretePassingData<T>::eraseTemplateMethod(int n, EraseType eraseType)
{
    auto it = (eraseType == EraseType::Head) ? std::begin(m_data) : std::end(m_data);

    if (eraseType == EraseType::Head)
        m_data.erase(it, it + n);
    else
        m_data.erase(it - n, it);
}

template <typename T>
const DataVector<T> &getData(const AbstractPassingDataPointer &passingData)
{
    return qSharedPointerCast<ConcretePassingData<T>>(passingData)->getData();
}

#endif // CONCRETEPASSINGDATA_H
