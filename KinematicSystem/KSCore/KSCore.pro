#-------------------------------------------------
#
# Project created by QtCreator 2015-08-23T23:54:05
#
#-------------------------------------------------

QT += core serialport sql

TARGET = KSCore
TEMPLATE = lib
CONFIG += staticlib c++14

SOURCES += comportworker.cpp \
    packet.cpp \
    framedecoder.cpp \
    logger.cpp \
    devicelistpacket.cpp \
    requestdatapacket.cpp \
    configurealldevicepacket.cpp \
    configuresingledevicepacket.cpp \
    configuretimerpacket.cpp \
    abstractworker.cpp \
    abstractinterpolator.cpp \
    linearinterpolator.cpp \
    magimpulsenoisefilter.cpp \
    abstractpassingdata.cpp \
    flowsynchronizer.cpp \
    flowqueue.cpp \
    frame.cpp \
    calibrator.cpp \
    abstractfusionfilter.cpp \
    sensorfusion.cpp \
    complementaryfusionfilter.cpp \
    datasystem.cpp \
    kinematicscalculator.cpp \
    kstypes.cpp \
    configuresyncpacket.cpp \
    timepassingdata.cpp \
    dipanglecalculator.cpp \
    abstractfusionfiltersettings.cpp \
    complementaryfusionfiltersettings.cpp

HEADERS += comportworker.h \
    packet.h \
    framedecoder.h \
    logger.h \
    devicelistpacket.h \
    requestdatapacket.h \
    configurealldevicepacket.h \
    configuresingledevicepacket.h \
    configuretimerpacket.h \
    abstractworker.h \
    abstractinterpolator.h \
    linearinterpolator.h \
    magimpulsenoisefilter.h \
    abstractpassingdata.h \
    kstypes.h \
    abstractpassingdatahandler.h \
    concretepassingdata.h \
    flowsynchronizer.h \
    flowqueue.h \
    frame.h \
    calibrator.h \
    abstractfusionfilter.h \
    sensorfusion.h \
    complementaryfusionfilter.h \
    datasystem.h \
    kinematicscalculator.h \
    configuresyncpacket.h \
    timepassingdata.h \
    dipanglecalculator.h \
    abstractfusionfiltersettings.h \
    complementaryfusionfiltersettings.h

OTHER_FILES += KSCore.pri
