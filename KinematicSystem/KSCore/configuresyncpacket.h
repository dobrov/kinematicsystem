#ifndef CONFIGURESYNCPACKET_H
#define CONFIGURESYNCPACKET_H
#include "packet.h"

class ConfigureSyncPacket : public Packet
{
public:
    ConfigureSyncPacket();
    ConfigureSyncPacket(quint8 enable, quint8 lev);

    void setEnable(const quint8 &enable);
    void setLev(const quint8 &lev);

private:
    virtual void addPacketData(QByteArray &packet) const override;

    quint8 m_enable;
    quint8 m_lev;
};

#endif // CONFIGURESYNCPACKET_H
