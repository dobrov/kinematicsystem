#ifndef FRAMECREATOR_H
#define FRAMECREATOR_H

#include <QString>
#include "packet.h"

class Frame
{
public:
    explicit Frame();
    void addPacket(const Packet &packet);
    const QByteArray &getFrame() const;
    int getNumberOfPackets() const;
    void clear();

private:
    void addFrameHead();
    QByteArray m_frame;
    quint16 m_frameSize;
    int m_numberOfPackets;
};

Q_DECLARE_METATYPE(Frame)

#endif // FRAMECREATOR_H
