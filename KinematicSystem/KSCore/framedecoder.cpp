#include "framedecoder.h"
#include "concretepassingdata.h"
#include "timepassingdata.h"
#include "kstypes.h"
#include <QMetaEnum>

FrameDecoder::FrameDecoder(QObject *parent)
    : AbstractWorker(parent), m_data()
{
}

void FrameDecoder::parseFrame(const QByteArray &data)
{
    Q_ASSERT(!data.isEmpty());
    m_data += data;
    const char *rawData = m_data.constData();
    uint dataSize = m_data.size();
    uint iter = 0;
    uint frameEnd = 0;
    while(iter < dataSize)
    {
        sendMessage(QStringLiteral("Starting for parse frame"));

        if(!isInFrame(iter + 2, dataSize))
        {
            if(frameEnd)
                m_data = m_data.mid(frameEnd);
            return;
        }
        if(!checkSignature(rawData + iter))
        {
            //Corrupted frame. Is it possible?
            m_data.clear();
            return;
        }
        iter += 2;

        if(!isInFrame(iter + 2, dataSize))
        {
            if(frameEnd)
                m_data = m_data.mid(frameEnd);
            return;
        }
        quint16 frameSize = getUnsignedShort(rawData + iter);
        iter += 2;

        //Wait for whole frame
        if (!isInFrame(iter + frameSize, dataSize))
        {
            if(frameEnd)
                m_data = m_data.mid(frameEnd);
            return;
        }
        frameEnd = iter + frameSize;

        quint64 baseTimestamp = 0;
        while (iter < frameEnd)
        {
            ReceivingPacketType packetType = getPacketType(rawData[iter]);
            static const auto metaEnum =  QMetaEnum::fromType<ReceivingPacketType>();
            static const QString stringTemplate("Starting for parse %1 packet");
            sendMessage(stringTemplate.arg(metaEnum.key(static_cast<int>(packetType))));
            iter++;

            quint16 packetSize = getUnsignedShort(rawData + iter);
            iter += 2;


            switch (packetType)
            {
            case ReceivingPacketType::Ack:
                Q_ASSERT(!packetSize);
                emit ackReceived();
                break;
            case ReceivingPacketType::ConnectedDevicesList:
                parseConnectedDevicePacket(rawData + iter, packetSize);
                break;
            case ReceivingPacketType::SyncData:
                Q_ASSERT(packetSize == sizeof(TimeSample));
                parseSyncPacket(rawData + iter);
                break;
            case ReceivingPacketType::TimestampData:
                Q_ASSERT(packetSize == sizeof(TimeSample));
                Q_ASSERT(!baseTimestamp);
                baseTimestamp = parseTimestampPacket(rawData + iter);
                break;
            case ReceivingPacketType::AccelData:
                parseSensorDataPacket(rawData + iter, PassingDataType::RawAccel, packetSize, baseTimestamp);
                break;
            case ReceivingPacketType::GyroData:
                parseSensorDataPacket(rawData + iter, PassingDataType::RawGyro, packetSize, baseTimestamp);
                break;
            case ReceivingPacketType::MagData:
                parseSensorDataPacket(rawData + iter, PassingDataType::RawMag,  packetSize, baseTimestamp);
                break;
            default:
                parseUnsupportedPacket();
            }
            iter += packetSize;
        }
    }
    m_data.clear();
}

FrameDecoder::ReceivingPacketType FrameDecoder::getPacketType(uchar packetCode) const
{
    switch (packetCode)
    {
    case 0x70:
        return ReceivingPacketType::Ack;
    case 0x71:
        return ReceivingPacketType::NAck;
    case 0x80:
        return ReceivingPacketType::SystemVersion;
    case 0x81:
        return ReceivingPacketType::ConnectedDevicesList;
    case 0x90:
        return ReceivingPacketType::TimerConfiguration;
    case 0x91:
        return ReceivingPacketType::SyncConfiguration;
    case 0x92:
        return ReceivingPacketType::SingleDeviceConfig;
    case 0xA0:
        return ReceivingPacketType::TimestampData;
    case 0xA1:
        return ReceivingPacketType::SyncData;
    case 0xA2:
        return ReceivingPacketType::AccelData;
    case 0xA3:
        return ReceivingPacketType::GyroData;
    case 0xA4:
        return ReceivingPacketType::MagData;
    default:
        return ReceivingPacketType::UnknownPacketData;
    }
}

bool FrameDecoder::checkSignature(const char *data)
{
    Q_CHECK_PTR(data);
    if (!(getUnsignedShort(data) == 0x4b53))
    {
        sendMessage(QStringLiteral("Invalid signature of frame"));
        return false;
    }
    return true;
}

bool FrameDecoder::isInFrame(uint position, uint dataSize) const
{
    if (position > dataSize)
    {
        sendMessage(QStringLiteral("Stop parsing partial frame (Out of frame)."));
        return false;
    }
    return true;
}

quint16 FrameDecoder::getUnsignedShort(const char *data) const
{
    Q_CHECK_PTR(data);
    return *reinterpret_cast<const quint16*>(data);
}

qint16 FrameDecoder::getSignedShort(const char *data) const
{
    Q_CHECK_PTR(data);
    return *reinterpret_cast<const qint16*>(data);
}

quint64 FrameDecoder::getUnsignedLongLong(const char *data) const
{
    Q_CHECK_PTR(data);
    return *reinterpret_cast<const quint64*>(data);
}

void FrameDecoder::parseConnectedDevicePacket(const char *data, quint16 packetSize)
{
    Q_CHECK_PTR(data);
    Q_ASSERT(!(packetSize % 2));
    DeviceList conDeviceData;
    conDeviceData.reserve(packetSize/2);
    for (uint i = 0; i < packetSize; i += 2)
    {
        uchar bus = static_cast<uchar>(data[i]);
        uchar device = static_cast<uchar>(data[i + 1]);
        conDeviceData.push_back(getDeviceId(bus, device));
    }
    emit connectedDeviceData(conDeviceData);
}

void FrameDecoder::parseSyncPacket(const char *data)
{
    Q_CHECK_PTR(data);
    TimeSample time = getUnsignedLongLong(data);
    static const QString stringTemplate("Received sync time: %1");
    sendMessage(stringTemplate.arg(time));
    emit syncTime(QSharedPointer<TimePassingData>::create(PassingDataType::ManualPulse, time - m_beginTime));
}

quint64 FrameDecoder::parseTimestampPacket(const char *data) const
{
    Q_CHECK_PTR(data);
    TimeSample time = getUnsignedLongLong(data);
    static const QString stringTemplate("Current base timestamp: %1");
    sendMessage(stringTemplate.arg(time));
    return time;
}

void FrameDecoder::parseSensorDataPacket(const char *data, PassingDataType sensorType,
                                         quint16 packetSize, quint64 baseTimestamp)
{
    Q_CHECK_PTR(data);
    Q_ASSERT(!((packetSize - 2) % 8));
    uchar bus = static_cast<uchar>(data[0]);
    uchar device = static_cast<uchar>(data[1]);
    quint8 id = getDeviceId(bus, device);

    RawDataVector rawData;
    TimeDataVector timeData;
    for (uint i = 2; i < packetSize; i += 8)
    {
        quint32 timeOffset = getUnsignedShort(data + i);
        timeOffset *= 10;
        TimeSample timestamp = baseTimestamp - timeOffset;
        qint16 x = getSignedShort(data + i + 2);
        qint16 y = getSignedShort(data + i + 4);
        qint16 z = getSignedShort(data + i + 6);
        static const QString stringTemplate("timestamp: %1, x: %2, y: %3, z: %4");
        sendMessage(stringTemplate.arg(timestamp).arg(x).arg(y).arg(z));
        rawData.append({x, y, z});
        timeData.append(timestamp);
    }

    emit sendData(QSharedPointer<RawPassingData>::create(id, sensorType, timeData, rawData));
}

void FrameDecoder::parseUnsupportedPacket() const
{
    sendMessage(QStringLiteral("Parsing of this packet doesn't support yet!"));
}

quint8 FrameDecoder::getDeviceId(quint8 bus, quint8 device) const
{
    Q_ASSERT(bus > 0);
    return 5 * (bus - 1) + device;
}

void FrameDecoder::setBeginTime(const TimeSample &beginTime)
{
    m_beginTime = beginTime;
}
