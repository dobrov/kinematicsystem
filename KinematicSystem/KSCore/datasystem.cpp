#include "datasystem.h"
#include <QTemporaryFile>
#include <QDataStream>
#include <QDir>
#include <QSqlQuery>
#include <QSqlError>
#include <QMetaEnum>
#include <algorithm>
#include <concretepassingdata.h>
#include <timepassingdata.h>
#include <QDebug>

const QString DataSystem::m_tempFileName(QStringLiteral("tempData.db3"));
const QStringList DataSystem::m_configTables{QStringLiteral("DataRate"), QStringLiteral("FullScale")};

DataSystem::DataSystem(QObject *parent)
    : AbstractWorker(parent), m_savingInterval(30000), m_timerId(0),
      m_db{QSqlDatabase::addDatabase(QStringLiteral("QSQLITE"))}
{
}

void DataSystem::clear()
{
    m_tempData.clear();
    stopTimer();
    clearDb();
    sendMessage(QStringLiteral("DataSystem has been cleared"));
}

void DataSystem::saveToFile(const QString &fileName)
{
    saveToJson(fileName);
}

void DataSystem::handleNewData(const AbstractPassingDataPointer &passingData)
{
    const auto dataType = passingData->getDataType();

    if (dataType == PassingDataType::Kin)
        return;

    if (!m_timerId)
    {
        m_timerId = startTimer(m_savingInterval);
        clearDb();
    }
    auto flowId = passingData->getFlowId();

    auto it = m_tempData.find(flowId);
    if (it == m_tempData.end())
        m_tempData.insert(flowId, passingData->clone());
    else
        it.value()->append(passingData);
}

void DataSystem::startWorkImpl()
{
    findTempFile();
    m_db.setDatabaseName(m_tempFileName);
    m_db.open();
}

void DataSystem::finishWorkImpl()
{
    stopTimer();
    m_db.close();
    QFile::remove(m_tempFileName);
}

int DataSystem::getSavingInterval() const
{
    return m_savingInterval;
}

void DataSystem::setSavingInterval(int savingInterval)
{
    m_savingInterval = savingInterval;
}

void DataSystem::setFullScale(quint16 accelFullScale, quint16 gyroFullScale, quint16 magFullScale)
{
    QVector<QVariant> configData;
    configData.resize(3);
    configData[Accel] = accelFullScale;
    configData[Gyro] = gyroFullScale;
    configData[Mag] = magFullScale;

    setConfigData(QStringLiteral("FullScale"), configData);
}

void DataSystem::setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate)
{
    QVector<QVariant> configData;
    configData.resize(3);
    configData[Accel] = accelDataRate;
    configData[Gyro] = gyroDataRate;
    configData[Mag] = magDataRate;

    setConfigData(QStringLiteral("DataRate"), configData);
}

void DataSystem::timerEvent(QTimerEvent *)
{
    static const QString insertTemplate("INSERT INTO %1 VALUES (?)");
    QSqlQuery query(m_db);
    const auto tables = m_db.tables();
    for (auto it = m_tempData.cbegin(); it != m_tempData.cend(); ++it)
    {
        const auto &flowId = it.key();
        auto tName = getDbName(flowId);
        if (!tables.contains(tName))
            createDataTable(tName);
        const auto serializedData = getSerializedData(it.value());

        query.prepare(insertTemplate.arg(tName));
        query.addBindValue(serializedData);
        bool result = query.exec();
        Q_ASSERT(result);
    }
    m_tempData.clear();
}

void DataSystem::saveToJson(const QString &fileName)
{
    static const QHash<PassingDataType, QString> dataTypeToName = {
        {PassingDataType::RawAccel, QStringLiteral("RawAccel")},
        {PassingDataType::RawGyro, QStringLiteral("RawGyro")},
        {PassingDataType::RawMag, QStringLiteral("RawMag")},
        {PassingDataType::CalAccel, QStringLiteral("CalAccel")},
        {PassingDataType::CalGyro, QStringLiteral("CalGyro")},
        {PassingDataType::CalMag, QStringLiteral("CalMag")},
        {PassingDataType::Quat, QStringLiteral("Quat")},
        {PassingDataType::Pos, QStringLiteral("Position")},
        {PassingDataType::ManualPulse, QStringLiteral("MP")}};

    auto flows = getExistingFlows();

    QJsonObject dataObject;
    for (const auto &flow : flows)
    {
        auto dataType = std::get<FPassingDataType>(flow);
        auto deviceId = std::get<FDeviceId>(flow);

        QJsonValue value;
        switch (dataType)
        {
        case PassingDataType::RawAccel:
        case PassingDataType::RawGyro:
        case PassingDataType::RawMag:
        {
            auto data = getDataFromDb<RawSample>(flow);
            value = dataToJson(data);
        }
            break;
        case PassingDataType::CalAccel:
        case PassingDataType::CalGyro:
        case PassingDataType::CalMag:
        {
            auto data = getDataFromDb<CalSample>(flow);
            value = dataToJson(data);
        }
            break;
        case PassingDataType::Quat:
        {
            auto data = getDataFromDb<QuatSample>(flow);
            value = dataToJson(data);
        }
            break;
        case PassingDataType::Pos:
        {
            auto data = getDataFromDb<PosSample>(flow);
            value = dataToJson(data);
        }
            break;
        case PassingDataType::ManualPulse:
        {
            auto data = getDataFromDb<TimeSample>(flow);
            value = dataToJson(data);
        }
            break;
//        case PassingDataType::Kin:
//            break;
        default:
            Q_ASSERT(false);
        }

        if (deviceId != 0)
        {
            const QString deviceName = QString("Device%1").arg(deviceId);
            auto it = dataObject.find(deviceName);
            if (it == dataObject.end())
                dataObject.insert(deviceName, QJsonObject{{dataTypeToName.value(dataType), value}});
            else
            {
                auto deviceObject = it.value().toObject();
                deviceObject.insert(dataTypeToName.value(dataType), value);
                dataObject[deviceName] = deviceObject;
            }
        }
        else
            dataObject.insert(dataTypeToName.value(dataType), value);
    }

    QVector<QString> fullScaleNames;
    fullScaleNames.resize(3);
    fullScaleNames[Accel] = QStringLiteral("AccelFullScale");
    fullScaleNames[Gyro] = QStringLiteral("GyroFullScale");
    fullScaleNames[Mag] = QStringLiteral("MagFullScale");

    QVector<QString> dataRateNames;
    dataRateNames.resize(3);
    dataRateNames[Accel] = QStringLiteral("AccelDataRate");
    dataRateNames[Gyro] = QStringLiteral("GyroDataRate");
    dataRateNames[Mag] = QStringLiteral("MagDataRate");

    QJsonObject configObject;
    auto writeConfig = [&configObject, this](const decltype(fullScaleNames) &names, const QString &tName)
    {
        static const QString selectTemplate("SELECT * FROM %1");
        QSqlQuery query(m_db);
        bool result = query.exec(selectTemplate.arg(tName));
        Q_ASSERT(result);

        QVector<int> config;
        while (query.next())
            config.push_back(query.value(0).toInt());
        for (int i = 0; i < config.size(); i++)
            configObject.insert(names.at(i), config.at(i));
    };
    writeConfig(fullScaleNames, QStringLiteral("FullScale"));
    writeConfig(dataRateNames, QStringLiteral("DataRate"));

    writeObjectFile(fileName, {{QStringLiteral("Config"), configObject},
                               {QStringLiteral("Data"), dataObject}});
}

void DataSystem::findTempFile()
{
    if (m_db.tables().size() > 2)
        emit tempFileFound();
}

void DataSystem::stopTimer()
{
    killTimer(m_timerId);
    m_timerId = 0;
}

void DataSystem::clearDb()
{
    static const QString dropTemplate("DROP TABLE %1");
    auto tables = m_db.tables();
    for (const auto &tName : m_configTables)
        tables.removeOne(tName);

    QSqlQuery query(m_db);
    for (const auto &tName : tables)
    {
        bool result = query.exec(dropTemplate.arg(tName));
        Q_ASSERT(result);
    }
}

void DataSystem::createDataTable(const QString &tName)
{
    QSqlQuery query(m_db);
    bool result = query.exec(QString("CREATE TABLE %1 (data BLOB)").arg(tName));
    Q_ASSERT(result);
}

void DataSystem::setConfigData(const QString &tName, const QVector<QVariant> &configData)
{
    QSqlQuery query(m_db);
    bool result = query.exec(QString("DROP TABLE IF EXISTS %1").arg(tName));
    Q_ASSERT(result);
    result = query.exec(QString("CREATE TABLE %1 (configData INTEGER)").arg(tName));
    Q_ASSERT(result);
    query.prepare(QString("INSERT INTO %1 VALUES (?)").arg(tName));
    query.addBindValue(configData.toList());
    result = query.execBatch();
    Q_ASSERT(result);
}

QString DataSystem::getDbName(const FlowId &flowId)
{
    static const auto metaEnum = QMetaEnum::fromType<PassingDataType>();
    return QString("%1_%2").arg(metaEnum.key(static_cast<int>(std::get<FPassingDataType>(flowId))))
            .arg(std::get<FDeviceId>(flowId));
}

QByteArray DataSystem::getSerializedData(const AbstractPassingDataPointer &pData)
{
    auto dataType = pData->getDataType();
    auto toByteArray = [](const auto &data)
    {
        QByteArray inByteArray;
        QDataStream ds(&inByteArray, QIODevice::WriteOnly);
        ds << data;
        return inByteArray;
    };

    QByteArray serializedData;

    switch (dataType)
    {
    case PassingDataType::RawAccel:
    case PassingDataType::RawGyro:
    case PassingDataType::RawMag:
    {
        auto tDataVector = getData<RawSample>(pData);
        serializedData = toByteArray(tDataVector);
    }
        break;
    case PassingDataType::CalAccel:
    case PassingDataType::CalGyro:
    case PassingDataType::CalMag:
    {
        auto tDataVector = getData<CalSample>(pData);
        serializedData = toByteArray(tDataVector);
    }
        break;
    case PassingDataType::Quat:
    {
        auto tDataVector = getData<QuatSample>(pData);
        serializedData = toByteArray(tDataVector);
    }
        break;
    case PassingDataType::Pos:
    {
        auto tDataVector = getData<PosSample>(pData);
        serializedData = toByteArray(tDataVector);
    }
        break;
    case PassingDataType::ManualPulse:
    {
        auto tDataVector = getData<TimeSample>(pData);
        serializedData = toByteArray(tDataVector);
    }
        break;
    default:
        Q_ASSERT(false);
    }
    return serializedData;
}

QList<FlowId> DataSystem::getExistingFlows()
{
    static const auto metaEnum = QMetaEnum::fromType<PassingDataType>();
    QSet<FlowId> existingFlow;
    auto tables = m_db.tables();
    for (const auto &tName : m_configTables)
        tables.removeOne(tName);
    for (const auto &tName: tables)
    {
        const auto splittedList = tName.split('_');
        Q_ASSERT(splittedList.size() == 2);
        const auto tempByteArray = splittedList.first().toLatin1();
        auto dataType = static_cast<PassingDataType>(
                    metaEnum.keyToValue(tempByteArray.constData()));
        auto id = static_cast<quint8>(splittedList.last().toUShort());
        existingFlow.insert(std::make_tuple(id, dataType));
    }

    for (const auto &flowId : m_tempData.keys())
        existingFlow.insert(flowId);

    return existingFlow.toList();
}

template <typename T>
DataVector<T> DataSystem::getDataFromDb(const FlowId &flowId)
{
    QSqlQuery query(m_db);
    auto dbName = getDbName(flowId);

    DataVector<T> dataVector;

    if (m_db.tables().contains(dbName))
    {
        static const QString reqDataTemplate("SELECT * FROM %1");
        bool result = query.exec(reqDataTemplate.arg(dbName));
        Q_ASSERT(result);

        while (query.next())
        {
            auto outByteArray = query.value(0).toByteArray();
            QDataStream ds(outByteArray);
            DataVector<T> tDataVector;
            ds >> tDataVector;
            dataVector.append(tDataVector);
        }
    }

    auto it = m_tempData.constFind(flowId);
    if (it != m_tempData.constEnd())
    {
        auto tDataVector = getData<T>(it.value());
        dataVector.append(tDataVector);
    }

    Q_ASSERT(!dataVector.isEmpty());
    return dataVector;
}
