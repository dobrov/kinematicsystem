#include "abstractpassingdata.h"
#include <algorithm>
#include <iterator>

AbstractPassingData::AbstractPassingData()
    : m_deviceId(0), m_dataType(PassingDataType::RawAccel)
{
}

AbstractPassingData::AbstractPassingData(quint8 deviceId, PassingDataType dataType)
    : m_deviceId(deviceId), m_dataType(dataType)
{
}

AbstractPassingData::AbstractPassingData(quint8 deviceId, PassingDataType dataType, const TimeDataVector &time)
    : AbstractPassingData(deviceId, dataType)
{
    m_time = time;
}

AbstractPassingDataPointer AbstractPassingData::clone() const
{
    return AbstractPassingDataPointer(doClone());
}

AbstractPassingData::~AbstractPassingData()
{
}

AbstractPassingDataPointer AbstractPassingData::split(int i)
{
    Q_ASSERT(i > 0 && i <= m_time.size());
    TimeDataVector time;
    time.reserve(i);
    auto it = m_time.begin();
    std::move(it, it + i, std::back_inserter(time));
    m_time.erase(it, it + i);
    return splitTemplateMethod(i, time);
}

void AbstractPassingData::append(const AbstractPassingDataPointer &appendingData)
{
    Q_ASSERT(m_deviceId == appendingData->getDeviceId());
    Q_ASSERT(m_dataType == appendingData->getDataType());
    m_time.append(appendingData->getTime());
    appendTemplateMethod(appendingData);
}

void AbstractPassingData::erase(int n, EraseType eraseType)
{
    Q_ASSERT(n >= 0 && n <= m_time.size());
    if (n == 0)
        return;

    auto it = (eraseType == EraseType::Head) ? std::begin(m_time) : std::end(m_time);
    if (eraseType == EraseType::Head)
        m_time.erase(it, it + n);
    else
        m_time.erase(it - n, it);
    eraseTemplateMethod(n, eraseType);
}

void AbstractPassingData::appendTemplateMethod(const AbstractPassingDataPointer &appendingData)
{
    Q_UNUSED(appendingData);
}

void AbstractPassingData::eraseTemplateMethod(int n, AbstractPassingData::EraseType eraseType)
{
    Q_UNUSED(n);
    Q_UNUSED(eraseType);
}
