QT += core serialport sql

INCLUDEPATH += $$PWD

LIBS += -lKSCore

Debug {
    LIBS += -L$$OUT_PWD/../KSCore/debug/
    msvc: PRE_TARGETDEPS += $$OUT_PWD/../KSCore/debug/KSCore.lib
    gcc: PRE_TARGETDEPS += $$OUT_PWD/../KSCore/debug/libKSCore.a
}
Release {
    LIBS += -L$$OUT_PWD/../KSCore/release/
    msvc: PRE_TARGETDEPS += $$OUT_PWD/../KSCore/release/KSCore.lib
    gcc: PRE_TARGETDEPS += $$OUT_PWD/../KSCore/release/libKSCore.a
}

#win32
#{
#    Debug
#    {
#        LIBS += -L$$OUT_PWD/../KSCore/debug/
#        msvc: PRE_TARGETDEPS += $$OUT_PWD/../KSCore/debug/KSCore.lib
#        gcc: PRE_TARGETDEPS += $$OUT_PWD/../KSCore/debug/libKSCore.a
#    }
#    Release
#    {
#        LIBS += -L$$OUT_PWD/../KSCore/release/
#        msvc: PRE_TARGETDEPS += $$OUT_PWD/../KSCore/release/KSCore.lib
#        gcc: PRE_TARGETDEPS += $$OUT_PWD/../KSCore/release/libKSCore.a
#    }
#}
#unix
#{
#    LIBS += -L$$OUT_PWD/../KSCore/
#    PRE_TARGETDEPS += $$OUT_PWD/../KSCore/libKSCore.a
#}

