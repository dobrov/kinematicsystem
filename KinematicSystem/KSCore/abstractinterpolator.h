#ifndef ABSTRACTINTERPOLATOR_H
#define ABSTRACTINTERPOLATOR_H

#include "concretepassingdata.h"
#include "abstractpassingdatahandler.h"
#include <QMap>

class AbstractInterpolator : public AbstractPassingDataHandler
{
    Q_OBJECT

protected:
    typedef std::tuple<TimeSample, QSharedPointer<RawPassingData>> FlowData;
    enum FlowDataIndex
    {
        CurrentInterpolateTime,
        LastSamples
    };

    typedef QMap<FlowId, FlowData> InterpolatorMap;

public:
    AbstractInterpolator(QObject *parent = 0);

public slots:
    virtual void clear() override final;
    void setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate);

protected:
    InterpolatorMap m_interpolatorMap;
    //in microseconds;
    quint64 m_accelPeriod;
    quint64 m_gyroPeriod;
    quint64 m_magPeriod;
};

#endif // ABSTRACTINTERPOLATOR_H
