#include "packet.h"
#include <QByteArray>

Packet::Packet()
    : m_packetType(0), m_size(0)
{
}

Packet::Packet(quint8 packetType, quint8 packetSize)
    : m_packetType(packetType), m_size(packetSize)
{
}

Packet::~Packet()
{
}

QByteArray Packet::getPacket() const
{
    QByteArray packet;
    packet.reserve(m_size + 3);
    packet[0] = m_packetType;

    const char *serializedSize = reinterpret_cast<const char*>(&m_size);
    packet[1] = serializedSize[0];
    packet[2] = serializedSize[1];
    addPacketData(packet);

    return packet;
}

quint16 Packet::getSize() const
{
    return m_size + 3;
}

void Packet::addPacketData(QByteArray &/*packet*/) const
{
}
