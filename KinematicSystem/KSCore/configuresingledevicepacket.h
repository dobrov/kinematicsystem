#ifndef CONFIGURESINGLEDEVICEPACKET_H
#define CONFIGURESINGLEDEVICEPACKET_H
#include "configurealldevicepacket.h"

class ConfigureSingleDevicePacket : public ConfigureAllDevicePacket
{
public:
    ConfigureSingleDevicePacket();
    ConfigureSingleDevicePacket(quint8 bus, quint8 device, quint8 enable, quint8 fullScaleAcc, quint8 fullScaleGyro,
                                quint8 fullScaleMag, quint8 dataRateAcc, quint8 dataRateGyro, quint8 dataRateMag);

    void setBus(const quint8 &bus);
    void setDevice(const quint8 &device);

private:
    virtual void addPacketData(QByteArray &packet) const override;

    quint8 m_bus;
    quint8 m_device;
};

#endif // CONFIGURESINGLEDEVICEPACKET_H
