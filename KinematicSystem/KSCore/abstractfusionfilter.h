#ifndef ABSTRACTFUSIONFILTER_H
#define ABSTRACTFUSIONFILTER_H

#include "abstractfusionfiltersettings.h"
#include "kstypes.h"
#include <array>

class AbstractFusionFilter
{
public:
    AbstractFusionFilter(float gyroPeriod);
    virtual ~AbstractFusionFilter() = 0;

    void updateIMU(float ax, float ay, float az,
                   float wx, float wy, float wz);

    void updateMARG(float ax, float ay, float az,
                    float wx, float wy, float wz,
                    float mx, float my, float mz);

    void clear();
    QuatSample getOrientation() const;
    void setOrientation(const QuatSample &quat);
    void setGyroPeriod(float gyroPeriod);

    bool isSteadyState(float ax, float ay, float az, float wx, float wy, float wz) const;
    void updateGyroBiases(float ax, float ay, float az, float wx, float wy, float wz);

    void setSettings(const AbstractFilterSettingsPointer &settings);

protected:
    bool m_isInit = false;

    //for integration (obtaining pred quat) in seconds
    float m_gyroPeriod;

    //Current quaternion
    float m_q0, m_q1, m_q2, m_q3;

    bool m_isValidMag = true;
    bool m_isValidAccel = true;

private:
    virtual void setConcreteSettings(const AbstractFilterSettingsPointer &settings) = 0;
    virtual void updateIMUImpl(float ax, float ay, float az,
                               float wx, float wy, float wz) = 0;
    virtual void updateMARGImpl(float ax, float ay, float az,
                                float wx, float wy, float wz,
                                float mx, float my, float mz) = 0;

    AbstractFilterSettingsPointer m_settings;

    uint m_steadyStateCount = 0;
    std::array<float, 3> m_gyroBiases = {{0.0f, 0.0f, 0.0f}};
    std::array<float, 3> m_gyroMeanRT = {{0.0f, 0.0f, 0.0f}};
    mutable std::array<float, 3> m_prevGyro = {{0.0f, 0.0f, 0.0f}};
    mutable float m_prevAccelMagn = 0.0f;
    uint m_minSteadyLenSamples;

    float m_magTrueMagn = 0.0f;
    float m_accelTrueMagn = 1.0f;
    float m_trueDipAngle = 0.0f;
};

// Utility math functions:

void normalizeVector(float& x, float& y, float& z);

void normalizeQuaternion(float& q0, float& q1, float& q2, float& q3);

void scaleQuaternion(float gain, float& dq0, float& dq1, float& dq2, float& dq3);

void invertQuaternion(float q0, float q1, float q2, float q3,
                      float& q0_inv, float& q1_inv, float& q2_inv, float& q3_inv);

void quaternionMultiplication(float p0, float p1, float p2, float p3,
                              float q0, float q1, float q2, float q3,
                              float& r0, float& r1, float& r2, float& r3);

void rotateVectorByQuaternion(float x, float y, float z,
                              float q0, float q1, float q2, float q3,
                              float& vx, float& vy, float& vz);

#endif // ABSTRACTFUSIONFILTER_H
