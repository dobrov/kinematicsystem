#ifndef SENSORFUSION_H
#define SENSORFUSION_H

#include "abstractpassingdatahandler.h"
#include "kstypes.h"
#include "abstractfusionfilter.h"
#include <QMap>

class SensorFusion : public AbstractPassingDataHandler
{
    Q_OBJECT

    struct DeviceData
    {
        DeviceData(TimeSample gyroPeriod = 0, TimeSample beginTime = 0);
        DeviceData(const AbstractFilterSettingsPointer &settings,
                   TimeSample gyroPeriod = 0, TimeSample beginTime = 0);

        QSharedPointer<AbstractFusionFilter> filter;
        bool isMagSample;
        TimeSample currentTime;
        CalDataVector accelData;
        CalDataVector gyroData;
        CalDataVector magData;
    };

public:
    explicit SensorFusion(QObject *parent = 0);

public slots:
    void setDataRate(quint16 /*accelDataRate*/, quint16 gyroDataRate, quint16 /*magDataRate*/);
    void setFilterSettings(const AbstractFilterSettingsPointer &settings);
    virtual void clear() override;
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;

private:
    TimeSample m_gyroPeriod;
    AbstractFilterSettingsPointer m_settings;
    QMap<quint8, DeviceData> m_deviceDataMap;

};

#endif // SENSORFUSION_H
