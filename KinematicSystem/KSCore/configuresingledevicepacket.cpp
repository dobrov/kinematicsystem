#include "configuresingledevicepacket.h"

ConfigureSingleDevicePacket::ConfigureSingleDevicePacket()
    : ConfigureAllDevicePacket(), m_bus(1), m_device(1)
{
    m_packetType = 0x13;
    m_size = 9;
}

ConfigureSingleDevicePacket::ConfigureSingleDevicePacket(quint8 bus, quint8 device, quint8 enable, quint8 fullScaleAcc, quint8 fullScaleGyro,
                                                      quint8 fullScaleMag, quint8 dataRateAcc, quint8 dataRateGyro, quint8 dataRateMag)
    : ConfigureAllDevicePacket(enable, fullScaleAcc, fullScaleGyro, fullScaleMag, dataRateAcc,
                               dataRateGyro, dataRateMag), m_bus(bus), m_device(device)
{
    m_packetType = 0x13;
    m_size = 9;
}

void ConfigureSingleDevicePacket::setDevice(const quint8 &device)
{
    m_device = device;
}

void ConfigureSingleDevicePacket::setBus(const quint8 &bus)
{
    m_bus = bus;
}

void ConfigureSingleDevicePacket::addPacketData(QByteArray &packet) const
{
    packet[3] = m_bus;
    packet[4] = m_device;
    packet[5] = m_enable;
    packet[6] = m_fullScaleAcc;
    packet[7] = m_fullScaleGyro;
    packet[8] = m_fullScaleMag;
    packet[9] = m_dataRateAcc;
    packet[10] = m_dataRateGyro;
    packet[11] = m_dataRateMag;
}
