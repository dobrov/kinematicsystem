#ifndef CONFIGUREALLDEVICEPACKET_H
#define CONFIGUREALLDEVICEPACKET_H
#include "packet.h"

class ConfigureAllDevicePacket : public Packet
{
public:
    ConfigureAllDevicePacket();
    ConfigureAllDevicePacket(quint8 enable, quint8 fullScaleAcc, quint8 fullScaleGyro, quint8 fullScaleMag,
                             quint8 dataRateAcc, quint8 dataRateGyro, quint8 dataRateMag);

    void setEnable(const quint8 &enable);
    void setFullScaleAcc(const quint8 &fullScaleAcc);
    void setFullScaleGyro(const quint8 &fullScaleGyro);
    void setFullScaleMag(const quint8 &fullScaleMag);
    void setDataRateAcc(const quint8 &dataRateAcc);
    void setDataRateGyro(const quint8 &dataRateGyro);
    void setDataRateMag(const quint8 &dataRateMag);

protected:
    quint8 m_enable;
    quint8 m_fullScaleAcc;
    quint8 m_fullScaleGyro;
    quint8 m_fullScaleMag;
    quint8 m_dataRateAcc;
    quint8 m_dataRateGyro;
    quint8 m_dataRateMag;

private:
    virtual void addPacketData(QByteArray &packet) const override;
};

#endif // CONFIGUREALLDEVICEPACKET_H
