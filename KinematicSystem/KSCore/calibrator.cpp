#include "calibrator.h"
#include <QDir>
#include <concretepassingdata.h>

Calibrator::Calibrator(QObject *parent)
    : AbstractPassingDataHandler(parent)
{
}

void Calibrator::handleNewData(const AbstractPassingDataPointer &passingData)
{
    auto dataType = passingData->getDataType();
    Q_ASSERT(dataType != PassingDataType::CalAccel ||
             dataType != PassingDataType::CalGyro ||
             dataType != PassingDataType::CalMag);

    auto sensorIndex = (dataType == PassingDataType::RawAccel) ? 0 :
                       (dataType == PassingDataType::RawGyro) ? 1 : 2;
    auto curSignMap = m_signMap.at(sensorIndex);
    auto curAxisMap = m_axisMap.at(sensorIndex);

    CalDataVector calData;
    const auto &data = qSharedPointerCast<RawPassingData>(passingData)->getData();

    for (int i = 0; i < data.size(); i++)
    {
        const auto &rawSample = data.at(i);
        auto x = curSignMap.at(X) * rawSample.at(curAxisMap.at(X)) *
                m_sensitivity.at(sensorIndex);
        auto y = curSignMap.at(Y) * rawSample.at(curAxisMap.at(Y)) *
                m_sensitivity.at(sensorIndex);
        auto z = curSignMap.at(Z) * rawSample.at(curAxisMap.at(Z)) *
                m_sensitivity.at(sensorIndex);

        if (!m_calDataMap.isEmpty())
        {
            const auto &calId = std::make_tuple(passingData->getDeviceId(), dataType,
                                                m_fullScale.at(sensorIndex));
            auto it = m_calDataMap.constFind(calId);
            if (it != m_calDataMap.cend())
            {
                const auto &calData = it.value();
                x -= calData.m_bias.at(X);
                y -= calData.m_bias.at(Y);
                z -= calData.m_bias.at(Z);
                auto x_c = x * calData.m_scale.at(X).at(X) + y * calData.m_scale.at(X).at(Y) +
                        z * calData.m_scale.at(X).at(Z);
                auto y_c = x * calData.m_scale.at(Y).at(X) + y * calData.m_scale.at(Y).at(Y) +
                        z * calData.m_scale.at(Y).at(Z);
                auto z_c = x * calData.m_scale.at(Z).at(X) + y * calData.m_scale.at(Z).at(Y) +
                        z * calData.m_scale.at(Z).at(Z);

                x = x_c;
                y = y_c;
                z = z_c;
            }
        }

        calData.push_back({x, y, z});
    }

    auto newDataType = (dataType == PassingDataType::RawAccel) ? PassingDataType::CalAccel :
        (dataType == PassingDataType::RawGyro) ? PassingDataType::CalGyro : PassingDataType::CalMag;

    sendData(QSharedPointer<CalPassingData>::create(passingData->getDeviceId(), newDataType,
                                                    passingData->getTime(), calData));
}

void Calibrator::setSensitivity(float accelSens, float gyroSens, float magSens)
{
    m_sensitivity[0] = accelSens;
    m_sensitivity[1] = gyroSens;
    m_sensitivity[2] = magSens;
}

void Calibrator::setFullScale(quint16 accelFullScale, quint16 gyroFullScale, quint16 magFullScale)
{
    m_fullScale[0] = accelFullScale;
    m_fullScale[1] = gyroFullScale;
    m_fullScale[2] = magFullScale;
}

void Calibrator::setAxisMap(const SensOrientArray &axisMap)
{
    m_axisMap = axisMap;
}

void Calibrator::setCalDataMap(const CalDataMap &calDataMap)
{
    m_calDataMap = calDataMap;
}

void Calibrator::setSignMap(const SensOrientArray &signMap)
{
    m_signMap = signMap;
}
