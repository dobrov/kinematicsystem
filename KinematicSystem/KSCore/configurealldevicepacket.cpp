#include "configurealldevicepacket.h"

ConfigureAllDevicePacket::ConfigureAllDevicePacket()
    : Packet(0x12, 7), m_enable(0), m_fullScaleAcc(0), m_fullScaleGyro(0),
      m_fullScaleMag(0), m_dataRateAcc(0), m_dataRateGyro(0), m_dataRateMag(0)
{
}

ConfigureAllDevicePacket::ConfigureAllDevicePacket(quint8 enable, quint8 fullScaleAcc, quint8 fullScaleGyro,
                                                   quint8 fullScaleMag, quint8 dataRateAcc, quint8 dataRateGyro, quint8 dataRateMag)
    : Packet(0x12, 7), m_enable(enable), m_fullScaleAcc(fullScaleAcc), m_fullScaleGyro(fullScaleGyro),
      m_fullScaleMag(fullScaleMag), m_dataRateAcc(dataRateAcc), m_dataRateGyro(dataRateGyro), m_dataRateMag(dataRateMag)
{
}

void ConfigureAllDevicePacket::setEnable(const quint8 &enable)
{
    m_enable = enable;
}

void ConfigureAllDevicePacket::setFullScaleAcc(const quint8 &fullScaleAcc)
{
    m_fullScaleAcc = fullScaleAcc;
}

void ConfigureAllDevicePacket::setFullScaleGyro(const quint8 &fullScaleGyro)
{
    m_fullScaleGyro = fullScaleGyro;
}

void ConfigureAllDevicePacket::setFullScaleMag(const quint8 &fullScaleMag)
{
    m_fullScaleMag = fullScaleMag;
}

void ConfigureAllDevicePacket::setDataRateAcc(const quint8 &dataRateAcc)
{
    m_dataRateAcc = dataRateAcc;
}

void ConfigureAllDevicePacket::setDataRateGyro(const quint8 &dataRateGyro)
{
    m_dataRateGyro = dataRateGyro;
}

void ConfigureAllDevicePacket::setDataRateMag(const quint8 &dataRateMag)
{
    m_dataRateMag = dataRateMag;
}

void ConfigureAllDevicePacket::addPacketData(QByteArray &packet) const
{
    packet[3] = m_enable;
    packet[4] = m_fullScaleAcc;
    packet[5] = m_fullScaleGyro;
    packet[6] = m_fullScaleMag;
    packet[7] = m_dataRateAcc;
    packet[8] = m_dataRateGyro;
    packet[9] = m_dataRateMag;
}
