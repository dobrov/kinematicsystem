#ifndef COMPORTWORKER_H
#define COMPORTWORKER_H
#include "abstractworker.h"
#include "frame.h"
#include <QString>
#include <QByteArray>
#include <QtSerialPort>
#include <QStringList>

class QTimer;

struct Settings
{
    Settings(const QSerialPortInfo &portInfo, const quint32 baudRate, const QSerialPort::DataBits dataBits, const QSerialPort::Parity parity,
             const QSerialPort::StopBits stopBits, const QSerialPort::FlowControl flowControl)
        : portInfo(portInfo), baudRate(baudRate), dataBits(dataBits), parity(parity),
          stopBits(stopBits), flowControl(flowControl) {}
    Settings() {}
    QSerialPortInfo portInfo;
    qint32 baudRate;
    QSerialPort::DataBits dataBits;
    QSerialPort::Parity parity;
    QSerialPort::StopBits stopBits;
    QSerialPort::FlowControl flowControl;
};

class ComPortWorker : public AbstractWorker
{
    Q_OBJECT

public:
    explicit ComPortWorker(QObject *parent = nullptr);
    explicit ComPortWorker(const Settings &settings, QObject *parent = 0);

    bool openPort();
    void tryOpenPort();
    void closePort();

    void write(const Frame &writeData, bool isPeriodic = false);

    int readInterval() const;
    void setReadInterval(int readInterval);

    void setSettings(const Settings &settings);
    Settings settings() const;

    int detectInterval() const;
    void setDetectInterval(int detectInterval);

    bool isAutoConnect() const;
    void setIsAutoConnect(bool isAutoConnect);

    bool messageError() const;
    void setMessageError(bool messageError);

    bool messageRawData() const;
    void setMessageRawData(bool messageRawData);

    void handleBytesWritten(qint64 bytes);
    void handleReadyRead();

    void handleStopPeriodic();

signals:
    void readDataAvailable(const QByteArray &data);
    void opened(const QString &port);
    void closed(const QString &port);

private slots:
    void handleError(QSerialPort::SerialPortError error);

private:
    void setupPort();
    void writeImpl();
    bool detectDevice();

    virtual void startWorkImpl();
    virtual void finishWorkImpl();

    QSerialPort *m_port;

    bool m_isAutoConnect;
    bool m_messageError;
    bool m_messageRawData;

    int m_readInterval;
    QTimer *m_readTimer;
    bool m_isPeriodic;

    int m_detectInterval;
    QTimer *m_detectTimer;

    QByteArray m_writeData;
    qint64 m_bytesWritten;

    Settings m_settings;

    static const QStringList possibleDevice;
    static const uint nFailedRead = 5;
};

#endif // COMPORTWORKER_H
