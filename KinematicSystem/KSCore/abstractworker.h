#ifndef ABSTRACTWORKER_H
#define ABSTRACTWORKER_H

#include <QObject>

class QString;

class AbstractWorker : public QObject
{
    Q_OBJECT
public:
    AbstractWorker(QObject *parent = 0);
    virtual ~AbstractWorker() = 0;

signals:
    void finished();
    void message(const QString &msg) const;

public slots:
    void startWork();
    void finishWork();

protected:
    //decorator to signal message
    void sendMessage(const QString &msg) const;

private:
    virtual void startWorkImpl();
    virtual void finishWorkImpl();
};

#endif // ABSTRACTWORKER_H
