#include "flowqueue.h"
#include <QTime>
#include <QMetaEnum>

FlowQueue::FlowQueue(QObject *parent)
    : AbstractPassingDataHandler(parent), m_refreshInterval(10), m_timerId(0),
      m_baseTime(0), m_delay(70000)
{
}

void FlowQueue::handleNewData(const AbstractPassingDataPointer &passingData)
{
    const auto flowId = getFlowId(passingData);

    if (!m_passingFlow.contains(flowId))
        return;

    if (!m_timerId)
    {
        m_timerId = startTimer(m_refreshInterval, Qt::PreciseTimer);
        Q_ASSERT(!m_baseTime);
        m_baseTime = passingData->getTime().first();
        sendMessage(QString("Start from %1").arg(m_baseTime));
        m_elapsedTimer.start();
    }

    auto it = m_flowData.find(flowId);
    if (it == m_flowData.end())
        m_flowData.insert(flowId, passingData);
    else
        it.value()->append(passingData);
}

void FlowQueue::clear()
{
    killTimer(m_timerId);
    m_timerId = 0;
    m_baseTime = 0;
    m_flowData.clear();
    sendMessage(QStringLiteral("FlowQueue has been cleared"));
}

void FlowQueue::setDelay(TimeSample delay)
{
    m_delay = delay;
}

void FlowQueue::setFlow(quint8 id, PassingDataType type)
{
    setFlow(std::make_tuple(id, type));
}

void FlowQueue::setFlow(const FlowId &flowId)
{
    Q_ASSERT(!m_passingFlow.contains(flowId));
    static const auto metaEnum = QMetaEnum::fromType<PassingDataType>();
    sendMessage(QString("New flow with id: %1, type %2").arg(std::get<FDeviceId>(flowId))
                .arg(metaEnum.key(static_cast<int>(std::get<FPassingDataType>(flowId)))));
    m_passingFlow.insert(flowId);
}

void FlowQueue::setFlowList(const FlowIdVector &flowIdList)
{
    m_passingFlow.clear();
    for (const auto flowId : flowIdList)
        setFlow(flowId);
    //temporary
    setFlow(0, PassingDataType::Kin);
    setFlow(0, PassingDataType::Pos);
}

void FlowQueue::setRefreshInterval(int refreshInterval)
{
    m_refreshInterval = refreshInterval;
}

void FlowQueue::timerEvent(QTimerEvent *)
{
    Q_ASSERT(m_elapsedTimer.isValid());

    auto currentTime = m_baseTime + static_cast<TimeSample>(m_elapsedTimer.nsecsElapsed() / 1000);
    if (currentTime < m_delay)
        return;
    else
        currentTime -= m_delay;

    for (auto &flowData : m_flowData)
    {
        const auto &flowTime = flowData->getTime();
        int i;
        for (i = 0; i < flowTime.size(); i++)
        {
            if (flowTime.at(i) > currentTime)
                break;
        }
        if (i)
            emit sendData(flowData->split(i));
    }
}
