#ifndef KINEMATICSCALCULATOR_H
#define KINEMATICSCALCULATOR_H

#include <abstractpassingdatahandler.h>
#include <kstypes.h>
#include <QQuaternion>

class KinematicsCalculator : public AbstractPassingDataHandler
{
    Q_OBJECT
    typedef BodyPartMap<QVector<QuatDataVector>> SensorData;
    typedef QHash<quint8, std::tuple<BodyPart, uint>> DeviceToBoneMap;
    enum DeviceToBoneMapIndex
    {
        BodyPartValue,
        BoneValue
    };

public:
    explicit KinematicsCalculator(QObject *parent = nullptr);

    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
    virtual void clear() override;

public slots:
    void setOffsetData(const OffsetData &offsetData);
    void setGlobalFrame(const QQuaternion &quatEG);
    void setHipLength(const QVector3D &hipLength);
    void setBodyOffset(const BodyParam &bodyOffset);
    void setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate);
    void setDeviceList(const BodyDevList &bodyDevList);

private:
    bool checkIsReadyESData();

    QQuaternion m_quatEG;
    QVector3D m_hipLength;
    BodyParam m_bodyParam;
    TimeSample m_gyroPeriod;
    SensorData m_quatSEData;
    TimeSample m_currentTime;
    DeviceToBoneMap m_deviceToBoneMap;
};

#endif // KINEMATICSCALCULATOR_H
