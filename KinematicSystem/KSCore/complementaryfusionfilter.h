#ifndef COMPLEMENTARYFILTER_H
#define COMPLEMENTARYFILTER_H

#include "abstractfusionfilter.h"

class ComplementaryFusionFilter : public AbstractFusionFilter
{
public:
    ComplementaryFusionFilter(float gyroPeriod = 0.01);

private:
    virtual void setConcreteSettings(const AbstractFilterSettingsPointer &settings) override;
    virtual void updateIMUImpl(float ax, float ay, float az,
                               float wx, float wy, float wz) override;
    virtual void updateMARGImpl(float ax, float ay, float az,
                                float wx, float wy, float wz,
                                float mx, float my, float mz) override;

    void getPrediction(float wx, float wy, float wz,
                       float &q0_pred, float &q1_pred, float &q2_pred, float &q3_pred) const;

    void getMeasurement(float ax, float ay, float az,
                        float& q0_meas, float& q1_meas, float& q2_meas, float& q3_meas);

    void getMeasurement(float ax, float ay, float az,
                        float mx, float my, float mz,
                        float& q0_meas, float& q1_meas, float& q2_meas, float& q3_meas);

    void getAccCorrection(float ax, float ay, float az,
                          float& dq0, float& dq1, float& dq2, float& dq3);

    void getMagCorrection(float mx, float my, float mz,
                          float& dq0, float& dq1, float& dq2, float& dq3);


    float m_magGain;
    float m_accelGain;

    static const float m_singularityThreshold;
};

#endif // COMPLEMENTARYFILTER_H
