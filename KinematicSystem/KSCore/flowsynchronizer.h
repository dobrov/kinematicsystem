#ifndef FLOWSYNCHRONIZER_H
#define FLOWSYNCHRONIZER_H

#include <abstractpassingdatahandler.h>
#include <QMap>

template <typename T>
inline T gcd(T a, T b)
{
    while (b != 0) {
        T r = a % b;
        a = b;
        b = r;
    }
    return a;
}

template <typename T>
inline T lcm(T a, T b)
{
    return (a * b) / gcd<T>(a, b);
}

class FlowSynchronizer : public AbstractPassingDataHandler
{
    Q_OBJECT

public:
    enum class Condition
    {
        Idle,
        Ready,
        Launch,
        Record,
        End,
    };
    Q_ENUM(Condition)

    explicit FlowSynchronizer(QObject *parent = nullptr);

public slots:
    void setSynchronizingFlows(const FlowIdVector &flowVector);
    void setDataRate(float accelDataRate, float gyroDataRate, float magDataRate);
    void handleStartRecording();
    void handleStopRecording();
    void forceStopRecording();
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
    virtual void clear() override;

signals:
    void stopRecording();
    void beginTime(TimeSample time);

private:
    void findPhase(const AbstractPassingDataPointer &passingData);
    void cutPhase(const AbstractPassingDataPointer &passingData);
    void initFlowMap();
    bool checkIsFullAllFlow() const;
    void checkIsCutPhase();
    void dropSamples();

    void setNewState(Condition &&newState);

    QMap<FlowId, AbstractPassingDataPointer> m_flowMap;
    FlowIdVector m_synchronizingFlows;
    TimeSample m_cutTime;
    //least common multiplier - lcm
    TimeSample m_lcmPeriod;
    Condition m_condition;
    bool m_isFindCutTime;
};

#endif // FLOWSYNCHRONIZER_H
