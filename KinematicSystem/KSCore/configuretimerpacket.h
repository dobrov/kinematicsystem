#ifndef CONFIGURETIMERPACKET_H
#define CONFIGURETIMERPACKET_H
#include "packet.h"

class ConfigureTimerPacket : public Packet
{
public:    
    ConfigureTimerPacket();
    ConfigureTimerPacket(quint8 enable, quint8 reset);

    void setEnable(const quint8 &enable);
    void setReset(const quint8 &reset);

private:
    virtual void addPacketData(QByteArray &packet) const override;

    quint8 m_enable;
    quint8 m_reset;
};

#endif // CONFIGURETIMERPACKET_H
