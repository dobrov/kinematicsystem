#ifndef KSTYPES
#define KSTYPES

#include <QtGlobal>
#include <tuple>
#include <array>
#include <type_traits>
#include <typeinfo>
#include <QVector>
#include <QMap>
#include <QHash>
#include <QSet>
#include <QStringList>
#include <QDataStream>
#include <QQuaternion>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonDocument>
#include <QFile>
#include <qmath.h>

class QVector3D;

class KSTypes
{
    Q_GADGET
public:
    KSTypes() = delete;

    enum class PassingDataType
    {
        RawAccel,
        RawGyro,
        RawMag,
        CalAccel,
        CalGyro,
        CalMag,
        Quat,
        Kin,
        Pos,
        ManualPulse,
        DipAngle
    };
    Q_ENUM(PassingDataType)

    enum class BodyPart
    {
        //LHL - left hindlimb
        LHL,
        //RHL - right hindlimb
        RHL
    };
    Q_ENUM(BodyPart)
};
typedef KSTypes::PassingDataType PassingDataType;
typedef KSTypes::BodyPart BodyPart;

template <typename T>
uint qHash(T key, std::enable_if_t<std::is_enum<T>::value>* = nullptr)
{
    return qHash(static_cast<uint>(key));
}
template <typename T>
using BodyPartMap = QHash<BodyPart, T>;

typedef QVector<quint8> DeviceList;
typedef std::tuple<float, QQuaternion> BoneParam;
enum BoneParamIndex
{
    BLength,
    BQuat
};
typedef QVector<BoneParam> BodyPartParam;
typedef BodyPartMap<BodyPartParam> BodyParam;
typedef BodyPartMap<DeviceList> BodyDevList;

struct OffsetData
{
    OffsetData();
    BodyDevList m_deviceList;
    BodyParam m_bodyInit;
    BodyParam m_bodyOffset;
    QQuaternion m_quatEG;
    QVector3D m_hipLength;
    bool operator==(const OffsetData &l) const;
};

QStringList findOffsetFiles(const QString &dirName);
OffsetData readOffsetFile(const QString &fileName);
void writeOffsetFile(const QString &fileName, const OffsetData &offsetData);

typedef std::tuple<quint8, PassingDataType, quint16> CalId;
enum CalIdIndex
{
    CId,
    CType,
    CFullScale
};
inline uint qHash(CalId calId)
{
    qint64 id = static_cast<qint64>(std::get<0>(calId)) +
            (static_cast<qint64>(std::get<1>(calId)) << 8) +
            (static_cast<qint64>(std::get<2>(calId)) << 32);
    return qHash(id);
}

struct CalData
{
    CalData();
    std::array<float, 3> m_bias;
    std::array<std::array<float, 3>, 3> m_scale;
    bool operator==(const CalData &l) const;
};
typedef QMap<CalId, QString> CalIdFileNamesMap;
typedef QMap<CalId, CalData> CalDataMap;
CalIdFileNamesMap findCalFiles(const QString &dirName);

CalData readCalFile(const QString &fileName);
void writeCalFile(const QString &fileName, const CalData &calData);
QString getCalFileName(const CalId &calId);

typedef std::tuple<quint8, PassingDataType> FlowId;
enum FlowIdIndex
{
    FDeviceId,
    FPassingDataType
};
Q_DECLARE_METATYPE(FlowId)
inline uint qHash(FlowId flowId)
{
    qint64 id = (static_cast<qint64>(std::get<FPassingDataType>(flowId)) << 8) + std::get<FDeviceId>(flowId);
    return qHash(id);
}

template <typename T>
using DataVector = QList<T>;

typedef float DipSample;
typedef DataVector<DipSample> DipDataVector;
Q_DECLARE_METATYPE(DipSample)

typedef quint64 TimeSample;
typedef DataVector<TimeSample> TimeDataVector;
Q_DECLARE_METATYPE(TimeSample)

typedef std::array<qint16, 3> RawSample;
typedef DataVector<RawSample> RawDataVector;
enum RawSampleIndex
{
    RValueX,
    RValueY,
    RValueZ
};
Q_DECLARE_METATYPE(RawSample)

typedef std::array<float, 3> CalSample;
typedef DataVector<CalSample> CalDataVector;
enum CalSampleIndex
{
    CValueX,
    CValueY,
    CValueZ
};
Q_DECLARE_METATYPE(CalSample)

typedef std::array<float, 4> QuatSample;
typedef DataVector<QuatSample> QuatDataVector;
enum QuatSampleIndex
{
    QValueS,
    QValueX,
    QValueY,
    QValueZ
};
Q_DECLARE_METATYPE(QuatSample)

typedef std::tuple<QVector3D, QQuaternion> SegmentData;
typedef BodyPartMap<QVector<SegmentData>> KinSample;
typedef DataVector<KinSample> KinDataVector;
enum KinematicsSampleIndex
{
    KSegTranslation,
    KSegRotation
};
Q_DECLARE_METATYPE(KinSample)

typedef BodyPartMap<QVector<QVector3D>> PosSample;
typedef DataVector<PosSample> PosDataVector;
Q_DECLARE_METATYPE(PosSample)

typedef std::array<std::array<int, 3>, 3> SensOrientArray;

constexpr float rad2deg()
{
    return static_cast<float>(180.0 / M_PI);
}

constexpr float deg2rad()
{
    return static_cast<float>(M_PI / 180.0);
}

typedef QList<FlowId> FlowIdVector;

QDataStream& operator <<(QDataStream& ds, const BodyPart &bodyPart);
QDataStream& operator >>(QDataStream& ds, BodyPart &bodyPart);

QDataStream& operator <<(QDataStream& ds, const FlowId &flowId);
QDataStream& operator >>(QDataStream& ds, FlowId &flowId);

QDataStream& operator <<(QDataStream& ds, const SegmentData &segData);
QDataStream& operator >>(QDataStream& ds, SegmentData &segData);

template <typename T>
QDataStream& operator <<(QDataStream& ds, const T &sample)
{
    for (const auto &chunk : sample)
    {
        ds << chunk;
    }
    return ds;
}

template <typename T>
QDataStream& operator >>(QDataStream& ds, T &sample)
{
    for (auto &chunk : sample)
    {
        ds >> chunk;
    };
    return ds;
}

QJsonObject getObjectFile(const QString &fileName);
void writeObjectFile(const QString &fileName, const QJsonObject &object);

template<typename>
struct is_std_array_3 : std::false_type {};

template<typename T>
struct is_std_array_3<std::array<T, 3>> : std::true_type {};

template <typename T>
QJsonValue dataToJson(const DataVector<T> &dataVector,
                      std::enable_if_t<is_std_array_3<T>::value>* = nullptr)
{
    static const std::array<QString, std::tuple_size<T>::value> fieldNames{
        QStringLiteral("x"), QStringLiteral("y"), QStringLiteral("z")};
    std::array<QJsonArray, std::tuple_size<T>::value> arrayVector;
    for (int i = 0; i < dataVector.size(); i++)
    {
        std::get<0>(arrayVector).append(std::get<0>(dataVector.at(i)));
        std::get<1>(arrayVector).append(std::get<1>(dataVector.at(i)));
        std::get<2>(arrayVector).append(std::get<2>(dataVector.at(i)));
    }

    QJsonObject object;
    for (typename std::tuple_size<T>::value_type i = 0; i < std::tuple_size<T>::value; i++)
        object.insert(fieldNames.at(i), arrayVector.at(i));

    return object;
}

QJsonValue dataToJson(const QuatDataVector &dataVector);
QJsonValue dataToJson(const PosDataVector &dataVector);
QJsonValue dataToJson(const TimeDataVector &dataVector);

#endif // KSTYPES
