#include "dipanglecalculator.h"
#include "concretepassingdata.h"
#include <cmath>

DipAngleCalculator::DeviceData::DeviceData(TimeSample beginTime)
    : isMagSample(true), currentTime(beginTime)
{
}

DipAngleCalculator::DipAngleCalculator(QObject *parent)
    : AbstractPassingDataHandler(parent)
{

}

void DipAngleCalculator::setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate)
{
    Q_UNUSED(accelDataRate);
    Q_UNUSED(gyroDataRate);

    m_magPeriod = 1000000 / magDataRate;
}

void DipAngleCalculator::handleNewData(const AbstractPassingDataPointer &passingData)
{
    auto id = passingData->getDeviceId();
    auto type = passingData->getDataType();
    if (type == PassingDataType::CalGyro)
        return;

    Q_ASSERT(type == PassingDataType::CalAccel || type == PassingDataType::CalMag);

    DeviceData &deviceData = m_deviceDataMap.contains(id) ? m_deviceDataMap[id] :
                             m_deviceDataMap.insert(id, DeviceData(passingData->getTime().first())).value();

    auto newData = getData<CalSample>(passingData);

    if (type == PassingDataType::CalAccel)
        deviceData.accelData.append(newData);
    else
        deviceData.magData.append(newData);

    DipDataVector outData;
    TimeDataVector outTime;
    while (!deviceData.accelData.isEmpty() && !deviceData.magData.isEmpty())
    {
        if (deviceData.isMagSample)
        {
            const auto accelSample = deviceData.accelData.takeFirst();
            auto ax = std::get<CValueX>(accelSample);
            auto ay = std::get<CValueY>(accelSample);
            auto az = std::get<CValueZ>(accelSample);
            const auto magSample = deviceData.magData.takeFirst();
            auto mx = std::get<CValueX>(magSample);
            auto my = std::get<CValueY>(magSample);
            auto mz = std::get<CValueZ>(magSample);

            QVector3D accel(ax, ay, az);
            QVector3D mag(mx, my, mz);
            accel.normalize();
            mag.normalize();

            auto dotProduct = QVector3D::dotProduct(accel, mag);
            outData.append(dotProduct);
            outTime.append(deviceData.currentTime);

            deviceData.currentTime += m_magPeriod;
        }
        else
            deviceData.accelData.removeFirst();

        deviceData.isMagSample = !deviceData.isMagSample;

    }
    if (!outData.isEmpty())
        emit sendData(QSharedPointer<DipPassingData>::create(id, PassingDataType::DipAngle, outTime, outData));
}

void DipAngleCalculator::clear()
{
    m_deviceDataMap.clear();
}
