#ifndef ABSTRACTPASSINGDATAHANDLER_H
#define ABSTRACTPASSINGDATAHANDLER_H

#include "kstypes.h"
#include "abstractworker.h"
#include "abstractpassingdata.h"
#include <QSharedPointer>

class AbstractPassingDataHandler : public AbstractWorker
{
    Q_OBJECT

public:
    AbstractPassingDataHandler(QObject *parent = nullptr)
        : AbstractWorker(parent)
    {}

signals:
    void sendData(const AbstractPassingDataPointer &sendingData);

public slots:
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) = 0;
    virtual void clear()
    {}

protected:
    inline FlowId getFlowId(const AbstractPassingDataPointer &passingData)
    {
        return std::make_tuple(passingData->getDeviceId(), passingData->getDataType());
    }
};
#endif // ABSTRACTPASSINGDATAHANDLER_H
