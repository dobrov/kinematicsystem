#include "logger.h"

Logger::Logger(QObject *parent)
    : AbstractWorker(parent), m_timer(new QElapsedTimer()), m_textStream()
{
    m_file = new QFile(QStringLiteral("log.txt"), this);
    m_file->open(QIODevice::WriteOnly);
    m_textStream.setDevice(m_file);
    m_timer->start();
}

Logger::Logger(const QString &fileName, QObject *parent)
    : AbstractWorker(parent), m_timer(new QElapsedTimer()), m_textStream()
{
    m_file = new QFile(fileName, this);
    m_file->open(QIODevice::WriteOnly);
    m_textStream.setDevice(m_file);
    m_timer->start();
}

Logger::Logger(FILE *handle, QObject *parent)
    : AbstractWorker(parent), m_timer(new QElapsedTimer()), m_textStream()
{
    m_file = new QFile(this);
    m_file->open(handle, QIODevice::WriteOnly);
    m_textStream.setDevice(m_file);
    m_timer->start();
}

Logger::~Logger()
{
    m_file->close();
}

void Logger::handleMessage(const QString &message)
{
    double time = m_timer->elapsed() / 1000.0;
    m_textStream << time << "s: " << message << endl;
}
