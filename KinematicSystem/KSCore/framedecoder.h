#ifndef FRAMEDECODER_H
#define FRAMEDECODER_H

#include "abstractworker.h"
#include "abstractpassingdata.h"
#include "kstypes.h"
#include <QSet>

class QString;


class FrameDecoder : public AbstractWorker
{
    Q_OBJECT
public:

    enum class ReceivingPacketType
    {
        Ack,
        NAck,
        SystemVersion,
        ConnectedDevicesList,
        TimerConfiguration,
        SyncConfiguration,
        SingleDeviceConfig,
        TimestampData,
        SyncData,
        AccelData,
        GyroData,
        MagData,
        UnknownPacketData
    };
    Q_ENUM(ReceivingPacketType)

    explicit FrameDecoder(QObject *parent = 0);

signals:
    void ackReceived();
    void connectedDeviceData(const DeviceList &data);
    void syncTime(const AbstractPassingDataPointer &sendingData);
    void sendData(const AbstractPassingDataPointer &sendingData);

public slots:
    void parseFrame(const QByteArray &data);
    void setBeginTime(const TimeSample &beginTime);

private:
    ReceivingPacketType getPacketType(uchar packetCode) const;
    bool checkSignature(const char *data);
    bool isInFrame(uint position, uint dataSize) const;
    quint16 getUnsignedShort(const char* data) const;
    qint16 getSignedShort(const char *data) const;
    quint64 getUnsignedLongLong(const char* data) const;

    void parseConnectedDevicePacket(const char *data, quint16 packetSize);
    void parseSyncPacket(const char *data);
    quint64 parseTimestampPacket(const char *data) const;
    void parseSensorDataPacket(const char *data, PassingDataType sensorType,
                               quint16 packetSize, quint64 baseTimestamp);
    //Temporary foo
    void parseUnsupportedPacket() const;

    quint8 getDeviceId(quint8 bus, quint8 device) const;

    QByteArray m_data;
    TimeSample m_beginTime;
};

#endif // FRAMEDECODER_H
