#include "frame.h"

Frame::Frame()
    : m_frameSize(0), m_numberOfPackets(0)
{
    addFrameHead();
}

void Frame::addPacket(const Packet &packet)
{
    m_numberOfPackets++;
    m_frame += packet.getPacket();
    m_frameSize += packet.getSize();
    m_frame.replace(2, 2, reinterpret_cast<const char*>(&m_frameSize), 2);
}

const QByteArray &Frame::getFrame() const
{
    return m_frame;
}

int Frame::getNumberOfPackets() const
{
    return m_numberOfPackets;
}

void Frame::clear()
{
    m_frame.clear();
    m_frameSize = 0;
    m_numberOfPackets = 0;
    addFrameHead();
}

void Frame::addFrameHead()
{
    QByteArray frameHeader;
    frameHeader.resize(4);
    frameHeader[0] = 0x53;
    frameHeader[1] = 0x4B;
    frameHeader[2] = 0x00;
    frameHeader[3] = 0x00;
    m_frame += frameHeader;
}
