#include "timepassingdata.h"

TimePassingData::TimePassingData(PassingDataType dataType, TimeSample time)
    : AbstractPassingData(0, dataType, {time})
{
}

TimePassingData::TimePassingData(PassingDataType dataType, const TimeDataVector &time)
    : AbstractPassingData(0, dataType, time)
{
}

TimePassingData *TimePassingData::doClone() const
{
    return new TimePassingData(*this);
}

AbstractPassingDataPointer TimePassingData::splitTemplateMethod(int i, const TimeDataVector &time)
{
    Q_UNUSED(i);
    return QSharedPointer<TimePassingData>::create(getDataType(), time);
}
