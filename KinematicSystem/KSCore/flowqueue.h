#ifndef PLAYER_H
#define PLAYER_H
#include "abstractpassingdatahandler.h"
#include "kstypes.h"
#include <QQueue>
#include <QElapsedTimer>
#include <QMap>
#include <QSet>

class FlowQueue : public AbstractPassingDataHandler
{
    Q_OBJECT

    typedef QMap<FlowId, QSharedPointer<AbstractPassingData>> FlowMap;
    typedef QSet<FlowId> PassingFlow;

public:
    explicit FlowQueue(QObject *parent = 0);

public slots:
    void setRefreshInterval(int refreshInterval);
    void setDelay(TimeSample delay);
    void setFlow(quint8 id, PassingDataType type);
    void setFlow(const FlowId &flowId);
    void setFlowList(const FlowIdVector &flowIdList);
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;
    virtual void clear() override;

protected:
    virtual void timerEvent(QTimerEvent *) override;

private:
    FlowMap m_flowData;
    PassingFlow m_passingFlow;
    QElapsedTimer m_elapsedTimer;
    // in ms
    int m_refreshInterval;
    int m_timerId;
    TimeSample m_baseTime;
    TimeSample m_delay;
};

#endif // PLAYER_H
