#include "kstypes.h"
#include <QDir>
#include <QJsonArray>
#include <QRegularExpression>
#include <QMetaEnum>

OffsetData::OffsetData()
{}

bool OffsetData::operator==(const OffsetData &l) const
{
    if (m_deviceList == l.m_deviceList && m_bodyOffset == l.m_bodyOffset &&
        m_quatEG == l.m_quatEG && m_bodyInit == l.m_bodyInit && m_hipLength == l.m_hipLength)
        return true;
    return false;
}

CalData::CalData()
    : m_bias{0.0, 0.0, 0.0}, m_scale{{{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}}}
{}

bool CalData::operator==(const CalData &l) const
{
    if (m_bias == l.m_bias && m_scale == l.m_scale)
        return true;
    return false;
}

QStringList findOffsetFiles(const QString &dirName)
{
    QDir dir(dirName);
    auto foundFiles = dir.entryList({QStringLiteral("*.offset")}, QDir::Files);
    for (auto &offsetFile : foundFiles)
        offsetFile = dir.absoluteFilePath(offsetFile);
    return foundFiles;
}

OffsetData readOffsetFile(const QString &fileName)
{
    static const auto fromJsonToQuat = [](const QJsonValue &value)
    {
        auto quatArray = value.toArray();
        Q_ASSERT(quatArray.size() == 4);
        auto scalar = static_cast<float>(quatArray.at(0).toDouble());
        auto x = static_cast<float>(quatArray.at(1).toDouble());
        auto y = static_cast<float>(quatArray.at(2).toDouble());
        auto z = static_cast<float>(quatArray.at(3).toDouble());
        return QQuaternion(scalar, x, y, z);
    };
    static const auto fromJsonToVec3D = [](const QJsonValue &value)
    {
        auto vecArray = value.toArray();
        Q_ASSERT(vecArray.size() == 3);
        auto x = static_cast<float>(vecArray.at(0).toDouble());
        auto y = static_cast<float>(vecArray.at(1).toDouble());
        auto z = static_cast<float>(vecArray.at(2).toDouble());
        return QVector3D(x, y, z);
    };

    const auto object = getObjectFile(fileName);

    OffsetData offsetData;

    const auto quatEG = object.value(QStringLiteral("QuatEG"));
    if (quatEG.type() == QJsonValue::Undefined)
        return OffsetData();
    offsetData.m_quatEG = fromJsonToQuat(quatEG);

    const auto hipLength = object.value(QStringLiteral("HipLength"));
    if (hipLength.type() == QJsonValue::Undefined)
        offsetData.m_hipLength = QVector3D();
    else
        offsetData.m_hipLength = fromJsonToVec3D(hipLength);

    static const auto metaEnum = QMetaEnum::fromType<BodyPart>();

    for (int i = 0; i < metaEnum.keyCount(); i++)
    {
        auto it = object.find(metaEnum.key(i));
        if (it != object.constEnd())
        {
            const auto bodyPartObject = it.value().toObject();
            const auto quatBSArray = bodyPartObject.value(QStringLiteral("QuatBS")).toArray();
            const auto initBGArray = bodyPartObject.value(QStringLiteral("InitBG")).toArray();
            const auto lengthArray = bodyPartObject.value(QStringLiteral("SegmentLength")).toArray();
            const auto devicesArray = bodyPartObject.value(QStringLiteral("DeviceList")).toArray();
            if (quatBSArray.isEmpty() || initBGArray.isEmpty() || lengthArray.isEmpty()
                || devicesArray.isEmpty())
                return OffsetData();

            Q_ASSERT(quatBSArray.size() == devicesArray.size());
            Q_ASSERT(initBGArray.size() == devicesArray.size());
            Q_ASSERT(lengthArray.size() == devicesArray.size());

            const auto bodyPart = static_cast<BodyPart>(metaEnum.value(i));
            for (int j = 0; j < quatBSArray.size(); j++)
            {
                auto quatBS = fromJsonToQuat(quatBSArray.at(j));
                auto initBG = fromJsonToQuat(initBGArray.at(j));
                auto length = static_cast<float>(lengthArray.at(j).toDouble());
                Q_ASSERT(!qFuzzyIsNull(length));

                auto boneOffset = std::make_tuple(length, quatBS);
                offsetData.m_bodyOffset[bodyPart].append(boneOffset);

                auto boneInit = std::make_tuple(length, initBG);
                offsetData.m_bodyInit[bodyPart].append(boneInit);
            }

            for (int i = 0; i < devicesArray.size(); ++i)
            {
                quint8 deviceId = static_cast<quint8>(devicesArray.at(i).toInt());
                Q_ASSERT(deviceId != 0);
                offsetData.m_deviceList[bodyPart].append(deviceId);
            }
        }
    }

    return offsetData;
}

void writeOffsetFile(const QString &fileName, const OffsetData &offsetData)
{
    Q_ASSERT(offsetData.m_bodyOffset.keys() == offsetData.m_bodyInit.keys());
    Q_ASSERT(offsetData.m_bodyOffset.keys() == offsetData.m_deviceList.keys());

    static const auto fromQuatToJson = [](const QQuaternion &quat)
    {
        QJsonArray quatArray;
        quatArray.append(quat.scalar());
        quatArray.append(quat.x());
        quatArray.append(quat.y());
        quatArray.append(quat.z());
        return quatArray;
    };
    static const auto fromVec3DtoJson = [](const QVector3D &vec)
    {
        QJsonArray vecArray;
        vecArray.append(vec.x());
        vecArray.append(vec.y());
        vecArray.append(vec.z());
        return vecArray;
    };

    static const auto metaEnum = QMetaEnum::fromType<BodyPart>();

    QJsonObject object;
    object.insert(QStringLiteral("QuatEG"), fromQuatToJson(offsetData.m_quatEG));
    if (offsetData.m_hipLength != QVector3D())
        object.insert(QStringLiteral("HipLength"), fromVec3DtoJson(offsetData.m_hipLength));

    const auto bodyPartsList = offsetData.m_bodyInit.keys();
    for (const auto bodyPart : bodyPartsList)
    {
        QJsonObject bodyPartObject;

        QJsonArray lengthArray, quatBSArray;
        const auto &bodyPartOffset = offsetData.m_bodyOffset.value(bodyPart);
        for (const auto &boneOffset : bodyPartOffset)
        {
            lengthArray.append(std::get<BLength>(boneOffset));
            quatBSArray.append(fromQuatToJson(std::get<BQuat>(boneOffset)));
        }
        bodyPartObject.insert(QStringLiteral("SegmentLength"), lengthArray);
        bodyPartObject.insert(QStringLiteral("QuatBS"), quatBSArray);

        QJsonArray devicesArray;
        const auto &bodyDeviceList = offsetData.m_deviceList.value(bodyPart);
        for (const auto &deviceId : bodyDeviceList)
            devicesArray.append(deviceId);
        bodyPartObject.insert(QStringLiteral("DeviceList"), devicesArray);

        QJsonArray initBGArray;
        const auto &bodyPartInit = offsetData.m_bodyInit.value(bodyPart);
        for (const auto &boneInit : bodyPartInit)
            initBGArray.append(fromQuatToJson(std::get<BQuat>(boneInit)));
        bodyPartObject.insert(QStringLiteral("InitBG"), initBGArray);

        object.insert(metaEnum.key(static_cast<int>(bodyPart)), bodyPartObject);
    }

    writeObjectFile(fileName, object);
}

CalData readCalFile(const QString &fileName)
{
    auto object = getObjectFile(fileName);

    CalData calData;

    auto biasArray = object.value(QStringLiteral("Bias")).toArray();
    if (biasArray.size() != 3)
        return CalData();

    for (int i = 0; i < biasArray.size(); ++i)
    {
        calData.m_bias[i] = static_cast<float>(biasArray.at(i).toDouble());
    }

    auto scaleArray = object.value(QStringLiteral("Scale")).toArray();
    if (scaleArray.size() != 3)
        return CalData();

    for (int i = 0; i < scaleArray.size(); ++i)
    {
        auto array = scaleArray.at(i).toArray();
        if(array.size() != 3)
            return CalData();
        for (int j = 0; j < array.size(); ++j)
            calData.m_scale[i][j] = static_cast<float>(array.at(j).toDouble());
    }

    return calData;
}

void writeCalFile(const QString &fileName, const CalData &calData)
{
    QJsonObject object;

    QJsonArray biasArray;
    for (const auto &biasValue : calData.m_bias)
    {
        biasArray.append(biasValue);
    }
    object.insert(QStringLiteral("Bias"), biasArray);

    QJsonArray scaleArray;
    for (const auto &row : calData.m_scale)
    {
        QJsonArray colArray;
        for (const auto &column : row)
        {
            colArray.append(column);
        }
        scaleArray.append(colArray);
    }
    object.insert(QStringLiteral("Scale"), scaleArray);

    writeObjectFile(fileName, object);
}


QString getCalFileName(const CalId &calId)
{
    QString calType;
    switch(std::get<CType>(calId))
    {
    case PassingDataType::RawAccel:
        calType = QStringLiteral("accel");
        break;
    case PassingDataType::RawGyro:
        calType = QStringLiteral("gyro");
        break;
    case PassingDataType::RawMag:
        calType = QStringLiteral("mag");
        break;
    default:
        Q_ASSERT(false);
    }

    QString id = QString::number(std::get<CId>(calId));
    QString range = QString::number(std::get<CFullScale>(calId));
    return QString("%1CalDataDevice%2Range%3.json").arg(calType).arg(id).arg(range);
}

QJsonValue dataToJson(const QuatDataVector &dataVector)
{
    static const std::array<QString, std::tuple_size<QuatSample>::value> fieldNames{
        QStringLiteral("w"), QStringLiteral("x"), QStringLiteral("y"), QStringLiteral("z")};
    std::array<QJsonArray, std::tuple_size<QuatSample>::value> arrayVector;
    for (int i = 0; i < dataVector.size(); i++)
    {
        std::get<0>(arrayVector).append(std::get<0>(dataVector.at(i)));
        std::get<1>(arrayVector).append(std::get<1>(dataVector.at(i)));
        std::get<2>(arrayVector).append(std::get<2>(dataVector.at(i)));
        std::get<3>(arrayVector).append(std::get<3>(dataVector.at(i)));
    }

    QJsonObject object;
    for (std::tuple_size<QuatSample>::value_type i = 0; i < std::tuple_size<QuatSample>::value; i++)
        object.insert(fieldNames.at(i), arrayVector.at(i));

    return object;
}

QJsonValue dataToJson(const PosDataVector &dataVector)
{
    Q_ASSERT(!dataVector.empty());

    BodyPartMap<QVector<std::array<QJsonArray, 3>>> jointArraysMap;
    for (auto it = dataVector.first().cbegin(); it != dataVector.first().cend(); ++it)
    {
        const auto nJoints = it.value().size();
        QVector<std::array<QJsonArray, 3>> jointArrays;
        jointArrays.resize(nJoints);
        jointArraysMap.insert(it.key(), jointArrays);
    }

    for (const auto &sample : dataVector)
    {
        for (auto it = sample.cbegin(); it != sample.cend(); ++it)
        {
            auto jointArrays = jointArraysMap.find(it.key());
            Q_ASSERT(jointArrays != jointArraysMap.end());
            const auto &jointPosVector = it.value();
            const auto nJoints = jointPosVector.size();
            for (int i = 0; i < nJoints; i++)
            {
                const auto &pos = jointPosVector.at(i);
                auto &joint = (*jointArrays)[i];
                std::get<0>(joint).append(pos.x() / 100.0);
                std::get<1>(joint).append(pos.y() / 100.0);
                std::get<2>(joint).append(pos.z() / 100.0);
            }
        }
    }

    static const std::array<QString, 3> fieldNames{
        QStringLiteral("x"), QStringLiteral("y"), QStringLiteral("z")};
    static const auto metaEnum = QMetaEnum::fromType<BodyPart>();

    QJsonObject object;
    for (auto it = jointArraysMap.cbegin(); it != jointArraysMap.cend(); ++it)
    {
        QJsonObject bodyPartObject;
        const auto &jointArrays = it.value();
        const auto nJoints = jointArrays.size();
        for (int i = 0; i < nJoints; i++)
        {
            QJsonObject jointObject;
            const auto &joint = jointArrays.at(i);
            jointObject.insert(std::get<0>(fieldNames), std::get<0>(joint));
            jointObject.insert(std::get<1>(fieldNames), std::get<1>(joint));
            jointObject.insert(std::get<2>(fieldNames), std::get<2>(joint));

            bodyPartObject.insert(QString::number(i + 1), jointObject);
        }
        object.insert(metaEnum.key(static_cast<int>(it.key())), bodyPartObject);
    }

    return object;
}


QJsonValue dataToJson(const TimeDataVector &dataVector)
{
    QJsonArray timeArray;
    for (const auto timeSample : dataVector)
    {
        auto seconds = timeSample / 1000000.0;
        timeArray.append(seconds);
    }
    return timeArray;
}

QDataStream &operator <<(QDataStream &ds, const BodyPart &bodyPart)
{
    ds << static_cast<uint>(bodyPart);
    return ds;
}

QDataStream &operator >>(QDataStream &ds, BodyPart &bodyPart)
{
    uint iType;
    ds >> iType;
    bodyPart = static_cast<BodyPart>(iType);
    return ds;
}

QDataStream &operator <<(QDataStream &ds, const FlowId &flowId)
{
    auto id = std::get<FDeviceId>(flowId);
    auto type = std::get<FPassingDataType>(flowId);
    ds << id;
    ds << static_cast<uint>(type);
    return ds;
}

QDataStream &operator >>(QDataStream &ds, FlowId &flowId)
{
    quint8 id;
    ds >> id;
    uint iType;
    ds >> iType;
    auto type = static_cast<PassingDataType>(iType);
    flowId = std::make_tuple(id, type);
    return ds;
}


QDataStream &operator <<(QDataStream &ds, const SegmentData &segData)
{
    ds << std::get<KSegTranslation>(segData);
    ds << std::get<KSegRotation>(segData);
    return ds;
}

QDataStream &operator >>(QDataStream &ds, SegmentData &segData)
{
    ds >> std::get<KSegTranslation>(segData);
    ds >> std::get<KSegRotation>(segData);
    return ds;
}

QJsonObject getObjectFile(const QString &fileName)
{
    QFile file(fileName);
    file.open(QIODevice::ReadOnly);
    QJsonDocument document(QJsonDocument::fromJson(file.readAll()));
    return document.object();
}

void writeObjectFile(const QString &fileName, const QJsonObject &object)
{
    QJsonDocument document(object);
    QFile file(fileName);
    file.open(QIODevice::WriteOnly);
    file.write(document.toJson());
}

CalIdFileNamesMap findCalFiles(const QString &dirName)
{
    QDir dir(dirName);
    const auto fileNameList = dir.entryList(QDir::Files);
    QRegularExpression regExp(QStringLiteral("^(?<type>\\w+)CalDataDevice(?<id>\\d+)Range"
                                             "(?<range>[\\d+]+).json$"));

    CalIdFileNamesMap calFilesMap;
    for (const auto &fileName : fileNameList)
    {
        const auto regExpMatch = regExp.match(fileName);
        if (regExpMatch.hasMatch())
        {
            auto type = regExpMatch.captured(QStringLiteral("type"));
            auto rawType = PassingDataType::RawAccel;
            if (type == QStringLiteral("accel"))
                rawType = PassingDataType::RawAccel;
            else if (type == QStringLiteral("mag"))
                rawType = PassingDataType::RawMag;
            else if (type == QStringLiteral("gyro"))
                rawType = PassingDataType::RawGyro;
            else
                Q_ASSERT(false);

            auto id = static_cast<quint8>(regExpMatch.captured(QStringLiteral("id")).toUShort());
            auto fullScale = regExpMatch.captured(QStringLiteral("range")).toUShort();
            auto calId = std::make_tuple(id, rawType, fullScale);

            calFilesMap.insert(calId, dir.absoluteFilePath(fileName));
        }
    }
    return calFilesMap;
}
