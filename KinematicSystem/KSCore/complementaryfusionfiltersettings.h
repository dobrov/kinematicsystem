#ifndef COMPLEMENTARYFUSIONFILTERSETTINGS_H
#define COMPLEMENTARYFUSIONFILTERSETTINGS_H
#include <abstractfusionfiltersettings.h>

class ComplementaryFusionFilterSettings : public AbstractFusionFilterSettings
{
public:
    ComplementaryFusionFilterSettings();

    float magGain() const;
    void setMagGain(float magGain);

    float accelGain() const;
    void setAccelGain(float accelGain);

private:
    float m_magGain = 0.02f;
    float m_accelGain = 0.02f;
};

#endif // COMPLEMENTARYFUSIONFILTERSETTINGS_H
