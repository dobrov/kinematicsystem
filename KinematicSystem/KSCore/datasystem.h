#ifndef DATASYSTEM_H
#define DATASYSTEM_H

#include <abstractworker.h>
#include <abstractpassingdata.h>
#include <QTime>
#include <QSqlDatabase>

class DataSystem : public AbstractWorker
{
    Q_OBJECT
public:
    typedef QHash<FlowId, AbstractPassingDataPointer> TempData;
    enum ConfigVectorIndex
    {
        Accel,
        Gyro,
        Mag
    };

public:
    explicit DataSystem(QObject *parent = 0);

    int getSavingInterval() const;
    void setSavingInterval(int savingInterval);

public slots:
    void clear();
    void stopTimer();
    void saveToFile(const QString &fileName);
    void handleNewData(const AbstractPassingDataPointer &passingData);
    void setFullScale(quint16 accelFullScale, quint16 gyroFullScale, quint16 magFullScale);
    void setDataRate(quint16 accelDataRate, quint16 gyroDataRate, quint16 magDataRate);

protected:
    virtual void timerEvent(QTimerEvent *) override;

signals:
    void tempFileFound();

private:
    void saveToJson(const QString &fileName);
    void findTempFile();
    void clearDb();
    void createDataTable(const QString &tName);
    void setConfigData(const QString &tName, const QVector<QVariant> &configData);
    QString getDbName(const FlowId &flowId);
    template <typename T> DataVector<T> getDataFromDb(const FlowId &flowId);
    QByteArray getSerializedData(const AbstractPassingDataPointer &pData);
    QList<FlowId> getExistingFlows();

    virtual void startWorkImpl() override;
    virtual void finishWorkImpl() override;

    //ms
    int m_savingInterval;
    int m_timerId;

    QSqlDatabase m_db;

    TempData m_tempData;

    static const QString m_tempFileName;
    static const QStringList m_configTables;
};

#endif // DATASYSTEM_H
