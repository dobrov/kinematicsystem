#ifndef CALIBRATOR_H
#define CALIBRATOR_H

#include "abstractpassingdatahandler.h"
#include <QMap>
#include <QStringList>

class Calibrator : public AbstractPassingDataHandler
{
    Q_OBJECT

public:
    explicit Calibrator(QObject *parent = 0);

public slots:
    virtual void handleNewData(const AbstractPassingDataPointer &passingData) override;

    void setSensitivity(float accelSens, float gyroSens, float magSens);
    void setFullScale(quint16 accelFullScale, quint16 gyroFullScale, quint16 magFullScale);
    void setSignMap(const SensOrientArray &signMap);
    void setAxisMap(const SensOrientArray &axisMap);
    void setCalDataMap(const CalDataMap &calDataMap);

private:
    CalDataMap m_calDataMap;
    std::array<float, 3> m_sensitivity;
    std::array<quint16, 3> m_fullScale;

    SensOrientArray m_signMap;
    SensOrientArray m_axisMap;
    enum Axis
    {
        X,
        Y,
        Z
    };
};
#endif // CALIBRATOR_H
