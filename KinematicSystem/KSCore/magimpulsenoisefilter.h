#ifndef MAGIMPULSENOISEFILTER_H
#define MAGIMPULSENOISEFILTER_H

#include <abstractpassingdatahandler.h>
#include <concretepassingdata.h>
#include <QMap>

class MagImpulseNoiseFilter : public AbstractPassingDataHandler
{
    Q_OBJECT

    struct MagImpulseFilterData
    {
        MagImpulseFilterData();

        bool isInitialized;
        quint32 prevMagn;
        qint32 prevDiff;
        RawDataVector prevData;
        TimeDataVector prevTime;
    };

    typedef QMap<quint8, MagImpulseFilterData> MagImpulseFilterMap;

public:
    explicit MagImpulseNoiseFilter(QObject *parent = 0);

public slots:
    virtual void handleNewData(const QSharedPointer<AbstractPassingData> &passingData);
    virtual void clear();

private:
    MagImpulseFilterMap m_flowMap;
    static const int m_initPhaseSamples = 3;
    static const int m_threshold = 500000;
};

#endif // MAGIMPULSENOISEFILTER_H
