#include "magimpulsenoisefilter.h"
#include <QVarLengthArray>
#include <algorithm>

MagImpulseNoiseFilter::MagImpulseNoiseFilter(QObject *parent)
    : AbstractPassingDataHandler(parent)
{
}

void MagImpulseNoiseFilter::handleNewData(const QSharedPointer<AbstractPassingData> &passingData)
{
    auto dataType = passingData->getDataType();
    if (dataType != PassingDataType::RawMag)
    {
        emit sendData(passingData);
        return;
    }

    auto deviceId = passingData->getDeviceId();
    auto &flowData = m_flowMap[deviceId];

    auto data = qSharedPointerCast<RawPassingData>(passingData)->getData();
    auto time = passingData->getTime();
    if (!flowData.isInitialized)
    {
        flowData.prevData.append(data);
        flowData.prevTime.append(time);
        if (flowData.prevData.size() < m_initPhaseSamples)
            return;

        //Find median and set it as last magnitude
        QVarLengthArray<quint32, m_initPhaseSamples> magnitudes;
        for (int i = 0; i < m_initPhaseSamples; i++)
        {
            const auto &rawSample = flowData.prevData.at(i);
            quint32 magnitude = std::get<RValueX>(rawSample) * std::get<RValueX>(rawSample) +
                                std::get<RValueY>(rawSample) * std::get<RValueY>(rawSample) +
                                std::get<RValueZ>(rawSample) * std::get<RValueZ>(rawSample);
            magnitudes.push_back(magnitude);
        }
        std::sort(magnitudes.begin(), magnitudes.end());
        auto mid = magnitudes.size() / 2;
        flowData.isInitialized = true;
        flowData.prevMagn = magnitudes.at(mid);
        data = flowData.prevData;
        time = flowData.prevTime;
        flowData.prevData = RawDataVector{data.first()};
        flowData.prevTime = TimeDataVector{time.first()};
    }

    auto prevSample = flowData.prevData.first();
    auto prevTime = flowData.prevTime.first();

    RawDataVector newData;
    TimeDataVector newTime;

    for (int i = 0; i < data.size(); i++)
    {
        const auto &currentSample = data.at(i);
        const auto &currentTime = time.at(i);

        quint32 currentMagnitude = std::get<RValueX>(currentSample) * std::get<RValueX>(currentSample) +
                                   std::get<RValueY>(currentSample) * std::get<RValueY>(currentSample) +
                                   std::get<RValueZ>(currentSample) * std::get<RValueZ>(currentSample);
        qint32 currentDiff = currentMagnitude - flowData.prevMagn;

        if (std::abs(currentDiff) < m_threshold)
        {
            // Prev diff is not equal 0 only if one is bigger than threshold
            if (flowData.prevDiff != 0)
            {
                newData.push_back(prevSample);
                newTime.push_back(prevTime);
                flowData.prevDiff = 0;
            }
            newData.push_back(currentSample);
            newTime.push_back(currentTime);
        }
        else
        {
            if (flowData.prevDiff != 0)
            {
                // true if diffes have same sign
                if ((flowData.prevDiff ^ currentDiff) >= 0)
                {
                    newData.push_back(prevSample);
                    newTime.push_back(prevTime);
                }
                else
                    sendMessage(QString("Dropped sample (device: %1, mag) : time %2, x %3, y %4, z %5")
                                .arg(deviceId).arg(prevTime).arg(std::get<RValueX>(prevSample))
                                .arg(std::get<RValueY>(prevSample)).arg(std::get<RValueZ>(prevSample)));
            }

            flowData.prevDiff = currentDiff;
        }

        prevSample = currentSample;
        prevTime = currentTime;
        flowData.prevMagn = currentMagnitude;
    }
    if (!newData.isEmpty())
        emit sendData(QSharedPointer<RawPassingData>::create(deviceId, PassingDataType::RawMag,
                                                             newTime, newData));

    flowData.prevData.first() = prevSample;
    flowData.prevTime.first() = prevTime;
}

void MagImpulseNoiseFilter::clear()
{
    m_flowMap.clear();
    sendMessage(QStringLiteral("Filter has been cleared"));
}

MagImpulseNoiseFilter::MagImpulseFilterData::MagImpulseFilterData()
    : isInitialized(false), prevMagn(0), prevDiff(0) {}
