#ifndef LOGGER_H
#define LOGGER_H

#include "abstractworker.h"
#include <QFile>
#include <QElapsedTimer>
#include <QTextStream>
#include <QScopedPointer>

class Logger : public AbstractWorker
{
    Q_OBJECT
public:
    explicit Logger(QObject *parent = 0);
    explicit Logger(const QString &fileName, QObject *parent = 0);
    explicit Logger(FILE *handle, QObject *parent = 0);
    virtual ~Logger();

public slots:
    void handleMessage(const QString &message);

private:
    QFile *m_file;
    QScopedPointer<QElapsedTimer> m_timer;
    QTextStream m_textStream;
};

#endif // LOGGER_H
