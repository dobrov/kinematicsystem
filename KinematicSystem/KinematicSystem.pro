TEMPLATE = subdirs
CONFIG += ordered
SUBDIRS += KSCore KSCoreTest \
    KSOpenGL \
    KSGui \
    KSOpenGLTest

#KSCoreTest.subdir = KSCoreTest
#KSCore.subdir = KSCore
#KSGui.subdir = KSGui
#KSOpenGL.subdir = KSOpenGL
#KSOpenGLTest.subdir = KSOpenGLTest

#KSCoreTest.depends = KSCore
#KSGui.depends = KSCore KSOpenGL
#KSOpenGLTest.depends = KSOpenGL

