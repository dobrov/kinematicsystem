#ifndef CAMERAROTATIONHELPER_H
#define CAMERAROTATIONHELPER_H
#include "gltypes.h"
#include "cuboidconstraint.h"
#include <QQuaternion>

class OrthoViewHelper
{
public:
    OrthoViewHelper();
    explicit OrthoViewHelper(View view);

    QQuaternion getRotation() const;
    CuboidConstraint getConstraint(CuboidConstraint initConstraint) const;

    void setReverseAxis(Axis axis, AxisDirection dir);

private:
    void recalculate() const;
    bool isReverseApplicate() const;
    void setReverseAbscissa(AxisDirection dir);
    void setReverseOrdinate(AxisDirection dir);

    View m_view;
    AxisDirection m_abscissaDirection;
    AxisDirection m_ordinateDirection;

    mutable QQuaternion m_rotation;
    mutable bool m_isDirty;
};

#endif // CAMERAROTATIONHELPER_H
