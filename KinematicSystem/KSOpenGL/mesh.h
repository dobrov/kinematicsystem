#ifndef SHAPE_H
#define SHAPE_H

#include <gltypes.h>
#include <utility>
#include <QByteArray>
#include <QSet>
#include "meshdata.h"
#include "shaderprogram.h"

class QOpenGLVertexArrayObject;
typedef QSharedPointer<QOpenGLVertexArrayObject> VaoPtr;

class Mesh
{
public:
    explicit Mesh(const MeshDataPtr &meshData,
                  const ShaderProgramPtr &p);

    void addUniform(QByteArray name, QVariant value);
    void render();

    template <typename T>
    void setUniform(const QByteArray &name, T&& var)
    {
        m_program->setUniform(name, std::forward<T>(var));
    }

private:
    void applyUniforms(const UniformsMap &uniforms);

    MeshDataPtr m_meshData;
    ShaderProgramPtr m_program;
    UniformsMap m_ownUniforms;
    VaoPtr m_vao;

    static const QMap<VertexDataType, int> DataTypeTupleSizeMap;
};

#endif // SHAPE_H
