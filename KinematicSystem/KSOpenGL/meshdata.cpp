#include "meshdata.h"
#include "shaderprogram.h"
#include <random>

QVector3D getColor()
{
    static const QVector<QVector3D> possibleColors{{1.0f, 0.0f, 0.0f}, {0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f},
                                                   {1.0f, 1.0f, 0.0f}, {0.0f, 1.0f, 1.0f}, {1.0f, 0.0f, 1.0f},
                                                   {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 1.0f}, {0.5f, 0.5f, 0.5f}};
    static std::mt19937 engine;
    static std::uniform_int_distribution<> gen(0, possibleColors.size() - 1);
    return possibleColors.at(gen(engine));
}

MeshData::MeshData(const PosVector &pos, const IndicesVector &indices, GLenum primitiveType)
    : m_eboMap(QOpenGLBuffer::IndexBuffer), m_primitiveType(primitiveType), m_size(indices.size())
{
    m_eboMap.create();
    m_eboMap.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_eboMap.bind();
    m_eboMap.allocate(indices.constData(), sizeof(IndexValue) * indices.size());
    m_eboMap.release();
    addBufferObject(VertexDataType::Position, pos);
}

MeshData::~MeshData()
{
    m_eboMap.destroy();
    for (auto &vbo : m_vboMap)
    {
        vbo.destroy();
    }
}

template <typename T>
void MeshData::addBufferObject(VertexDataType type, const QVector<T> data)
{
    QOpenGLBuffer vbo(QOpenGLBuffer::VertexBuffer);
    vbo.create();
    vbo.setUsagePattern(QOpenGLBuffer::StaticDraw);
    vbo.bind();
    vbo.allocate(data.constData(), sizeof(T) * data.size());
    vbo.release();
    m_vboMap.insert(type, vbo);
}

MeshTempData::MeshTempData(GLenum type)
    : MeshTempData(type, PosVector(), IndicesVector(), ColorVector())
{
}

MeshTempData::MeshTempData(GLenum type, const PosVector &pos,
                           const IndicesVector &indices)
    : MeshTempData(type, pos, indices, ColorVector())
{
}

MeshTempData::MeshTempData(GLenum type, const PosVector &pos,
                           const IndicesVector &indices, const ColorVector &color)
    : m_position(pos), m_indices(indices), m_color(color), m_primitiveType(type)
{
}

void MeshTempData::applyTransform(const QMatrix4x4 &transform)
{
    for (auto &position : m_position)
        position = transform * position;
}

MeshTempData MeshTempData::operator+(const MeshTempData &other) const
{
    auto copy = *this;
    copy += other;
    return copy;
}

MeshTempData &MeshTempData::operator+=(const MeshTempData &other)
{
    Q_ASSERT(other.m_primitiveType == this->m_primitiveType);

    auto nPosition = m_position.size();
    for (const auto index : other.m_indices)
        m_indices.append(index + nPosition);

    m_position += other.m_position;
    m_color += other.m_color;
    return *this;
}

void MeshTempData::setRandomColor()
{
    m_color.resize(m_indices.size());
    for (auto &color : m_color)
        color = getColor();
}

void MeshTempData::setColor(const QVector3D &color)
{
    m_color.resize(m_position.size());
    for (auto &clr : m_color)
        clr = color;
}

MeshDataPtr MeshTempData::getMeshData() const
{
    Q_ASSERT(m_position.size() == m_color.size());

    auto dataPtr = MeshDataPtr::create(m_position, m_indices, m_primitiveType);
    dataPtr->addBufferObject(VertexDataType::Color, m_color);
    if (m_primitiveType == GL_TRIANGLES)
    {
        NormalVector normals;
        generateNormals(normals);
        dataPtr->addBufferObject(VertexDataType::Normal, normals);
    }

    return dataPtr;
}

void MeshTempData::generateNormals(NormalVector &normals) const
{
    normals.resize(m_position.size());

    for (int i = 0; i < m_indices.size(); i+=3)
    {
        auto firstEdge = m_position.at(m_indices.at(i + 1)) - m_position.at(m_indices.at(i));
        auto secondEdge = m_position.at(m_indices.at(i + 2)) - m_position.at(m_indices.at(i));
        auto normal = QVector3D::crossProduct(firstEdge, secondEdge);
        normals[m_indices.at(i)] += normal;
        normals[m_indices.at(i + 1)] += normal;
        normals[m_indices.at(i + 2)] += normal;
    }

    for (auto &normal : normals)
        normal.normalize();
}

void MeshTempData::setPrimitiveType(const GLenum &primitiveType)
{
    m_primitiveType = primitiveType;
}
