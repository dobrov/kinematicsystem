#ifndef CUBOIDCONSTRAINT_H
#define CUBOIDCONSTRAINT_H

#include "gltypes.h"
#include <QMatrix4x4>

class CuboidConstraint
{
public:
    friend class ThirdAngleBuilder;

    CuboidConstraint();
    CuboidConstraint(qreal xMin, qreal xMax, qreal yMin, qreal yMax, qreal zMin, qreal zMax);

    CuboidConstraint createViewConstraint(View view) const;
    void setReverseAxis(Axis axis);

    QMatrix4x4 getProjMatrix() const;
    int getWidth() const;
    int getHeight() const;

private:
    qreal m_xMin, m_xMax, m_yMin, m_yMax, m_zMin, m_zMax;

    friend QDataStream& operator <<(QDataStream& ds, const CuboidConstraint &constraint);
    friend QDataStream& operator >>(QDataStream& ds, CuboidConstraint &constraint);
    friend CuboidConstraint createTriAngleConstraint(const CuboidConstraint &main, const CuboidConstraint &top,
                                                     const CuboidConstraint &right, qreal &topTrans, qreal &rightTrans);
};

QDataStream& operator <<(QDataStream& ds, const CuboidConstraint &constraint);
QDataStream& operator >>(QDataStream& ds, CuboidConstraint &constraint);

CuboidConstraint createTriAngleConstraint(const CuboidConstraint &main, const CuboidConstraint &top,
                                          const CuboidConstraint &right, qreal &topTrans, qreal &rightTrans);

#endif // CUBOIDCONSTRAINT_H
