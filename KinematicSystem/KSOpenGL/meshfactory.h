#ifndef MESHFACTORY_H
#define MESHFACTORY_H

#include "gltypes.h"
#include "meshdata.h"

class Mesh;

class MeshFactory
{
public:
    MeshFactory();
    MeshFactory(const ShaderProgramPtr &program);
    void setProgram(const ShaderProgramPtr &program);
    void setColor(const QVector3D &color);
    void setRandomColor(bool isRandomColor);

    QSharedPointer<Mesh> createCylinder(qreal base, qreal top, qreal height,
                                        uint slices = 10, uint stacks = 2) const;
    QSharedPointer<Mesh> createArrow(qreal smallRadius, qreal largeRadius, qreal headRatio,
                                     qreal height, uint slices = 10, uint stacks = 2) const;
    QSharedPointer<Mesh> createCornerXYZ(qreal scale = 1.0) const;
    QSharedPointer<Mesh> createIcoSphere(qreal radius, uint refinement = 2) const;
    QSharedPointer<Mesh> createBox(qreal width, qreal height,
                                   qreal depth, uint refinement = 0) const;
    QSharedPointer<Mesh> createBone(qreal length, qreal radius, bool joint = true) const;
    QSharedPointer<Mesh> createLine(const QVector3D &begin, const QVector3D &end) const;
    QSharedPointer<Mesh> createSquare(const QVector3D &topLeft, const QVector3D &topRight,
                                      const QVector3D &bottomLeft, const QVector3D &bottomRight) const;

private:
    void applyColor(const MeshTempDataPtr &meshDataPtr) const;

    MeshTempDataPtr getCylinderData(qreal base, qreal top, qreal height,
                                uint slices, uint stacks) const;
    MeshTempDataPtr getArrowData(qreal smallRadius, qreal largeRadius, qreal headRatio,
                             qreal height, uint slices, uint stacks) const;
    MeshTempDataPtr getDiskData(qreal innerRadius, qreal outerRadius,
                            uint slices, uint loops) const;
    MeshTempDataPtr getIcoSphereData(qreal radius, uint refinement) const;
    MeshTempDataPtr getBoxData(qreal width, qreal height, qreal depth, uint refinement) const;
    MeshTempDataPtr getLineData(const QVector3D &begin, const QVector3D &end) const;
    MeshTempDataPtr getSquareData(const QVector3D &topLeft, const QVector3D &topRight,
                                  const QVector3D &bottomLeft, const QVector3D &bottomRight) const;

    ShaderProgramPtr m_program;
    QVector3D m_color;
    bool m_isRandomColor;

};

#endif // MESHFACTORY_H
