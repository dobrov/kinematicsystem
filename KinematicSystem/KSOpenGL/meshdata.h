#ifndef MESHDATA_H
#define MESHDATA_H
#include <QVector3D>
#include <QVector>
#include <QOpenGLBuffer>
#include <QMap>
#include "gltypes.h"

typedef GLuint IndexValue;
typedef QVector<IndexValue> IndicesVector;
typedef QVector<QVector3D> PosVector;
typedef QVector<QVector3D> ColorVector;
typedef QVector<QVector3D> NormalVector;

class Mesh;
class MeshData
{
    friend class Mesh;

public:
    MeshData(const PosVector &pos, const IndicesVector &indices,
             GLenum primitiveType);
    ~MeshData();
    template <typename T>
    void addBufferObject(VertexDataType type, const QVector<T> data);

private:
    QMap<VertexDataType, QOpenGLBuffer> m_vboMap;
    QOpenGLBuffer m_eboMap;
    GLenum m_primitiveType;
    uint m_size;
};

typedef QSharedPointer<MeshData> MeshDataPtr;

class MeshTempData
{
public:
    MeshTempData(GLenum type = GL_TRIANGLES);
    MeshTempData(GLenum type, const PosVector &pos, const IndicesVector &indices);
    MeshTempData(GLenum type, const PosVector &pos, const IndicesVector &indices,
                 const ColorVector &color);

    inline PosVector &getPosition()
    {
        return m_position;
    }

    inline IndicesVector &getIndices()
    {
        return m_indices;
    }

    void applyTransform(const QMatrix4x4 &transform);
    MeshTempData operator+ (const MeshTempData &other) const;
    MeshTempData &operator+= (const MeshTempData &other);
    void setRandomColor();
    void setColor(const QVector3D &color);
    MeshDataPtr getMeshData() const;

    void setPrimitiveType(const GLenum &primitiveType);

private:
    void generateNormals(NormalVector &normals) const;

    PosVector m_position;
    IndicesVector m_indices;
    ColorVector m_color;
    GLenum m_primitiveType;
};

typedef QSharedPointer<MeshTempData> MeshTempDataPtr;

#endif // MESHDATA_H
