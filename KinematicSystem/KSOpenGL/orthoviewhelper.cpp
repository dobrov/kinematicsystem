#include "orthoviewhelper.h"

QVector3D getAxisVector(Axis axis)
{
    switch(axis)
    {
    case Axis::X:
        return QVector3D(1.0f, 0.0f, 0.0f);
    case Axis::Y:
        return QVector3D(0.0f, 1.0f, 0.0f);
    case Axis::Z:
        return QVector3D(0.0f, 0.0f, 1.0f);
    default:
        Q_ASSERT(false);
    }
    return QVector3D();
}

OrthoViewHelper::OrthoViewHelper()
    : m_view(View::XY), m_abscissaDirection(AxisDirection::Normal),
      m_ordinateDirection(AxisDirection::Normal), m_isDirty(true)
{
}

OrthoViewHelper::OrthoViewHelper(View view)
    : m_view(view), m_abscissaDirection(AxisDirection::Normal),
      m_ordinateDirection(AxisDirection::Normal), m_isDirty(true)
{
}

QQuaternion OrthoViewHelper::getRotation() const
{
    if (m_isDirty)
        recalculate();
    return m_rotation;
}

CuboidConstraint OrthoViewHelper::getConstraint(CuboidConstraint initConstraint) const
{
    if (m_isDirty)
        recalculate();
    if (m_abscissaDirection == AxisDirection::Reverse)
        initConstraint.setReverseAxis(getAbscissa(m_view));
    if (m_ordinateDirection == AxisDirection::Reverse)
        initConstraint.setReverseAxis(getOrdinate(m_view));
    if (isReverseApplicate())
        initConstraint.setReverseAxis(getApplicate(m_view));

    return initConstraint.createViewConstraint(m_view);
}

void OrthoViewHelper::setReverseAxis(Axis axis, AxisDirection dir)
{
    switch (axis)
    {
    case Axis::X:
        if (m_view == View::XY || m_view == View::XZ)
            setReverseAbscissa(dir);
        else if (m_view == View::YX || m_view == View::ZX)
            setReverseOrdinate(dir);
        break;
    case Axis::Y:
        if (m_view == View::YX || m_view == View::YZ)
            setReverseAbscissa(dir);
        else if (m_view == View::XY || m_view == View::ZY)
            setReverseOrdinate(dir);
        break;
    case Axis::Z:
        if (m_view == View::ZX || m_view == View::ZY)
            setReverseAbscissa(dir);
        else if (m_view == View::XZ || m_view == View::YZ)
            setReverseOrdinate(dir);
        break;
    default:
        Q_ASSERT(false);
    }
}

void OrthoViewHelper::setReverseAbscissa(AxisDirection dir)
{
    if (m_abscissaDirection != dir)
    {
        m_abscissaDirection = dir;
        m_isDirty = true;
    }
}

void OrthoViewHelper::setReverseOrdinate(AxisDirection dir)
{
    if (m_ordinateDirection != dir)
    {
        m_ordinateDirection = dir;
        m_isDirty = true;
    }
}

bool OrthoViewHelper::isReverseApplicate() const
{
    Q_ASSERT(m_isDirty == false);

    QVector3D thirdAxis = m_rotation.rotatedVector(getAxisVector(getApplicate(m_view)));
    return qFuzzyCompare(-1.0f, thirdAxis.z());
}

void OrthoViewHelper::recalculate() const
{
    switch (m_view)
    {
    case View::XY:
        m_rotation = QQuaternion();
        break;
    case View::XZ:
        m_rotation = QQuaternion::fromAxisAndAngle(1.0f, 0.0f, 0.0f, 90.0f);
        break;
    case View::YX:
        m_rotation = QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, 180.0f) *
                     QQuaternion::fromAxisAndAngle(0.0f, 0.0f, 1.0f, 90.0f);
        break;
    case View::YZ:
        m_rotation = QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, 90.0f) *
                     QQuaternion::fromAxisAndAngle(0.0f, 0.0f, 1.0f, 90.0f);
        break;
    case View::ZX:
        m_rotation = QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, -90.0f) *
                     QQuaternion::fromAxisAndAngle(1.0f, 0.0f, 0.0f, -90.0f);
        break;
    case View::ZY:
        m_rotation = QQuaternion::fromAxisAndAngle(0.0f, 1.0f, 0.0f, -90.0f);
        break;
    default:
        Q_ASSERT(false);
    }
    m_rotation = m_rotation.conjugated();

    QQuaternion reverseRotation;
    if (m_abscissaDirection == AxisDirection::Reverse)
        reverseRotation *= QQuaternion::fromAxisAndAngle(getAxisVector(getOrdinate(m_view)), 180.0f);
    if (m_ordinateDirection == AxisDirection::Reverse)
        reverseRotation *= QQuaternion::fromAxisAndAngle(getAxisVector(getAbscissa(m_view)), 180.0f);
    m_rotation *= reverseRotation;
    m_isDirty = false;
}
