#include "shaderprogram.h"
#include <QFile>

const QMap<QByteArray, VertexDataType> ShaderProgram::VertexDataTypeByAttributeName{{QByteArrayLiteral("position"), VertexDataType::Position},
                                                                                    {QByteArrayLiteral("color"), VertexDataType::Color},
                                                                                    {QByteArrayLiteral("normal"), VertexDataType::Normal}};

ShaderProgram::ShaderProgram(const QString &shaderTemplate)
{
    prepareProgram(shaderTemplate);
    getProgramUniforms();
    getProgramAttributes();
}

bool ShaderProgram::bind()
{
    return m_program.bind();
}

void ShaderProgram::release()
{
    m_program.release();
}

bool ShaderProgram::isExistUniform(const QByteArray &name)
{
    return m_programUniforms.contains(name);
}

int ShaderProgram::getAttributeLocation(VertexDataType vertexDataType)
{
    auto attrLocationIt = m_programAttribsByVertexDataType.constFind(vertexDataType);
    return attrLocationIt != m_programAttribsByVertexDataType.constEnd() ? attrLocationIt.value() : -1;
}

void ShaderProgram::setUniform(const QByteArray &name, const QVariant &var)
{
    if (!isExistUniform(name))
        return;

    m_program.bind();
    QByteArray typeName(var.typeName());
    if (typeName == QByteArrayLiteral("QSharedPointer<QVector3D>"))
    {
        m_program.setUniformValue(name.constData(), *var.value<QSharedPointer<QVector3D>>());
    }
    else if (typeName == QByteArrayLiteral("QSharedPointer<GLfloat>"))
    {
        m_program.setUniformValue(name.constData(), *var.value<QSharedPointer<GLfloat>>());
    }
    else if (typeName == QByteArrayLiteral("QSharedPointer<QMatrix4x4>"))
    {
        m_program.setUniformValue(name.constData(), *var.value<QSharedPointer<QMatrix4x4>>());
    }
    else if (typeName == QByteArrayLiteral("QSharedPointer<QMatrix3x3>"))
    {
        m_program.setUniformValue(name.constData(), *var.value<QSharedPointer<QMatrix3x3>>());
    }
    else if (typeName == QByteArrayLiteral("QVector3D"))
    {
        m_program.setUniformValue(name.constData(), var.value<QVector3D>());
    }
    else if (typeName == QByteArrayLiteral("QMatrix4x4"))
    {
        m_program.setUniformValue(name.constData(), var.value<QMatrix4x4>());
    }
    else if (typeName == QByteArrayLiteral("QMatrix3x3"))
    {
        m_program.setUniformValue(name.constData(), var.value<QMatrix3x3>());
    }
    else
    {
        Q_ASSERT(false);
    }
    m_program.release();
}

void ShaderProgram::prepareProgram(const QString &shaderTemplate)
{
    Q_ASSERT(!m_program.isLinked());

    const auto vertName = shaderTemplate.arg(QStringLiteral("vert"));
    const auto fragName = shaderTemplate.arg(QStringLiteral("frag"));
    Q_ASSERT(QFile::exists(vertName) && QFile::exists(fragName));

    m_program.addShaderFromSourceFile(QOpenGLShader::Vertex, vertName);
    m_program.addShaderFromSourceFile(QOpenGLShader::Fragment, fragName);

    const auto geomName = shaderTemplate.arg(QStringLiteral("geom"));
    if (QFile::exists(geomName))
    {
        m_program.addShaderFromSourceFile(QOpenGLShader::Geometry, geomName);
    }

    m_program.link();
}

void ShaderProgram::getProgramUniforms()
{
    Q_ASSERT(m_programUniforms.empty());

    auto f = QOpenGLContext::currentContext()->functions();
    GLint numActiveUniforms = 0;
    f->glGetProgramiv(m_program.programId(), GL_ACTIVE_UNIFORMS, &numActiveUniforms);

    for (GLint i = 0; i < numActiveUniforms; i++)
    {
        constexpr int maxLength = 256;
        QByteArray array;
        array.resize(maxLength);
        GLint arraySize = 0;
        GLenum type = 0;
        GLsizei actualLength = 0;
        f->glGetActiveUniform(m_program.programId(), i, maxLength,
                              &actualLength, &arraySize, &type, array.data());
        QByteArray name(array.mid(0, actualLength));
        m_programUniforms.insert(name);
    }
}

void ShaderProgram::getProgramAttributes()
{
    Q_ASSERT(m_programAttribsByVertexDataType.empty());

    auto f = QOpenGLContext::currentContext()->functions();
    GLint numActiveAttribs  = 0;
    f->glGetProgramiv(m_program.programId(), GL_ACTIVE_ATTRIBUTES, &numActiveAttribs);

    for (GLint i = 0; i < numActiveAttribs; i++)
    {
        constexpr int maxLength = 256;
        QByteArray array;
        array.resize(maxLength);
        GLint arraySize = 0;
        GLenum type = 0;
        GLsizei actualLength = 0;
        f->glGetActiveAttrib(m_program.programId(), i, maxLength,
                              &actualLength, &arraySize, &type, array.data());
        QByteArray name(array.mid(0, actualLength));

        auto vertexDataTypeIt = VertexDataTypeByAttributeName.constFind(name);
        if (vertexDataTypeIt != VertexDataTypeByAttributeName.constEnd())
        {
            m_programAttribsByVertexDataType.insert(vertexDataTypeIt.value(), m_program.attributeLocation(name));
        }
    }
}

QOpenGLShaderProgram &ShaderProgram::getProgram()
{
    return m_program;
}
