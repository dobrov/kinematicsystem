#include "mesh.h"
#include "shaderprogram.h"
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>

const QMap<VertexDataType, int> Mesh::DataTypeTupleSizeMap{{VertexDataType::Position, 3},
                                                           {VertexDataType::Color, 3},
                                                           {VertexDataType::Normal, 3}};

Mesh::Mesh(const MeshDataPtr &meshData, const ShaderProgramPtr &p)
    : m_meshData(meshData), m_program(p),
      m_vao(new QOpenGLVertexArrayObject, [](QOpenGLVertexArrayObject *object){object->destroy();})
{
    m_vao->create();
    m_program->bind();

    m_vao->bind();

    m_meshData->m_eboMap.bind();

    auto &program = m_program->getProgram();
    for (const auto vboType : m_meshData->m_vboMap.keys())
    {
        auto location = m_program->getAttributeLocation(vboType);
        if (location == -1)
            continue;

        auto curVbo = m_meshData->m_vboMap[vboType];
        curVbo.bind();
        program.enableAttributeArray(location);
        program.setAttributeBuffer(location, GL_FLOAT, 0, DataTypeTupleSizeMap.value(vboType));
        curVbo.release();
    }
    m_vao->release();
    m_meshData->m_eboMap.release();
    m_program->release();
}

void Mesh::applyUniforms(const UniformsMap &uniforms)
{
    for (auto it = uniforms.begin(); it != uniforms.end(); ++it)
    {
        m_program->setUniform(it.key(), it.value());
    }
}

void Mesh::render()
{
    auto f = QOpenGLContext::currentContext()->functions();
    applyUniforms(m_ownUniforms);
    m_program->bind();

    QOpenGLVertexArrayObject::Binder binder(m_vao.data());
    f->glDrawElements(m_meshData->m_primitiveType,
                      m_meshData->m_size, GL_UNSIGNED_INT, 0);
#ifdef QT_DEBUG
    checkOpenGL();
#endif

    m_program->release();
}

void Mesh::addUniform(QByteArray name, QVariant value)
{
    if(m_program->isExistUniform(name))
        m_ownUniforms.insert(name, value);
}
