#ifndef THIRDANGLEBUILDER_H
#define THIRDANGLEBUILDER_H

#include <QVector>
#include <QMatrix4x4>
#include "gltypes.h"
#include "cuboidconstraint.h"
#include <array>

enum class CameraType
{
    MainCamera,
    TopCamera,
    RightCamera
};

class QComboBox;
class QDoubleSpinBox;
class AnimationSettings;

typedef std::array<std::array<QVector3D, 2>, 2> BordersData;

class ThirdAngleBuilder
{
public:
    enum class Constraint
    {
        xMin,
        xMax,
        yMin,
        yMax,
        zMin,
        zMax
    };

    ThirdAngleBuilder(qreal xMin = 0.0, qreal xMax = 0.0, qreal yMin = 0.0,
                      qreal yMax = 0.0, qreal zMin = 0.0, qreal zMax = 0.0);

    void setMainView(View mainView);
    void setAxisDirection(Axis axis, AxisDirection direction);

    QMatrix4x4 getProjMatrix() const;
    QMatrix4x4 getCameraMatrix(CameraType type) const;
    QMatrix4x4 getMainCameraMatrix() const;
    QMatrix4x4 getRightCameraMatrix() const;
    QMatrix4x4 getTopCameraMatrix() const;
    BordersData getBordersData() const;

    void adjustGeometry(QWidget* widget) const;
    void restoreMainView(QComboBox* comboBox) const;
    void restoreAxisDirection(QComboBox* comboBox, Axis axis) const;
    void restoreConstraint(QDoubleSpinBox* comboBox, Constraint constraint) const;

    static const QVector<CameraType> Cameras;

private:
    void recalculate() const;
    void getOtherViews(View &topView, View &rightView) const;

    CuboidConstraint m_initConstraint;
    View m_mainView;
    QMap<Axis, AxisDirection> m_axisDirMap;

    mutable bool m_isDirty;
    mutable CuboidConstraint m_endConstraint;
    mutable QMatrix4x4 m_mainCamera;
    mutable QMatrix4x4 m_rightCamera;
    mutable QMatrix4x4 m_topCamera;
    mutable BordersData m_bordersData;

    friend QDataStream& operator <<(QDataStream& ds, const ThirdAngleBuilder &builder);
    friend QDataStream& operator >>(QDataStream& ds, ThirdAngleBuilder &builder);
};

QDataStream& operator <<(QDataStream& ds, const ThirdAngleBuilder &builder);
QDataStream& operator >>(QDataStream& ds, ThirdAngleBuilder &builder);

#endif // THIRDANGLEBUILDER_H
