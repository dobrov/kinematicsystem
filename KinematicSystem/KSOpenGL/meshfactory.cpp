#include "meshfactory.h"
#include "meshdata.h"
#include "mesh.h"
#include <array>
#include <qmath.h>
#include <QHash>

void refine(PosVector &position, IndicesVector &indices)
{
    using std::swap;

    IndicesVector newIndices;

    //v1----middle----v2, map key - [v1, v2] combined in int64, where v1 is always lower than v2
    QHash<quint64, IndexValue> midIndicesMap;

    std::array<IndexValue, 6> localIndices;
    for (int i = 0; i < indices.size(); i += 3)
    {
        //old indices
        std::get<0>(localIndices) = indices.at(i);
        std::get<1>(localIndices) = indices.at(i + 1);
        std::get<2>(localIndices) = indices.at(i + 2);

        static const std::array<std::array<int, 2>, 3> edgeIndices{{{0, 1}, {1, 2}, {0, 2}}};
        for (int j = 0; j < 3; j++)
        {
            auto indexV1 = localIndices.at(edgeIndices.at(j).at(0));
            auto indexV2 = localIndices.at(edgeIndices.at(j).at(1));
            if (indexV2 < indexV1)
                swap(indexV1, indexV2);
            quint64 key = (static_cast<quint64>(indexV1) << 32) + indexV2;
            auto it = midIndicesMap.constFind(key);
            if (it != midIndicesMap.constEnd())
                localIndices[3 + j] = it.value();
            else
            {
                const auto &vertexV1 = position.at(indexV1);
                const auto &vertexV2 = position.at(indexV2);
                auto midVertex = (vertexV1 + vertexV2) / 2;
                localIndices[3 + j] = position.size();
                position.append(midVertex);
                midIndicesMap.insert(key, localIndices.at(3 + j));
            }
        }

        //adding new faces
        static const std::array<std::array<int , 3>, 4> localFaces{{{0, 3, 5}, {3, 1, 4},
                                                                    {5, 4, 2}, {5, 3, 4}}};
        for (const auto faces : localFaces)
        {
            newIndices.append(localIndices.at(std::get<0>(faces)));
            newIndices.append(localIndices.at(std::get<1>(faces)));
            newIndices.append(localIndices.at(std::get<2>(faces)));
        }
    }

    indices = newIndices;
}

void fillTrigArrays(QVector<qreal> &sinArray, QVector<qreal> &cosArray, uint slices)
{
    sinArray.resize(slices);
    cosArray.resize(slices);
    qreal dtheta = 2.0 * M_PI / slices;
    qreal theta = 0;
    for (uint i = 0; i < slices; i++)
    {
        sinArray[i] = qSin(theta);
        cosArray[i] = qCos(theta);
        theta += dtheta;
    }
}

MeshFactory::MeshFactory()
    : m_color(0.0f, 0.0f, 1.0f), m_isRandomColor(false)
{
}

MeshFactory::MeshFactory(const ShaderProgramPtr &program)
    : m_program(program), m_color(0.0f, 0.0f, 1.0f), m_isRandomColor(false)
{
}

QSharedPointer<Mesh> MeshFactory::createCylinder(qreal base, qreal top, qreal height,
                                                 uint slices, uint stacks) const
{
    Q_ASSERT(!m_program.isNull());
    auto meshTempDataPtr = getCylinderData(base, top, height, slices, stacks);
    applyColor(meshTempDataPtr);
    return QSharedPointer<Mesh>::create(meshTempDataPtr->getMeshData(), m_program);
}

QSharedPointer<Mesh> MeshFactory::createArrow(qreal smallRadius, qreal largeRadius, qreal headRatio,
                                              qreal height, uint slices, uint stacks) const
{
    Q_ASSERT(!m_program.isNull());
    auto meshTempDataPtr = getArrowData(smallRadius, largeRadius, headRatio,
                                    height, slices, stacks);
    applyColor(meshTempDataPtr);
    return QSharedPointer<Mesh>::create(meshTempDataPtr->getMeshData(), m_program);
}

QSharedPointer<Mesh> MeshFactory::createCornerXYZ(qreal scale) const
{
    Q_ASSERT(!m_program.isNull());
    QMatrix4x4 transform;
    auto arrowDataX = getArrowData(0.025, 0.06, 0.15, 1, 20, 5);
    arrowDataX->setColor(QVector3D(0.8f, 0.0f, 0.0f));
    MeshTempData arrowDataY(GL_TRIANGLES, arrowDataX->getPosition(), arrowDataX->getIndices());
    arrowDataY.setColor(QVector3D(0.0f, 0.8f, 0.0f));
    MeshTempData arrowDataZ(GL_TRIANGLES, arrowDataX->getPosition(), arrowDataX->getIndices());
    arrowDataZ.setColor(QVector3D(0.0f, 0.0f, 0.8f));

    transform.rotate(90.0f, 0.0f, 1.0f, 0.0f);
    arrowDataX->applyTransform(transform);

    transform.setToIdentity();
    transform.rotate(-90.0f, 1.0f, 0.0f, 0.0f);
    arrowDataY.applyTransform(transform);

    auto meshTempDataPtr = arrowDataX;
    *meshTempDataPtr += arrowDataY;
    *meshTempDataPtr += arrowDataZ;

    auto sphereData = getIcoSphereData(0.07, 2);
    sphereData->setColor(QVector3D(0.0f, 0.0f, 0.0f));
    *meshTempDataPtr += *sphereData;

    if (!qFuzzyCompare(1.0, scale))
    {
        transform.setToIdentity();
        transform.scale(scale);
        meshTempDataPtr->applyTransform(transform);
    }

    return QSharedPointer<Mesh>::create(meshTempDataPtr->getMeshData(), m_program);
}

QSharedPointer<Mesh> MeshFactory::createIcoSphere(qreal radius, uint refinement) const
{
    Q_ASSERT(!m_program.isNull());
    auto meshTempDataPtr = getIcoSphereData(radius, refinement);
    meshTempDataPtr->setColor(m_color);
    return QSharedPointer<Mesh>::create(meshTempDataPtr->getMeshData(), m_program);
}

QSharedPointer<Mesh> MeshFactory::createBox(qreal width, qreal height, qreal depth, uint refinement) const
{
    Q_ASSERT(!m_program.isNull());
    auto meshTempDataPtr = getBoxData(width, height, depth, refinement);
    meshTempDataPtr->setColor(m_color);
    return QSharedPointer<Mesh>::create(meshTempDataPtr->getMeshData(), m_program);
}

QSharedPointer<Mesh> MeshFactory::createBone(qreal length, qreal radius, bool joint) const
{
    Q_ASSERT(!m_program.isNull());
    auto meshTempDataPtr = getCylinderData(radius, radius, length, 10, 2);
    QMatrix4x4 transform;
    transform.rotate(90.0f, 0.0f, 1.0f, 0.0f);
    meshTempDataPtr->applyTransform(transform);
    meshTempDataPtr->setColor(m_color);
    if (joint)
    {
        static const qreal jointRadius = radius * 2.0;
        auto sphereDataPtr = getIcoSphereData(jointRadius, 2);
        sphereDataPtr->setColor(QVector3D(0.0f, 0.0f, 0.0f));
        transform.translate(QVector3D(0.0f, 0.0f, length - radius));
        sphereDataPtr->applyTransform(transform);
        *meshTempDataPtr += *sphereDataPtr;
    }
    else
    {
        auto diskDataPtr = getDiskData(0.0, radius, 10, 2);
        diskDataPtr->setColor(m_color);
        transform.translate(0.0f, 0.0f, length);
        diskDataPtr->applyTransform(transform);
        *meshTempDataPtr += *diskDataPtr;
    }
    return QSharedPointer<Mesh>::create(meshTempDataPtr->getMeshData(), m_program);
}

QSharedPointer<Mesh> MeshFactory::createLine(const QVector3D &begin, const QVector3D &end) const
{
    return QSharedPointer<Mesh>::create(getLineData(begin, end)->getMeshData(), m_program);
}

QSharedPointer<Mesh> MeshFactory::createSquare(const QVector3D &topLeft, const QVector3D &topRight,
                                               const QVector3D &bottomLeft, const QVector3D &bottomRight) const
{
    return QSharedPointer<Mesh>::create(getSquareData(topLeft, topRight, bottomLeft, bottomRight)->getMeshData(), m_program);
}

void MeshFactory::applyColor(const MeshTempDataPtr &meshTempDataPtr) const
{
    if (m_isRandomColor)
        meshTempDataPtr->setRandomColor();
    else
        meshTempDataPtr->setColor(m_color);
}

MeshTempDataPtr MeshFactory::getCylinderData(qreal base, qreal top, qreal height, uint slices, uint stacks) const
{
    Q_ASSERT(base >= 0 && top >= 0 && height > 0 && slices > 2 && stacks > 1);

    auto meshTempDataPtr = MeshTempDataPtr::create();

    auto &position = meshTempDataPtr->getPosition();
    auto &indices = meshTempDataPtr->getIndices();

    QVector<qreal> sinArray, cosArray;
    fillTrigArrays(sinArray, cosArray, slices);

    auto dradius = base - top;
    for (uint i = 0; i < stacks; i++)
    {
        auto curRadius = base - dradius * i / (stacks - 1);
        GLfloat curHeight = i * height/ (stacks - 1);
        if (qFuzzyIsNull(curRadius))
        {
            Q_ASSERT(i == 0 || i == (stacks - 1));
            Q_ASSERT(base != top);
            position.append({0.0f, 0.0f, curHeight});
            if (i == (stacks - 1))
            {
                for (uint j = 0; j < slices; j++)
                {
                    auto jnext = j == (slices - 1) ? 0 : j + 1;
                    indices.append(position.size() - 1);
                    indices.append((i - 1) * slices + j);
                    indices.append((i - 1) * slices + jnext);
                }
            }
            continue;
        }

        if (i != 0)
        {
            if (position.size() == 1)
            {
                for (uint j = 0; j < slices; j++)
                {
                    auto jnext = j == (slices - 1) ? 0 : j + 1;
                    indices.append(j + 1);
                    indices.append(0);
                    indices.append(jnext + 1);
                }
            }
            else
            {
                for (uint j = 0; j < slices; j++)
                {
                    auto jnext = j == (slices - 1) ? 0 : j + 1;
                    indices.append(i * slices + j);
                    indices.append((i-1) * slices + j);
                    indices.append((i-1) * slices + jnext);

                    indices.append(i * slices + j);
                    indices.append((i-1) * slices + jnext);
                    indices.append(i * slices + jnext);
                }
            }
        }
        for (uint j = 0; j < slices; j++)
            position.append({static_cast<GLfloat>(cosArray.at(j) * curRadius),
                            static_cast<GLfloat>(sinArray.at(j) * curRadius), curHeight});
    }

    return meshTempDataPtr;
}

MeshTempDataPtr MeshFactory::getArrowData(qreal smallRadius, qreal largeRadius, qreal headRatio,
                                      qreal height, uint slices, uint stacks) const
{
    QMatrix4x4 transform;

    auto meshData = getCylinderData(smallRadius, smallRadius, (1 - headRatio) * height, slices, stacks);
    auto baseDisk = getDiskData(0.0, smallRadius, slices, 2);
    transform.rotate(180.0f, 1.0f, 0.0f, 0.0f);
    baseDisk->applyTransform(transform);
    *meshData += *baseDisk;

    auto topDisk = getDiskData(smallRadius, largeRadius, slices, 2);
    transform.setToIdentity();
    transform.translate(0.0f, 0.0f, (1 - headRatio) * height);
    transform.rotate(180.0f, 1.0f, 0.0f, 0.0f);
    topDisk->applyTransform(transform);
    *meshData += *topDisk;

    auto headCone = getCylinderData(largeRadius, 0.0, headRatio * height, slices, stacks);
    transform.setToIdentity();
    transform.translate(0.0f, 0.0f, (1 - headRatio) * height);
    headCone->applyTransform(transform);
    *meshData += *headCone;
    return meshData;
}

MeshTempDataPtr MeshFactory::getDiskData(qreal innerRadius, qreal outerRadius, uint slices, uint loops) const
{
    Q_ASSERT(slices > 2 && loops > 1 && innerRadius >= 0.0 && outerRadius > 0.0 &&
             outerRadius > innerRadius);

    auto meshTempDataPtr = MeshTempDataPtr::create();

    auto &position = meshTempDataPtr->getPosition();
    auto &indices = meshTempDataPtr->getIndices();

    QVector<qreal> sinArray, cosArray;
    fillTrigArrays(sinArray, cosArray, slices);

    auto dradius = outerRadius - innerRadius;
    for (uint i = 0; i < loops; i++)
    {
        auto curRadius = outerRadius - dradius * i / (loops - 1);
        if (qFuzzyIsNull(curRadius))
        {
            Q_ASSERT(i == (loops - 1) && innerRadius == 0.0);
            position.append({0.0f, 0.0f, 0.0f});
            for (uint j = 0; j < slices; j++)
            {
                auto jnext = j == (slices - 1) ? 0 : j + 1;
                indices.append(position.size() - 1);
                indices.append((i - 1) * slices + j);
                indices.append((i - 1) * slices + jnext);
            }
            continue;
        }

        if (i > 0)
        {
            for (uint j = 0; j < slices; j++)
            {
                auto jnext = j == (slices - 1) ? 0 : j + 1;
                indices.append(i * slices + jnext);
                indices.append(i * slices + j);
                indices.append((i - 1) * slices + j);

                indices.append(i * slices + jnext);
                indices.append((i - 1) * slices + j);
                indices.append((i - 1) * slices + jnext);
            }
        }
        for (uint j = 0; j < slices; j++)
            position.append({static_cast<GLfloat>(cosArray.at(j) * curRadius),
                             static_cast<GLfloat>(sinArray.at(j) * curRadius), 0.0f});
    }

    return meshTempDataPtr;
}

MeshTempDataPtr MeshFactory::getIcoSphereData(qreal radius, uint refinement) const
{
    Q_ASSERT(radius > 0);
    static const GLfloat w = .525731112119133606f;
    static const GLfloat l = .850650808352039932f;

    static const PosVector icoPos{{-w, l, 0.0f}, {w, l, 0.0f}, {-w, -l, 0.0f}, {w, -l, 0.0f},
                                  {0.0f, -w, l}, {0.0f, w, l}, {0.0f, -w, -l}, {0.0f, w, -l},
                                  {l, 0.0f, -w}, {l, 0.0f, w}, {-l, 0.0f, -w}, {-l, 0.0f, w}};
    static const IndicesVector icoIndices{0,11,5, 0,5,1, 0,1,7, 0,7,10, 0,10,11,
                                          1,5,9, 5,11,4, 11,10,2, 10,7,6, 7,1,8,
                                          3,9,4, 3,4,2, 3,2,6, 3,6,8, 3,8,9,
                                          4,9,5, 2,4,11, 6,2,10, 8,6,7, 9,8,1};

    auto meshTempDataPtr = MeshTempDataPtr::create();

    auto &position = meshTempDataPtr->getPosition();
    auto &indices = meshTempDataPtr->getIndices();

    position = icoPos;
    indices = icoIndices;
    for (uint i = 0; i < refinement; i++)
        refine(position, indices);

    for (auto &pos : position)
    {
        pos.normalize();
        pos *= radius;
    }
    return meshTempDataPtr;
}

MeshTempDataPtr MeshFactory::getBoxData(qreal width, qreal height, qreal depth, uint refinement) const
{
    Q_ASSERT(width > 0 && height > 0 && depth > 0);

    static const PosVector cubePos{{1.0f, 1.0f, 1.0f}, {-1.0f, 1.0f, 1.0f}, {-1.0f, -1.0f, 1.0f},
                                   {1.0f, -1.0f, 1.0f}, {1.0f, 1.0f, -1.0f}, {-1.0f, 1.0f, -1.0f},
                                   {-1.0f, -1.0f, -1.0f}, {1.0f, -1.0f, -1.0f}};
    static const IndicesVector cubeIndices{1,2,3, 1,3,0, 0,3,7, 0,7,4, 3,2,6, 3,6,7,
                                           1,6,2, 1,5,6, 0,5,1, 0,4,5, 6,5,4, 6,4,7};

    auto meshTempDataPtr = MeshTempDataPtr::create();

    auto &position = meshTempDataPtr->getPosition();
    auto &indices = meshTempDataPtr->getIndices();

    position = cubePos;
    indices = cubeIndices;

    for (uint i = 0; i < refinement; i++)
        refine(position, indices);

    QMatrix4x4 scaleMatrix;
    scaleMatrix.scale(width/2, height/2, depth/2);

    for (auto &pos : position)
        pos = scaleMatrix * pos;

    return meshTempDataPtr;
}

MeshTempDataPtr MeshFactory::getLineData(const QVector3D &begin, const QVector3D &end) const
{
    auto meshTempDataPtr = MeshTempDataPtr::create(GL_LINES, PosVector{begin, end}, IndicesVector{0, 1});
    applyColor(meshTempDataPtr);
    return meshTempDataPtr;
}

MeshTempDataPtr MeshFactory::getSquareData(const QVector3D &topLeft, const QVector3D &topRight,
                                           const QVector3D &bottomLeft, const QVector3D &bottomRight) const
{
    auto meshTempDataPtr = MeshTempDataPtr::create(GL_LINE_STRIP, PosVector{topLeft, topRight, bottomRight, bottomLeft},
                                                   IndicesVector{0, 1, 2, 3, 0});
    applyColor(meshTempDataPtr);
    return meshTempDataPtr;
}

void MeshFactory::setColor(const QVector3D &color)
{
    m_isRandomColor = false;
    m_color = color;
}

void MeshFactory::setProgram(const ShaderProgramPtr &program)
{
    m_program = program;
}

void MeshFactory::setRandomColor(bool isRandomColor)
{
    m_isRandomColor = isRandomColor;
}
