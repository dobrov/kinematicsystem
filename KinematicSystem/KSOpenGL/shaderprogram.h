#ifndef SHADERPROGRAM_H
#define SHADERPROGRAM_H

#include "gltypes.h"
#include <QOpenGLShaderProgram>
#include <type_traits>

class ShaderProgram
{
public:
    explicit ShaderProgram(const QString &shaderTemplate);

    bool bind();
    void release();

    bool isExistUniform(const QByteArray &name);
    int getAttributeLocation(VertexDataType vertexDataType);

    void setUniform(const QByteArray &name, const QVariant &var);

    template <typename T>
    void setUniform(const QByteArray &name, const T &var, std::enable_if_t<!std::is_same<QVariant, T>::value>* = 0)
    {
        m_program.bind();
        m_program.setUniformValue(name.constData(), var);
#ifdef QT_DEBUG
        checkOpenGL();
#endif
        m_program.release();
    }

    QOpenGLShaderProgram &getProgram();

private:
    void prepareProgram(const QString &shaderTemplate);
    void getProgramUniforms();
    void getProgramAttributes();

    QOpenGLShaderProgram m_program;
    QSet<QByteArray> m_programUniforms;
    QMap<VertexDataType, int> m_programAttribsByVertexDataType;

    static const QMap<QByteArray, VertexDataType> VertexDataTypeByAttributeName;
};

#endif // SHADERPROGRAM_H
