QT += core gui

INCLUDEPATH += $$PWD

LIBS += -lKSOpenGL

Debug {
    LIBS += -L$$OUT_PWD/../KSOpenGL/debug/
    msvc: PRE_TARGETDEPS += $$OUT_PWD/../KSOpenGL/debug/KSOpenGL.lib
    gcc: PRE_TARGETDEPS += $$OUT_PWD/../KSOpenGL/debug/libKSOpenGL.a
}
Release {
    LIBS += -L$$OUT_PWD/../KSOpenGL/release/
    msvc: PRE_TARGETDEPS += $$OUT_PWD/../KSOpenGL/release/KSOpenGL.lib
    gcc: PRE_TARGETDEPS += $$OUT_PWD/../KSOpenGL/release/libKSOpenGL.a
}
