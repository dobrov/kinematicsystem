#ifndef GLTYPES_H
#define GLTYPES_H

#include <QtGlobal>
#include <QSharedPointer>
#include <QOpenGLContext>
#include <QOpenGLFunctions>
#include <QMatrix3x3>
#include <QMatrix4x4>
#include <QVector3D>
#include <QMap>

class QByteArray;
class QVariant;
class ShaderProgram;
class Mesh;
typedef QMap<QByteArray, QVariant> UniformsMap;
typedef QSharedPointer<ShaderProgram> ShaderProgramPtr;
typedef QSharedPointer<Mesh> MeshPtr;

Q_DECLARE_METATYPE(QSharedPointer<QVector3D>)
Q_DECLARE_METATYPE(QSharedPointer<QMatrix4x4>)
Q_DECLARE_METATYPE(QSharedPointer<QMatrix3x3>)
Q_DECLARE_METATYPE(QSharedPointer<GLfloat>)

inline void checkOpenGL()
{
    auto f = QOpenGLContext::currentContext()->functions();
    auto error = f->glGetError();
    if(error != GL_NO_ERROR)
    {
        qDebug() << error;
        Q_ASSERT(false);
    }
}

enum class VertexDataType
{
    Position,
    Color,
    Normal,
};

class GLTypes
{
    Q_GADGET
public:
    GLTypes() = delete;

    enum class View
    {
        XY,
        XZ,
        YX,
        YZ,
        ZX,
        ZY
    };
    Q_ENUM(View)

    enum class Axis
    {
        X,
        Y,
        Z
    };
    Q_ENUM(Axis)

    enum class AxisDirection
    {
        Normal,
        Reverse
    };
    Q_ENUM(AxisDirection)
};
typedef GLTypes::View View;
typedef GLTypes::Axis Axis;
typedef GLTypes::AxisDirection AxisDirection;

inline Axis getAbscissa(View view)
{
    Axis axis = Axis::X;
    switch (view)
    {
    case View::XY:
    case View::XZ:
        axis = Axis::X;
        break;
    case View::YX:
    case View::YZ:
        axis = Axis::Y;
        break;
    case View::ZX:
    case View::ZY:
        axis = Axis::Z;
        break;
    default:
        Q_ASSERT(false);
    }
    return axis;
}

inline Axis getOrdinate(View view)
{
    Axis axis = Axis::Y;
    switch (view)
    {
    case View::YX:
    case View::ZX:
        axis = Axis::X;
        break;
    case View::XY:
    case View::ZY:
        axis = Axis::Y;
        break;
    case View::XZ:
    case View::YZ:
        axis = Axis::Z;
        break;
    default:
        Q_ASSERT(false);
    }
    return axis;
}

inline Axis getApplicate(View view)
{
    Axis axis = Axis::Z;
    switch (view)
    {
    case View::YZ:
    case View::ZY:
        axis = Axis::X;
        break;
    case View::XZ:
    case View::ZX:
        axis = Axis::Y;
        break;
    case View::XY:
    case View::YX:
        axis = Axis::Z;
        break;
    default:
        Q_ASSERT(false);
    }
    return axis;
}

#endif // GLTYPES_H
