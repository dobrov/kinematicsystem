#include "cuboidconstraint.h"

CuboidConstraint::CuboidConstraint()
{
    m_xMin = m_yMin = m_zMin = m_xMax = m_yMax = m_zMax = 0;
}

CuboidConstraint::CuboidConstraint(qreal xMin, qreal xMax, qreal yMin,
                                   qreal yMax, qreal zMin, qreal zMax)
    : m_xMin(xMin), m_xMax(xMax), m_yMin(yMin), m_yMax(yMax), m_zMin(zMin), m_zMax(zMax)
{
}

CuboidConstraint CuboidConstraint::createViewConstraint(View view) const
{
    switch (view)
    {
    case View::XY:
        return CuboidConstraint(m_xMin, m_xMax, m_yMin, m_yMax, m_zMin, m_zMax);
    case View::XZ:
        return CuboidConstraint(m_xMin, m_xMax, m_zMin, m_zMax, m_yMin, m_yMax);
    case View::YX:
        return CuboidConstraint(m_yMin, m_yMax, m_xMin, m_xMax, m_zMin, m_zMax);
    case View::YZ:
        return CuboidConstraint(m_yMin, m_yMax, m_zMin, m_zMax, m_xMin, m_xMax);
    case View::ZX:
        return CuboidConstraint(m_zMin, m_zMax, m_xMin, m_xMax, m_yMin, m_yMax);
    case View::ZY:
        return CuboidConstraint(m_zMin, m_zMax, m_yMin, m_yMax, m_xMin, m_xMax);
    default:
        Q_ASSERT(false);
    }
    return *this;
}

void CuboidConstraint::setReverseAxis(Axis axis)
{
    switch (axis)
    {
    case Axis::X:
        std::swap(m_xMin, m_xMax);
        m_xMin = -m_xMin;
        m_xMax = -m_xMax;
        break;
    case Axis::Y:
        std::swap(m_yMin, m_yMax);
        m_yMin = -m_yMin;
        m_yMax = -m_yMax;
        break;
    case Axis::Z:
        std::swap(m_zMin, m_zMax);
        m_zMin = -m_zMin;
        m_zMax = -m_zMax;
        break;
    default:
        Q_ASSERT(false);
    }
}

QMatrix4x4 CuboidConstraint::getProjMatrix() const
{
    QMatrix4x4 matrix;
    matrix.ortho(m_xMin, m_xMax, m_yMin, m_yMax, -m_zMax, -m_zMin);
    return matrix;
}

int CuboidConstraint::getWidth() const
{
    return std::abs(m_xMin - m_xMax);
}

int CuboidConstraint::getHeight() const
{
    return std::abs(m_yMin - m_yMax);
}

CuboidConstraint createTriAngleConstraint(const CuboidConstraint &main, const CuboidConstraint &top,
                                          const CuboidConstraint &right, qreal &topTrans, qreal &rightTrans)
{
    Q_ASSERT(right.m_yMin == main.m_yMin);
    Q_ASSERT(right.m_yMax == main.m_yMax);
    Q_ASSERT(top.m_xMin == main.m_xMin);
    Q_ASSERT(top.m_xMax == main.m_xMax);

    auto zMin = std::min({main.m_zMin, top.m_zMin, right.m_zMin});
    auto zMax = std::max({main.m_zMax, top.m_zMax, right.m_zMax});
    auto yMin = main.m_yMin;
    auto xMin = main.m_xMin;

    topTrans = std::abs(top.m_yMin - main.m_yMax);
    auto yMax = top.m_yMax + topTrans;

    rightTrans = std::abs(right.m_xMin - main.m_xMax);
    auto xMax = right.m_xMax + rightTrans;

    return CuboidConstraint(xMin, xMax, yMin, yMax, zMin, zMax);
}

QDataStream &operator >>(QDataStream &ds, CuboidConstraint &constraint)
{
    ds >> constraint.m_xMax;
    ds >> constraint.m_xMin;
    ds >> constraint.m_yMax;
    ds >> constraint.m_yMin;
    ds >> constraint.m_zMax;
    ds >> constraint.m_zMin;

    return ds;
}

QDataStream &operator <<(QDataStream &ds, const CuboidConstraint &constraint)
{
    ds << constraint.m_xMax;
    ds << constraint.m_xMin;
    ds << constraint.m_yMax;
    ds << constraint.m_yMin;
    ds << constraint.m_zMax;
    ds << constraint.m_zMin;

    return ds;
}
