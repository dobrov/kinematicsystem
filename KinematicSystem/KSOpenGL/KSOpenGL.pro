#-------------------------------------------------
#
# Project created by QtCreator 2016-02-09T00:29:39
#
#-------------------------------------------------

QT += core gui widgets

TARGET = KSOpenGL
TEMPLATE = lib
CONFIG += staticlib c++14

SOURCES += \
    camera3d.cpp \
    input.cpp \
    transform3d.cpp \
    mesh.cpp \
    meshdata.cpp \
    meshfactory.cpp \
    thirdanglebuilder.cpp \
    cuboidconstraint.cpp \
    orthoviewhelper.cpp \
    shaderprogram.cpp

HEADERS += \
    camera3d.h \
    input.h \
    transform3d.h \
    mesh.h \
    meshdata.h \
    meshfactory.h \
    thirdanglebuilder.h \
    gltypes.h \
    cuboidconstraint.h \
    orthoviewhelper.h \
    shaderprogram.h

OTHER_FILES += \
    KSOpenGL.pri

