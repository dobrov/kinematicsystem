#include "thirdanglebuilder.h"
#include "orthoviewhelper.h"
#include <type_traits>
#include <QVector>
#include <QComboBox>
#include <QDoubleSpinBox>

const QVector<CameraType> ThirdAngleBuilder::Cameras{CameraType::MainCamera, CameraType::RightCamera, CameraType::TopCamera};

ThirdAngleBuilder::ThirdAngleBuilder(qreal xMin, qreal xMax, qreal yMin, qreal yMax, qreal zMin, qreal zMax)
    : m_initConstraint(xMin, xMax, yMin, yMax, zMin, zMax), m_mainView(View::XY),
      m_axisDirMap{{Axis::X, AxisDirection::Normal}, {Axis::Y, AxisDirection::Normal},
{Axis::Z, AxisDirection::Normal}}, m_isDirty(true)
{
}

void ThirdAngleBuilder::setMainView(View mainView)
{
    if (m_mainView != mainView)
    {
        m_mainView = mainView;
        m_isDirty = true;
    }
}

void ThirdAngleBuilder::setAxisDirection(Axis axis, AxisDirection direction)
{
    auto it = m_axisDirMap.find(axis);
    Q_ASSERT(it != m_axisDirMap.end());

    if (it.value() != direction)
    {
        *it = direction;
        m_isDirty = true;
    }
}

QMatrix4x4 ThirdAngleBuilder::getProjMatrix() const
{
    if(m_isDirty)
        recalculate();
    return m_endConstraint.getProjMatrix();
}

QMatrix4x4 ThirdAngleBuilder::getCameraMatrix(CameraType type) const
{
    if(m_isDirty)
        recalculate();
    switch (type)
    {
    case CameraType::MainCamera:
        return m_mainCamera;
    case CameraType::TopCamera:
        return m_topCamera;
    case CameraType::RightCamera:
        return m_rightCamera;
    default:
        Q_ASSERT(false);
    }

    return QMatrix4x4();
}

QMatrix4x4 ThirdAngleBuilder::getMainCameraMatrix() const
{
    if(m_isDirty)
        recalculate();
    return m_mainCamera;
}

QMatrix4x4 ThirdAngleBuilder::getRightCameraMatrix() const
{
    if(m_isDirty)
        recalculate();
    return m_rightCamera;
}

QMatrix4x4 ThirdAngleBuilder::getTopCameraMatrix() const
{
    if(m_isDirty)
        recalculate();
    return m_topCamera;
}

BordersData ThirdAngleBuilder::getBordersData() const
{
    if(m_isDirty)
        recalculate();
    return m_bordersData;
}

void ThirdAngleBuilder::adjustGeometry(QWidget *widget) const
{
    if(m_isDirty)
        recalculate();

    auto scaleWidth = static_cast<qreal>(widget->width()) / m_endConstraint.getWidth();
    auto scaleHeight = static_cast<qreal>(widget->height()) / m_endConstraint.getHeight();

    if (std::abs(scaleWidth - scaleHeight) < 1.0)
        return;
    auto scale = std::min(scaleWidth, scaleHeight);
    auto newWidth = static_cast<int>(m_endConstraint.getWidth() * scale);
    auto newHeight = static_cast<int>(m_endConstraint.getHeight() * scale);

    widget->setGeometry((widget->width() - newWidth) / 2,
                        (widget->height() - newHeight) / 2, newWidth, newHeight);
}

void ThirdAngleBuilder::restoreMainView(QComboBox *comboBox) const
{
    comboBox->setCurrentIndex(static_cast<int>(m_mainView));
}

void ThirdAngleBuilder::restoreAxisDirection(QComboBox *comboBox, Axis axis) const
{
    comboBox->setCurrentIndex(static_cast<int>(m_axisDirMap.value(axis)));
}

void ThirdAngleBuilder::restoreConstraint(QDoubleSpinBox *comboBox, ThirdAngleBuilder::Constraint constraint) const
{
    switch (constraint)
    {
    case Constraint::xMax:
        comboBox->setValue(m_initConstraint.m_xMax);
        break;
    case Constraint::xMin:
        comboBox->setValue(m_initConstraint.m_xMin);
        break;
    case Constraint::yMax:
        comboBox->setValue(m_initConstraint.m_yMax);
        break;
    case Constraint::yMin:
        comboBox->setValue(m_initConstraint.m_yMin);
        break;
    case Constraint::zMax:
        comboBox->setValue(m_initConstraint.m_zMax);
        break;
    case Constraint::zMin:
        comboBox->setValue(m_initConstraint.m_zMin);
        break;
    default:
        Q_ASSERT(false);
    }
}

void ThirdAngleBuilder::getOtherViews(View &topView, View &rightView) const
{
    switch (m_mainView)
    {
    case View::XY:
        topView = View::XZ;
        rightView = View::ZY;
        break;
    case View::XZ:
        topView = View::XY;
        rightView = View::YZ;
        break;
    case View::YX:
        topView = View::YZ;
        rightView = View::ZX;
        break;
    case View::YZ:
        topView = View::YX;
        rightView = View::XZ;
        break;
    case View::ZX:
        topView = View::ZY;
        rightView = View::YX;
        break;
    case View::ZY:
        topView = View::ZX;
        rightView = View::XY;
        break;
    default:
        Q_ASSERT(false);
    }
}

void ThirdAngleBuilder::recalculate() const
{
    View topView, rightView;
    getOtherViews(topView, rightView);

    QMap<View, OrthoViewHelper> orthoHelpers{{m_mainView, OrthoViewHelper(m_mainView)},
                                             {topView, OrthoViewHelper(topView)},
                                             {rightView, OrthoViewHelper(rightView)}};

    for (const auto &axis : m_axisDirMap.keys())
    {
        if (m_axisDirMap.value(axis) == AxisDirection::Reverse)
        {
            for (auto &helper : orthoHelpers)
                helper.setReverseAxis(axis, m_axisDirMap.value(axis));
        }
    }

    auto mainConstraint = orthoHelpers.value(m_mainView).getConstraint(m_initConstraint);
    auto topConstraint = orthoHelpers.value(topView).getConstraint(m_initConstraint);
    auto rightConstraint = orthoHelpers.value(rightView).getConstraint(m_initConstraint);

    qreal topTrans, rightTrans;
    m_endConstraint =  createTriAngleConstraint(mainConstraint, topConstraint,
                                                rightConstraint, topTrans, rightTrans);

    m_bordersData[0][0] = QVector3D{static_cast<float>(mainConstraint.m_xMax),
            static_cast<float>(m_endConstraint.m_yMin + 0.01),
            static_cast<float>(m_endConstraint.m_zMin + 0.01)};
    m_bordersData[0][1] = QVector3D{static_cast<float>(mainConstraint.m_xMax),
            static_cast<float>(m_endConstraint.m_yMax - 0.01),
            static_cast<float>(m_endConstraint.m_zMin + 0.01)};

    m_bordersData[1][0] = QVector3D{static_cast<float>(m_endConstraint.m_xMin + 0.01),
            static_cast<float>(mainConstraint.m_yMax),
            static_cast<float>(m_endConstraint.m_zMin + 0.01)};
    m_bordersData[1][1] = QVector3D{static_cast<float>(m_endConstraint.m_xMax - 0.01),
            static_cast<float>(mainConstraint.m_yMax),
            static_cast<float>(m_endConstraint.m_zMin + 0.01)};

    //clear
    m_mainCamera.setToIdentity();
    m_topCamera.setToIdentity();
    m_rightCamera.setToIdentity();

    m_mainCamera.rotate(orthoHelpers.value(m_mainView).getRotation());

    m_topCamera.translate(0.0f, topTrans, 0.0f);
    m_topCamera.rotate(orthoHelpers.value(topView).getRotation());

    m_rightCamera.translate(rightTrans, 0.0f, 0.0f);
    m_rightCamera.rotate(orthoHelpers.value(rightView).getRotation());

    m_isDirty = false;
}

namespace {
template <typename T>
QDataStream& operator >>(QDataStream& ds, T &value)
{
    uint tValue;
    ds >> tValue;
    value = static_cast<T>(tValue);
    return ds;
}

template <typename T>
QDataStream& operator <<(QDataStream& ds, const T &value)
{
    ds << static_cast<uint>(value);
    return ds;
}
}


QDataStream& operator <<(QDataStream& ds, const ThirdAngleBuilder &builder)
{

    ds << builder.m_initConstraint;
    ds << builder.m_mainView;
    ds << builder.m_axisDirMap;
    return ds;
}

QDataStream& operator >>(QDataStream& ds, ThirdAngleBuilder &builder)
{
    ds >> builder.m_initConstraint;
    ds >> builder.m_mainView;
    ds >> builder.m_axisDirMap;
    builder.m_isDirty = true;
    return ds;
}
