#ifndef PASSINGDATATEST_H
#define PASSINGDATATEST_H

#include <QObject>
#include "autotest.h"

class PassingDataTest : public QObject
{
    Q_OBJECT

private slots:
    void PassDataTest();
};

DECLARE_TEST(PassingDataTest)
#endif // PASSINGDATATEST_H
