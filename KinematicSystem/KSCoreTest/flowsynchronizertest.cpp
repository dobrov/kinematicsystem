#include "flowsynchronizertest.h"
#include <flowsynchronizer.h>
#include <concretepassingdata.h>
#include <random>

void FlowSynchronizerTest::FlowSyncTest()
{
    constexpr TimeSample maxTime = 1000000;
    constexpr TimeSample dT = 50000;
    constexpr int nPacket = maxTime / dT;
    constexpr int nId = 8;
    constexpr double accelDataRate = 50;
    constexpr double gyroDataRate = 200;
    constexpr double magDataRate = 10;
    // !!!

    constexpr TimeSample accelPeriod = 1000000 / accelDataRate;
    constexpr TimeSample gyroPeriod = 1000000 / gyroDataRate;
    constexpr TimeSample magPeriod = 1000000 / magDataRate;
    QCOMPARE(std::min(std::min(accelPeriod, gyroPeriod), magPeriod),
             gcd<TimeSample>(gcd<TimeSample>(accelPeriod, gyroPeriod), magPeriod));

    FlowSynchronizer flowSynchronizer;
    flowSynchronizer.setDataRate(accelDataRate, gyroDataRate, magDataRate);
    FlowIdVector syncFlows;
    for (int i = 0; i < nId; i++)
    {
        syncFlows.append(std::make_tuple(i, PassingDataType::RawAccel));
        syncFlows.append(std::make_tuple(i, PassingDataType::RawGyro));
        syncFlows.append(std::make_tuple(i, PassingDataType::RawMag));
    }
    flowSynchronizer.setSynchronizingFlows(syncFlows);

    QMap<FlowId, TimeDataVector> timeMap;
    auto isStopNewData = false;
    QObject::connect(&flowSynchronizer, &AbstractPassingDataHandler::sendData, [&timeMap]
                     (const QSharedPointer<AbstractPassingData> &passingData)
    {
        auto id = passingData->getDeviceId();
        auto dataType = passingData->getDataType();
        timeMap[std::make_tuple(id, dataType)].append(passingData->getTime());
    });
    QObject::connect(&flowSynchronizer, &FlowSynchronizer::stopRecording, [&isStopNewData]()
    {
        isStopNewData = true;
    });

    QList<TimeSample> periodList{accelPeriod, gyroPeriod, magPeriod};
    QList<PassingDataType> packetType{PassingDataType::RawAccel,
                PassingDataType::RawGyro, PassingDataType::RawMag};
    constexpr RawSample emptySample{0, 0, 0};
    QList<int> order{0, 1, 2};
    std::mt19937 engine;
    std::uniform_int_distribution<TimeSample> sampleTimeGenerator(0, maxTime/4);
    QMap<FlowId, TimeSample> sampleTime;
    std::uniform_int_distribution<TimeSample> endTimeGenerator(maxTime/2, 3 * maxTime/4);
    std::uniform_int_distribution<int> endDeviceGenerator(0, 2);
    std::uniform_int_distribution<int> endIdGenerator(0, nId - 1);

    for (int m = 0; m < 10; m++)
    {
        bool isEnd = false;
        for (int i = 0; i < nId; i++)
        {
            for (int j = 0; j < 3; j++)
            {
                auto time = sampleTimeGenerator(engine);
                if (time % periodList.at(j))
                    time = time / periodList.at(j) * periodList.at(j);

                sampleTime.insert(std::make_tuple(static_cast<quint8>(i), packetType.at(j)), time);
            }
        }
        auto endTime = endTimeGenerator(engine);
        auto endDevice = endDeviceGenerator(engine);
        auto endId = endIdGenerator(engine);
        flowSynchronizer.handleStartRecording();
        for (auto i = 1; i <= nPacket; i++)
        {
            for (auto j = 0; j < nId; j++)
            {
                std::random_shuffle(std::begin(order), std::end(order));
                for (int k = 0; k < order.size(); k++)
                {
                    if (isStopNewData)
                        break;
                    TimeDataVector timeVector;
                    RawDataVector rawVector;
                    auto curDevice = order.at(k);
                    auto &currentFlowTime = sampleTime[std::make_tuple(static_cast<quint8>(j),
                                                                       packetType.at(curDevice))];
                    while(currentFlowTime < i * dT)
                    {
                        timeVector.append(currentFlowTime);
                        rawVector.append(emptySample);
                        currentFlowTime += periodList.at(curDevice);
                    }
                    if (!timeVector.isEmpty())
                        flowSynchronizer.handleNewData(QSharedPointer<RawPassingData>::create
                                                       (j, packetType.at(curDevice), timeVector, rawVector));
                    if (endTime < i * dT && endDevice == curDevice && endId == j && !isEnd)
                    {
                        flowSynchronizer.handleStopRecording();
                        isEnd = true;
                    }
                }
            }
        }

        for (auto i = timeMap.constBegin(); i != timeMap.constEnd(); ++i)
        {
            for (auto j = i + 1; j != timeMap.constEnd(); ++j)
            {
                //qDebug() << i->first() << j->first();
                //qDebug() << i->last() << j->last();
                QCOMPARE(i->first(), j->first());
                QCOMPARE(i->last(), j->last());
            }
        }

        timeMap.clear();
        sampleTime.clear();
        //flowSynchronizer.clear();
        isStopNewData = false;
    }
}
