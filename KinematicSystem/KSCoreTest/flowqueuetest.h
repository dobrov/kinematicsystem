#ifndef FLOWQUEUETEST_H
#define FLOWQUEUETEST_H

#include <QObject>
#include "autotest.h"

class FlowQueueTest : public QObject
{
    Q_OBJECT

private slots:
    void FlowQTest();
};

DECLARE_TEST(FlowQueueTest)
#endif // FLOWQUEUETEST_H
