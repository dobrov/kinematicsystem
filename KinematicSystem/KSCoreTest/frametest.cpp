#include "frametest.h"
#include <configuresingledevicepacket.h>
#include <configuretimerpacket.h>
#include <requestdatapacket.h>
#include <devicelistpacket.h>
#include <frame.h>

void FrameTest::createPacketTest()
{
    QByteArray raw;

    ConfigureAllDevicePacket cadp;
    raw.fill(0, 10);
    raw[0] = 0x12;
    raw[1] = 7;
    raw[2] = 0;
    QCOMPARE(cadp.getPacket(), raw);

    cadp.setDataRateAcc(1);
    cadp.setEnable(1);
    raw[3] = 1;
    raw[7] = 1;
    QCOMPARE(cadp.getPacket(), raw);

    ConfigureSingleDevicePacket csdp(1, 2, 3, 4, 5, 6, 7, 8, 9);
    raw.fill(0, 12);
    raw[0] = 0x13;
    raw[1] = 9;
    raw[2] = 0;
    for(int i = 3; i < 12; i++)
    {
        raw[i] = i - 2;
    }
    QCOMPARE(csdp.getPacket(), raw);

    ConfigureTimerPacket ctp;
    raw.fill(0, 5);
    raw[0] = 0x10;
    raw[1] = 2;
    raw[2] = 0;
    raw[3] = 0;
    raw[4] = 2;
    ctp.setReset(2);
    QCOMPARE(ctp.getPacket(), raw);

    RequestDataPacket rdp;
    raw.fill(0, 3);
    raw[0] = 0x32;
    QCOMPARE(rdp.getPacket(), raw);
}

void FrameTest::createFrameTest()
{
    QByteArray raw;
    Frame frame;

    ConfigureTimerPacket ctp;
    ctp.setEnable(1);
    frame.addPacket(ctp);
    raw.fill(0, 9);
    raw[0] = 0x53;
    raw[1] = 0x4B;
    raw[2] = 0x05;
    raw[3] = 0x00;
    raw[4] = 0x10;
    raw[5] = 0x2;
    raw[6] = 0x0;
    raw[7] = 0x1;
    raw[8] = 0x0;
    QCOMPARE(frame.getFrame(), raw);

    ConfigureSingleDevicePacket csdp(1, 1, 1, 0, 0, 0, 0, 0, 0);
    frame.addPacket(csdp);
    raw.resize(21);
    raw[2] = 0x11;
    raw[9] = 0x13;
    raw[10] = 0x9;
    raw[11] = 0x0;
    raw[12] = 0x1;
    raw[13] = 0x1;
    raw[14] = 0x1;
    raw[15] = 0x0;
    raw[16] = 0x0;
    raw[17] = 0x0;
    raw[18] = 0x0;
    raw[19] = 0x0;
    raw[20] = 0x0;
    QCOMPARE(frame.getFrame(), raw);

    frame.clear();
    RequestDataPacket rdp;
    frame.addPacket(rdp);
    raw.resize(7);
    raw[0] = 0x53;
    raw[1] = 0x4B;
    raw[2] = 0x03;
    raw[3] = 0x00;
    raw[4] = 0x32;
    raw[5] = 0x0;
    raw[6] = 0x0;
    QCOMPARE(frame.getFrame(), raw);
}
