#include "passingdatatest.h"
#include "concretepassingdata.h"

void PassingDataTest::PassDataTest()
{
    const qint16 size = 100;
    TimeDataVector time;
    RawDataVector data;
    quint8 deviceId = 5;
    PassingDataType dataType = PassingDataType::RawAccel;
    for (qint16 i = 0; i < size; i++)
    {
        time.append(static_cast<TimeSample>(i));
        data.append({i, i, i});
    }
    QSharedPointer<AbstractPassingData> ptrBaseF = QSharedPointer<RawPassingData>::create(
                deviceId, dataType, time, data);

    QCOMPARE(time, ptrBaseF->getTime());
    QCOMPARE(dataType, ptrBaseF->getDataType());
    QCOMPARE(deviceId, ptrBaseF->getDeviceId());

    ptrBaseF->append(ptrBaseF);
    auto ptrBaseS = ptrBaseF->split(size);

    QCOMPARE(ptrBaseF->getDataType(), ptrBaseS->getDataType());
    QCOMPARE(ptrBaseF->getDeviceId(), ptrBaseS->getDeviceId());
    QCOMPARE(ptrBaseF->getTime(), ptrBaseS->getTime());

    QSharedPointer<RawPassingData> ptrDeriveF = qSharedPointerCast<RawPassingData>(ptrBaseF);
    QSharedPointer<RawPassingData> ptrDeriveS = qSharedPointerCast<RawPassingData>(ptrBaseS);

    QCOMPARE(ptrDeriveF->getData(), ptrDeriveS->getData());
    QCOMPARE(ptrDeriveF->getData(), data);

    ptrBaseF->erase(size/4, AbstractPassingData::EraseType::Head);
    QCOMPARE(time.mid(size/4), ptrBaseF->getTime());
    QCOMPARE(data.mid(size/4), ptrDeriveF->getData());

    ptrBaseS->erase(size/4, AbstractPassingData::EraseType::Tail);
    QCOMPARE(time.mid(0, 3*size/4), ptrBaseS->getTime());
    QCOMPARE(data.mid(0, 3*size/4), ptrDeriveS->getData());

}
