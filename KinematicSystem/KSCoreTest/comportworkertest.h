#ifndef COMPORTWORKERTEST_H
#define COMPORTWORKERTEST_H

#include <QObject>
#include <QByteArray>
#include <QThread>
#include "autotest.h"

class ComPortWorkerTest : public QObject
{
    Q_OBJECT

private slots:
    void test1();
    void test2();
};

//DECLARE_TEST(ComPortWorkerTest)
#endif // COMPORTWORKERTEST_H
