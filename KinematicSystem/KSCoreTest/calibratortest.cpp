#include "calibratortest.h"
#include <calibrator.h>
#include <concretepassingdata.h>
#include <QTextStream>
#include <QRegularExpression>
#include <random>

void CalibratorTest::CalibratorWorkTest()
{
    Calibrator calibrator;
    const SensOrientArray signMap{{{1, -1, -1},
                                   {-1, 1, -1},
                                   {-1, -1, 1}}};
    calibrator.setSignMap(signMap);
    const SensOrientArray axisMap{{{RValueX, RValueY, RValueZ},
                                   {RValueY, RValueY, RValueX},
                                   {RValueZ, RValueY, RValueX}}};
    calibrator.setAxisMap(axisMap);
    const QMap<PassingDataType, int> typeMapper{{PassingDataType::RawAccel, 0},
                                                {PassingDataType::RawGyro, 1},
                                                {PassingDataType::RawMag, 2}};
    const float sens = 0.1f;
    calibrator.setSensitivity(sens, sens, sens);

    QMap<CalId, CalData> calMap;
    CalData calDataFirst;
    calDataFirst.m_bias = {0.0f, 1.0f, -1.0f};
    calDataFirst.m_scale = {{{0.0f, 1.0f, 0.0f},
                             {1.0f, 0.0f, 0.0f},
                             {0.0f, 0.0f, 1.0f}}};
    CalId calIdFirst = std::make_tuple(1, PassingDataType::RawAccel, sens);
    calMap.insert(calIdFirst, calDataFirst);

    CalData calDataSecond;
    calDataSecond.m_bias = {1.0f, -1.0f, 0.0f};
    calDataSecond.m_scale = {{{0.5f, 0.5f, 0.0f},
                              {0.0f, 0.5f, 0.5f},
                              {0.33f, 0.33f, 0.33f}}};
    CalId calIdSecond = std::make_tuple(1, PassingDataType::RawMag, sens);
    calMap.insert(calIdSecond, calDataSecond);

    calibrator.setCalDataMap(calMap);

    QMap<FlowId, CalDataVector> receivedData;
    QObject::connect(&calibrator, &Calibrator::sendData, [&receivedData](
                     const QSharedPointer<AbstractPassingData> &data)
    {
        quint8 id = data->getDeviceId();
        PassingDataType type = (data->getDataType() == PassingDataType::CalAccel)? PassingDataType::RawAccel :
                                                                                   PassingDataType::RawMag;
        receivedData[std::make_tuple(id, type)].append(qSharedPointerCast<CalPassingData>(data)->getData());
    });

    std::mt19937 engine;
    std::uniform_int_distribution<qint16> uniformInt;
    std::bernoulli_distribution newSample(0.2);
    const int nSamples = 1;
    const TimeSample emptyTime = 0;
    QMap<FlowId, CalDataVector> calibratedDataMap;
    for (auto it = calMap.cbegin(); it != calMap.cend(); ++it)
    {
        RawDataVector rawDataVector;
        TimeDataVector time;
        quint8 id = std::get<CId>(it.key());
        PassingDataType type = std::get<CType>(it.key());
        const auto &calData = it.value();

        const auto &curAxisMap = axisMap.at(typeMapper.value(type));
        const auto &curSignMap = signMap.at(typeMapper.value(type));

        CalDataVector calDataVector;
        for (int i = 0; i < nSamples; i++)
        {
            time.append(emptyTime);
            RawSample rawSample{uniformInt(engine), uniformInt(engine), uniformInt(engine)};
            rawDataVector.append(rawSample);

            float x = curSignMap.at(0) * rawSample.at(curAxisMap.at(0)) * sens;
            float y = curSignMap.at(1) * rawSample.at(curAxisMap.at(1)) * sens;
            float z = curSignMap.at(2) * rawSample.at(curAxisMap.at(2)) * sens;

            x -= calData.m_bias.at(0);
            y -= calData.m_bias.at(1);
            z -= calData.m_bias.at(2);

            float x_c = x * calData.m_scale.at(0).at(0) +
                    y * calData.m_scale.at(0).at(1) +
                    z * calData.m_scale.at(0).at(2);
            float y_c = x * calData.m_scale.at(1).at(0) +
                    y * calData.m_scale.at(1).at(1) +
                    z * calData.m_scale.at(1).at(2);
            float z_c = x * calData.m_scale.at(2).at(0) +
                    y * calData.m_scale.at(2).at(1) +
                    z * calData.m_scale.at(2).at(2);

            CalSample calSample{x_c, y_c, z_c};
            calDataVector.append(calSample);

            if (newSample(engine) || i == nSamples - 1)
            {
                calibrator.handleNewData(QSharedPointer<RawPassingData>::create(id, type, time, rawDataVector));
                time.clear();
                rawDataVector.clear();
            }
        }
        calibratedDataMap[std::make_tuple(id, type)].append(calDataVector);
    }

    QCOMPARE(calibratedDataMap, receivedData);
}
