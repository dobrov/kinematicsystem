#ifndef FLOWSYNCHRONIZERTEST_H
#define FLOWSYNCHRONIZERTEST_H

#include <QObject>
#include "autotest.h"

class FlowSynchronizerTest : public QObject
{
    Q_OBJECT

private slots:
    void FlowSyncTest();
};

DECLARE_TEST(FlowSynchronizerTest)
#endif // FLOWSYNCHRONIZERTEST_H
