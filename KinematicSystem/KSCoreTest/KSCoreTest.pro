#-------------------------------------------------
#
# Project created by QtCreator 2015-08-24T16:47:43
#
#-------------------------------------------------

QT       += testlib gui

include(../KSCore/KSCore.pri)

TARGET = kscoretest
CONFIG   += console c++14
CONFIG   -= app_bundle

TEMPLATE = app

HEADERS += \
    autotest.h \
    framedecodertest.h \
    passingdatatest.h \
    frametest.h \
    flowqueuetest.h \
    flowsynchronizertest.h \
    linearinterpolatortest.h \
    magimpulsenoisefiltertest.h \
    calibratortest.h \
    complementaryfusionfiltertest.h \
    datasystemtest.h \
    iotest.h
SOURCES += \
    main.cpp \
    framedecodertest.cpp \
    passingdatatest.cpp \
    frametest.cpp \
    flowqueuetest.cpp \
    flowsynchronizertest.cpp \
    linearinterpolatortest.cpp \
    magimpulsenoisefiltertest.cpp \
    calibratortest.cpp \
    complementaryfusionfiltertest.cpp \
    datasystemtest.cpp \
    iotest.cpp

DEFINES += SRCDIR=\\\"$$PWD/\\\"
