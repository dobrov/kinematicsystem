#include "comportworkertest.h"
#include <comportworker.h>
#include <logger.h>
#include <framedecoder.h>
#include <framecreator.h>
#include <configuretimerpacket.h>
#include <configurealldevicepacket.h>
#include <devicelistpacket.h>
#include <requestdatapacket.h>
#include <datasystem.h>
#include <functional>
#include <QDebug>

void ComPortWorkerTest::test1()
{
    ComPortWorker *comWorker = new ComPortWorker();
    Logger *logger = new Logger(stdout);
    QThread *threadCom = new QThread(this);
    QThread *threadLogger = new QThread(this);
    qDebug() << "Main thread: " << QThread::currentThread();
    comWorker->moveToThread(threadCom);
    logger->moveToThread(threadLogger);

    QTimer::singleShot(100000, comWorker, &ComPortWorker::finishWork);

    connect(comWorker, &ComPortWorker::readDataAvailable, [](const QByteArray &data)
    {
        qDebug() << "Read Data: " << data.toHex();
    });
    connect(comWorker, &ComPortWorker::opened, [comWorker]()
    {
        QByteArray writeData;
        writeData.resize(7);
        writeData[0] = 0x53;
        writeData[1] = 0x4B;
        writeData[2] = 0x03;
        writeData[3] = 0x00;
        writeData[4] = 0x00;
        writeData[5] = 0x00;
        writeData[6] = 0x00;
        comWorker->write(writeData);
    });
    connect(comWorker, &ComPortWorker::message, logger, &Logger::handleMessage);
    connect(threadLogger, SIGNAL(started()), logger, SLOT(startWork()));
    connect(threadCom, SIGNAL(started()), comWorker, SLOT(startWork()));

    threadLogger->start();
    threadCom->start();

    QTest::qWait(100000);
}

void ComPortWorkerTest::test2()
{
    ComPortWorker *comWorker = new ComPortWorker();
    //comWorker->setMessageRawData(true);
    Logger *logger = new Logger("log.txt");
    FrameDecoder *frameDecoder = new FrameDecoder();
    DataSystem *dataSystem = new DataSystem();
    QThread *thread = new QThread(this);
    QThread *threadLogger = new QThread(this);
    comWorker->moveToThread(thread);
    logger->moveToThread(threadLogger);
    frameDecoder->moveToThread(thread);
    //dataSystem->moveToThread(thread);

    connect(comWorker, &ComPortWorker::readDataAvailable, frameDecoder, &FrameDecoder::parseFrame);
    connect(comWorker, &ComPortWorker::readDataAvailable, [comWorker]()
    {
        static bool isFirst = true;
        if(isFirst)
        {
            FrameCreator creator;
            RequestDataPacket reqPacket;
            creator.addPacket(reqPacket.getPacket());
            QTest::qSleep(500);
            comWorker->write(creator.getFrame(), true);
            isFirst = false;
        }
    });

    connect(comWorker, &ComPortWorker::opened, [comWorker]()
    {
        FrameCreator creator;
        ConfigureTimerPacket timerPacket;
        timerPacket.setEnable(1);
        timerPacket.setReset(1);
        ConfigureAllDevicePacket devicePacket;
        devicePacket.setEnable(1);
        creator.addPacket(timerPacket.getPacket());
        creator.addPacket(devicePacket.getPacket());
        comWorker->write(creator.getFrame());
    });


    connect(dataSystem, &AbstractWorker::message, logger, &Logger::handleMessage);
    connect(frameDecoder, &AbstractWorker::message, logger, &Logger::handleMessage);
    connect(comWorker, &AbstractWorker::message, logger, &Logger::handleMessage);
    connect(logger, &AbstractWorker::message, logger, &Logger::handleMessage);
    connect(frameDecoder, &FrameDecoder::sensorData, dataSystem, &DataSystem::handleSensorData);

    connect(threadLogger, SIGNAL(started()), logger, SLOT(startWork()));
    connect(thread, SIGNAL(started()), comWorker, SLOT(startWork()));
    connect(thread, SIGNAL(started()), frameDecoder, SLOT(startWork()));
    threadLogger->start();
    thread->start();
    QTimer::singleShot(99000, [dataSystem](){
        dataSystem->saveToJson("test.json");
    });
    QTest::qWait(100000);

}
