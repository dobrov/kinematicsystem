#include "flowqueuetest.h"
#include <flowqueue.h>
#include <functional>
#include <QCoreApplication>
#include <QTimer>
#include <QDebug>
#include <concretepassingdata.h>
#include <algorithm>

void FlowQueueTest::FlowQTest()
{

    const qint64 deltaError = 10; // ms - max possible error
    const int testDuration = 5000; //ms
    const TimeSample delay = 30000; // micro second
    const TimeSample chunkLen = 10;
    const TimeSample accelPeriod = 2000; // micro second
    const TimeSample quatPeriod = 1000; // micro second
    const QuatSample quatSample{0.0, 0.0, 0.0, 0.0};
    const RawSample accelSample{0, 0, 0};

    QMap<PassingDataType, TimeSample> genPeriods{{PassingDataType::RawAccel, accelPeriod},
                                                 {PassingDataType::Quat, quatPeriod}};
    TimeDataVector timeData;
    RawDataVector accelData;
    QList<QSharedPointer<AbstractPassingData>> accelPacketArray;
    int curChunkSize = 0;
    for (TimeSample time = 0; time < testDuration * 1000; time += accelPeriod)
    {
        if (curChunkSize == chunkLen)
        {
            curChunkSize = 0;
            accelPacketArray.append(QSharedPointer<RawPassingData>::create(0, PassingDataType::RawAccel,
                                                                           timeData, accelData));
            timeData.clear();
            accelData.clear();
        }
        timeData.append(time);
        accelData.append(accelSample);
        curChunkSize++;
    }

    QuatDataVector quatData;
    QList<QSharedPointer<AbstractPassingData>> quatPacketArray;
    timeData.clear();
    curChunkSize = 0;
    for (TimeSample time = 0; time < testDuration * 1000; time += quatPeriod)
    {
        if (curChunkSize == chunkLen)
        {
            curChunkSize = 0;
            quatPacketArray.append(QSharedPointer<QuatPassingData>::create(0, PassingDataType::Quat,
                                                                                 timeData, quatData));
            timeData.clear();
            quatData.clear();
        }
        timeData.append(time);
        quatData.append(quatSample);
        curChunkSize++;
    }
    QTimer quatTimer;
    quatTimer.setTimerType(Qt::PreciseTimer);
    QTimer accelTimer;
    accelTimer.setTimerType(Qt::PreciseTimer);


    FlowQueue flow;
    flow.setRefreshInterval(0);
    flow.setDelay(delay);
    flow.setFlow(0, PassingDataType::RawAccel);
    flow.setFlow(0, PassingDataType::Quat);

    auto chunkSender = [&flow](QList<QSharedPointer<AbstractPassingData>> &data)
    {
        if (!data.isEmpty())
            flow.handleNewData(data.takeFirst());
    };

    QObject::connect(&accelTimer, &QTimer::timeout, std::bind(chunkSender, std::ref(accelPacketArray)));
    QObject::connect(&quatTimer, &QTimer::timeout, std::bind(chunkSender, std::ref(quatPacketArray)));

    QMap<PassingDataType, QVector<TimeSample>> timeStamps;
    timeStamps.insert(PassingDataType::RawAccel, QVector<TimeSample>());
    timeStamps.insert(PassingDataType::Quat, QVector<TimeSample>());
    QObject::connect(&flow, &FlowQueue::sendData, [&timeStamps](QSharedPointer<AbstractPassingData> data)
    {
        static QElapsedTimer timer;
        if (!timer.isValid())
        {
            timer.start();
            return;
        }

        timeStamps[data->getDataType()].append(timer.nsecsElapsed());
    });

    quatTimer.start(quatPeriod * chunkLen / 1000);
    accelTimer.start(accelPeriod * chunkLen / 1000);
    QTimer::singleShot(testDuration, [&timeStamps, &genPeriods, deltaError]()
    {
        for (auto it = timeStamps.cbegin(); it != timeStamps.cend(); ++it)
        {
            QVector<TimeSample> intervals;
            const auto &timeStamp = it.value();
            for (int i = 1; i < timeStamp.size(); i++)
            {
                intervals.append(timeStamp[i] - timeStamp[i-1]);
            }
            //mean
            int size = intervals.size();
            TimeSample mean = std::accumulate(std::begin(intervals), std::end(intervals),
            static_cast<TimeSample>(0), [size](TimeSample sum, TimeSample interval) -> auto
            {
                    return sum + interval / size;
            });
            QVERIFY(std::abs(static_cast<qint64>(mean / 1000 - genPeriods.value(it.key()))) < deltaError);
        }
        QCoreApplication::instance()->quit();
    });
    QCoreApplication::instance()->exec();
}
