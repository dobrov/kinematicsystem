#ifndef FRAMEDECODERTEST_H
#define FRAMEDECODERTEST_H

#include <QObject>
#include "autotest.h"

class FrameDecoderTest : public QObject
{
    Q_OBJECT

private slots:
    void FrameDecTest();
};

DECLARE_TEST(FrameDecoderTest)
#endif // FRAMEDECODERTEST_H
