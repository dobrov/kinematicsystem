#ifndef LINEARINTERPOLATORTEST_H
#define LINEARINTERPOLATORTEST_H

#include <QObject>
#include <autotest.h>

class LinearInterpolatorTest : public QObject
{
    Q_OBJECT

private slots:
    void LinearInterpTest();
};

DECLARE_TEST(LinearInterpolatorTest)
#endif // LINEARINTERPOLATORTEST_H
