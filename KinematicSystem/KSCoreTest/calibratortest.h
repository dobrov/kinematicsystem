#ifndef CALIBRATORTEST_H
#define CALIBRATORTEST_H

#include <QObject>
#include "autotest.h"

class CalibratorTest : public QObject
{
    Q_OBJECT

private slots:
    void CalibratorWorkTest();
};

DECLARE_TEST(CalibratorTest)
#endif // CALIBRATORTEST_H
