#ifndef FRAMECREATORTEST_H
#define FRAMECREATORTEST_H

#include <QObject>
#include "autotest.h"

class FrameTest : public QObject
{
    Q_OBJECT

private slots:
    void createPacketTest();
    void createFrameTest();
};

DECLARE_TEST(FrameTest)
#endif // FRAMECREATORTEST_H
