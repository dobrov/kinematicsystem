#ifndef IOTEST_H
#define IOTEST_H

#include <QObject>
#include "autotest.h"

class IOTest : public QObject
{
    Q_OBJECT

private slots:
    void offsetTest();
    void calTest();
};

DECLARE_TEST(IOTest)
#endif // IOTEST_H
