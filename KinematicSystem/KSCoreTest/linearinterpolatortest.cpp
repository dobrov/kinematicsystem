#include "linearinterpolatortest.h"
#include <linearinterpolator.h>

void LinearInterpolatorTest::LinearInterpTest()
{
    const quint16 accelFreq = 100;
    constexpr TimeSample accelPeriodMicro = 1000000 / accelFreq;
    const int size = 100;

    LinearInterpolator interpolator;
    interpolator.setDataRate(accelFreq, 100, 50);
    TimeDataVector interpTime;
    RawDataVector interpData;
    QObject::connect(&interpolator, &AbstractPassingDataHandler::sendData, [&interpTime, &interpData]
                     (const QSharedPointer<AbstractPassingData> &passingData)
    {
        interpTime.append(passingData->getTime());
        interpData.append(qSharedPointerCast<RawPassingData>(passingData)->getData());
    });

    TimeDataVector inTime;
    for (int i = 1; i <= size; i++)
    {
        inTime.append(static_cast<TimeSample>((i - 0.5) * accelPeriodMicro ));
    }

    RawDataVector inData;
    for (int i = 0; i < size; i++)
    {
        inData.append({static_cast<qint16>(i * 2), static_cast<qint16>(i * 4),
                       static_cast<qint16>(i * 8)});
    }

    for (int j = 0; j < 2; j++)
    {
        auto secondPart = QSharedPointer<RawPassingData>::create(1, PassingDataType::RawAccel, inTime, inData);
        auto firstPart = secondPart->split(size/2);
        interpolator.handleNewData(firstPart);

        for (int i = 0; i < size/2 - 1; i++)
        {
            QCOMPARE(interpTime.at(i), accelPeriodMicro * (i + 1));
            auto &sample = interpData.at(i);
            QCOMPARE(std::get<RValueX>(sample), static_cast<qint16>(1 + 2 * i));
            QCOMPARE(std::get<RValueY>(sample), static_cast<qint16>(2 + 4 * i));
            QCOMPARE(std::get<RValueZ>(sample), static_cast<qint16>(4 + 8 * i));
        }
        interpolator.handleNewData(secondPart);

        for (int i = size/2; i < size - 1; i++)
        {
            QCOMPARE(interpTime.at(i), accelPeriodMicro * (i + 1));
            auto &sample = interpData.at(i);
            QCOMPARE(std::get<RValueX>(sample), static_cast<qint16>(1 + 2 * i));
            QCOMPARE(std::get<RValueY>(sample), static_cast<qint16>(2 + 4 * i));
            QCOMPARE(std::get<RValueZ>(sample), static_cast<qint16>(4 + 8 * i));
        }

        interpolator.clear();
    }
}
