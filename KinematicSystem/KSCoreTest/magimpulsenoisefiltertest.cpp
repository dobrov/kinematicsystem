#include "magimpulsenoisefiltertest.h"
#include <magimpulsenoisefilter.h>

void MagImpulseNoiseFilterTest::FilterTest()
{
    MagImpulseNoiseFilter filter;
    RawDataVector filteredMagData;
    QObject::connect(&filter, &MagImpulseNoiseFilter::sendData, [&filteredMagData](const AbstractPassingDataPointer &data)
    {
        filteredMagData.append(qSharedPointerCast<RawPassingData>(data)->getData());
    });

    RawDataVector unfilteredMagData1;
    RawDataVector unfilteredMagData2;
    TimeDataVector timeData1;
    TimeDataVector timeData2;
    RawSample sample{1000, 0, 0};
    RawSample brokenSample{0, 0, 0};
    for (int i = 0; i < 3; i++)
    {
        unfilteredMagData1.append(sample);
        timeData1.append(0);
    }
    unfilteredMagData2 = unfilteredMagData1;
    unfilteredMagData1.append(brokenSample);
    timeData2 = timeData1;
    timeData1.append(0);

    filter.handleNewData(QSharedPointer<RawPassingData>::create(1, PassingDataType::RawMag, timeData1, unfilteredMagData1));
    filter.handleNewData(QSharedPointer<RawPassingData>::create(1, PassingDataType::RawMag, timeData2, unfilteredMagData2));

    auto trueFilteredMagData = unfilteredMagData2;
    trueFilteredMagData.append(unfilteredMagData2);
    QCOMPARE(filteredMagData, trueFilteredMagData);
}
