#ifndef MAGIMPULSENOISEFILTERTEST_H
#define MAGIMPULSENOISEFILTERTEST_H

#include <QObject>
#include "autotest.h"

class MagImpulseNoiseFilterTest : public QObject
{
    Q_OBJECT

private slots:
    void FilterTest();
};

DECLARE_TEST(MagImpulseNoiseFilterTest)
#endif // MAGIMPULSENOISEFILTERTEST_H
