#include "iotest.h"
#include "kstypes.h"
#include <random>
#include <limits>
#include <functional>
#include <QRegularExpression>

void IOTest::offsetTest()
{
    static const uint number = 2;
    std::mt19937 engine;
    OffsetData offsetData;

    std::uniform_int_distribution<> idDistribution(std::numeric_limits<quint8>::min(),
                                                   std::numeric_limits<quint8>::max());
    std::uniform_real_distribution<float> quatDistribution(-1.0f, 1.0f);
    std::uniform_real_distribution<float> lengthDistribution(0.1f, 100.0f);
    auto quatGen = std::bind(quatDistribution, engine);
    static const auto getRandomQuat = [&quatGen]()
    {
        return QQuaternion(quatGen(), quatGen(), quatGen(), quatGen());
    };

    static const auto metaEnum = QMetaEnum::fromType<BodyPart>();
    for (int i = 0; i < metaEnum.keyCount(); i++)
    {
        DeviceList deviceList;
        BodyPartParam bodyPartOffset;
        BodyPartParam bodyPartInit;
        for (uint j = 0; j < number; j++)
        {
            deviceList.append(idDistribution(engine));
            auto length = lengthDistribution(engine);
            BoneParam boneOffset = std::make_tuple(length, getRandomQuat());
            bodyPartOffset.append(boneOffset);
            BoneParam boneInit = std::make_tuple(length, getRandomQuat());
            bodyPartInit.append(boneInit);
        }
        offsetData.m_deviceList.insert(static_cast<BodyPart>(metaEnum.value(i)), deviceList);
        offsetData.m_bodyOffset.insert(static_cast<BodyPart>(metaEnum.value(i)), bodyPartOffset);
        offsetData.m_bodyInit.insert(static_cast<BodyPart>(metaEnum.value(i)), bodyPartInit);
    }

    offsetData.m_quatEG = getRandomQuat();
    offsetData.m_hipLength = QVector3D(lengthDistribution(engine), lengthDistribution(engine),
                                       lengthDistribution(engine));

    writeOffsetFile(QStringLiteral("test.offset"), offsetData);

    auto fileNameList = findOffsetFiles(QDir::current().absolutePath());
    QCOMPARE(fileNameList.size(), 1);

    auto restoredOffsetData = readOffsetFile(fileNameList.first());

    QCOMPARE(offsetData, restoredOffsetData);
    QDir::current().remove(fileNameList.first());
}

void IOTest::calTest()
{
    const int nFiles = 20;

    QMap<CalId, CalData> calMap;
    std::mt19937 engine;
    std::uniform_real_distribution<float> floatGenerator;
    std::uniform_int_distribution<> intGenerator(1, 100);
    std::uniform_int_distribution<> typeGenerator(0, 2);
    for (int i = 0; i < nFiles; i++)
    {
        quint8 id = intGenerator(engine);
        CalData calData;
        calData.m_bias = {floatGenerator(engine), floatGenerator(engine), floatGenerator(engine)};
        calData.m_scale = {{{floatGenerator(engine), floatGenerator(engine), floatGenerator(engine)},
                           {floatGenerator(engine), floatGenerator(engine), floatGenerator(engine)},
                           {floatGenerator(engine), floatGenerator(engine), floatGenerator(engine)}}};
        PassingDataType type;
        switch (typeGenerator(engine))
        {
        case 0:
            type = PassingDataType::RawAccel;
            break;
        case 1:
            type = PassingDataType::RawGyro;
            break;
        case 2:
            type = PassingDataType::RawMag;
            break;
        default:
            Q_ASSERT(false);
        }

        quint16 fullScale = intGenerator(engine);
        auto calId = std::make_tuple(id, type, fullScale);
        auto fileName = getCalFileName(calId);
        writeCalFile(fileName, calData);
        calMap.insert(calId, calData);
    }

    auto calId = std::make_tuple(1, PassingDataType::RawGyro, 2000);
    auto brokenFileName = getCalFileName(calId);
    QFile file(brokenFileName);
    file.open(QFile::WriteOnly);
    QTextStream out(&file);
    out << QStringLiteral("");
    file.close();

    auto calFilesMap = findCalFiles(QDir::currentPath());
    CalDataMap resCalDataMap;
    static const CalData emptyData;
    for (auto it = calFilesMap.begin(); it != calFilesMap.end(); ++it)
    {
        const auto fileName = it.value();
        const auto calData = readCalFile(fileName);
        if (calData == emptyData)
        {
            QCOMPARE(fileName, QDir::current().filePath(brokenFileName));
            continue;
        }
        resCalDataMap.insert(it.key(), calData);
    }
    QCOMPARE(resCalDataMap, calMap);

    QRegularExpression regExp(QStringLiteral("^[\\w]+CalDataDevice[\\d]+Range[\\d+]+.json$"));
    const auto fileList = QDir::current().entryList(QDir::Files);
    for (const auto &fileName : fileList)
    {
        if(regExp.match(fileName).hasMatch())
            QDir::current().remove(fileName);
    }
}
