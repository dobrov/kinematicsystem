#ifndef DATASYSTEMTEST_H
#define DATASYSTEMTEST_H

#include <QObject>
#include "autotest.h"

class DataSystemTest : public QObject
{
    Q_OBJECT

private slots:
    void dataSysTest();
};

DECLARE_TEST(DataSystemTest)
#endif // DATASYSTEMTEST_H
