#include "framedecodertest.h"
#include <QByteArray>
#include <QVector>
#include <framedecoder.h>
#include <concretepassingdata.h>
#include <random>

void FrameDecoderTest::FrameDecTest()
{
    QByteArray connectedDeviceFrame = QByteArray::fromHex("534b0d00810a0002010202020302040205");
    DeviceList connectedDeviceList{6,7,8,9,10};
    QByteArray ackFrame = QByteArray::fromHex("534b0900700000700000700000");
    const auto numberOfAck = 3;
    QByteArray dataPacket = QByteArray::fromHex("534b3200a008000000000000000000a20a0002010000d619a0c5450"
                                                "5a30a00020100000e000600e0ffa40a000201000024f2741cef01");
    QSharedPointer<RawPassingData> accelData(new RawPassingData(6, PassingDataType::RawAccel,
        TimeDataVector{0}, RawDataVector{{6614, -14944, 1349}}));
    QSharedPointer<RawPassingData> gyroData(new RawPassingData(6, PassingDataType::RawGyro,
        TimeDataVector{0}, RawDataVector{{14, 6, -32}}));
    QSharedPointer<RawPassingData> magData(new RawPassingData(6, PassingDataType::RawMag,
        TimeDataVector{0}, RawDataVector{{-3548, 7284, 495}}));

    QByteArray incomingMessage = connectedDeviceFrame + ackFrame + dataPacket;
    QVector<QByteArray> randomSplitMessage;
    std::mt19937 engine;
    const auto newMessageProb = 0.1;
    std::bernoulli_distribution newMessageDist(newMessageProb);
    auto beginIndex = 0;
    for (auto i = 1; i < incomingMessage.size(); i++)
    {
        if (newMessageDist(engine))
        {
            randomSplitMessage.push_back(incomingMessage.mid(beginIndex, i - beginIndex));
            beginIndex = i;
        }
    }
    randomSplitMessage.push_back(incomingMessage.mid(beginIndex));

    FrameDecoder frameDec;
    DeviceList decodedDeviceList;
    QObject::connect(&frameDec, &FrameDecoder::connectedDeviceData, [&decodedDeviceList](const DeviceList &deviceList)
    {
        decodedDeviceList = deviceList;
    });

    auto nAckReceived = 0;
    QObject::connect(&frameDec, &FrameDecoder::ackReceived, [&nAckReceived]()
    {
        ++nAckReceived;
    });

    QSharedPointer<RawPassingData> decodedAccelData;
    QSharedPointer<RawPassingData> decodedGyroData;
    QSharedPointer<RawPassingData> decodedMagData;
    QObject::connect(&frameDec, &FrameDecoder::sendData, [&decodedAccelData, &decodedGyroData, &decodedMagData]
                     (const AbstractPassingDataPointer &data)
    {
        switch (data->getDataType()) {
        case PassingDataType::RawAccel:
            decodedAccelData = qSharedPointerCast<RawPassingData>(data);
            return;
        case PassingDataType::RawGyro:
            decodedGyroData = qSharedPointerCast<RawPassingData>(data);
            return;
        case PassingDataType::RawMag:
            decodedMagData = qSharedPointerCast<RawPassingData>(data);
            return;
        default:
            Q_ASSERT(false);
        }
    });

    for (const auto &message : randomSplitMessage)
    {
        frameDec.parseFrame(message);
    }

    QCOMPARE(connectedDeviceList, decodedDeviceList);
    QCOMPARE(numberOfAck, nAckReceived);
    QCOMPARE(accelData->getTime(), decodedAccelData->getTime());
    QCOMPARE(accelData->getData(), decodedAccelData->getData());
    QCOMPARE(gyroData->getTime(), decodedGyroData->getTime());
    QCOMPARE(gyroData->getData(), decodedGyroData->getData());
    QCOMPARE(magData->getTime(), decodedMagData->getTime());
    QCOMPARE(magData->getData(), decodedMagData->getData());
}
