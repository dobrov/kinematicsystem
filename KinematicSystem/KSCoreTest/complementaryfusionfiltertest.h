#ifndef COMPLEMENTARYFILTERTEST_H
#define COMPLEMENTARYFILTERTEST_H

#include <QObject>
#include "autotest.h"

class ComplementaryFusionFilterTest : public QObject
{
    Q_OBJECT

private slots:
    void SimDataTest();
};

DECLARE_TEST(ComplementaryFusionFilterTest)
#endif // COMPLEMENTARYFILTERTEST_H
