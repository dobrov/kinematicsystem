#include "complementaryfusionfiltertest.h"
#include <QDebug>
#include <QQuaternion>
#include <QVector3D>
#include <complementaryfusionfilter.h>
#include <complementaryfusionfiltersettings.h>
#include <qmath.h>

void ComplementaryFusionFilterTest::SimDataTest()
{
    struct MeasuredData
    {
        float ax;
        float ay;
        float az;
        float mx;
        float my;
        float mz;
        float wx;
        float wy;
        float wz;
    };

    QVector<MeasuredData> measuredData;
    measuredData.reserve(1080);

    QVector3D g(0, 0, 1);
    QVector3D l(cos(M_PI/3), 0, -sin(M_PI/3));

    for (int i = 1; i <= 360; i++)
    {        
        QQuaternion quat = QQuaternion::fromAxisAndAngle({-1, 0, 0}, i);
        QVector3D rotG = quat.rotatedVector(g);
        QVector3D rotL = quat.rotatedVector(l);
        measuredData.push_back(MeasuredData {rotG.x(), rotG.y(), rotG.z(),
                                             rotL.x(), rotL.y(), rotL.z(),
                                             (1 * deg2rad()), 0, 0});
    }
    for (int i = 1; i <= 360; i++)
    {
        QQuaternion quat = QQuaternion::fromAxisAndAngle({0, -1, 0}, i);
        QVector3D rotG = quat.rotatedVector(g);
        QVector3D rotL = quat.rotatedVector(l);
        measuredData.push_back(MeasuredData {rotG.x(), rotG.y(), rotG.z(),
                                             rotL.x(), rotL.y(), rotL.z(),
                                             0, (1 * deg2rad()), 0});
    }
    for (int i = 1; i <= 360; i++)
    {
        QQuaternion quat = QQuaternion::fromAxisAndAngle({0, 0, -1}, i);
        QVector3D rotG = quat.rotatedVector(g);
        QVector3D rotL = quat.rotatedVector(l);
        measuredData.push_back(MeasuredData {rotG.x(), rotG.y(), rotG.z(),
                                             rotL.x(), rotL.y(), rotL.z(),
                                             0, 0, (1 * deg2rad())});
    }

    AbstractFusionFilter *filter = new ComplementaryFusionFilter(1);
    AbstractFilterSettingsPointer filterSettings = QSharedPointer<ComplementaryFusionFilterSettings>::create();
    filterSettings->setDoBiasEstimation(false);
    filter->setSettings(filterSettings);
    for(int i = 0; i < 1080; i++)
    {
        const auto &data = measuredData.at(i);
        filter->updateMARG(data.ax, data.ay, data.az, data.wx, data.wy,
                           data.wz, data.mx, data.my, data.mz);
        float scalar, quatX, quatY, quatZ;
        auto orientation = filter->getOrientation();
        scalar = std::get<QValueS>(orientation);
        quatX = std::get<QValueX>(orientation);
        quatY = std::get<QValueY>(orientation);
        quatZ = std::get<QValueZ>(orientation);

        float gx, gy, gz;
        rotateVectorByQuaternion(data.ax, data.ay, data.az, scalar, quatX,
                                 quatY, quatZ, gx, gy, gz);

        float lx, ly, lz;
        rotateVectorByQuaternion(data.mx, data.my, data.mz, scalar, quatX,
                                 quatY, quatZ, lx, ly, lz);

        QVERIFY(std::fabs(gx) < 0.001);
        QVERIFY(std::fabs(gy) < 0.001);
        QVERIFY(std::fabs(1 - gz) < 0.001);
        QVERIFY(std::fabs(cos(M_PI/3) - lx) < 0.001);
        QVERIFY(std::fabs(ly) < 0.001);
        QVERIFY(std::fabs(-sin(M_PI/3) - lz) < 0.001);
    }

    filter->clear();
    for(int i = 0; i < 720; i++)
    {
        const MeasuredData &data = measuredData.at(i);
        filter->updateIMU(data.ax, data.ay, data.az, data.wx, data.wy, data.wz);
        float scalar, quatX, quatY, quatZ;
        auto orientation = filter->getOrientation();
        scalar = std::get<QValueS>(orientation);
        quatX = std::get<QValueX>(orientation);
        quatY = std::get<QValueY>(orientation);
        quatZ = std::get<QValueZ>(orientation);

        float gx, gy, gz;
        rotateVectorByQuaternion(data.ax, data.ay, data.az, scalar, quatX,
                                 quatY, quatZ, gx, gy, gz);
        QVERIFY(std::fabs(gx) < 0.001);
        QVERIFY(std::fabs(gy) < 0.001);
        QVERIFY(std::fabs(1 - gz) < 0.001);
    }
}
