#include "datasystemtest.h"
#include <datasystem.h>
#include <concretepassingdata.h>
#include <timepassingdata.h>
#include <random>
#include <limits>
#include <QTimer>

QVector3D genVector3D()
{
    static std::mt19937 engine;
    std::uniform_real_distribution<float> dist(-5000.0f, 5000.0f);
    return QVector3D(dist(engine), dist(engine), dist(engine));
}

template <typename T>
T genSample(std::enable_if_t<!std::is_arithmetic<typename T::mapped_type>::value>* = nullptr);

template <>
PosSample genSample(std::enable_if_t<!std::is_arithmetic<typename PosSample::mapped_type>::value>*)
{
    static const auto metaEnum = QMetaEnum::fromType<BodyPart>();
    static std::mt19937 engine;
    //std::uniform_int_distribution<> nBodyPartsDistr(1, metaEnum.keyCount());
    std::uniform_int_distribution<> nJointsDistr(1, 10);

    static const int nBodyParts = 2;
    static const int nJoints = nJointsDistr(engine);
    PosSample sample;
    for (int i = 0; i < nBodyParts; i++)
    {
        QVector<QVector3D> bodyPartData;
        for (int j = 0; j < nJoints; j++)
            bodyPartData.append(genVector3D());
        sample.insert(static_cast<BodyPart>(metaEnum.value(i)), bodyPartData);
    }
    return sample;
}

template <typename T>
T genSample(std::enable_if_t<std::is_integral<typename T::value_type>::value>* = nullptr)
{
    static std::mt19937 engine;
    std::uniform_int_distribution<typename T::value_type> dist(
                std::numeric_limits<typename T::value_type>::min(),
                std::numeric_limits<typename T::value_type>::max());
    T sample;
    for (auto &part : sample)
        part = dist(engine);

    return sample;
}

template <typename T>
T genSample(std::enable_if_t<std::is_floating_point<typename T::value_type>::value>* = 0)
{
    static std::mt19937 engine;
    std::uniform_real_distribution<typename T::value_type> dist(-5000.0f, 5000.0f);
    T sample;
    for (auto &part : sample)
        part = dist(engine);

    return sample;
}

auto genTime(uint length)
{
    static std::mt19937 engine;
    std::uniform_int_distribution<TimeSample> timeDistr(0, 10000);
    QList<TimeSample> timeList;
    TimeSample prevTime = 0;
    for (uint i = 0; i < length; i++)
    {
        prevTime += timeDistr(engine);
        timeList.append(prevTime);
    }
    return timeList;
}

template <typename T>
DataVector<T> genData(uint length)
{
    DataVector<T> data;
    for (uint i = 0; i < length; i++)
        data.append(genSample<T>());
    return data;
}

template <typename T>
AbstractPassingDataPointer genPassingData(const FlowId &flowId, uint length)
{

    auto time = genTime(length);
    auto data = genData<T>(length);
    return QSharedPointer<ConcretePassingData<T>>::create(flowId, time, data);
}

template <>
AbstractPassingDataPointer genPassingData<TimeSample>(const FlowId &flowId, uint length)
{
    auto time = genTime(length);
    return QSharedPointer<TimePassingData>::create(std::get<FPassingDataType>(flowId), time);
}

auto genPassingDataVector(const FlowId &flowId, uint length, uint nChunks)
{
    Q_ASSERT(nChunks <= length);
    Q_ASSERT(!(length % nChunks));

    QList<AbstractPassingDataPointer> pList;
    auto chunkLen = length / nChunks;
    for (uint i = 0; i < nChunks; i++)
    {
        auto dataType = std::get<FPassingDataType>(flowId);
        switch (dataType)
        {
        case PassingDataType::RawAccel:
        case PassingDataType::RawGyro:
        case PassingDataType::RawMag:
            pList.append(genPassingData<RawSample>(flowId, chunkLen));
            break;
        case PassingDataType::CalAccel:
        case PassingDataType::CalGyro:
        case PassingDataType::CalMag:
            pList.append(genPassingData<CalSample>(flowId, chunkLen));
            break;
        case PassingDataType::Quat:
            pList.append(genPassingData<QuatSample>(flowId, chunkLen));
            break;
//        case PassingDataType::Kin:
//            break;
        case PassingDataType::Pos:
            pList.append(genPassingData<PosSample>(flowId, chunkLen));
            break;
        case PassingDataType::ManualPulse:
            pList.append(genPassingData<TimeSample>(flowId, chunkLen));
            break;
        default:
            Q_ASSERT(false);
        }
    }

    return pList;
}

void DataSystemTest::dataSysTest()
{
    DataSystem ds;
    ds.startWork();
    ds.setDataRate(100, 100, 50);
    ds.setFullScale(2, 2000, 2);
    ds.setSavingInterval(100);
    auto pdVector = genPassingDataVector(std::make_tuple(1, PassingDataType::Quat), 1000, 20);
    pdVector.append(genPassingDataVector(std::make_tuple(0, PassingDataType::Pos), 1000, 20));
    pdVector.append(genPassingDataVector(std::make_tuple(1, PassingDataType::RawAccel), 1000, 20));
    pdVector.append(genPassingDataVector(std::make_tuple(1, PassingDataType::CalGyro), 1000, 20));
    pdVector.append(genPassingDataVector(std::make_tuple(0, PassingDataType::ManualPulse), 2, 2));
    std::random_shuffle(pdVector.begin(), pdVector.end());
    QTimer timer;
    QObject::connect(&timer, &QTimer::timeout, [&timer, &ds, &pdVector]()
    {
        if (!pdVector.isEmpty())
            ds.handleNewData(pdVector.takeFirst());
        else
        {
            ds.saveToFile("test.json");
            //ds.finishWork();
            timer.stop();
            qApp->quit();
        }
    });

    timer.start(50);
    qApp->exec();
}
