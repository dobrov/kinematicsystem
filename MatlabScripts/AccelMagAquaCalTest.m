addpath('Utils');
clear;

gFrameVec = @(v) GetFrameVector(rotx(5) * roty(5), v);
dipAngle = 70;
g = gFrameVec([0 0 1]);
l = gFrameVec((roty(dipAngle) *[0 0 1]')');

len = 24;
angle = 90;
rotQuats = zeros(24, 4);
rotQuats(13:end, :) = GenerateRotQuats(len - 12);
rotQuats(1, :) = [1 0 0 0];
rotQuats(2, :) = quatmultiply([cosd(angle/2), -sind(angle/2) * gFrameVec([0 0 1])], rotQuats(1, :));
rotQuats(3, :) = quatmultiply([cosd(angle/2), -sind(angle/2) * gFrameVec([1 0 0])], rotQuats(2, :));
rotQuats(4, :) = quatmultiply([cosd(-angle/2), -sind(-angle/2) * gFrameVec([0 1 0])], rotQuats(3, :));
rotQuats(5, :) = quatmultiply([cosd(angle/2), -sind(angle/2) * gFrameVec([0 0 1])], rotQuats(4, :));
rotQuats(6, :) = quatmultiply([cosd(angle/2), -sind(angle/2) * gFrameVec([1 0 0])], rotQuats(5, :));
rotQuats(7, :) = quatmultiply([cosd(angle/2), -sind(angle/2) * gFrameVec([0 0 1])], rotQuats(6, :));
rotQuats(8, :) = quatmultiply([cosd(angle/2), -sind(angle/2) * gFrameVec([1 0 0])], rotQuats(7, :));
rotQuats(9, :) = quatmultiply([cosd(-angle/2), -sind(-angle/2) * gFrameVec([0 1 0])], rotQuats(8, :));
rotQuats(10, :) = quatmultiply([cosd(angle/2), -sind(angle/2) * gFrameVec([0 0 1])], rotQuats(9, :));
rotQuats(11, :) = quatmultiply([cosd(-angle/2), -sind(-angle/2) * gFrameVec([0 1 0])], rotQuats(10, :));
rotQuats(12, :) = quatmultiply([cosd(-angle/2), -sind(-angle/2) * gFrameVec([1 0 0])], rotQuats(11, :));

accel = quatrotate(rotQuats, repmat(g, len, 1));
mag = quatrotate(rotQuats, repmat(l, len, 1));

sMag = [1 0.2 0.5;
        0.2 1 0.3;
        0.1 -0.1 1];        
bMag = [0.5; 0.1; -1];

magSensor = zeros(len, 3);
for i = 1:len
    magSensor(i, :) = (sMag \ mag(i, :)' + bMag)';
end
magSensor = magSensor + 0.01 * randn(size(magSensor));

sAccel = [1 0.2 0.4;
          0.2 1 0.6;
          0.1 -0.3 1];
bAccel = [0.05; -0.05; 0.01];

accelSensor = zeros(len, 3);
for i = 1:len
    accelSensor(i, :) = (sAccel \ accel(i, :)' + bAccel)';
end
accelSensor = accelSensor + 0.01 * randn(size(accelSensor));

[e_center, e_radii, e_eigenvecs] = ellipsoid_fit(magSensor);
R = e_eigenvecs;
sMagRes = R * diag([1 / e_radii(1), 1 / e_radii(2), 1 / e_radii(3)]) * R';
bMagRes = e_center;

[e_center, e_radii, e_eigenvecs] = ellipsoid_fit(accelSensor);
R = e_eigenvecs;
sAccelRes = R * diag([1 / e_radii(1), 1 / e_radii(2), 1 / e_radii(3)]) * R';
bAccelRes = e_center;

accelRes = zeros(size(accelSensor));
magRes = zeros(size(magSensor));
for i = 1:size(accelRes, 1)
    accelRes(i, :) = sAccelRes * (accelSensor(i, :)' - bAccelRes);
    magRes(i, :) = sMagRes * (magSensor(i, :)' - bMagRes);
end

E = zeros(1, 8);
E(1) = 1;
E(5) = 1;
ObjFun = @(E) AquaCostFunction(E, accelRes(1:12, :), magRes(1:12, :));
options = optimset('MaxFunEvals', 150000, 'MaxIter', 6000, 'TolFun', 10^(-10), 'Algorithm', 'levenberg-marquardt');

[E, rsnorm] = lsqnonlin(ObjFun, E, [], [], options);
sAccelRes1 = quatnormalize(reshape(E(1:4), 1, 4));
sMagRes1 = quatnormalize(reshape(E(5:end), 1, 4));

sAccelRes = quat2rotm(quatconj(sAccelRes1)) * sAccelRes;
sMagRes = quat2rotm(quatconj(sMagRes1)) * sMagRes;

accelMagn = zeros(size(accelSensor, 1), 1);
magMagn = zeros(size(magSensor, 1), 1);
dipRes = zeros(size(magSensor, 1), 1);
normCross = zeros(size(magSensor, 1), 1);
for i = 1:size(accelMagn,1)
    accelRes(i, :) = quatrotate(sAccelRes1, accelRes(i, :));
    magRes(i, :) = quatrotate(sMagRes1, magRes(i, :));
    accelMagn(i) = norm(accelRes(i, :));
    magMagn(i) = norm(magRes(i, :));
    dipRes(i) = dot(accelRes(i, :), magRes(i, :));
end

figure('NumberTitle', 'off', 'Name', 'Magnitudes');
hold on;
plot(magMagn, '-r.');
plot(accelMagn, '-b.');
title('Magnitudes');
legend('Mag', 'Accel');
hold off;

figure('NumberTitle', 'off', 'Name', 'DipAngle');
hold on;
plot(dipRes, '-r.');
title('DipAngle');
legend('DipAngle');
hold off;

disp(bAccelRes);
disp(bAccel);
disp(sAccelRes);
disp(sAccel);

disp(bMagRes);
disp(bMag);
disp(sMagRes);
disp(sMag);

PlotSphere(roty(70)* [0 0 1]', sMag, bMag, sMagRes, bMagRes);
PlotSphere(roty(70)* [0 0 1]', sAccel, bAccel, sAccelRes, bAccelRes);