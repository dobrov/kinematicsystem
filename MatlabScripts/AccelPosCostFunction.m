function [residuals] = AccelPosCostFunction(E, accel, trueMeas)

sAccel = reshape(E(1:4), 1, 4);
sAccel = quatnormalize(sAccel);

residuals = zeros(length(accel), 1);
for i = 1:length(accel)
    calAccel = quatrotate(sAccel, accel(i, :) / norm(accel(i, :)));
    residuals(i) = norm(trueMeas(i, :) - calAccel);
end

end

