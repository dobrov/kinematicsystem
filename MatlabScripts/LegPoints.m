addpath('jsonlab');
addpath('Utils');

inDataFolder = '../20160714Kol/';
calDataFolder = 'Calibration';
offsetFileName = 'Offset/leg1.offset';
outDataFolder = '../20160714Kol/points/';

filterFileName = 'settings.filter';
filterSettings.GainAcc = 0.02;
filterSettings.GainMag = 0.02;

filterSettings.DoBiasEstimation = true;
if (filterSettings.DoBiasEstimation)
    filterSettings.GyroBiasAlpha = 0.05;
    filterSettings.MinSteadyLen = 1; %seconds
end

filterSettings.DoThresholding = true;
if (filterSettings.DoThresholding)
    filterSettings.AccelMagnThrAlpha = 0.01;
    filterSettings.MagMagnThrAlpha = 0.02;
    filterSettings.DipThrAlpha = 0.03;
end

filterSettingsChar = savejson('', filterSettings);
fileID = fopen(filterFileName, 'w');
fprintf(fileID, '%s', filterSettingsChar);
fclose(fileID);

if ~exist(outDataFolder, 'dir')
    mkdir(outDataFolder);
end

copyfile(calDataFolder, strcat(outDataFolder, 'Calibration'), 'f'); 
copyfile(offsetFileName, outDataFolder);
copyfile(filterFileName, outDataFolder);
if exist('Offset/offset.mat', 'file')
    copyfile('Offset/offset.mat', outDataFolder);
end
addpath(calDataFolder);

dataFiles = dir(strcat(inDataFolder, '/*move*.json'));

for i = 1:length(dataFiles)
    fprintf('Now processing %s.....\n', dataFiles(i).name);
    CalcLegPoints(strcat(inDataFolder, dataFiles(i).name), offsetFileName, outDataFolder, ...
    'filterSettingsName', filterFileName);
end