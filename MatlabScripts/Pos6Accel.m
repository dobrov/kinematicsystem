addpath('Utils');
accel = [0 0 1; 0 0 -1; 0 1 0; 0 -1 0; 1 0 0; -1 0 0];

vX = accel(:, 1);
vY = accel(:, 2);
vZ = accel(:, 3);

scaleAccel = [1.10 0.5 0.2;
              0.1 1.05 0.03;
              0.2 0.04 0.95];
biasAccel = [0.03; 0.02; -0.06];
disp('=============Real=================');
disp(scaleAccel);
disp(biasAccel);


accelSensor = zeros(size(accel));
for i = 1:size(accelSensor, 1)
    accelSensor(i, :) = (scaleAccel \ (roty(10) * accel(i, :)') + biasAccel)';
end
%accelSensor = accelSensor + 0.1 * randn(size(accelSensor));

[scaleRes, biasRes] = Cal6PosAccel(accelSensor);


disp('=============Res=================');
disp(scaleRes);
disp(biasRes');

PlotSphere(roty(70)* [0 0 1]', scaleAccel, biasAccel, scaleRes, biasRes);
