addpath('Utils');
addpath('Offset');

close all;
clear;
clc;
warning('off', 'Images:initSize:adjustingMag');

nJoints = 5;
len = 9.75;

leftImage = strcat('../20160726Kol/left.JPG');
rightImage = strcat('../20160726Kol/right.JPG');
backImage = strcat('../20160726Kol/back.JPG');

figure('NumberTitle', 'off', 'Name', 'XZ - Left image');
bitmapFirst = imread(leftImage);
imshow(bitmapFirst);
tightfig;
% scale
[x, z] = ginput_zoom(2);
leftScale = norm([(x(2) - x(1)) (z(2) - z(1))]) / len;
originX = x(1);
originZ = z(1);

rotVector = [(x(2) - x(1)) (z(2) - z(1))] / norm([(x(2) - x(1)) (z(2) - z(1))]);
rotAxis = cross([rotVector 0], [1 0 0]);
rotAngle = sign(rotAxis(3)) * acosd(dot(rotVector, [1 0]));
if abs(rotAngle) > 90
    rotAngle = rotAngle - sign(rotAngle) * 180;
end

% joints 
[x, z] = ginput_zoom(nJoints);
jointsLHL = zeros(nJoints, 3);
for i = 1:nJoints
    jointsLHL(i, 1) = (originX - x(i)) / leftScale;
    jointsLHL(i, 3) = (originZ - z(i)) / leftScale;
    jointsLHL(i, :) = roty(-rotAngle) * jointsLHL(i, :)'; 
end

figure('NumberTitle', 'off', 'Name', 'XZ - Right image');
bitmapFirst = imread(rightImage);
imshow(bitmapFirst);
tightfig;
% scale
[x, z] = ginput_zoom(2);
leftScale = norm([(x(2) - x(1)) (z(2) - z(1))]) / len;
originX = x(1);
originZ = z(1);

rotVector = [(x(2) - x(1)) (z(2) - z(1))] / norm([(x(2) - x(1)) (z(2) - z(1))]);
rotAxis = cross([rotVector 0], [1 0 0]);
rotAngle = sign(rotAxis(3)) * acosd(dot(rotVector, [1 0]));
if abs(rotAngle) > 90
    rotAngle = rotAngle - sign(rotAngle) * 180;
end

% joints 

[x, z] = ginput_zoom(nJoints);
jointsRHL = zeros(nJoints, 3);
for i = 1:nJoints
    jointsRHL(i, 1) = (x(i) - originX) / leftScale;
    jointsRHL(i, 3) = (originZ - z(i)) / leftScale;
    jointsRHL(i, :) = roty(rotAngle) * jointsRHL(i, :)';
end

figure('NumberTitle', 'off', 'Name', 'YZ - Back image');
bitmapSecond = imread(backImage);
imshow(bitmapSecond);
tightfig;

% scale
[y, z] = ginput_zoom(2);
backScale = norm([(y(2) - y(1)) (z(2) - z(1))]) / len;

originY = y(1);
originZ = z(1);

rotVector = [(y(2) - y(1)) (z(2) - z(1))] / norm([(y(2) - y(1)) (z(2) - z(1))]);
rotAxis = cross([rotVector 0], [1 0 0]);
rotAngle = sign(rotAxis(3)) * acosd(dot(rotVector, [1 0]));
if abs(rotAngle) > 90
    rotAngle = rotAngle - sign(rotAngle) * 180;
end

% joints
joints = [jointsLHL(:, 3); jointsRHL(:, 3)];
% joints
[y, z] = ginput_zoom(nJoints * 2, originZ - joints .* backScale);

tJoint = zeros(3, 1);
for i = 1:nJoints
    tJoint(2) = (originY - y(i)) / backScale;
    tJoint(3) = (originZ - z(i)) / backScale;
    tJoint = rotx(rotAngle) * tJoint;
    jointsLHL(i, 2) = tJoint(2);
end

for i = nJoints + 1:2 * nJoints
    tJoint(2) = (originY - y(i)) / backScale;
    tJoint(3) = (originZ - z(i)) / backScale;
    tJoint = rotx(rotAngle) * tJoint;
    jointsRHL(i, 2) = tJoint(2);
end

segmentsFVectors = zeros(nJoints - 1, 3);
segmentsLength = zeros(nJoints - 1, 1);
segmentsQuat = zeros(nJoints - 1, 4);
for i = 1:nJoints - 1
    segmentsFVectors(i, :) = jointsLHL(i + 1, :) - jointsLHL(i, :);
    segmentsLength(i) = norm(segmentsFVectors(i, :));
    segmentsFVectors(i, :) = segmentsFVectors(i, :) / segmentsLength(i);
    segmentsQuat(i, :) = rotationTo([1 0 0], segmentsFVectors(i, :));
end
LHL = struct('Quat', segmentsQuat, 'Length', segmentsLength);

segmentsFVectors = zeros(nJoints - 1, 3);
segmentsLength = zeros(nJoints - 1, 1);
segmentsQuat = zeros(nJoints - 1, 4);
for i = 1:nJoints - 1
    segmentsFVectors(i, :) = jointsRHL(i + 1, :) - jointsRHL(i, :);
    segmentsLength(i) = norm(segmentsFVectors(i, :));
    segmentsFVectors(i, :) = segmentsFVectors(i, :) / segmentsLength(i);
    segmentsQuat(i, :) = rotationTo([1 0 0], segmentsFVectors(i, :));
end
RHL = struct('Quat', segmentsQuat, 'Length', segmentsLength);

zeroPoint = jointsLHL(1, :);
hipLength = jointsRHL(1, :) - zeroPoint;

save(strcat('Offset/offset.mat'), 'LHL', 'RHL', 'hipLength');

fig = figure('NumberTitle', 'off', 'Name', 'Leg Animation');
set(gcf, 'Renderer', 'zbuffer');
set(gca, 'SortMethod', 'depth');
hold on;

colors = [0.8 0 0; 0 0.8 0; 0 0 0.8; 0.8 0 0.8; 0.8 0.8 0; 0 0.8 0.8];
line([0, hipLength(1)], [0, hipLength(2)], [0, hipLength(3)], ...
'Color', [0 0 0], 'LineWidth', 2);
for j = 1:2
    if j == 1
        prevPoint = [0 0 0];
        segmentsQuat = LHL.Quat;
        segmentsLength = LHL.Length;
    else
        prevPoint = hipLength;
        segmentsQuat = RHL.Quat;
        segmentsLength = RHL.Length;
    end
    for i = 2:nJoints
        point = prevPoint + quatrotate(segmentsQuat(i - 1, :), [segmentsLength(i - 1) 0 0]);
        line([prevPoint(1) point(1)], [prevPoint(2) point(2)], ...
             [prevPoint(3) point(3)], 'Color', colors(i - 1, :), 'LineWidth', 2);
        scatter3(point(1), point(2), point(3), 'ko', 'filled');
        midPoint = prevPoint + (point - prevPoint) / 2;
        xPoint = quatrotate(segmentsQuat(i - 1, :), [1 0 0]);
        yPoint = quatrotate(segmentsQuat(i - 1, :), [0 1 0]);
        zPoint = quatrotate(segmentsQuat(i - 1, :), [0 0 1]);
        line([midPoint(1), midPoint(1) + xPoint(1)], [midPoint(2), midPoint(2) + xPoint(2)],...
            [midPoint(3), midPoint(3) + xPoint(3)], 'Color', [1 0 0], 'LineWidth', 3);
        line([midPoint(1), midPoint(1) + yPoint(1)], [midPoint(2), midPoint(2) + yPoint(2)],...
            [midPoint(3), midPoint(3) + yPoint(3)], 'Color', [0 1 0], 'LineWidth', 3);
        line([midPoint(1), midPoint(1) + zPoint(1)], [midPoint(2), midPoint(2) + zPoint(2)],...
            [midPoint(3), midPoint(3) + zPoint(3)], 'Color', [0 0 1], 'LineWidth', 3);
        prevPoint = point;
    end
end

view([-180 0]);
axis tight;
axis equal;