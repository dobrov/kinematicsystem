%close all;
clear;
%clc;
addpath('jsonlab');
addpath('Logs');
addpath('Utils');

fileName = '701Layout/701calib2.json';
rawData = loadjson(fileName);

accelDataRate = rawData.Config.AccelDataRate;
gyroDataRate = rawData.Config.GyroDataRate;

if accelDataRate ~= gyroDataRate
    disp('Accel data rate should be same as gyro data rate');
end

data = ProcessRawData(rawData);

staticPos = FindStaticPos(data, 'isAccel', true, 'isGyro', true, ...
    'isMag', true, 'isPlottingSteadyState', false, 'isGyroBetween', true);
names = SortNames(fieldnames(data));
nDevice = length(names);

for i = 1:nDevice
    ax = staticPos.(names{i}).Accel.x;
    ay = staticPos.(names{i}).Accel.y;
    az = staticPos.(names{i}).Accel.z;
        
    [sAccel, bAccel] = CalEllipsoid([ax' ay' az']);
    calAccel = ApplyCalibration(sAccel, bAccel, [ax' ay' az']);
    
    mx = data.(names{i}).mx;
    my = data.(names{i}).my;
    mz = data.(names{i}).mz;
    [sMag, bMag] = CalEllipsoid([mx' my' mz']);
    
    mx = staticPos.(names{i}).Mag.x;
    my = staticPos.(names{i}).Mag.y;
    mz = staticPos.(names{i}).Mag.z;
    calMag = ApplyCalibration(sMag, bMag, [mx' my' mz']);
    
%     [rotAccel, rotMag] = CalAccelPosMagRot(calAccel, calMag);
    
%     calAccel = ApplyCalibration(rotAccel1 * sAccel, bAccel, [ax' ay' az']);
%     calMag = ApplyCalibration(rotMag1 * sMag, bMag, [mx' my' mz']);
%     
    [rotAccel, rotMag] = CalRotation(calAccel, calMag);
%     aquaPairs = GetAquaPos(calAccel, calMag);
%     [rotAccel, rotMag] = CalAqua(aquaPairs);
    
   %sAccel = rotAccel * sAccel;
   %sMag = rotMag * sMag;
    
    calAccel = ApplyCalibration(sAccel, bAccel, [ax' ay' az']);
    calMag = ApplyCalibration(sMag, bMag, [mx' my' mz']);
    
    bGyro = [staticPos.(names{i}).Gyro.x', ...
        staticPos.(names{i}).Gyro.y', staticPos.(names{i}).Gyro.z'];
    bGyro = mean(bGyro);
    sGyro = CalGyroAqua(calAccel, calMag, bGyro, ...
        staticPos.(names{i}).GyroBetween, data.Config.GyroDataRate);
    
    
    calData = struct('Bias', bAccel, 'Scale', sAccel);
    calDataChar = savejson('', calData);
    fileID = fopen(strcat('Calibration/accelCalData', names{i}, ...
        'Range', num2str(data.Config.AccelFullScale), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
    
    calData = struct('Bias', bMag, 'Scale', sMag);
    calDataChar = savejson('', calData);
    fileID = fopen(strcat('Calibration/magCalData', names{i}, ...
        'Range', num2str(data.Config.MagFullScale), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
    
    calData = struct('Bias', bGyro, 'Scale', sGyro);
    calDataChar = savejson('', calData);
    fileID = fopen(strcat('Calibration/gyroCalData', names{i}, ...
        'Range', num2str(data.Config.GyroFullScale), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
end
