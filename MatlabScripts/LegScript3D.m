addpath('jsonlab');
addpath('Utils');
addpath('Logs');
addpath('Offset');
addpath('Calibration');
clear;
%close all;
%clc;

isShowingAnimation = false;
isVideo = false;
isCalcRMSEApproximation = false;
isPlottingCalMag = false;
isPlottingMag = false;
isPlottingCalAccel = false;
isPlottingAccel = false;
isPlottingCalGyro = false;
isPlottingGyro = false;
isPlottingRawQuat = false;
isShowThresholding = false;
isPlottingDipAngle = false;

isCalGyro = true;
isCalMag = true;
isCalAccel = true;
isFiltered = true;
DoThresholding = false;
DoBiasEstimation = true;

beginTime = 0;
endTime = 0; % second

% beginTime = 40;
% endTime = 50;

fileName = './705Layout/705move.json';
rawData = loadjson(fileName);
offsetData = loadjson('Offset/leg1.offset');

data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isPlottingAccel', isPlottingAccel, ...
    'isPlottingGyro', isPlottingGyro, 'isPlottingMag', isPlottingMag, 'isCalMag', isCalMag, ...
    'isPlottingCalMag', isPlottingCalMag, 'isCalAccel', isCalAccel, 'isCalGyro', isCalGyro, ...
    'isPlottingDipAngle', isPlottingDipAngle, 'isPlottingCalGyro', isPlottingCalGyro, ...
    'isPlottingCalAccel', isPlottingCalAccel);

[names, allDeviceList] = SortNames(fieldnames(data));
bodyParts = fieldnames(offsetData);
deviceList = [];
for i = 1:length(bodyParts)
    if isfield(offsetData.(bodyParts{i}), 'DeviceList')
        deviceList = [deviceList offsetData.(bodyParts{i}).DeviceList];
    end
end
unusedDevices = setdiff(allDeviceList, deviceList);
for i = 1:length(unusedDevices)
    j = find(allDeviceList == unusedDevices(i));
    data = rmfield(data, names{j});
end

deviceQuat = GetQuaternions(data, rawData.Config.GyroDataRate, 'isPlottingQuat', isPlottingRawQuat, ...
    'DoBiasEstimation', DoBiasEstimation, 'DoThresholding', DoThresholding, ...
    'isShowThresholding', isShowThresholding);

if beginTime ~= 0 || endTime ~= 0
    beginTime = beginTime * power(10, 6);
    endTime = endTime * power(10, 6);
    
    beginIndex = find(data.tgyro == beginTime);
    endIndex = find(data.tgyro == endTime);
    
    if ~(isempty(beginIndex) || isempty(endIndex))
        deviceQuat = deviceQuat(beginIndex:endIndex, :, :);
        data.tgyro = data.tgyro(beginIndex:endIndex);
    end
end

[names, deviceList] = SortNames(fieldnames(data));
len = length(data.tgyro);
nDevice = length(names);

qEG = offsetData.QuatEG;
for i = 1:2
    if (i == 1)
        qBS = offsetData.LHL.QuatBS;
        L = offsetData.LHL.SegmentLength;
        curDeviceList = offsetData.LHL.DeviceList;
    elseif sum(strcmp(fieldnames(offsetData), 'RHL')) == 1
        qBS = offsetData.RHL.QuatBS;
        L = offsetData.RHL.SegmentLength;
        curDeviceList = offsetData.RHL.DeviceList;
    else
        break;
    end
       
    deviceId = zeros(size(curDeviceList));
    for j = 1:length(curDeviceList)
        deviceId(j) = find(deviceList == curDeviceList(j));
    end
    
    qES = deviceQuat(:, :, deviceId);
    qGB = zeros(size(qES));
    nSegments = length(curDeviceList);
    for j = 1:nSegments
        qSE = quatconj(qES(:, :, j));
        temp = quatmultiply(qSE, qBS(j, :));
        qGB(:, :, j) = quatconj(quatmultiply(qEG, temp));
    end
    
    tPoints = zeros(len, 3, nSegments + 1);
    if (i == 1) %LHL
        tPoints(:, :, 1) = zeros(len, 3);
    else %RHL
        tPoints(:, :, 1) = repmat(offsetData.HipLength, len, 1);
    end
    for j = 2:nSegments + 1
        tPoints(:, :, j) = tPoints(:, :, j - 1) + quatrotate(qGB(:, :, j - 1), [L(j-1) 0 0]);
    end
    
    if (i == 1)
        points.LHL = tPoints;
        segmentsQuat.LHL = qGB;
    else
        points.RHL = tPoints;
        segmentsQuat.RHL = qGB;
    end
end

PlotDiffQuat(segmentsQuat.LHL(:, :, 1), segmentsQuat.RHL(:, :, 1), 'isPlotQuats', true, ...
'isPlotDiffQuat', true, 'isPlotDiffAngles', true, 'isShowMeanVar', true);
% PlotDiffQuat(segmentsQuat.LHL(:, :, 2), segmentsQuat.RHL(:, :, 2), 'isPlotQuats', true, ...
% 'isPlotDiffQuat', true, 'isPlotDiffAngles', true, 'isShowMeanVar', true);
% PlotDiffQuat(segmentsQuat.LHL(:, :, 3), segmentsQuat.RHL(:, :, 3), 'isPlotQuats', true, ...
% 'isPlotDiffQuat', true, 'isPlotDiffAngles', true, 'isShowMeanVar', true);

freq = rawData.Config.GyroDataRate;

if isCalcRMSEApproximation
    tSec = data.tgyro ./ power(10, 6);
    CalcRMSEApproximation(points.LHL(:, :, 3), tSec);
end

% points.LHL = points.RHL;
if isVideo
    LegAnimation(freq, points, 'VideoFileName', 'LegScript');
else
    LegAnimation(freq, points, 'isPlotSquare', false, 'PlotHist', [1 0 0; 1 0 0], ...
        'nPlotSegment', [3 3], 'isAnimation', isShowingAnimation);
end
% LegPlot(points.LHL);