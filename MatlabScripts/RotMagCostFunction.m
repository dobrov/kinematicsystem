function [residuals] = RotMagCostFunction(E, accel, mag)
sMag = reshape(E(1:4), 1, 4);
sMag = quatnormalize(sMag);

dotProduct = zeros(length(accel), 1);
for i = 1:length(accel)
    calMag = quatrotate(sMag, mag(i, :) / norm(mag(i, :)));
    dotProduct(i) = dot(accel(i, :), calMag);
end

posComb = nchoosek(1:length(accel), 2);
residuals = zeros(length(posComb), 1);
for i = 1:length(posComb)
    residuals(i) = abs(dotProduct(posComb(i, 1)) - dotProduct(posComb(i, 2)));
end

end