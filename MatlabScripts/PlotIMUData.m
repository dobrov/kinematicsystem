close all;
clear;
%clc;
addpath('Calibration');
addpath('jsonlab');
addpath('Utils');
addpath('Logs');
rawData = loadjson('724test.json');
data = ProcessRawData(rawData, 'isPlottingMag', false, 'isPlottingAccel', false, ...
    'isPlottingGyro', false, 'isPlottingCalGyro', true, 'isPlottingCalMag', false, 'isFiltered', true, 'isCalMag', true, ...
    'isCalAccel', true, 'isCalGyro', true, 'isPlottingCalAccel', false, 'isCompareAccelMagn', false, 'isMean', false, ...
    'isPlottingDipAngle', true);
% staticPos = FindStaticPos(data, 'isAccel', true, 'isMag', true);
% CalcDipMeanStd(staticPos, 'isPlottingDipAngle', false);

data = struct('Device14', data.Device14, 'Device9', data.Device9, 'tgyro', data.tgyro, 'tmag', data.tmag);

qES = GetQuaternions(data, rawData.Config.GyroDataRate, 'isPlottingQuat', false, ...
'DoBiasEstimation', true, 'DoThresholding', true, 'isPlottingDipAngle', false, ...
'isShowThresholding', false, 'isPlottingGyroE', true);

% PlotDiffQuat(qES(:, :, 1), qES(:, :, 2), 'isPlotDiffQuat', true, 'isPlotQuats', true, ...
%     'isPlotDiffAngles', true, 'isShowMeanVar', true);
