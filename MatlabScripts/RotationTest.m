len = 1000;
rotQuats = GenerateRotQuats(len);
g = [0 0 1];
l = (roty(50) * g')';
accel = quatrotate(rotQuats, repmat(g, len, 1));
mag = quatrotate(rotQuats, repmat(l, len, 1));
sReal = [1.0000    0.1000    0.1000
         0.3000    1.0000    0.1000
         0.5000    0.3000    1.0000];
sRes = [1.0957    0.2372    0.2873
        0.2372    1.0059    0.1735
        0.2873    0.1735    0.9533];
% sReal = eye(3);
% sRes = eye(3);
for i = 1:length(mag)
    mag(i, :) = (sRes * inv(sReal)) * mag(i, :)';
end
mag = mag + 0.01 * randn(size(mag));

M = [-accel(:, 1) .* mag(:, 2) -accel(:, 1) .* mag(:, 3) -accel(:, 2) .* mag(:, 1) ...
     -accel(:, 2) .* mag(:, 2) -accel(:, 2) .* mag(:, 3) -accel(:, 3) .* mag(:, 1) ...
     -accel(:, 3) .* mag(:, 2) -accel(:, 3) .* mag(:, 3) ones(size(accel(:, 1)))];
v = accel(:, 1) .* mag(:, 1);

h = ( M' * M ) \ ( M' * v );
T = [1 h(1) h(2);
     h(3) h(4) h(5);
     h(6) h(7) h(8)];
r11 = (1/det(T)) ^ (1/3);
R = r11 * T;

disp(acosd(h(9) * r11));

magn = zeros(size(mag, 1), 1);
product = zeros(size(mag, 1), 1);
for i = 1:length(accel)
    mag(i, :) = (R * mag(i, :)')';
    magn(i) = norm(mag(i, :));
    product(i) = acosd(dot(accel(i, :), mag(i, :)));
end

disp(mean(magn));
disp(std(magn));
disp(mean(product));
disp(std(product));
disp(R * sRes);