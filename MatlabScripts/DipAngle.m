%close all;
clear;
clc;
addpath('jsonlab');
addpath('Utils');
addpath('Logs');
addpath('../20160222Kol');
rawData = loadjson('accel0.json');
data = ProcessRawData(rawData, 'isCalMag', true, 'isCalAccel', true, ...
    'isPlottingDipAngle', true, 'isFiltered', true, 'isCompareAccelMagn', true, ...
    'isPlottingCalMag', true, 'isPlottingCalAccel', true);