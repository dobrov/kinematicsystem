%close all;
clear;
%clc;
addpath('jsonlab');
addpath('Utils');
addpath('Logs');

isPlottingGyro = true;
isPlottingGyroCal = true;

rawData = loadjson('531move.json');
data = ProcessRawData(rawData, 'isPlottingGyro', isPlottingGyro, 'isMean', false);

names = SortNames(fieldnames(data));

gyroFS = data.Config.GyroFullScale;

for i = 1:length(names)
    currentDevice = data.(names{i});
    
    gx = currentDevice.gx(1:800);
    gy = currentDevice.gy(1:800);
    gz = currentDevice.gz(1:800);
    
    disp(names{i});
    disp([mean(gx) mean(gy) mean(gz)]);
    
    calData = struct('Bias', [mean(gx), mean(gy), mean(gz)], 'Scale', eye(3));
    calDataChar = savejson('', calData);
    fileID = fopen(strcat('Calibration/gyroCalData', names{i}, 'Range', num2str(gyroFS), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
end
