addpath('jsonlab');
addpath('Utils');
addpath('Logs');
addpath('Offset');
addpath('Calibration');

%close all;
clear;
%clc;

isFiltered = true;
isCalMag = true;
isCalAccel = true;
isCalGyro = true;

rawData = loadjson('../20160726Kol/726KolOffset.json');
data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, ...
    'isCalGyro', isCalGyro, 'isCalAccel', isCalAccel, 'isMean', true);
[names, DeviceList] = SortNames(fieldnames(data));


LHLDevice = [12 13 14 15];
RHLDevice = [7 8 9 10];
nSegments = length(LHLDevice);

load('Offset/offset.mat');

% angle = 147;
% qGE = [cosd(angle/2), -sind(angle/2) * [0 0 1]];
% qEG = quatconj(qGE);
% disp(quat2angle(qEG));

DeviceQuat = GetQuaternions(data, rawData.Config.GyroDataRate, ...
    'isPlottingQuat', false, 'DoThresholding', true, 'DoBiasEstimation', true);

gDevice = 11;
gDeviceIndex = find(DeviceList == gDevice);

forwardVector = quatrotate(DeviceQuat(:, :, gDeviceIndex), [1 0 0]);
%qEG = rotationTo([1 0 0], [forwardVector(1:2) 0]);
%qEG = quatconj(qEG);
% angle = atan2(norm(cross(forwardVector, [1 0 0])), dot(forwardVector, [1 0 0]));
% qEG = [cos(angle/2), sin(angle/2) * [0 0 1]];
angle = 15;
qEG = quatmultiply([cosd(angle/2), sind(angle/2) * [0 0 1]], DeviceQuat(:, :, gDeviceIndex));
% BoxAnimation(100, DeviceQuat(:, :, gDeviceIndex));
% BoxAnimation(100, qEG);

for i = 1:2
    if (i == 1)
        qGB = LHL.Quat;
        curDeviceList = LHLDevice;
    elseif exist('RHL', 'var')
        qGB = RHL.Quat;
        curDeviceList = RHLDevice;
    else
        break;
    end
    
    DeviceId = zeros(size(curDeviceList));
    for j = 1:length(curDeviceList)
        DeviceId(j) = find(DeviceList == curDeviceList(j));
    end
    
    qES = DeviceQuat(:, :, DeviceId);
    qSB = zeros(nSegments, 4);
    for j = 1:nSegments
        qSE = quatconj(qES(1, :, j));
        temp = quatmultiply(qGB(j, :), qEG);
        qSB(j, :) = quatmultiply(temp, qSE);
    end
    
    if i == 1
        LHL = struct('DeviceList', LHLDevice, 'SegmentLength', LHL.Length', ...
                     'QuatBS', quatconj(qSB), 'InitBG', quatconj(qGB));
    else
        RHL = struct('DeviceList', RHLDevice, 'SegmentLength', RHL.Length', ...
                     'QuatBS', quatconj(qSB), 'InitBG', quatconj(qGB));
    end
end

if exist('RHL', 'var')
    offData = struct('QuatEG', qEG, 'HipLength', hipLength, 'RHL', RHL, 'LHL', LHL);
else
    offData = struct('QuatEG', qEG, 'LHL', LHL);
end
offDataChar = savejson('', offData);

fileID = fopen('Offset/leg1.offset', 'w');
fprintf(fileID,'%s', offDataChar);
fclose(fileID);