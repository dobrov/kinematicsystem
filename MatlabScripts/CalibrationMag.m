%close all;
clear;
%clc;
addpath('jsonlab');
addpath('Utils');
addpath('Logs');
rawData = loadjson('607mag.json');

isPlottingMag = false;
isPlottingMagCal = false;

data = ProcessRawData(rawData);
names = SortNames(fieldnames(data));

magFS = data.Config.MagFullScale;

for i = 1:length(names)
    currentDevice = data.(names{i});
    
    mx = medfilt1(currentDevice.mx);
    my = medfilt1(currentDevice.my);
    mz = medfilt1(currentDevice.mz);
%     mx = currentDevice.mx;
%     my = currentDevice.my;
%     mz = currentDevice.mz;
%     
%     mag = medfilt3([mx; my; mz]);
%     
%     mx = mag(1, :);
%     my = mag(2, :);
%     mz = mag(3, :);
    
    if (isPlottingMag)
        figure('NumberTitle', 'off', 'Name', strcat('Magnetometer', names{i}));
        hold on;
        plot(data.tmag ./ power(10, 6), mx, '-r.');
        plot(data.tmag ./ power(10, 6), my, '-g.');
        plot(data.tmag ./ power(10, 6), mz, '-g.');
        plot(data.tmag ./ power(10, 6), sqrt(mz(:) .^ 2 + my(:) .^ 2 + mx(:) .^ 2), '-k.');
        xlabel('seconds');
        title('Magnetometer');
        legend('X', 'Y', 'Z', 'magn');
        hold off;

%         figure('NumberTitle', 'off', 'Name', strcat('MagnetometerXY', names{i}));
%         hold on;
%         axis equal
%         scatter(mx(:), my(:), 4, [0 0 1], 'filled');
% 
%         figure('NumberTitle', 'off', 'Name', strcat('MagnetometerYZ', names{i}));
%         hold on;
%         axis equal
%         scatter(my(:), mz(:), 4, [0 0 1], 'filled');
% 
%         figure('NumberTitle', 'off', 'Name', strcat('MagnetometerXZ', names{i}));
%         hold on;
%         axis equal
%         scatter(mx(:), mz(:), 4, [0 0 1], 'filled');

        figure('NumberTitle', 'off', 'Name', strcat('MagnetometerXYZ', names{i}));
        grid on;
        scatter3(mx(:), my(:), mz(:), 4, [0 0 1], 'filled');
        axis equal;
        hold off;
    end
   
    [Scale, Bias] = CalEllipsoid([mx', my', mz']);
    disp(Scale);
    disp(Bias);

    calData = struct('Bias', Bias, 'Scale', Scale);
    calDataChar = savejson('', calData);
    
    fileID = fopen(strcat('Calibration/magCalData', names{i}, 'Range', num2str(magFS) ,'.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
    
    x_c = mx - Bias(1);
    y_c = my - Bias(2);
    z_c = mz - Bias(3);

    x_cal = Scale(1, 1) * x_c + Scale(1, 2) * y_c + Scale(1, 3) * z_c;
    y_cal = Scale(2, 1) * x_c + Scale(2, 2) * y_c + Scale(2, 3) * z_c;
    z_cal = Scale(3, 1) * x_c + Scale(3, 2) * y_c + Scale(3, 3) * z_c;
    
    if (isPlottingMagCal)
        figure('NumberTitle', 'off', 'Name', strcat('MagnetometerCal', names{i}));
        hold on;
        plot(data.tmag ./ power(10, 6), x_cal(:), '-r.');
        plot(data.tmag ./ power(10, 6), y_cal(:), '-g.');
        plot(data.tmag ./ power(10, 6), z_cal(:), '-b.');
        plot(data.tmag ./ power(10, 6), sqrt(x_cal(:) .^ 2 + ...
        y_cal(:) .^ 2 + z_cal(:) .^ 2), '-k.');
        xlabel('seconds');
        title('Magnetometer');
        legend('X', 'Y', 'Z');

%         figure('NumberTitle', 'off', 'Name', strcat('MagnetometerXYcal', names{i}));
%         hold on;
%         axis equal
%         scatter(x_cal(:), y_cal(:), 4, [1 0 0], 'filled');
% 
%         figure('NumberTitle', 'off', 'Name', strcat('MagnetometerYZcal', names{i}));
%         hold on;
%         axis equal
%         scatter(y_cal(:), z_cal(:), 4, [1 0 0], 'filled');
% 
%         figure('NumberTitle', 'off', 'Name', strcat('MagnetometerXZcal', names{i}));
%         hold on;
%         axis equal
%         scatter(x_cal(:), z_cal(:), 4, [1 0 0], 'filled');

        figure('NumberTitle', 'off', 'Name', strcat('MagnetometerXYZcal', names{i}));
        grid on;
        scatter3(x_cal(:), y_cal(:), z_cal(:), 4, [1 0 0], 'filled');  
        axis equal;
        hold off;
    end
end
