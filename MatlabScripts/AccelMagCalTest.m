addpath('Utils');
clear;

dipAngle = 70;
len = 1000;
rotQuats = GenerateRotQuats(len);
g = [0 0 1];
l = (roty(dipAngle) * g')';
accel = quatrotate(rotQuats, repmat(g, len, 1));
mag = quatrotate(rotQuats, repmat(l, len, 1));

scaleMag = [1 0.5 0.2;
            0.1 1 0.1;
            0.4 0.1 1];        
biasMag = [0.5; 0.1; -1];

% disp('=============Mag=================');
% disp(scaleMag);
% disp(biasMag);

magSensor = zeros(len, 3);
for i = 1:len
    magSensor(i, :) = (scaleMag \ mag(i, :)' + biasMag)';
end
%magSensor = magSensor + 0.001 * randn(size(magSensor));

scaleAccel = [0.9003    0.5    0.0316;
              0.1    0.7986    0.0519;
              0.0316    0.0519    1.2028];
%scaleAccel = eye(3);
biasAccel = [0.05; -0.05; 0.01];

% disp('=============Accel=================');
% disp(scaleAccel);
% disp(biasAccel);
% 
accelSensor = zeros(len, 3);
for i = 1:len
    accelSensor(i, :) = (scaleAccel \ accel(i, :)' + biasAccel)';
end
%accelSensor = accelSensor + 0.001 * randn(size(accelSensor));

[e_center, e_radii, e_eigenvecs] = ellipsoid_fit(magSensor);
R = e_eigenvecs;
Smag = R * diag([1 / e_radii(1), 1 / e_radii(2), 1 / e_radii(3)]) * R';
Bmag = e_center;
% disp('=============Mag=================');
% disp(Smag);
% disp(Bmag);

[e_center, e_radii, e_eigenvecs] = ellipsoid_fit(accelSensor);
R = e_eigenvecs;
Saccel = R * diag([1 / e_radii(1), 1 / e_radii(2), 1 / e_radii(3)]) * R';
Baccel = e_center;
% disp('=============Accel=================');
% disp(Saccel);
% disp(Baccel);
% 
accelRes = zeros(size(accelSensor));
for i = 1:size(accelRes, 1)
    %accelRes(i, :) = (Saccel \ accel(i, :)' + Baccel)';
    accelRes(i, :) = Saccel * (accelSensor(i, :)' - Baccel);
end

magRes = zeros(size(magSensor));
for i = 1:size(magRes, 1)
    magRes(i, :) = Smag * (magSensor(i, :)' - Bmag);
end

M = [-accelRes(:, 1) .* magRes(:, 2) -accelRes(:, 1) .* magRes(:, 3) -accelRes(:, 2) .* magRes(:, 1) ...
     -accelRes(:, 2) .* magRes(:, 2) -accelRes(:, 2) .* magRes(:, 3) -accelRes(:, 3) .* magRes(:, 1) ...
     -accelRes(:, 3) .* magRes(:, 2) -accelRes(:, 3) .* magRes(:, 3) ones(size(accelRes(:, 1)))];
v = accelRes(:, 1) .* magRes(:, 1);

h = ( M' * M ) \ ( M' * v );
T = [1 h(1) h(2);
     h(3) h(4) h(5);
     h(6) h(7) h(8)];
r11 = (1/det(T)) ^ (1/3);
R = r11 * T;

disp(acosd(h(9) * r11));

accelMagn = zeros(size(accelSensor, 1), 1);
magMagn = zeros(size(magSensor, 1), 1);
dipRes = zeros(size(magSensor, 1), 1);
for i = 1:size(accelMagn,1)
    accelRes = Saccel * (accelSensor(i, :)' - Baccel);
    magRes = R * Smag * (magSensor(i, :)' - Bmag);
    accelMagn(i) = norm(Saccel * (accelSensor(i, :)' - Baccel));
    magMagn(i) = norm(Smag * (magSensor(i, :)' - Bmag));
    dipRes(i) = acosd(dot(accelRes / accelMagn(i), magRes / magMagn(i)));
end

PlotSphere(roty(70)* [0 0 1]', scaleMag, biasMag, Smag, Bmag);
PlotSphere(roty(70)* [0 0 1]', scaleMag, biasMag, R * Smag, Bmag);
PlotSphere(roty(70)* [0 0 1]', scaleAccel, biasAccel, Saccel, Baccel);

figure('NumberTitle', 'off', 'Name', 'Magnitudes');
hold on;
plot(magMagn, '-r.');
plot(accelMagn, '-b.');
title('Magnitudes');
legend('Mag', 'Accel');
hold off;

figure('NumberTitle', 'off', 'Name', 'DipAngle');
hold on;
plot(dipRes, '-r.');
title('DipAngle');
legend('DipAngle');
hold off;
