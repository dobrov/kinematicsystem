%close all;
clear;
%clc;
addpath('jsonlab');
addpath('Logs');
addpath('Utils');

steadyTime = 2; %seconds
kAngularVelocityThreshold = 3; %deg
kDeltaAngularVelocityThreshold = 1;
kAccelMagnThreshold = 0.25; %g
kAccelMagnVelocityThreshold = 0.015;

isPlottingSteadyState = false;
isPlottingOldNewMagn = false;
is6PosAccelCalibration = false;
isDipAngleCalibration = false;

fileName = '624calib0.json';
rawData = loadjson(fileName);

accelDataRate = rawData.Config.AccelDataRate;
gyroDataRate = rawData.Config.GyroDataRate;

if accelDataRate ~= gyroDataRate
    disp('Accel data rate should be same as gyro data rate');
end

data = ProcessRawData(rawData, 'isCalMag', false, 'isPlottingAccel', false);

accelFS = data.Config.AccelFullScale;
magFS = data.Config.MagFullScale;
gyroFS = data.Config.GyroFullScale;

names = SortNames(fieldnames(data));
nDevice = length(names);

for i = 1:nDevice
    currentDevice = data.(names{i});
    ax = medfilt1(currentDevice.ax);
    ay = medfilt1(currentDevice.ay);
    az = medfilt1(currentDevice.az);
    mx = medfilt1(currentDevice.mx);
    my = medfilt1(currentDevice.my);
    mz = medfilt1(currentDevice.mz);
    gx = medfilt1(currentDevice.gx);
    gy = medfilt1(currentDevice.gy);
    gz = medfilt1(currentDevice.gz);
    
    len = length(ax);
    
    steadyState = ones(len, 1);
    steadyState(1) = 0;
    steadyState(end) = 0;
    accMagn = zeros(len, 1);
    accMagn(1) = sqrt(ax(1)^2 + ay(1)^2 + az(1)^2);
    for j = 2:len
        gx_prev = gx(j - 1);
        gy_prev = gy(j - 1);
        gz_prev = gz(j - 1);
        
        accMagnPrev = accMagn(j - 1);
        accMagn(j) = sqrt(ax(j)^2 + ay(j)^2 + az(j)^2);
        
        if abs(accMagn(j) - 1) > kAccelMagnThreshold
            steadyState(j) = 0;
            continue;
        end
        
        if abs(accMagnPrev - accMagn(j)) > kAccelMagnVelocityThreshold
            steadyState(j) = 0;
            continue;
        end
        
        if abs(gx(j)) > kAngularVelocityThreshold || ...
           abs(gy(j)) > kAngularVelocityThreshold || ...
           abs(gz(j)) > kAngularVelocityThreshold
            steadyState(j) = 0;
            continue;
        end
        
        if abs(gx(j) - gx_prev) > kDeltaAngularVelocityThreshold || ...
           abs(gy(j) - gy_prev) > kDeltaAngularVelocityThreshold || ...
           abs(gz(j) - gz_prev) > kDeltaAngularVelocityThreshold
            steadyState(j) = 0;
            continue;
        end
        
    end
       
    steadyStateBeginEnd = find(steadyState == 0);
    steadyStateLen = diff(steadyStateBeginEnd);
    steadyStateLenThreshold = steadyStateLen > (accelDataRate * steadyTime);
    nSamples = sum(steadyStateLenThreshold);
    measuredAccel = zeros(nSamples, 3);
    measuredMag = zeros(nSamples, 3);
    measuredGyro = zeros(nSamples, 3);
    steadyStateBeginEndFiltered = zeros(len, 1);
    
    k = 1;
    for j = 1:length(steadyStateLenThreshold)
        if steadyStateLenThreshold(j)
            beginIndex = steadyStateBeginEnd(j) + 10;
            endIndex = steadyStateBeginEnd(j + 1) - 10;
            steadyStateBeginEndFiltered(beginIndex) = 1.5;
            steadyStateBeginEndFiltered(endIndex) = 1.5;
            
            axMean = mean(ax(beginIndex:endIndex));
            ayMean = mean(ay(beginIndex:endIndex));
            azMean = mean(az(beginIndex:endIndex));
            measuredAccel(k, :) = [axMean ayMean azMean];
            
            % dr: accel = 2 * mag
            mxMean = mean(mx(ceil(beginIndex/2):floor(endIndex/2)));
            myMean = mean(my(ceil(beginIndex/2):floor(endIndex/2)));
            mzMean = mean(mz(ceil(beginIndex/2):floor(endIndex/2)));
            measuredMag(k, :) = [mxMean myMean mzMean];
            
            %measuredMag(k, :) = measuredMag(k, :) / norm(measuredMag(k, :));
            
            gxMean = mean(gx(beginIndex:endIndex));
            gyMean = mean(gy(beginIndex:endIndex));
            gzMean = mean(gz(beginIndex:endIndex));
            measuredGyro(k, :) = [gxMean gyMean gzMean];
            
            k = k + 1;
        end
    end
    
    if isPlottingSteadyState
        timeInSecAccel = data.tacc ./ power(10, 6);       
        figure('NumberTitle', 'off', 'Name', strcat('Accel ', names{i}));
        hold on;
        plot(timeInSecAccel, accMagn, '-k.');
        stem(timeInSecAccel, steadyStateBeginEndFiltered), '-m.';
        legend('magn', 'steadyState');
        xlabel('seconds');
        grid on;
    end
    
    fprintf('%s : Gyroscope\n', names{i});
    Bias = [mean(measuredGyro(:, 1)), mean(measuredGyro(:, 2)), mean(measuredGyro(:, 3))];
    disp(Bias);
    calData = struct('Bias', Bias, 'Scale', eye(3));
    calDataChar = savejson('', calData);
    fileID = fopen(strcat('Calibration/gyroCalData', names{i}, 'Range', num2str(gyroFS), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
    
    if is6PosAccelCalibration
        [Scale, Bias] = CalPosition(measuredAccel, [0 0 1; 0 0 -1; 0 1 0; 0 -1 0; 1 0 0; -1 0 0]); 
    else
        [Scale, Bias] = CalEllipsoid(measuredAccel);
    end

    fprintf('%s : Accelerometer\n', names{i});
    disp(Scale);
    disp(Bias);
      
%     calData = struct('Bias', Bias, 'Scale', Scale);
%     calDataChar = savejson('', calData);
%     fileID = fopen(strcat('Calibration/accelCalData', names{i}, 'Range', num2str(accelFS), '.json'), 'w');
%     fprintf(fileID,'%s', calDataChar);
%     fclose(fileID);
    
    oldMagn = zeros(nSamples, 1);
    newMagn = zeros(nSamples, 1);
    
    for j = 1:nSamples
        ax = measuredAccel(j, 1);
        ay = measuredAccel(j, 2);
        az = measuredAccel(j, 3);
        
        x_c = ax - Bias(1);
        y_c = ay - Bias(2);
        z_c = az - Bias(3);

        ax_c = Scale(1, 1) * x_c + Scale(1, 2) * y_c + Scale(1, 3) * z_c;
        ay_c = Scale(2, 1) * x_c + Scale(2, 2) * y_c + Scale(2, 3) * z_c;
        az_c = Scale(3, 1) * x_c + Scale(3, 2) * y_c + Scale(3, 3) * z_c;
        
%         fprintf('[%1.3f %1.3f %1.3f]: [%1.3f %1.3f %1.3f]\n', ax, ay, az, ax_c, ay_c, az_c);
        
        oldMagn(j) = sqrt(ax^2 + ay^2 + az^2);
        newMagn(j) = sqrt(ax_c^2 + ay_c^2 + az_c^2);
        
        measuredAccel(j, 1) = ax_c;
        measuredAccel(j, 2) = ay_c;
        measuredAccel(j, 3) = az_c;
        measuredAccel(j, :) = measuredAccel(j, :) / norm(measuredAccel(j, :));
    end
    
    if isPlottingOldNewMagn
        figure('NumberTitle', 'off', 'Name', strcat('OldNewMagn ', names{i}));
        hold on;
        plot(oldMagn, '-r.');
        plot(newMagn, '-g.');
        legend('oldMagnitude', 'newMagnitude');
        grid on;
    end
    
    [Scale, Bias] = CalEllipsoid(measuredMag);
    
    calMag = zeros(3, nSamples);
    for j = 1:nSamples
        mx = measuredMag(j, 1);
        my = measuredMag(j, 2);
        mz = measuredMag(j, 3);
        
        %measuredMag(j, :) = measuredMag(j, :) / norm(measuredMag(j, :)); 
        x_c = mx - Bias(1);
        y_c = my - Bias(2);
        z_c = mz - Bias(3);

        calMag(1, j) = Scale(1, 1) * x_c + Scale(1, 2) * y_c + Scale(1, 3) * z_c;
        calMag(2, j) = Scale(2, 1) * x_c + Scale(2, 2) * y_c + Scale(2, 3) * z_c;
        calMag(3, j) = Scale(3, 1) * x_c + Scale(3, 2) * y_c + Scale(3, 3) * z_c;
        
    end
    
    
    if isDipAngleCalibration
        [R, dipAngle] = DipRotation(measuredAccel, measuredMag);
    
        calData = data.Calibration.Mag.(names{i});
        calData.Scale = R * calData.Scale;

%         calDataChar = savejson('', calData);
%         fileID = fopen(strcat('Calibration/magCalData', names{i}, 'Range', num2str(magFS) ,'.json'), 'w');
%         fprintf(fileID,'%s', calDataChar);
%         fclose(fileID);

        fprintf('%s : Magnetometer\n', names{i});
        disp(calData.Scale);
        disp(calData.Bias);
        fprintf('DipAngle : %f\n', dipAngle - 90);
        
    end
end;
