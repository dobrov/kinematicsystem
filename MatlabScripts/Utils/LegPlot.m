function LegPlot(varargin)
points = varargin{1};
limOffset = 0.5;
figure('NumberTitle', 'off', 'Name', 'Leg Plot');
hold on;
set(gcf, 'Renderer', 'zbuffer');
set(gca,'SortMethod','depth');
grid minor;
axis equal;

xRange = reshape(points(:, 1, :), 1, numel(points(:, 1, :)));
yRange = reshape(points(:, 2, :), 1, numel(points(:, 2, :)));
zRange = reshape(points(:, 3, :), 1, numel(points(:, 3, :)));

maxX = max(xRange);
minX = min(xRange);
maxY = max(yRange);
minY = min(yRange);
maxZ = max(zRange);
minZ = min(zRange);

Ylim = [(minY - limOffset) (maxY + limOffset)];
Zlim = [(minZ - limOffset) (maxZ + limOffset)];
set(gca, 'Ylim', Zlim);

stepTrans = [maxX - minX, 0, 0];
[len, ~, nJoints] = size(points);
curTrans = [0, 0, 0];
curLimb = zeros(3, nJoints);
for i = 1:100:len
    for j = 1:nJoints
        curLimb(:, j) = points(i, :, j) + curTrans;
    end
    line(curLimb(1, :), curLimb(3, :), 'Color', [1 0 0], 'LineWidth', 2);
    curTrans = curTrans + stepTrans;
end

hold off;
end