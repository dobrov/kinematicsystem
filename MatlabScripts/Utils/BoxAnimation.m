function BoxAnimation(varargin)
freq = varargin{1};
q = varargin{2};
videoObj = [];
FullScreen = false;
for i = 3:2:nargin
    if strcmp(varargin{i}, 'VideoFileName')       
        fileName = strrep(strcat(varargin{i+1}, '.avi'), ' ', '');
        videoObj = VideoWriter(fileName);
        videoObj.FrameRate = fps;
        open(videoObj);
    elseif  strcmp(varargin{i}, 'FullScreen'), FullScreen = varargin{i+1};
    end                                                
end
fig = figure('NumberTitle', 'off', 'Name', 'Box');

view(30, 30);
lighting phong;
set(gca,'SortMethod','depth');
axis equal;
grid on;

Xlim = [-2 2];
Ylim = [-2 2];
Zlim = [-2 2];
set(gca, 'Xlim', Xlim, 'Ylim', Ylim, 'Zlim', Zlim);
if(FullScreen)
    screenSize = get(0, 'ScreenSize');
    set(fig, 'Position', [0 0 screenSize(3) screenSize(4)]);
end
xlabel('X');
ylabel('Y');
zlabel('Z');


%% Cylinders
noPoints = 20;
theta = linspace(0, 2*pi, noPoints);
radius = 0.05;
height = 2.5;

first = radius .* cos(theta);
second = radius .* sin(theta);

bottomX = [zeros(noPoints, 1), first', second'];
topX = [repmat(height, noPoints, 1), first', second'];
cylVertsX = [bottomX; topX];
bottomY = [first', zeros(noPoints, 1), second'];
topY = [first', repmat(height, noPoints, 1), second'];
cylVertsY = [bottomY; topY];
bottomZ = [first', second', zeros(noPoints, 1)];
topZ = [first', second', repmat(height, noPoints, 1)];
cylVertsZ = [bottomZ; topZ];

% faces
faces = [(1:noPoints)', (noPoints+1:2*noPoints)', ((noPoints+1:2*noPoints)+1)'];
faces(end, 3) = 1;

faces = [faces; (1:noPoints)', (2:noPoints+1)', ((noPoints+1:2*noPoints)+1)'];
faces(end, 2) = 1;
faces(end, 3) = noPoints + 1;

cylX = patch('faces', faces, 'vertices', cylVertsX, 'facecolor', [1 0 0], 'linestyle', 'none');
cylY = patch('faces', faces, 'vertices', cylVertsY, 'facecolor', [0 1 0], 'linestyle', 'none');
cylZ = patch('faces', faces, 'vertices', cylVertsZ, 'facecolor', [0 0 1], 'linestyle', 'none');

cylVertsXRot = zeros(size(cylVertsX));
cylVertsYRot = zeros(size(cylVertsY));
cylVertsZRot = zeros(size(cylVertsZ));

legend('X','Y','Z')

%% Cuboid
xyzCuboid = [-1  1 -1
              1  1 -1
              1  1  1
             -1  1  1
             -1 -1  1
              1 -1  1
              1 -1 -1
             -1 -1 -1];
   
facsCuboid = [1 2 3 4
              5 6 7 8
              4 3 6 5
              3 2 7 6
              2 1 8 7
              1 4 5 8];

CuboidHandle = patch('faces', facsCuboid, 'vertices', xyzCuboid, ...
                     'faceColor', [0.5 0.5 0.5], 'FaceAlpha',0.5);
cuboidVerts = zeros(size(xyzCuboid));

%% Rotations
for i = 1:size(q, 1)
    t_loopstart = tic();
    
    for j = 1:size(cuboidVerts, 1)
        cuboidVerts(j, :) = quatrotate(q(i, :), xyzCuboid(j, :));
    end
    set(CuboidHandle, 'vertices', cuboidVerts);
    
    for j = 1:size(cylVertsX, 1)
        cylVertsXRot(j, :) = quatrotate(q(i, :), cylVertsX(j, :));
        cylVertsYRot(j, :) = quatrotate(q(i, :), cylVertsY(j, :));
        cylVertsZRot(j, :) = quatrotate(q(i, :), cylVertsZ(j, :));
    end
    set(cylX, 'vertices', cylVertsXRot);
    set(cylY, 'vertices', cylVertsYRot);
    set(cylZ, 'vertices', cylVertsZRot);
    
    if(~isempty(videoObj))
        frame = getframe(fig);
        writeVideo(videoObj, frame);
     end
    el_time = toc(t_loopstart);
    pause(1/freq - el_time);
end

if(~isempty(videoObj))
    close(videoObj);
end