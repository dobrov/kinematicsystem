function SSE = objFunAccelMag(optVal, data, magnitude)
    gain = optVal;    
    x_c = gain(1) * data(1) + gain(2) * data(2) + gain(3) * data(3);
    y_c = gain(4) * data(1) + gain(2) * data(2) + gain(6) * data(3);
    z_c = gain(7) * data(1) + gain(8) * data(2) + gain(3) * data(3);
	error = magnitude - sqrt(x_c.^2 + y_c.^2 + z_c.^2);
    SSE = error' * error;               % return the Sum of the Squares of the Errors
end
