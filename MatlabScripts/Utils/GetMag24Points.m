function points = GetMag24Points(initPoint)

r = zeros(6, 3);
b = zeros(6, 3);

b(1, :) = initPoint;
r(1, :) = [1 0 0];

b(2, :) = rotz(90) * b(1, :)'; 
r(2, :) = [0 1 0];

b(3, :) = rotx(90) * b(2, :)'; 
r(3, :) = [0 0 1];

b(4, :) = roty(-90) * b(3, :)'; 
r(4, :) = [-1 0 0];

b(5, :) = rotz(90) * b(4, :)'; 
r(5, :) = [0 -1 0];

b(6, :) = rotx(90) * b(5, :)'; 
r(6, :) = [0 0 -1];

points = zeros(size(b, 1) * 4, 3);
for i = 1:6
    for j = 1:4
        points(4 * (i - 1) + j, :) = quatrotate([cosd(j*90/2), -sind(j*90/2) * r(i, :)], b(i, :));
    end
end

end