function [orderedValues] = OrderMeasure(trueValues, sensorValues)
orderedValues = zeros(size(trueValues));
for i = 1:size(trueValues, 1)
    curVector = zeros(1, 3);
    minNorm = 100;
    for j = 1:size(sensorValues, 1)
        tNorm = norm(trueValues(i, :) - sensorValues(j, :));
        if tNorm < minNorm
            minNorm = tNorm;
            curVector = sensorValues(j, :);
        end
    end
    orderedValues(i, :) = curVector;
end
end

