function quat = rotationTo (from, to)
from = from / norm(from);
to = to / norm(to);

d = dot(from, to) + 1;

if abs(d) < 0.001
    axis = cross([1 0 0], from);
    if norm(axis) < 0.001
        axis = cross([0 1 0], from);
    end
    axis = axis / norm(axis);
    quat = [0, axis];
    return;
end

d = sqrt(2.0 * d);
axis = cross(from, to) / d;

quat = [d * 0.5, -axis];
quat = quat / norm(quat);

% d = dot(from, to);
% k = norm(from) * norm(to);
% 
% if d / k == -1
%     axis = cross([1 0 0], from);
%     if norm(axis) < 0.001
%         axis = cross([0 1 0], from);
%     end
%     axis = axis / norm(axis);
%     quat = [0, axis];
%     return;    
% end
% 
% quat = [d + k, cross(from, to)];
% quat = quat / norm(quat);

end