function q = AquaMeasurement(a, m, qPrev)
a = a / norm(a);
m = m / norm(m);

ax = a(1);
ay = a(2);
az = a(3);

mx = m(1);
my = m(2);
mz = m(3);

qa = zeros(1, 4);
if az >= 0
    lambda = sqrt((az + 1) / 2);
    qa(1) = lambda;
    qa(2) = -ay / (2 * lambda);
    qa(3) = ax / (2 * lambda);
else
    lambda = sqrt((1 - az) / 2);
    qa(1) = -ay / (2 * lambda);
    qa(2) = lambda;
    qa(4) = ax / (2 * lambda);
end

l = quatrotate(qa, m);
lx = l(1);
ly = l(2);

qm = zeros(1, 4);
gamma = lx * lx + ly * ly;
if lx >= 0
    qm(1) = sqrt(gamma + lx * sqrt(gamma)) / sqrt(2 * gamma);
    qm(4) = ly / (sqrt(2) * sqrt(gamma + lx * sqrt(gamma))); 
else
    qm(1) = ly / (sqrt(2) * sqrt(gamma - lx * sqrt(gamma)));
    qm(4) = sqrt(gamma - lx * sqrt(gamma)) / sqrt(2 * gamma);
end

q = quatmultiply(qa, qm);
if sum(qPrev ~= [0 0 0 0]) ~= 0 && dot(qPrev, q) < 0
    q = -q;
end
end

