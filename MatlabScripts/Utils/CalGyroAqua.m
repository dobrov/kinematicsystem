function sGyro = CalGyroAqua(calAccel, calMag, bGyro, gyroBetween, gyroFreq)

for i = 1:length(calAccel) - 1
    gyroBetween.(strcat('x', int2str(i))) = gyroBetween.(strcat('x', int2str(i))) - bGyro(1);
    gyroBetween.(strcat('y', int2str(i))) = gyroBetween.(strcat('y', int2str(i))) - bGyro(2);
    gyroBetween.(strcat('z', int2str(i))) = gyroBetween.(strcat('z', int2str(i))) - bGyro(3);
end

staticQuat = zeros(length(calAccel), 4);
for i = 1:length(calAccel)
    staticQuat(i, :) = AquaMeasurement(calAccel(i, :)', calMag(i, :)');
end

E = [1, 0, 0, 0, 1, 0, 0, 0, 1];
ObjFun = @(E) GyroCostFunction(E, gyroBetween, staticQuat, 1/gyroFreq);
options = optimset('MaxFunEvals', 150000, 'MaxIter', 6000, ...
    'TolFun', 10^(-5), 'Algorithm', 'levenberg-marquardt', 'TolX', 10^(-5));
%'Display', 'iter'
[E, rsnorm] = lsqnonlin(ObjFun, E, [], [], options);
disp(rsnorm);
sGyro = reshape(E, 3, 3);

end
