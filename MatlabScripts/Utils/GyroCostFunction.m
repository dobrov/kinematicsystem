function [residuals] = GyroCostFunction(E, gyroBetween, staticQuat, dt)

scale = reshape(E, 3, 3);
residuals = zeros(size(staticQuat, 1) - 1, 1);
    
for i = 1:size(staticQuat, 1) - 1
    gx = gyroBetween.(strcat('x', int2str(i)));
    gy = gyroBetween.(strcat('y', int2str(i)));
    gz = gyroBetween.(strcat('z', int2str(i)));
    
    gyroQuat = staticQuat(i, :);
    for j = 1:length(gx)
        calGyro = deg2rad(scale * [gx(j), gy(j) gz(j)]');
        qDot = -0.5 * quatmultiply([0 calGyro'], gyroQuat);
        gyroQuat = quatnormalize(gyroQuat + qDot * dt);
    end
    measQuat = staticQuat(i + 1, :);
    if dot(gyroQuat, measQuat) < 0
        gyroQuat = -gyroQuat;
    end
    %disp(norm(measQuat - gyroQuat));
    residuals(i) = norm(measQuat - gyroQuat);
end

end