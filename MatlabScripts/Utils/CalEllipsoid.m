function [Scale, Bias] = CalEllipsoid(measuredSensor)
[e_center, e_radii, e_eigenvecs, ~] = ellipsoid_fit(measuredSensor);
R = e_eigenvecs;
Scale = R * diag([1 / e_radii(1), 1 / e_radii(2), 1 / e_radii(3)]) * R';
Bias = e_center';
end

