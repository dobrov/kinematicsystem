function [R, angle] = DipRotation(accel, mag)
M = [-accel(:, 1) .* mag(:, 2) -accel(:, 1) .* mag(:, 3) -accel(:, 2) .* mag(:, 1) ...
     -accel(:, 2) .* mag(:, 2) -accel(:, 2) .* mag(:, 3) -accel(:, 3) .* mag(:, 1) ...
     -accel(:, 3) .* mag(:, 2) -accel(:, 3) .* mag(:, 3) ones(size(accel(:, 1)))];
v = accel(:, 1) .* mag(:, 1);

h = ( M' * M ) \ ( M' * v );
T = [1 h(1) h(2);
     h(3) h(4) h(5);
     h(6) h(7) h(8)];
r11 = (1/det(T)) ^ (1/3);

R = r11 * T;
angle = acosd(h(9) * r11);
end

