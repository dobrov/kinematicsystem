function [staticPos, nSamples] = FindStaticPos(data, varargin)
isAccel = false;
isGyro = false;
isGyroBetween = false;
isMag = false;
isPlottingSteadyState = false;

accelDataRate = data.Config.AccelDataRate;
gyroDataRate = data.Config.GyroDataRate;

if accelDataRate ~= gyroDataRate
    error('Accel data rate should be same as gyro data rate');
end

minTransTime = 0.3; %second
steadyTime = 2; %seconds

for i = 1:2:nargin-1
    if strcmp(varargin{i}, 'isAccel'), isAccel = varargin{i+1};
    elseif strcmp(varargin{i}, 'isGyroBetween'), isGyroBetween = varargin{i+1};
    elseif strcmp(varargin{i}, 'isGyro'), isGyro = varargin{i+1};
    elseif strcmp(varargin{i}, 'isMag'), isMag = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingSteadyState'), isPlottingSteadyState = varargin{i+1};
    end                                                
end

names = SortNames(fieldnames(data));
nDevice = length(names);

len = length(data.tacc);
steadyState = ones(len, nDevice);

for i = 1:nDevice
    currentDevice = data.(names{i});
    ax = medfilt1(currentDevice.ax);
    ay = medfilt1(currentDevice.ay);
    az = medfilt1(currentDevice.az);
    gx = medfilt1(currentDevice.gx);
    gy = medfilt1(currentDevice.gy);
    gz = medfilt1(currentDevice.gz);
    
    for j = 2:len
        steadyState(j, :) = CheckState([ax(j) ay(j) az(j)], [gx(j) gy(j) gz(j)], ...
            [ax(j - 1) ay(j - 1) az(j - 1)], [gx(j - 1) gy(j - 1) gz(j - 1)]);
    end
    
    if (steadyState(2, i) == 1)
        steadyState(1, i) = 0;
    elseif (steadyState(end, i) == 1)
        steadyState(end, i) = 0;
    end
end

steadyStateShared = zeros(len, nDevice);
for i = 1:len
    if (sum(~steadyState(i, :)) == 0)
        steadyStateShared(i) = 1;
    else
        steadyStateShared(i) = 0;
    end
end
    
steadyStateBeginEnd = find(steadyStateShared == 0);
steadyStateLen = diff(steadyStateBeginEnd);
steadyStateLenThreshold = steadyStateLen > (data.Config.AccelDataRate * steadyTime);

for i = 1:nDevice
    currentDevice = data.(names{i});
    if isAccel
        ax = currentDevice.ax;
        ay = currentDevice.ay;
        az = currentDevice.az;
    end

    if isGyro || isGyroBetween
        gx = currentDevice.gx;
        gy = currentDevice.gy;
        gz = currentDevice.gz;
    end

    if isMag
        mx = currentDevice.mx;
        my = currentDevice.my;
        mz = currentDevice.mz;
    end

    if isPlottingSteadyState
        steadyStateBeginEndFiltered = zeros(len, 1);
    end

    k = 1;
    prevEnd = -1000;
    for j = 1:length(steadyStateLenThreshold)
        if steadyStateLenThreshold(j)
            beginIndex = steadyStateBeginEnd(j) + 5;
            endIndex = steadyStateBeginEnd(j + 1) - 5;
            if abs(prevEnd - beginIndex) < data.Config.AccelDataRate * minTransTime
                continue;
            end
            
            if isPlottingSteadyState
                steadyStateBeginEndFiltered(beginIndex) = 1.5;
                steadyStateBeginEndFiltered(endIndex) = 1.5;
            end

            if isAccel
                staticPos.(names{i}).Accel.x(k) = mean(ax(beginIndex:endIndex));
                staticPos.(names{i}).Accel.y(k) = mean(ay(beginIndex:endIndex));
                staticPos.(names{i}).Accel.z(k) = mean(az(beginIndex:endIndex));         
            end

            if isMag
                % dr: accel = 2 * mag
                staticPos.(names{i}).Mag.x(k) = mean(mx(ceil(beginIndex/2):floor(endIndex/2)));
                staticPos.(names{i}).Mag.y(k) = mean(my(ceil(beginIndex/2):floor(endIndex/2)));
                staticPos.(names{i}).Mag.z(k) = mean(mz(ceil(beginIndex/2):floor(endIndex/2)));
            end

            if isGyro
                staticPos.(names{i}).Gyro.x(k) = mean(gx(beginIndex:endIndex));
                staticPos.(names{i}).Gyro.y(k) = mean(gy(beginIndex:endIndex));
                staticPos.(names{i}).Gyro.z(k) = mean(gz(beginIndex:endIndex));
            end
            
            if isGyroBetween && prevEnd ~= -1000
                staticPos.(names{i}).GyroBetween.(strcat('x', int2str(k-1))) = gx(prevEnd:beginIndex);
                staticPos.(names{i}).GyroBetween.(strcat('y', int2str(k-1))) = gy(prevEnd:beginIndex);
                staticPos.(names{i}).GyroBetween.(strcat('z', int2str(k-1))) = gz(prevEnd:beginIndex);
            end
            
            k = k + 1;
            prevEnd = endIndex;
        end
    end

    nSamples = k - 1;
    if isPlottingSteadyState
        timeInSecAccel = data.tacc ./ power(10, 6);       
        figure('NumberTitle', 'off', 'Name', strcat('SteadyState ', names{i}));
        hold on;
        plot(timeInSecAccel, sqrt(ax(:).^2 + ay(:).^2 + az(:).^2), '-k.');
        stem(timeInSecAccel, steadyStateBeginEndFiltered), '-m.';
        legend('magn', 'steadyState');
        xlabel('seconds');
        grid on;
    end
end
end
