function isSteadyState = CheckState(accel, gyro, prevAccel, prevGyro)

kAngularVelocityThreshold = 3; %deg
kDeltaAngularVelocityThreshold = 1;
kAccelMagnThreshold = 0.25; %g
kAccelMagnVelocityThreshold = 0.015;

accelMagn = norm(accel);
if abs(1 - accelMagn) > kAccelMagnThreshold
    isSteadyState = false;
    return;
end

prevAccelMagn = norm(prevAccel);
if abs(prevAccelMagn - accelMagn) > kAccelMagnVelocityThreshold
    isSteadyState = false;
    return;
end

gx = gyro(1);
gy = gyro(2);
gz = gyro(3);
if abs(gx) > kAngularVelocityThreshold || ...
   abs(gy) > kAngularVelocityThreshold || ...
   abs(gz) > kAngularVelocityThreshold
    isSteadyState = false;
    return;
end

gx_prev = prevGyro(1);
gy_prev = prevGyro(2);
gz_prev = prevGyro(3);
if abs(gx - gx_prev) > kDeltaAngularVelocityThreshold || ...
   abs(gy - gy_prev) > kDeltaAngularVelocityThreshold || ...
   abs(gz - gz_prev) > kDeltaAngularVelocityThreshold
    isSteadyState = false;
    return;
end   

isSteadyState = true;

end

