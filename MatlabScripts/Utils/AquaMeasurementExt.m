function [q, J] = AquaMeasurementExt(a, m, qPrev)
a = a / norm(a);
m = m / norm(m);

ax = a(1);
ay = a(2);
az = a(3);

mx = m(1);
my = m(2);
mz = m(3);

qa = zeros(1, 4);
if az >= 0
    lambda = sqrt((az + 1) / 2);
    qa(1) = lambda;
    qa(2) = -ay / (2 * lambda);
    qa(3) = ax / (2 * lambda);
else
    lambda = sqrt((1 - az) / 2);
    qa(1) = -ay / (2 * lambda);
    qa(2) = lambda;
    qa(4) = ax / (2 * lambda);
end

l = quatrotate(qa, m');
lx = l(1);
ly = l(2);

qm = zeros(1, 4);
gamma = lx * lx + ly * ly;
if lx >= 0
    qm(1) = sqrt(gamma + lx * sqrt(gamma)) / sqrt(2 * gamma);
    qm(4) = ly / (sqrt(2) * sqrt(gamma + lx * sqrt(gamma))); 
else
    qm(1) = ly / (sqrt(2) * sqrt(gamma - lx * sqrt(gamma)));
    qm(4) = sqrt(gamma - lx * sqrt(gamma)) / sqrt(2 * gamma);
end

q = quatmultiply(qa, qm);
if sum(qPrev ~= [0; 0; 0; 0]) ~= 0 && dot(qPrev, q) < 0
    q = -q;
end

k = sqrt(1 + az);
y = gamma;
b1 = sqrt(y + lx * sqrt(y));
b2 = sqrt(y - lx * sqrt(y));
if az >= 0
    dqdf1 = [qm(1) 0 0 -qm(4) qa(1) -qa(2) qa(3) 0;
             0 qm(1) qm(4) 0 qa(2) qa(1) 0 qa(3);
             0 -qm(4) qm(1) 0 qa(3) 0 qa(1) -qa(2);
             qm(4) 0 0 qm(1) 0 -qa(3) qa(2) qa(1)];
   if lx >= 0
       df1df2 = [0 0 1/k 0 0 0;
                 0 -2/k ay/k^3 0 0 0;
                 2/k 0 -ax/k^3 0 0 0;
                 0 0 0 0 0 0;
                 0 0 0 ly^2/(b1*y) lx*ly/(b1*y) 0;
                 0 0 0 0 0 0;
                 0 0 0 0 0 0;
                 0 0 0 -ly*b1/y^(3/2) lx*b1/y^(3/2) 0];
   else
       df1df2 = [0 0 1/k 0 0 0;
                 0 -2/k ay/k^3 0 0 0;
                 2/k 0 -ax/k^3 0 0 0;
                 0 0 0 0 0 0;
                 0 0 0 ly*b2/y^(3/2) lx*b2/y^(3/2) 0;
                 0 0 0 0 0 0;
                 0 0 0 0 0 0;
                 0 0 0 -ly^2/(b2*y) lx*ly/(b2*y) 0];
   end
   df2du = [mz - (2*ax*mx + ay*my)/k^2 -ax*my/k^2 ax*(ax*mx + ay*my)/k^4 1 - ax^2/k^2 -ax*ay/k^2 ax;
            -ay*mx/k^2 mz - (ax*mx + 2*ay*my)/k^2 ay*(ax*mx + ay*my)/k^4 -ax*ay/k^2 1 - ay^2/k^2 ay;
            -mx -my mz -ax -ay az];
else
    dqdf1 = [qm(1) 0 0 -qm(4) qa(1) -qa(2) 0 -qa(4);
             0 qm(1) qm(4) 0 qa(2) qa(1) -qa(4) 0;
             0 -qm(4) qm(1) 0 0 qa(4) qa(1) -qa(2);
             qm(3) 0 0 qm(1) qa(4) 0 qa(2) qa(1)];
    if lx >= 0
        df1df2 = [0 -2/k -ay/k^3 0 0 0;
                  0 0 -1/k 0 0 0;
                  0 0 0 0 0 0;
                  2/k 0 ax/k^3 0 0 0;
                  0 0 0 ly^2/(b1*y) -lx*ly/(b1*y) 0;
                  0 0 0 0 0 0;
                  0 0 0 0 0 0;
                  0 0 0 -ly*b1/y^(3/2) lx*b1/y^(3/2) 0];
    else
        df1df2 = [0 -2/k -ay/k^3 0 0 0;
                  0 0 -1/k 0 0 0;
                  0 0 0 0 0 0;
                  2/k 0 ax/k^3 0 0 0;
                  0 0 0 ly*b2/y^(3/2) -lx*b2/y^(3/2) 0;
                  0 0 0 0 0 0;
                  0 0 0 0 0 0;
                  0 0 0 -ly^2/(b2*y) lx*ly/(b2*y) 0];
    end
    df2du = [mz - (2*ax*mx - ay*my)/k^2 ax*my/k^2 ax*(-ax*mx + ay*my)/k^4 1 - ax^2/k^2 -ax*ay/k^2 ax;
             -ay*mx/k^2 mz - (ax*mx - 2*ay*my)/k^2 ay*(-ax*mx + ay*my)/k^4 -ax*ay/k^2 -1 + ay^2/k^2 ay;
             mx -my -mz ax -ay -az];
end

df1df2 = 1/(2*sqrt(2)) * df1df2;
df2du = [eye(3), zeros(3, 3);
         df2du];
     
J = dqdf1 * df1df2 * df2du;
end

