function equal = IsEqual(q1, q2)
    for i = 1:4
        if abs(q1(i) - q2(i)) > 0.01
            equal = 0;
            return;
        end
    end
    equal = 1;
end