function calData = ApplyCalibration(scale, bias, data)
if (size(data, 2) ~= 3)
    error('Input data must be Nx3 dimension');
end

calData = zeros(size(data));
for i = 1:size(data, 1)
    calData(i, :) = (scale * (data(i, :) - bias)')';
end

end

