function ForwardKinematics(freq, Rgb, L, varargin)

isPlottingJointAnglesX = false;
isPlottingJointAnglesY = false;
isPlottingJointAnglesZ = false;
isShowingAnimation = false;
isVideo = false;
isMirror = false;
isPlotLine = false;

for i = 1:2:nargin-4
    if strcmp(varargin{i}, 'isPlottingJointAnglesX'), isPlottingJointAnglesX = varargin{i+1};
    elseif  strcmp(varargin{i}, 'isPlottingJointAnglesY'), isPlottingJointAnglesY = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingJointAnglesZ'), isPlottingJointAnglesZ = varargin{i+1};
    elseif strcmp(varargin{i}, 'isShowingAnimation'), isShowingAnimation = varargin{i+1};
    elseif strcmp(varargin{i}, 'isVideo'), isVideo = varargin{i+1};
    elseif strcmp(varargin{i}, 'isMirror'), isMirror = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlotLine'), isPlotLine = varargin{i+1};
    end                                                
end

nDevice = size(Rgb, 4);
len = size(Rgb, 3);

if isPlottingJointAnglesX || isPlottingJointAnglesY || isPlottingJointAnglesZ
    
    zAngle = zeros(len, nDevice);
    yAngle = zeros(len, nDevice);
    xAngle = zeros(len, nDevice);
    legnd = cell(nDevice, 1);
    for i = 1:nDevice        
        for j = 1:len
            [zAngle(j, i), yAngle(j, i), xAngle(j, i)] = dcm2angle(Rgb(:, :, j, i));
        end
        legnd{i} = ['Angle ', num2str(i)];
    end
    
    possibleColors = cell(7, 1);
    possibleColors{1} = '-r.';
    possibleColors{2} = '-g.';
    possibleColors{3} = '-b.';
    possibleColors{4} = '-k.';
    possibleColors{5} = '-m.';
    possibleColors{6} = '-y.';
    possibleColors{7} = '-c.';
    
    if isPlottingJointAnglesX        
        figure('NumberTitle', 'off', 'Name', 'JointAnglesX');
        hold on;
        grid minor;
        title('JointAngles');
        
        for i = 1:nDevice
            plot(rad2deg(xAngle(:, i)), possibleColors{i});
        end
        legend(legnd{:});
    end
       
    if isPlottingJointAnglesY
        figure('NumberTitle', 'off', 'Name', 'JointAnglesY');
        hold on;
        grid minor;
        title('JointAngles');        
        
        for i = 1:nDevice
            plot(rad2deg(yAngle(:, i)), possibleColors{i});
        end
        legend(legnd{:});
    end
    
    if isPlottingJointAnglesZ
        figure('NumberTitle', 'off', 'Name', 'JointAnglesZ');
        hold on;
        grid minor;
        title('JointAngles');        
        
        for i = 1:nDevice
            plot(rad2deg(zAngle(:, i)), possibleColors{i});
        end
        legend(legnd{:});
    end
end

point1 = zeros(3, len);
T01 = rotm2tform(Rgb(:, :, :, 1));
T12 = trvec2tform([L(1) 0 0]);
T02 = zeros(4, 4, len);

for i = 1:len
    T02(:, :, i) = T01(:, :, i) * T12;
    point1(:, i) = T02(1:3, 4, i);
end

point2 = zeros(3, len);
T23 = rotm2tform(Rgb(:, :, :, 2));
T34 = trvec2tform([L(2) 0 0]);
T04 = zeros(4, 4, len);

for i = 1:len
    T04(:, :, i) = T02(:, :, i) * T23(:, :, i) * T34;
    point2(:, i) = T04(1:3, 4, i);
end

point3 = zeros(3, len);
T45 = rotm2tform(Rgb(:, :, :, 3));
T56 = trvec2tform([L(3) 0 0]);
T06 = zeros(4, 4, len);

for i = 1:len
    T06(:, :, i) = T04(:, :, i) * T45(:, :, i) * T56;
    point3(:, i) = T06(1:3, 4, i);
end

point4 = zeros(3, len);
T67 = rotm2tform(Rgb(:, :, :, 4));
T78 = trvec2tform([L(4) 0 0]);

for i = 1:len
    T08 = T06(:, :, i) * T67(:, :, i) * T78;
    point4(:, i) = T08(1:3, 4);
end

points = zeros(size(point1, 2), size(point1, 1), 4);
points(:, :, 1) = point1';
points(:, :, 2) = point2';
points(:, :, 3) = point3';
points(:, :, 4) = point4';

if isShowingAnimation
    if isVideo
        LegAnimation(freq, points, 'VideoFileName', ...
            'LegScript', 'PlotLine', isPlotLine);
    else
        LegAnimation(freq, points, 'isPlotLine', isPlotLine, 'isMirror', isMirror);
    end   
end

end