function [procData] = ProcessRawData(rawData, varargin)
addpath('Calibration');
isPlottingAccel = false;
isPlottingGyro = false;
isPlottingMag = false;
isPlottingCalMag = false;
isPlottingCalAccel = false;
isPlottingCalGyro = false;
isCalMag = false;
isCalAccel = false;
isCalGyro = false;
isCompareAccelMagn = false;
isPlottingDipAngle = false;
isFiltered = false;
isMean = false;

for i = 1:2:nargin-1
    if strcmp(varargin{i}, 'isPlottingAccel'), isPlottingAccel = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingCalAccel'), isPlottingCalAccel = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingGyro'), isPlottingGyro = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingCalGyro'), isPlottingCalGyro = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingMag'), isPlottingMag = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingCalMag'), isPlottingCalMag = varargin{i+1};
    elseif strcmp(varargin(i), 'isPlottingDipAngle'), isPlottingDipAngle = varargin{i+1};
    elseif strcmp(varargin{i}, 'isCalMag'), isCalMag = varargin{i+1};
    elseif strcmp(varargin{i}, 'isCalAccel'), isCalAccel = varargin{i+1};
    elseif strcmp(varargin{i}, 'isCalGyro'), isCalGyro = varargin{i+1};
    elseif strcmp(varargin{i}, 'isCompareAccelMagn'), isCompareAccelMagn = varargin{i+1};
    elseif strcmp(varargin{i}, 'isFiltered'), isFiltered = varargin{i+1};
    elseif strcmp(varargin{i}, 'isMean'), isMean = varargin{i+1};
    end
end

accMap = [2, 4, 8, 16; 0.000061, 0.000122, 0.000244, 0.000732];
gyroMap = [250, 500, 2000; 0.00875, 0.0175, 0.07];
magMap = [2, 4, 8, 16; 0.00008, 0.00016, 0.00032, 0.000479];

procData.Config = rawData.Config;

if isfield(rawData.Config, 'AccelSens')
    aSens = rawData.Config.AccelSens;
    gSens = rawData.Config.GyroSens;
    mSens = rawData.Config.MagSens;
    procData.Config.AccelFullScale = accMap(1, accMap(2, :) == aSens);
    procData.Config.GyroFullScale = gyroMap(1, gyroMap(2, :) == gSens);
    procData.Config.MagFullScale = magMap(1, magMap(2, :) == mSens);
else    
    aFS = rawData.Config.AccelFullScale;
    gFS = rawData.Config.GyroFullScale;
    mFS = rawData.Config.MagFullScale;
    procData.Config.AccelSens = accMap(2, accMap(1, :) == aFS);
    procData.Config.GyroSens = gyroMap(2, gyroMap(1, :) == gFS);
    procData.Config.MagSens = magMap(2, magMap(1, :) == mFS);
end

accelDataRate = rawData.Config.AccelDataRate;
gyroDataRate = rawData.Config.GyroDataRate;
magDataRate = rawData.Config.MagDataRate;

deviceNames = SortNames(fieldnames(rawData.Data));
nDevice = length(deviceNames);

inTime = cell(nDevice * 3, 1);
outIters = cell(nDevice * 6, 1);
if isfield(rawData.Data.(deviceNames{1}), 'tacc')
    for i = 1:nDevice
        currentDevice = rawData.Data.(deviceNames{i});
        inTime{3*i - 2} = currentDevice.tacc;
        inTime{3*i - 1} = currentDevice.tgyro;
        inTime{3*i} = currentDevice.tmag;
    end
    [outIters{:}] = CutTime(inTime{:});
    firstDevice = rawData.Data.(deviceNames{1});
    procData.tacc = firstDevice.tacc(outIters{1}:outIters{2});
    procData.tgyro = firstDevice.tgyro(outIters{3}:outIters{4});
    procData.tmag = firstDevice.tmag(outIters{5}:outIters{6});
else
    accelPeriod = 1 / accelDataRate * 1000000;
    gyroPeriod = 1 / gyroDataRate * 1000000;
    magPeriod = 1 / magDataRate * 1000000;
    for i = 1:nDevice
        currentDevice = rawData.Data.(deviceNames{i});
        accelLength = numel(currentDevice.RawAccel.x);
        gyroLength = numel(currentDevice.RawGyro.x);
        magLength = numel(currentDevice.RawMag.x);
        inTime{3*i - 2} = 0: accelPeriod: (accelLength - 1) * accelPeriod;
        inTime{3*i - 1} = 0: gyroPeriod: (gyroLength - 1) * gyroPeriod;
        inTime{3*i} = 0: magPeriod: (magLength - 1) * magPeriod;
    end
    [outIters{:}] = CutTime(inTime{:});
    accelLength = outIters{2} - outIters{1};
    gyroLength = outIters{4} - outIters{3};
    magLength = outIters{6} - outIters{5};
    procData.tacc = 0: accelPeriod: accelLength * accelPeriod;
    procData.tgyro = 0: gyroPeriod: gyroLength * gyroPeriod;
    procData.tmag = 0: magPeriod: magLength * magPeriod;
end

for i = 1:nDevice
    currentDevice = rawData.Data.(deviceNames{i});
    
    if isfield(currentDevice, 'ax')
        ax = currentDevice.ax(outIters{6*i - 5}:outIters{6*i - 4});
        ay = currentDevice.ay(outIters{6*i - 5}:outIters{6*i - 4});
        az = currentDevice.az(outIters{6*i - 5}:outIters{6*i - 4});

        % not mistake: gy and gx should be swap
        gx = currentDevice.gy(outIters{6*i - 3}:outIters{6*i - 2});
        gy = currentDevice.gx(outIters{6*i - 3}:outIters{6*i - 2});
        gz = currentDevice.gz(outIters{6*i - 3}:outIters{6*i - 2});
        
        mx = currentDevice.mx(outIters{6*i - 1}:outIters{6*i});
        my = currentDevice.my(outIters{6*i - 1}:outIters{6*i});
        mz = currentDevice.mz(outIters{6*i - 1}:outIters{6*i});
        
    else
        ax = currentDevice.RawAccel.x(outIters{6*i - 5}:outIters{6*i - 4});
        ay = currentDevice.RawAccel.y(outIters{6*i - 5}:outIters{6*i - 4});
        az = currentDevice.RawAccel.z(outIters{6*i - 5}:outIters{6*i - 4});
        
        gx = currentDevice.RawGyro.y(outIters{6*i - 3}:outIters{6*i - 2});
        gy = currentDevice.RawGyro.x(outIters{6*i - 3}:outIters{6*i - 2});
        gz = currentDevice.RawGyro.z(outIters{6*i - 3}:outIters{6*i - 2});
        
        mx = currentDevice.RawMag.x(outIters{6*i - 1}:outIters{6*i});
        my = currentDevice.RawMag.y(outIters{6*i - 1}:outIters{6*i});
        mz = currentDevice.RawMag.z(outIters{6*i - 1}:outIters{6*i});
    end
   
    ax = ax .* procData.Config.AccelSens;
    ay = ay .* procData.Config.AccelSens;
    az = az .* procData.Config.AccelSens;
    gx = gx .* procData.Config.GyroSens;
    gy = gy .* procData.Config.GyroSens;
    gz = gz .* procData.Config.GyroSens;
    mx = mx .* procData.Config.MagSens;
    my = my .* procData.Config.MagSens;
    mz = mz .* procData.Config.MagSens;
    
    gx = -gx;
    gy = -gy;
    gz = -gz;
    
    if isFiltered
        bCutOff = 3;
        bOrder = 2;
        [b, a] = butter(bOrder, 2*bCutOff/accelDataRate, 'low');
        ax = filtfilt(b, a, ax);
        ay = filtfilt(b, a, ay);
        az = filtfilt(b, a, az);
        
%         [b, a] = butter(bOrder, 2*bCutOff/magDataRate, 'low');
%         mx = filtfilt(b, a, mx);
%         my = filtfilt(b, a, my);
%         mz = filtfilt(b, a, mz);
    end
    
    if isPlottingAccel        
        timeInSecAccel = procData.tacc ./ power(10, 6);       
        figure('NumberTitle', 'off', 'Name', strcat('Accel ', deviceNames{i}));
        hold on;
        plot(timeInSecAccel, ax, '-r.');
        plot(timeInSecAccel, ay, '-g.');
        plot(timeInSecAccel, az, '-b.');
        plot(timeInSecAccel, sqrt(ax .^ 2 + ay .^ 2 + az .^ 2), '-k.');
        legend('accelX', 'accelY', 'accelZ', 'magn');
        xlabel('seconds');
        grid on;
    end
    
    if isPlottingGyro        
        timeInSecGyro = procData.tgyro ./ power(10, 6);
        figure('NumberTitle', 'off', 'Name', strcat('Gyro ', deviceNames{i}));
        hold on;
        plot(timeInSecGyro, gx, '-r.');
        plot(timeInSecGyro, gy, '-g.');
        plot(timeInSecGyro, gz, '-b.');
        legend('gyroX', 'gyroY', 'gyroZ');
        xlabel('seconds');
        grid on;
    end
    
    if isCalGyro
        fileName = strcat('gyroCalData', deviceNames{i}, 'Range', num2str(procData.Config.GyroFullScale), '.json');
        if exist(fileName, 'file')                     
            calData = loadjson(fileName);
            procData.Calibration.Gyro.(deviceNames{i}) = calData;
            
            calData = ApplyCalibration(calData.Scale, calData.Bias, [gx', gy', gz']);
            gx = calData(:, 1)';
            gy = calData(:, 2)';
            gz = calData(:, 3)';
        end
    end
    
    if isPlottingCalGyro && isCalGyro
        timeInSecGyro = procData.tgyro ./ power(10, 6);
        figure('NumberTitle', 'off', 'Name', strcat('Calibrated Gyro ', deviceNames{i}));
        hold on;
        plot(timeInSecGyro, gx, '-r.');
        plot(timeInSecGyro, gy, '-g.');
        plot(timeInSecGyro, gz, '-b.');
        legend('gyroX', 'gyroY', 'gyroZ');
        xlabel('seconds');
        grid on;
    end
    
    if isCalAccel
        fileName = strcat('accelCalData', deviceNames{i}, 'Range', num2str(procData.Config.AccelFullScale), '.json');
        if exist(fileName, 'file')           
            if isCompareAccelMagn
                oldMagn = sqrt(ax .^ 2 + ay .^ 2 + az .^ 2);
            end
            
            calData = loadjson(fileName);
            procData.Calibration.Accel.(deviceNames{i}) = calData;
            
            calData = ApplyCalibration(calData.Scale, calData.Bias, [ax', ay', az']);
            ax = calData(:, 1)';
            ay = calData(:, 2)';
            az = calData(:, 3)';
            
            if isCompareAccelMagn
                newMagn = sqrt(ax .^ 2 + ay .^ 2 + az .^ 2);
            end
        end
    end
    
    if isPlottingCalAccel && isCalAccel
        timeInSecAccel = procData.tacc ./ power(10, 6);       
        figure('NumberTitle', 'off', 'Name', strcat('Calibrated Accel ', deviceNames{i}));
        hold on;
        plot(timeInSecAccel, ax, '-r.');
        plot(timeInSecAccel, ay, '-g.');
        plot(timeInSecAccel, az, '-b.');
        plot(timeInSecAccel, sqrt(ax .^ 2 + ay .^ 2 + az .^ 2), '-k.');
        legend('accelX', 'accelY', 'accelZ', 'magn');
        xlabel('seconds');
        grid on;
    end
    
    if isCompareAccelMagn && isCalAccel
        timeInSecAccel = procData.tacc ./ power(10, 6);       
        figure('NumberTitle', 'off', 'Name', strcat('ComparingAccelMagnitude', deviceNames{i}));
        hold on;
        plot(timeInSecAccel, oldMagn, '-r.');
        plot(timeInSecAccel, newMagn, '-g.');
        legend('oldMagn', 'newMagn');
        xlabel('seconds');
        grid on;
    end
           
    if isPlottingMag
        timeInSecMag = procData.tmag ./ power(10, 6);
        figure('NumberTitle', 'off', 'Name', strcat('Mag ', deviceNames{i}));
        hold on;
        plot(timeInSecMag, mx, '-r.');
        plot(timeInSecMag, my, '-g.');
        plot(timeInSecMag, mz, '-b.');
        plot(timeInSecMag, sqrt(mx .^ 2 + my .^ 2 + mz .^ 2), '-k.');
        legend('magX', 'magY', 'magZ', 'magn');
        xlabel('seconds');
        grid on;
    end

    if isCalMag
        fileName = strcat('magCalData', deviceNames{i}, 'Range', num2str(procData.Config.MagFullScale), '.json');
        if exist(fileName, 'file')
            calData = loadjson(fileName);
            procData.Calibration.Mag.(deviceNames{i}) = calData;
            
            calData = ApplyCalibration(calData.Scale, calData.Bias, [mx', my', mz']);
            mx = calData(:, 1)';
            my = calData(:, 2)';
            mz = calData(:, 3)';
            
        end
    end

    if isPlottingCalMag && isCalMag
        timeInSecMag = procData.tmag ./ power(10, 6);
        figure('NumberTitle', 'off', 'Name', strcat('Calibrated Mag ', deviceNames{i}));
        hold on;
        plot(timeInSecMag, mx, '-r.');
        plot(timeInSecMag, my, '-g.');
        plot(timeInSecMag, mz, '-b.');
        plot(timeInSecMag, sqrt(mx .^ 2 + my .^ 2 + mz .^ 2), '-k.');
        legend('magX', 'magY', 'magZ', 'magn');
        xlabel('seconds');
        grid on;
    end

    if isPlottingDipAngle
        lenMag = length(mz);
        lenAccel = length(ax);
        dipAngle = zeros(1, lenMag - 1);
        for j = 1:lenMag - 1
            if 2*j >  lenAccel
                break;
            end
            accelVector = [ax(2*j - 1) ay(2*j - 1) az(2*j - 1)];
            accelNorm = norm(accelVector);
            accelVector = accelVector ./ accelNorm;
            magVector = [mx(j) my(j) mz(j)];
            magVector = magVector ./ norm(magVector);
            dipAngle(j) = acosd(dot(accelVector, magVector)) - 90;
        end
        figure('NumberTitle', 'off', 'Name', strcat('Dip Angle ', deviceNames{i}));
        hold on;
        plot(dipAngle, '-b.');          
        legend('angle');
        grid on;
    end
    
    if isMean
        ax = mean(ax);
        ay = mean(ay);
        az = mean(az);
        gx = mean(gx);
        gy = mean(gy);
        gz = mean(gz);
        mx = mean(mx);
        my = mean(my);
        mz = mean(mz);
    end

    procData.(deviceNames{i}) = struct('ax', ax, 'ay', ay, 'az', az, 'gx', gx, ...
        'gy', gy, 'gz', gz, 'mx', mx, 'my', my, 'mz', mz);
end

end