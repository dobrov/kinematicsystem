function [Scale, Bias] = CalPosition(measValues, trueValues)
% assumed that bias = 0 (after ellipsoid calibration);
% trueValues = [0 0 1; 0 0 -1; 0 1 0; 0 -1 0; 1 0 0; -1 0 0];
if (size(measValues) < size(trueValues))
    error('measValuse must be same dimensions as trueValues');
end

M = OrderMeasure(trueValues, measValues);

vX = trueValues(:, 1);
vY = trueValues(:, 2);
vZ = trueValues(:, 3);

hX = ( M' * M ) \ ( M' * vX );
hY = ( M' * M ) \ ( M' * vY );
hZ = ( M' * M ) \ ( M' * vZ );

Scale = [hX(1:3)'; hY(1:3)'; hZ(1:3)'];
Bias = [0, 0, 0];

end

