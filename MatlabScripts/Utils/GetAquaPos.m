function aquaPairs = GetAquaPos(calAccel, calMag)
permAccel = [0 0 1; 0 0 -1; 0 1 0; 0 -1 0; 1 0 0; -1 0 0];

% first position must be correct;
minDistance = 100;
for i = 1:length(permAccel)
    curDistance = norm(calAccel(1, :) - permAccel(i, :));
    if curDistance < minDistance
        minDistance = curDistance;
    end
end
maxDistance = 1.5 * minDistance;

isAquaPos = false(length(calAccel), 1);
for i = 1:length(calAccel)
    minDistance = 100;
    for j = 1:length(permAccel)
        curDistance = norm(calAccel(i, :) - permAccel(j, :));
        if curDistance < minDistance
            minDistance = curDistance;
        end
    end
    if minDistance < maxDistance
        isAquaPos(i) = true;
    end
end

calAccel = calAccel(isAquaPos == 1, :);
calMag = calMag(isAquaPos == 1, :);

posRotAxis = [0 0 1; 0 0 -1; 0 1 0; 0 -1 0; 1 0 0; -1 0 0];
posAngles = [90, 180];
posRotQuats = zeros(length(posRotAxis) * length(posAngles), 4);
for i = 1:length(posAngles)
    for j = 1:length(posRotAxis)
        posRotQuats((i - 1) * length(posRotAxis) + j, :) = ...
            [cosd(posAngles(i)/2), sind(posAngles(i)/2) * posRotAxis(j, :)];
    end
end

k = 1;
for i = 1:size(calAccel, 1) - 1
    begQuat = AquaMeasurement(calAccel(i, :), calMag(i, :), [1 0 0 0]);
    endQuat = AquaMeasurement(calAccel(i + 1, :), calMag(i + 1, :), begQuat);
    diffQuat = quatmultiply(begQuat, quatconj(endQuat));
    minDist = 100;
    for j = 1:length(posRotQuats)
        curDist = norm(posRotQuats(j, :) - diffQuat);
        if minDist > curDist
            rDiffQuat = posRotQuats(j, :);
            minDist = curDist;
        end
    end
    if minDist < 0.15
        aquaPairs(k) = struct('bAccel', calAccel(i, :), 'bMag', calMag(i, :), ...
        'eAccel', calAccel(i + 1, :), 'eMag', calMag(i + 1, :), 'rDiffQuat', rDiffQuat);
        disp(rDiffQuat);
        disp(diffQuat);
        k = k + 1;
    end
end

end