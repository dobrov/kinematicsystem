function [rotAccel, rotMag] = CalAqua(aquaPairs)
E = zeros(1, 8);
E(1) = 1;
E(5) = 1;
ObjFun = @(E) AquaCostFunction(E, aquaPairs);
options = optimset('MaxFunEvals', 150000, 'MaxIter', 600, 'TolFun', 10^(-10),...
    'TolX', 10^(-10), 'Algorithm', 'levenberg-marquardt');

[E, res] = lsqnonlin(ObjFun, E, [], [], options);
disp(res);
sAccelResQuat = quatnormalize(reshape(E(1:4), 1, 4));
sMagResQuat = quatnormalize(reshape(E(5:end), 1, 4));

rotAccel = quat2rotm(quatconj(sAccelResQuat));
rotMag = quat2rotm(quatconj(sMagResQuat));

end

