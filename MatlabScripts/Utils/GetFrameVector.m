function vCorrected = GetFrameVector(rotM, v)
vCorrected = (rotM * v')';
end

