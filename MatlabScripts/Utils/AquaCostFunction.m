function [residuals] = AquaCostFunction(E, aquaPairs)

sAccel = reshape(E(1:4), 1, 4);
sMag = reshape(E(5:end), 1, 4);

sAccel = quatnormalize(sAccel);
sMag = quatnormalize(sMag);

residuals = zeros(size(aquaPairs));
for i = 1:length(aquaPairs)
    begAccel = quatrotate(sAccel, aquaPairs(i).bAccel);
    begMag = quatrotate(sMag, aquaPairs(i).bMag);
    endAccel = quatrotate(sAccel, aquaPairs(i).eAccel);
    endMag = quatrotate(sMag, aquaPairs(i).eMag);
    
    begQuat = AquaMeasurement(begAccel, begMag, [1 0 0 0]);
    endQuat = AquaMeasurement(endAccel, endMag, begQuat);
    diffQuat = quatmultiply(begQuat, quatconj(endQuat));
    
    residuals(i) = norm(aquaPairs(i).rDiffQuat - diffQuat);
end

end

