function [x_n, y_n, z_n] = Normalize(x, y, z)
magnitude = sqrt(x .^ 2 + y .^ 2 + z .^ 2);
x_n = x ./ magnitude;
y_n = y ./ magnitude;
z_n = z ./ magnitude;