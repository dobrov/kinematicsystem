function LegAnimation(varargin)
freq = varargin{1};
points = varargin{2};
fps = 30;
scale = cast(cast(freq / fps, 'uint32'), 'double');
len = ceil(size(points.LHL, 1) / scale);
nJoints = size(points.LHL, 3);
plotHist = ones(1, nJoints);
plotSegment = 4;
limOffset = 2;
pointSize = 50;
videoObj = [];
FullScreen = false;
isPlotLine = false;
isPlotSquare = false;
isMirror = false;
isAnimation = true;
for i = 3:2:nargin
    if strcmp(varargin{i}, 'VideoFileName')       
        fileName = strrep(strcat(varargin{i+1}, '.avi'), ' ', '');
        videoObj = VideoWriter(fileName);
        videoObj.FrameRate = fps;
        open(videoObj);
    elseif  strcmp(varargin{i}, 'FullScreen'), FullScreen = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlotLine'), isPlotLine = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlotSquare'), isPlotSquare = varargin{i+1};
    elseif strcmp(varargin{i}, 'PlotHist'), plotHist = varargin{i+1};
    elseif strcmp(varargin{i}, 'nPlotSegment'), plotSegment = varargin{i+1};
    elseif strcmp(varargin{i}, 'isMirror'), isMirror = varargin{i+1};
    elseif strcmp(varargin{i}, 'isAnimation'), isAnimation = varargin{i+1};
    end                                                
end

%% Decimate

bodyParts = fieldnames(points);
for i = 1:length(bodyParts)
    decPoints = zeros(len, 3, nJoints);
    for j = 1:nJoints
        for k = 1:3
            decPoints(:, k, j) = decimate(points.(bodyParts{i})(:, k, j), scale);
        end
    end
    points.(bodyParts{i}) = decPoints;
end

%% Setup figure
fig = figure('NumberTitle', 'off', 'Name', 'Leg Animation');
hold on;
set(gcf, 'Renderer', 'zbuffer');
set(gca,'SortMethod','depth');
grid minor;
axis equal;
xlabel('X (cm)');
ylabel('Y (cm)');
zlabel('Z (cm)');

%yx
view(-180, 90);
%xz
%view(-180, 0);
%zy
%view(-90, 0)
if isMirror
    view(-180, -90);
end

xRange = reshape(points.LHL(:, 1, :), 1, numel(points.LHL(:, 1, :)));
yRange = reshape(points.LHL(:, 2, :), 1, numel(points.LHL(:, 2, :)));
zRange = reshape(points.LHL(:, 3, :), 1, numel(points.LHL(:, 3, :)));

if sum(strcmp(fieldnames(points), 'RHL')) == 1
    xRange = [xRange reshape(points.RHL(:, 1, :), 1, numel(points.RHL(:, 1, :)))];
    yRange = [yRange reshape(points.RHL(:, 2, :), 1, numel(points.RHL(:, 2, :)))];
    zRange = [zRange reshape(points.RHL(:, 3, :), 1, numel(points.RHL(:, 3, :)))];
end

% for 2d layout
if isPlotLine
    lineX = [14.569 -5.831];
    lineY = [-26.568 -26.568];
    lineZ = [0 0];
    line(lineX, lineY, lineZ, 'Color', [0 0 0], 'LineWidth', 2);
    xRange = [xRange lineX];
    yRange = [yRange lineY];
    zRange = [zRange lineZ];   
end

% for 3d layout
if isPlotSquare
    lineX = [10 -7 -7 10 10];
    lineY = [-11.1 -11.1 7.8 7.8 -11.1];
    lineZ = [-27.1 -27.1 -27.1 -27.1 -27.1];
    line(lineX, lineY, lineZ, 'Color', [0 0 0], 'LineWidth', 2);
    xRange = [xRange lineX];
    yRange = [yRange lineY];
    zRange = [zRange lineZ]; 
end

maxX = max(xRange);
minX = min(xRange);
maxY = max(yRange);
minY = min(yRange);
maxZ = max(zRange);
minZ = min(zRange);

Xlim = [(minX - limOffset) (maxX + limOffset)];
Ylim = [(minY - limOffset) (maxY + limOffset)];
Zlim = [(minZ - limOffset) (maxZ + limOffset)];
set(gca, 'Xlim', Xlim, 'Ylim', Ylim, 'Zlim', Zlim);

if(FullScreen)
    screenSize = get(0, 'ScreenSize');
    set(fig, 'Position', [0 0 screenSize(3) screenSize(4)]);
end

colors = [1 0 0; 0 1 0; 0 0 1; 1 0 1; 0 1 1; 1 1 0];

if (sum(strcmp(fieldnames(points), 'RHL')) == 1)
    nBodyParts = 2;
else
    nBodyParts = 1;
end 

if isAnimation
    
    lineHandles = gobjects(nBodyParts, nJoints);
    pointHandles = gobjects(nBodyParts, nJoints);
    pointHistHandles = gobjects(nBodyParts, nJoints);
    
    for i = 1:nBodyParts
        if i == 1
            curPoints = points.LHL;
        else
            curPoints = points.RHL;
            line([points.LHL(1, 1, 1) points.RHL(1, 1, 1)], ...
                 [points.LHL(1, 2, 1) points.RHL(1, 2, 1)], ...
                 [points.LHL(1, 3, 1) points.RHL(1, 3, 1)], ...
                'Color', [0.5 0.5 0.5], 'LineWidth', 2);
        end
        
        for j = 1:nJoints
            if j > (plotSegment(i) + 1)
                break;
            end
            pointHandles(i, j) = scatter3(curPoints(1, 1, j), curPoints(1, 2, j), ...
                curPoints(1, 3, j), pointSize, 'ko', 'filled');
            if j == 1
                continue;
            end

            lineHandles(i, j - 1) = line([curPoints(1, 1, j - 1) curPoints(1, 1, j)], ...
                [curPoints(1, 2, j - 1) curPoints(1, 2, j)], [curPoints(1, 3, j - 1) curPoints(1, 3, j)], ...
                'Color', colors(mod(i - 1, size(colors, 1)) + 1, :), 'LineWidth', 2);

            if plotHist(i, j - 1)
                pointHistHandles(i, j - 1) = line(curPoints(1, 1, j), curPoints(1, 2, j), curPoints(1, 3, j), ...
                    'Color', 0.5 * colors(mod(i - 1, size(colors, 1)) + 1, :), 'LineWidth', 0.01);
            end
        end
    end
    
    %% Animation
    for i = 2:len
        t_loopstart=tic();
        for j = 1:nBodyParts
            if j == 1
                curPoints = points.LHL;
            else
                curPoints = points.RHL;
            end
            
            for k = 2:nJoints
                if k > (plotSegment + 1)
                    break;
                end
                
                set(pointHandles(j, k), 'Xdata', curPoints(i, 1, k), ...
                'Ydata', curPoints(i, 2, k), 'Zdata', curPoints(i, 3, k));
            
                set(lineHandles(j, k - 1), 'Xdata', [curPoints(i, 1, k - 1) curPoints(i, 1, k)], ... 
                'Ydata', [curPoints(i, 2, k - 1) curPoints(i, 2, k)], ...
                'Zdata', [curPoints(i, 3, k - 1) curPoints(i, 3, k)]);
            
                if plotHist(j, k - 1)
                    set(pointHistHandles(j, k - 1), 'Xdata', curPoints(1:i, 1, k), ...
                        'Ydata', curPoints(1:i, 2, k), 'Zdata', curPoints(1:i, 3, k));
                end
            end
        end
        
        el_time=toc(t_loopstart);
        pause(1/fps - el_time);
        if(~isempty(videoObj))
            frame = getframe(fig);
            writeVideo(videoObj, frame);
        end
    end  
else
    
    for j = 1:nBodyParts
        if j == 1
            curPoints = points.LHL;
        else
            curPoints = points.RHL;
        end
        
        for i = 2:nJoints
            if plotHist(j, i - 1)
                line(curPoints(:, 1, i), curPoints(:, 2, i), curPoints(:, 3, i), ...
                    'Color', 0.5 * colors(j, :), 'LineWidth', 0.01);
            end
        end
    end
    
end

if(~isempty(videoObj))
    close(videoObj);
end
end