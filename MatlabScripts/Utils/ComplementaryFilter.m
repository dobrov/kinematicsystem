classdef ComplementaryFilter < handle
    
    properties (Access = public)
        Quaternion = [1 0 0 0]; % output quaternion describing the Earth relative to the sensor
        
        GainAcc = 0.02;
        GainMag = 0.02;
        GyroBiasAlpha = 0.05;
        AccelMagnThrAlpha = 0.01;
        MagMagnThrAlpha = 0.02;
        DipThrAlpha = 0.03;
        MinSteadyLen = 1; %seconds
        
        DoBiasEstimation = false;
        DoThresholding = false;
        
        GyroBias = [0 0 0];
        IsSteadyState = false;
        IsThresholdedAccel = false;
        IsThresholdedMag = false;
    end
    
    properties (Access = private)
        Initialized = false;
        
        PrevGyro = [0 0 0];
        PrevAccel = [0 0 0];
        MinSteadyLenSamples = 0;
        GyroMeanRT = [0 0 0];
        SteadyStateLen = 0;
        
        MagTrueMagn = 0;
        AccelTrueMagn = 1;
        TrueDipAngle = 0;
        dt = 0;
    end
    
    methods (Access = public)
        function obj = ComplementaryFilter(freq)
            obj.MinSteadyLenSamples = obj.MinSteadyLen * freq;
            obj.dt = 1 / freq;
        end
        
        function UpdateMARG(obj, gyro, accel, mag)
            if (~obj.Initialized)
                obj.GetMeasurement(accel, mag);
                obj.MagTrueMagn = norm(mag);
                obj.Initialized = true;
                return;
            end
                        
            if obj.DoBiasEstimation
                obj.UpdateBiases(gyro, accel);
                gyro = gyro - obj.GyroBias;
            end
            
            obj.GetPrediction(gyro);
            
            if obj.DoThresholding
                isValidAccel = abs(norm(accel) - obj.AccelTrueMagn) < ...
                    obj.AccelMagnThrAlpha * obj.AccelTrueMagn;
                if isValidAccel
                    obj.IsThresholdedAccel = false;
                    obj.GetAccCorrection(accel);
                else
                    obj.IsThresholdedAccel = true;
                end
                isValidMagMagn = abs(norm(mag) - obj.MagTrueMagn) < ...
                    obj.MagMagnThrAlpha * obj.MagTrueMagn;
                if isValidAccel && isValidMagMagn
                    accel = accel / norm(accel);
                    mag = mag / norm(mag);
                    dipAngle = dot(accel, mag);
                    if obj.TrueDipAngle == 0
                        obj.TrueDipAngle = dipAngle;
                        obj.GetMagCorrection(mag);
                    else
                        isValidDip = abs(dipAngle - obj.TrueDipAngle) < obj.DipThrAlpha * abs(obj.TrueDipAngle);
                        if isValidDip
                            obj.IsThresholdedMag = false;
                            obj.GetMagCorrection(mag);
                        else
                            obj.IsThresholdedMag = true;
                        end
                    end
                else
                    obj.IsThresholdedMag = true;
                end
            else
                obj.GetAccCorrection(accel);
                obj.GetMagCorrection(mag);
            end
        end
        
        function UpdateIMU(obj, gyro, accel)
            
            if obj.DoBiasEstimation
                obj.UpdateBiases(gyro, accel);
                gyro = gyro - obj.GyroBias;
            end
          
            obj.GetPrediction(gyro);
            
            if obj.DoThresholding
                isValidAccel = abs(norm(accel) - obj.AccelTrueMagn) < ...
                    obj.AccelMagnThrAlpha * obj.AccelTrueMagn;
                if isValidAccel
                    obj.IsThresholdedAccel = false;
                    obj.GetAccCorrection(accel);
                else
                    obj.IsThresholdedAccel = true;
                end
            else
                obj.GetAccCorrection(accel);
            end
         end
    end
    
    methods (Access = private)
        function GetMeasurement(obj, accel, mag)
            accel = accel / norm(accel);
            mag = mag / norm(mag);
            ax = accel(1);
            ay = accel(2);
            az = accel(3);
            
            qa = zeros(1, 4);
            if az >= 0
                lambda = sqrt((az + 1) / 2);
                qa(1) = lambda;
                qa(2) = -ay / (2 * lambda);
                qa(3) = ax / (2 * lambda);
            else
                lambda = sqrt((1 - az) / 2);
                qa(1) = -ay / (2 * lambda);
                qa(2) = lambda;
                qa(4) = ax / (2 * lambda);
            end

            l = quatrotate(qa, mag);
            lx = l(1);
            ly = l(2);

            qm = zeros(1, 4);
            gamma = lx * lx + ly * ly;
            if lx >= 0
                qm(1) = sqrt(gamma + lx * sqrt(gamma)) / sqrt(2 * gamma);
                qm(4) = ly / (sqrt(2) * sqrt(gamma + lx * sqrt(gamma))); 
            else
                qm(1) = ly / (sqrt(2) * sqrt(gamma - lx * sqrt(gamma)));
                qm(4) = sqrt(gamma - lx * sqrt(gamma)) / sqrt(2 * gamma);
            end

            obj.Quaternion = quatmultiply(qa, qm);
        end
        
        function GetPrediction(obj, gyro)
            gx = deg2rad(gyro(1));
            gy = deg2rad(gyro(2));
            gz = deg2rad(gyro(3));
            qDot = -0.5 * quatmultiply([0 gx gy gz], obj.Quaternion);
 
            obj.Quaternion = quatnormalize(obj.Quaternion + qDot * obj.dt);
        end
        
        function GetAccCorrection(obj, accel)
            accel = accel / norm(accel);
            g = quatrotate(obj.Quaternion, accel);
            gx = g(1);
            gy = g(2);
            gz = g(3);
            dq0 = sqrt((gz + 1) * 0.5);
            dq1 = -gy / (2.0 * dq0);
            dq2 = gx / (2.0 * dq0);
            dq3 = 0;
            
            %adaptive gain
            dq = slerp([dq0 dq1 dq2 dq3], obj.GainAcc, 0.9); % 0.0  
            obj.Quaternion = quatnormalize(quatmultiply(obj.Quaternion, dq));
        end
        
        function GetMagCorrection(obj, mag)
            mag = mag / norm(mag);
            l = quatrotate(obj.Quaternion, mag);
            lx = l(1);
            ly = l(2);
            
            gamma = lx * lx + ly * ly;
            beta = sqrt(gamma + lx * sqrt(gamma));
            dq0 = beta / (sqrt(2.0 * gamma));
            dq1 = 0.0;
            dq2 = 0.0;
            dq3 = ly / (sqrt(2.0) * beta);
            
            dq = slerp([dq0 dq1 dq2 dq3], obj.GainMag, 0.9);
            obj.Quaternion = quatnormalize(quatmultiply(obj.Quaternion, dq));
        end
        
        function UpdateBiases(obj, gyro, accel)
            curIsSteady = CheckState(accel, gyro, obj.PrevAccel, obj.PrevGyro);
            
            if curIsSteady
                if obj.SteadyStateLen
                    obj.GyroMeanRT = (obj.GyroMeanRT * obj.SteadyStateLen + gyro) / ...
                        (obj.SteadyStateLen + 1);
                else
                    obj.GyroMeanRT = gyro;
                end
                obj.SteadyStateLen = obj.SteadyStateLen + 1;
                if obj.MinSteadyLenSamples < obj.SteadyStateLen;
                    obj.GyroBias = (1 - obj.GyroBiasAlpha) * obj.GyroBias + ...
                        obj.GyroBiasAlpha * obj.GyroMeanRT;
                end
            else
                obj.SteadyStateLen = 0;
            end
            
            obj.PrevAccel = accel;
            obj.PrevGyro = gyro;
        end
    end
    
end