function [mFreq, vFreq, delay, mDelay] = GetFreq(time)

freqArray = zeros(1, 2000);
timeInSec = time ./ 1000000; 
delay = zeros(1, size(time, 2) - 1);

prevTime = timeInSec(1);
for i = 2:size(timeInSec, 2)
    delay(i - 1) = timeInSec(i) - prevTime;
    prevTime = timeInSec(i);
    freqArray(cast(timeInSec(i), 'uint64') + 1) = freqArray(cast(timeInSec(i), 'uint64') + 1) + 1;
end

freqArray = freqArray(freqArray ~= 0);
freqArray = freqArray(2:size(freqArray, 2) - 1);

mDelay = mean(delay);
%delay = delay(delay < 0.011);

mFreq = mean(freqArray);
vFreq = var(freqArray);


