function [sAccel, sMag] = CalAccelPosMagRot(accel, mag)

trueAccel = [0 0 1; 0 0 -1; 0 1 0; 0 -1 0; 1 0 0; -1 0 0];

posAccel = zeros(size(trueAccel));
for i = 1:length(trueAccel)
    minDistance = 100;
    k = 0;
    for j = 1:length(accel)
        curDistance = norm(accel(j, :) - trueAccel(i, :));
        if curDistance < minDistance
            minDistance = curDistance;
            k = j;
        end
    end
    posAccel(i, :) = accel(k, :);
end

trueAccel = GetAccel6Points(posAccel(1, :));
E = [1, 0, 0, 0];
ObjFun = @(E) AccelPosCostFunction(E, posAccel, trueAccel);
options = optimset('MaxFunEvals', 150000, 'MaxIter', 6000, ...
    'TolFun', 10^(-10), 'Algorithm', 'levenberg-marquardt', 'TolX', 10^(-10));

[E, rsnorm] = lsqnonlin(ObjFun, E, [], [], options);
rotAccelQuat = quatnormalize(reshape(E(1:4), 1, 4));
disp(rsnorm);
sAccel = quat2dcm(rotAccelQuat);

for i = 1:size(accel,1)
    disp(norm(accel(i, :)));
    accel(i, :) = quatrotate(rotAccelQuat, accel(i, :));
    disp(norm(accel(i, :)));
end

E = [1, 0, 0, 0];
ObjFun = @(E) RotMagCostFunction(E, accel, mag);
options = optimset('MaxFunEvals', 150000, 'MaxIter', 6000, ...
    'TolFun', 10^(-10), 'Algorithm', 'levenberg-marquardt');

[E, rsnorm] = lsqnonlin(ObjFun, E, [], [], options);
rotMagQuat = quatnormalize(reshape(E(1:4), 1, 4));
disp(rsnorm);
sMag = quat2dcm(rotMagQuat);

end
