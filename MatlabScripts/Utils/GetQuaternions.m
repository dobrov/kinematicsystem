function [quat] = GetQuaternions(data, freq, varargin)

isPlottingQuat = false;
isShowThresholding = false;
isPlottingDipAngle = false;
isPlottingGyroE = false;
filterSettingsName = 0;

for i = 1:2:nargin-3
    if strcmp(varargin{i}, 'isPlottingQuat'), isPlottingQuat = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingDipAngle'), isPlottingDipAngle = varargin{i+1};
    elseif strcmp(varargin{i}, 'filterSettingsName'), filterSettingsName = varargin{i+1};
    elseif strcmp(varargin{i}, 'isShowThresholding'), isShowThresholding = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlottingGyroE'), isPlottingGyroE = varargin{i+1};
    end
end

names = SortNames(fieldnames(data));
len = length(data.(names{1}).ax);
nDevice = length(names);
quat = zeros(len, 4, nDevice);
time = data.tgyro;
tSec = time ./ power(10, 6);

for i = 1:length(names)
    currentDevice = data.(names{i});
    ax = currentDevice.ax;
    ay = currentDevice.ay;
    az = currentDevice.az;
    gx = currentDevice.gx;
    gy = currentDevice.gy;
    gz = currentDevice.gz;
    mx = currentDevice.mx;
    my = currentDevice.my;
    mz = currentDevice.mz;
       
    AHRS = ComplementaryFilter(freq);
    
    if (filterSettingsName)
        settings = loadjson(filterSettingsName);
        AHRS.GainAcc = settings.GainAcc;
        AHRS.GainMag = settings.GainMag;
        
        AHRS.DoBiasEstimation = settings.DoBiasEstimation;
        if AHRS.DoBiasEstimation
            AHRS.GyroBiasAlpha = settings.GyroBiasAlpha;
            AHRS.MinSteadyLen = settings.MinSteadyLen;
        end
        
        AHRS.DoThresholding = settings.DoThresholding;
        if AHRS.DoThresholding
            AHRS.AccelMagnThrAlpha = settings.AccelMagnThrAlpha;
            AHRS.MagMagnThrAlpha = settings.MagMagnThrAlpha;
            AHRS.DipThrAlpha = settings.DipThrAlpha;
        end
    end
    
    lenMag = length(mz);
    lenAccel = length(az);
    if isPlottingDipAngle
        dipAngle = zeros(1, lenMag);
    end
    if isShowThresholding
        thrAccel = zeros(1, lenAccel);
        thrMag = zeros(1, lenMag);
    end
    for j = 1:lenMag
        AHRS.UpdateMARG([gx(2*j - 1) gy(2*j - 1) gz(2*j - 1)], ...
            [ax(2*j - 1) ay(2*j - 1) az(2*j - 1)], ...
            [mx(j) my(j) mz(j)]);
        quat(2*j - 1, :, i) = AHRS.Quaternion;
        rotatedMag = quatrotate(AHRS.Quaternion, [mx(j) my(j) mz(j)]);
        dipAngle(j) = atand(rotatedMag(3) / sqrt(rotatedMag(1)^2 + rotatedMag(2)^2));
        thrMag(j) = AHRS.IsThresholdedMag;
        thrAccel(j) = AHRS.IsThresholdedAccel;
        if 2*j >  length(ax)
            break;
        end
        AHRS.UpdateIMU([gx(2*j) gy(2*j) gz(2*j)], [ax(2*j) ay(2*j) az(2*j)]);
        quat(2*j, :, i) = AHRS.Quaternion;
    end
    
    if isPlottingQuat
        figure('NumberTitle', 'off', 'Name', strcat('Quaternion', names{i}));
        hold on;
        grid on;
        grid minor;
        plot(tSec, quat(:, 1, i), '-k.');
        plot(tSec, quat(:, 2, i), '-r.');
        plot(tSec, quat(:, 3, i), '-g.');
        plot(tSec, quat(:, 4, i), '-b.');
        if isShowThresholding
            tSecMag = data.tmag ./ power(10, 6); 
            stem(tSecMag, thrMag, '-y');
            stem(tSec, thrAccel, '-m');
            legend('q0', 'q1', 'q2', 'q3', 'thrMag', 'thrAccel');
        else
            legend('q0', 'q1', 'q2', 'q3');
        end
        title('Quaternion');
    end
    
    if isPlottingDipAngle
        figure('NumberTitle', 'off', 'Name', strcat('Dip Angle ', names{i}));
        hold on;
        plot(dipAngle, '-b.');          
        legend('angle');
        grid on;
    end
    
    if isPlottingGyroE
        gE = zeros(len, 3);
        for j = 1:len
            gE(j, :) = quatrotate(quat(j, :, i), [gx(j) gy(j) gz(j)]);
        end
        figure('NumberTitle', 'off', 'Name', strcat('Gyro in EarthFrame', names{i}));
        hold on;
        plot(tSec, gE(:, 1), '-r.');
        plot(tSec, gE(:, 2), '-g.');
        plot(tSec, gE(:, 3), '-b.');
        legend('gyroX', 'gyroY', 'gyroZ');
        xlabel('seconds');
        grid on;
    end
end

end