function [varargout] = CutTime(varargin)
% synchronize time between different stream
% return begin and end iter for every time array
% input time's arrays

maxTime = -1;
j = 1;
while true
    for i = 1:nargin
        if varargin{i}(j) > maxTime
            maxTime = varargin{i}(j);
        end
    end
    isEnd = true;
    for i = 1:nargin
        if ~ismember(maxTime, varargin{i})
            isEnd = false;
            break;
        end
    end
    if isEnd
        break;
    end
    j = j + 1;
end

beginIters = ones(1, nargin);
for i = 1:nargin
    currentArray = varargin{i};
    len = length(currentArray);
    while beginIters(i) <= len && currentArray(beginIters(i)) ~= maxTime
        beginIters(i) = beginIters(i) + 1;
    end
end

minTime = realmax('double');
j = 0;
while true
    for i = 1:nargin
        if varargin{i}(end - j) < minTime
            minTime = varargin{i}(end - j);
        end
    end
    isEnd = true;
    for i = 1:nargin
        if ~ismember(minTime, varargin{i})
            isEnd = false;
            break;
        end
    end
    if isEnd
        break;
    end
    j = j + 1;
end
    
endIters = zeros(1, nargin);
for i = 1:nargin
    currentArray = varargin{i};
    endIters(i) = length(currentArray);
    while endIters(i) >= 1 && currentArray(endIters(i)) ~= minTime
        endIters(i) = endIters(i) - 1;
    end
end

varargout = cell(nargin * 2, 1);
for i = 1:nargin
    varargout{2 * i - 1} = beginIters(i);
    varargout{2 * i} = endIters(i);
end

end