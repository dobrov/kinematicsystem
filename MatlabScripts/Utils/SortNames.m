function [sortedNames, deviceList] = SortNames(names)

digit = zeros(length(names), 2);
for i = 1:length(names)
    iter = regexp(names{i}, '(?!Device)\d+$');
    if ~isempty(iter)
        digit(i, :) = [str2double(names{i}(iter:end)), i];
    end
end
digit = digit(digit(:, 1) ~= 0, :);
digit = sortrows(digit);
deviceList = digit(:, 1);
sortedNames = cell(size(digit, 1), 1);
for i = 1:size(digit, 1)
    sortedNames{i} = names{digit(i, 2)};
end
end