function CalcRMSEApproximation(points, t)

diffX = diff(points(:, 1));
diffY = diff(points(:, 2));
len = size(diffX, 1);

bCutOff = 5;
bOrder = 2;
[b, a] = butter(bOrder, 2*bCutOff/100, 'low');
diffX = filtfilt(b, a, diffX);
diffY = filtfilt(b, a, diffY);

steadyState = zeros(len, 1);
currentAngle = zeros(len, 1);
tempAngle = 0;
thresholdState = 0.01;
thresholdAngle = 0.1;
for i = 1:len
    if (abs(diffX(i)) < thresholdState) && (abs(diffY(i)) < thresholdState)
        steadyState(i) = 0.5;
    end
    
    if diffX(i) > thresholdAngle
        tempAngle = 1;
    elseif diffX(i) < -thresholdAngle
        tempAngle = 3;
    elseif diffY(i) > thresholdAngle
        tempAngle = 4;
    elseif diffY(i) < -thresholdAngle
        tempAngle = 2;
    end
    currentAngle(i) = tempAngle;
end

bCutOff = 10;
bOrder = 1;
[b, a] = butter(bOrder, 2*bCutOff/100, 'low');
steadyState = filtfilt(b, a, steadyState);

steadyStateLogic = zeros(len, 1);
for i = 1:len
    if steadyState(i) > 0.05
        steadyStateLogic(i) = 1;
    end
end

diffSteadyState = diff(steadyStateLogic);
beginIndex = find(diffSteadyState > 0);
endIndex = find(diffSteadyState < 0);
if endIndex(1) < beginIndex(1)
    endIndex = endIndex(2:end);
end
if beginIndex(end) > endIndex(end)
    beginIndex = beginIndex(1:end-1);
end

angle1Points = [];
angle2Points = [];
angle3Points = [];
angle4Points = [];
for i = 1:length(beginIndex)
    point = median(points(beginIndex(i):endIndex(i), :));
    angle = round(mean(currentAngle(beginIndex(i):endIndex(i))));
    if angle == 1
        angle1Points = [angle1Points; point];
    elseif angle == 2
        angle2Points = [angle2Points; point];
    elseif angle == 3
        angle3Points = [angle3Points; point];
    elseif angle == 4
        angle4Points = [angle4Points; point];
    end
end


figure('NumberTitle', 'off', 'Name', 'Diff')
hold on;
grid minor;
plot(t(1:end-1), diffX, 'r-');
plot(t(1:end-1), diffY, 'g-');
plot(t(1:end-1), steadyState, 'k-');
%plot(t(1:end-1), currentAngle, 'm-');
legend('X', 'Y', 'SteadyState');

expRectangle = zeros(5, 3);
expRectangle(1, :) = mean(angle1Points);
expRectangle(2, :) = mean(angle2Points);
expRectangle(3, :) = mean(angle3Points);
expRectangle(4, :) = mean(angle4Points);
expRectangle(5, :) = expRectangle(1, :);

trueRectangle = [10 -11.1 -27.1;
                 -7 -11.1 -27.1;
                 -7 7.8 -27.1;
                 10 7.8 -27.1;
                 10 -11.1 -27.1];
             
nQuantLine = 500;
nAngle = 4;
SE = zeros(1, nQuantLine * nAngle);
figure('NumberTitle', 'off', 'Name', 'CompareRectangle');
hold on;
grid minor;
for i = 1:nAngle
    tx = linspace(trueRectangle(i, 1), trueRectangle(i + 1, 1), nQuantLine);
    ty = linspace(trueRectangle(i, 2), trueRectangle(i + 1, 2), nQuantLine);
    tz = linspace(trueRectangle(i, 3), trueRectangle(i + 1, 3), nQuantLine);
    plot3(tx, ty, tz, 'k-');
    ex = linspace(expRectangle(i, 1), expRectangle(i + 1, 1), nQuantLine);
    ey = linspace(expRectangle(i, 2), expRectangle(i + 1, 2), nQuantLine);
    ez = linspace(expRectangle(i, 3), expRectangle(i + 1, 3), nQuantLine);
    plot3(ex, ey, ez, 'r-');
    for j = 1:nQuantLine
        SE((i - 1) * nQuantLine + j) = norm([tx(j) ty(j) tz(j)] - [ex(j) ey(j) ez(j)]);
    end
end
plot3(points(:, 1), points(:, 2), points(:, 3));
legend('TrueRectangle', 'ExperimentRectangle');

RMSE = mean(SE);
maxRSE = max(SE);
figure();
histogram(SE, 100);

fprintf('RMSE: %f\n', RMSE);
fprintf('maxRSE: %f\n', maxRSE);
end