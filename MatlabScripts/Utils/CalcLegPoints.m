function CalcLegPoints(dataFileName, offsetFileName, outDataFolder, varargin)

filterSettingsName = 0;
for i = 1:2:nargin-3
    if strcmp(varargin{i}, 'filterSettingsName'), filterSettingsName = varargin{i+1};
    end
end

rawData = loadjson(dataFileName);
offsetData = loadjson(offsetFileName);

data = ProcessRawData(rawData, 'isFiltered', true, 'isCalMag', true, ...
    'isCalAccel', true, 'isCalGyro', true);

[names, allDeviceList] = SortNames(fieldnames(data));
bodyParts = fieldnames(offsetData);
deviceList = [];
for i = 1:length(bodyParts)
    if isfield(offsetData.(bodyParts{i}), 'DeviceList')
        deviceList = [deviceList offsetData.(bodyParts{i}).DeviceList];
    end
end
unusedDevices = setdiff(allDeviceList, deviceList);
for i = 1:length(unusedDevices)
    j = allDeviceList == unusedDevices(i);
    data = rmfield(data, names{j});
end

deviceQuat = GetQuaternions(data, rawData.Config.GyroDataRate, ...
    'filterSettingsName', filterSettingsName);

[~, deviceList] = SortNames(fieldnames(data));
len = length(data.tgyro);

qEG = offsetData.QuatEG;
for i = 1:2
    if (i == 1)
        qBS = offsetData.LHL.QuatBS;
        L = offsetData.LHL.SegmentLength;
        curDeviceList = offsetData.LHL.DeviceList;
    elseif sum(strcmp(fieldnames(offsetData), 'RHL')) == 1
        qBS = offsetData.RHL.QuatBS;
        L = offsetData.RHL.SegmentLength;
        curDeviceList = offsetData.RHL.DeviceList;
    else
        break;
    end
       
    deviceId = zeros(size(curDeviceList));
    for j = 1:length(curDeviceList)
        deviceId(j) = find(deviceList == curDeviceList(j));
    end
    
    qES = deviceQuat(:, :, deviceId);
    qGB = zeros(size(qES));
    nSegments = length(curDeviceList);
    for j = 1:nSegments
        qSE = quatconj(qES(:, :, j));
        temp = quatmultiply(qSE, qBS(j, :));
        qGB(:, :, j) = quatconj(quatmultiply(qEG, temp));
    end
    
    tPoints = zeros(len, 3, nSegments + 1);
    if (i == 1) %LHL
        tPoints(:, :, 1) = zeros(len, 3);
    else %RHL
        tPoints(:, :, 1) = repmat(offsetData.HipLength, len, 1);
    end
    for j = 2:nSegments + 1
        tPoints(:, :, j) = tPoints(:, :, j - 1) + quatrotate(qGB(:, :, j - 1), [L(j-1) 0 0]);
    end
    
    if (i == 1)
        points.LHL = tPoints;
    else
        points.RHL = tPoints;
    end
end

temp = strsplit(dataFileName, '/');
nameWithExt = temp{end};
temp = strsplit(nameWithExt, '.');
name = temp{1};
DR = rawData.Config.GyroDataRate;
if isfield(rawData.Data, 'MP')
    MP = rawData.Data.MP;
    save(strcat(outDataFolder, name, '.mat'), 'points', 'DR', 'MP');
else
    save(strcat(outDataFolder, name, '.mat'), 'points', 'DR');
end
    
end