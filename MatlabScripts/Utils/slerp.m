function [qm] = slerp (dq, gain , eps)

dq0 = dq(1);
dq1 = dq(2);
dq2 = dq(3);
dq3 = dq(4);

if (dq0 < eps)
    angle = acos(dq0);
    a = sin(angle * (1.0 - gain)) / sin(angle);
    b = sin(angle * gain) / sin(angle);
    
    qm0 = a + b * dq0;
    qm1 = b * dq1;
    qm2 = b * dq2;
    qm3 = b * dq3;
else
    
    qm0 = (1.0 - gain) + gain * dq0;
    qm1 = gain * dq1;
    qm2 = gain * dq2;
    qm3 = gain * dq3;
end

qm = [qm0 qm1 qm2 qm3];
qm = qm / quatnorm(qm);
end
