function points = GetAccel6Points(initPoint)
truePoints = [0 0 1; 0 0 -1; 0 1 0; 0 -1 0; 1 0 0; -1 0 0];
rotQuat = rotationTo(truePoints(1, :), initPoint);
points = zeros(size(truePoints));

for i = 1:length(points)
    points(i, :) = quatrotate(rotQuat, truePoints(i, :));
end

end

