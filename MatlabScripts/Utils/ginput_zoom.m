function [x,y] = ginput_zoom(n, z)
%  GINPUT_ZOOM Graphical input from mouse with Zoom In/Out function.
%     [X,Y] = GINPUT_ZOOM(N, STYLE) gets N points from the current axes and returns 
%     the X- and Y-coordinates in length N vectors X and Y.  The cursor
%     can be positioned using a mouse (or by using the Arrow Keys on some 
%     systems).  Data points are entered by pressing a mouse button
%     or any key on the keyboard except carriage return, which terminates
%     the input before N points are entered. The style of cursor can be
%     change by setting STYLE (please reference to "Specifying the Figure
%     Pointer" in the help, default: 'fullcross').
%
%     User can click and drag to Zoom In / Out
%     - Left-Top to Right-Down means Zoom In
%     - Right-Down to left-Top means Zoom Out
%  
%     [X,Y] = GINPUT_ZOOM gathers an unlimited number (less then 100000) 
%     of points until the return key is pressed.
%   
%     [X,Y,BUTTON] = GINPUT_ZOOM(N) returns a third result, BUTTON, that 
%     contains a vector of integers specifying which mouse button was
%     used (1,2,3 from left) or ASCII numbers if a key on the keyboard
%     was used.
%
%     Chang Yi-Chung 20091002

figureHandle = gcf();
h = zoom(figureHandle);
hold on;


if exist('n', 'var') == 0
    n = 100000;
end

if exist('z', 'var') == 0
    isPlotLine = false;
else
    isPlotLine = true;
end

x = zeros(n, 1);
y = zeros(n, 1);
children = get(gca,'Children');
image = children(end);
for i = 1:n
    if isPlotLine
        lineHandle = plot(image.XData, [z(i) z(i)], 'b-', 'LineWidth', 3);
    end
    while 1
        waitforbuttonpress;
        if strcmp(h.Enable, 'off') && strcmp(get(gcf,'SelectionType'), 'normal')
            point1 = get(gca, 'CurrentPoint');
            point1 = point1(1,1:2);
            x(i) = point1(1);
            y(i) = point1(2);
            plot(point1(1), point1(2), 'r.', 'markersize', 16);
            break;
        end
    end
    if isPlotLine
        delete(lineHandle);
    end
end

% compatible with ginput
x = x';
y = y';