function CalcDipMeanStd(staticPos, varargin)
isPlottingDipAngle = false;
for i = 1:2:nargin-1
    if strcmp(varargin{i}, 'isPlottingDipAngle'), isPlottingDipAngle = varargin{i+1};
    end                                                
end

deviceNames = SortNames(fieldnames(staticPos));
for i = 1:length(deviceNames)
    ax = staticPos.(deviceNames{i}).Accel.x;
    ay = staticPos.(deviceNames{i}).Accel.y;
    az = staticPos.(deviceNames{i}).Accel.z;
    mx = staticPos.(deviceNames{i}).Mag.x;
    my = staticPos.(deviceNames{i}).Mag.y;
    mz = staticPos.(deviceNames{i}).Mag.z;
    dipAngle = zeros(size(ax));
    for j = 1:length(ax)
        accelVector = [ax(j) ay(j) az(j)];
        accelVector = accelVector ./ norm(accelVector);
        magVector = [mx(j) my(j) mz(j)];
        magVector = magVector ./ norm(magVector);
        dipAngle(j) = acosd(dot(accelVector, magVector)) - 90;
    end
    if isPlottingDipAngle
        figure('NumberTitle', 'off', 'Name', strcat('Dip Angle ', deviceNames{i}));
        hold on;
        plot(dipAngle, '-b.');          
        legend('angle');
        grid on;
        fprintf('(%s, DipAngle) mean: %f, std: %f\n', deviceNames{i}, ...
            mean(dipAngle), std(dipAngle));
    end
end
end

