function angles = CalcLegAngles(memsData, dt)

names = SortNames(fieldnames(memsData));
len = length(memsData.(names{1}).ax);
nDevices = length(names);

angles = zeros(nDevices, len);

acc = zeros(nDevices + 1, len, 3);
% gz
gyro = zeros(nDevices + 1, len);
    
% virt IMU (need to calc first angle)
for i=1:len
    acc(1, i, :) = [-1 0 0]; % xyz
    gyro(1, i) = 0; % z
end
    
for i=2:nDevices + 1
    currentDevice = memsData.(names{i-1});
    [ax_n, ay_n, az_n] = Normalize(currentDevice.ax, currentDevice.ay, currentDevice.az);
    acc(i, :, :) = [ax_n' ay_n' az_n'];
    gyro(i, :) = currentDevice.gz;
    
    for j=1:len
        dotProd = dot(acc(i, j, :), acc(i - 1, j, :));
        crossProd = cross(acc(i, j, :), acc(i - 1, j, :));
        if sign(crossProd(3)) == 1;
            angleAcc = acosd(dotProd);
        else
            angleAcc = 360 - acosd(dotProd);
        end
              
        if j == 1          
           angles(i - 1, j) = angleAcc;         
        else 
            angleGyroVel = gyro(i, j) - gyro(i - 1, j);
            angles(i - 1, j) = 0.99 * (angles(i - 1, j - 1) + angleGyroVel * dt) + ...
                0.01 * angleAcc;
        end
    end
end
end
