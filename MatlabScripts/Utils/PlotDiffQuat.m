function PlotDiffQuat(firstQuat, secondQuat, varargin)

isPlotQuats = false;
isPlotDiffQuat = false;
isPlotDiffAngles = false;
isShowMeanVar = false;
for i = 1:2:nargin-3
    if strcmp(varargin{i}, 'isPlotDiffQuat'), isPlotDiffQuat = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlotQuats'), isPlotQuats = varargin{i+1};
    elseif strcmp(varargin{i}, 'isPlotDiffAngles'), isPlotDiffAngles = varargin{i+1};
    elseif strcmp(varargin{i}, 'isShowMeanVar'), isShowMeanVar = varargin{i+1};
    end                                                
end

if isPlotQuats
    figure('NumberTitle', 'off', 'Name', 'ComparingQuats');
    hold on;
    grid on;
    grid minor;
    plot(firstQuat(:, 1, 1), '-k', 'LineWidth', 2);
    plot(firstQuat(:, 2, 1), '-r', 'LineWidth', 2);
    plot(firstQuat(:, 3, 1), '-g', 'LineWidth', 2);
    plot(firstQuat(:, 4, 1), '-b', 'LineWidth', 2);
    plot(secondQuat(:, 1, 1), '--k', 'LineWidth', 2);
    plot(secondQuat(:, 2, 1), '--r', 'LineWidth', 2);
    plot(secondQuat(:, 3, 1), '--g', 'LineWidth', 2);
    plot(secondQuat(:, 4, 1), '--b', 'LineWidth', 2);
    title('ComparingQuats');
    legend('fq0', 'fq1', 'fq2', 'fq3', 'sq0', 'sq1', 'sq2', 'sq3');
end

if isPlotDiffQuat
    diff = quatmultiply(firstQuat, quatconj(secondQuat));
    figure('NumberTitle', 'off', 'Name', 'Diff');
    hold on;
    grid on;
    grid minor;
    plot(diff(:, 1, 1), '-k.');
    plot(diff(:, 2, 1), '-r.');
    plot(diff(:, 3, 1), '-g.');
    plot(diff(:, 4, 1), '-b.');
    title('Quaternion');
    legend('q0', 'q1', 'q2', 'q3');
end

[yaw, pitch, roll] = quat2angle(diff);
yaw = rad2deg(yaw);
pitch = rad2deg(pitch);
roll = rad2deg(roll);

if isShowMeanVar
    fprintf('yaw mean: %f, std: %f\n', mean(abs(yaw)), std(abs(yaw)));
    fprintf('pitch mean: %f, std: %f\n', mean(abs(pitch)), std(abs(pitch)));
    fprintf('roll mean: %f, std: %f\n', mean(abs(roll)), std(abs(roll)));
    
    subVector = firstQuat - secondQuat;
    dist = zeros(length(subVector), 1);
    for i = 1:length(subVector)
        dist(i) = norm(subVector(i));
    end
    fprintf('mean dist: %f\n', mean(dist));
end

if isPlotDiffAngles
    figure('NumberTitle', 'off', 'Name', 'Diff Angles');
    hold on;
    grid on;
    grid minor;
    plot(yaw, '-b.');
    plot(pitch, '-g.');
    plot(roll, '-r.');
    title('Diff Angles');
    legend('yaw', 'pitch', 'roll');
end

end