%close all;
clear;
%clc;
addpath('jsonlab');
addpath('Logs');
addpath('Utils');

steadyTime = 2; %seconds
kDeltaAngularVelocityThreshold = 0.1; %rad
kAccelerationThreshold = 0.1; %g

isPlottingSteadyState = true;
isPlottingOldNewMagn = false;

fileName = '607accel.json';
rawData = loadjson(fileName);

accelDataRate = rawData.Config.AccelDataRate;
gyroDataRate = rawData.Config.GyroDataRate;

if accelDataRate ~= gyroDataRate
    disp('Accel data rate should be same as gyro data rate');
end

data = ProcessRawData(rawData);

accelFS = data.Config.AccelFullScale;

names = SortNames(fieldnames(data));
nDevice = length(names);

for i = 1:nDevice
    currentDevice = data.(names{i});
    ax = currentDevice.ax;
    ay = currentDevice.ay;
    az = currentDevice.az;
    gx = deg2rad(currentDevice.gx);
    gy = deg2rad(currentDevice.gy);
    gz = deg2rad(currentDevice.gz);
    len = length(ax);
    
    steadyState = ones(len, 1);
    steadyState(1) = 0;
    steadyState(end) = 0;
    accMagn = zeros(len, 1);
    for j = 2:len
        gx_prev = gx(j - 1);
        gy_prev = gy(j - 1);
        gz_prev = gz(j - 1);
        accMagn(j) = sqrt(ax(j)^2 + ay(j)^2 + az(j)^2);
        
        if abs(accMagn(j) - 1) > kAccelerationThreshold
            steadyState(j) = 0;
            continue;
        end
        if abs(gx(j) - gx_prev) > kDeltaAngularVelocityThreshold || ...
           abs(gy(j) - gy_prev) > kDeltaAngularVelocityThreshold || ...
           abs(gz(j) - gz_prev) > kDeltaAngularVelocityThreshold
            steadyState(j) = 0;
            continue;
        end
    end
       
    steadyStateBeginEnd = find(steadyState == 0);
    steadyStateLen = diff(steadyStateBeginEnd);
    steadyStateLenThreshold = steadyStateLen > (accelDataRate * steadyTime);
    nSamples = sum(steadyStateLenThreshold);
    measuredAccel = zeros(nSamples, 3);
    
    steadyStateBeginEndFiltered = zeros(len, 1);
    k = 1;
    for j = 1:length(steadyStateLenThreshold)
        if steadyStateLenThreshold(j)
            beginIndex = steadyStateBeginEnd(j) + 20;
            endIndex = steadyStateBeginEnd(j + 1);
            steadyStateBeginEndFiltered(beginIndex) = 1.5;
            steadyStateBeginEndFiltered(endIndex) = 1.5;
            
            axMean = mean(ax(beginIndex:endIndex));
            ayMean = mean(ay(beginIndex:endIndex));
            azMean = mean(az(beginIndex:endIndex));
            
            measuredAccel(k, :) = [axMean ayMean azMean];
            k = k + 1;
        end
    end
    
    if isPlottingSteadyState
        timeInSecAccel = data.tacc ./ power(10, 6);       
        figure('NumberTitle', 'off', 'Name', strcat('Accel ', names{i}));
        hold on;
        plot(timeInSecAccel, accMagn, '-k.');
        stem(timeInSecAccel, steadyStateBeginEndFiltered), '-m.';
        legend('magn', 'steadyState');
        xlabel('seconds');
        grid on;
    end
    
    [e_center, e_radii, e_eigenvecs, v] = ellipsoid_fit(measuredAccel);
    scale = inv([e_radii(1) 0 0; 0 e_radii(2) 0; 0 0 e_radii(3)]) .* min(e_radii); % scaling matrix
    R = e_eigenvecs;
    Scale = R * diag([1 / e_radii(1), 1 / e_radii(2), 1 / e_radii(3)]) * R';
    Bias = e_center;

    disp(Scale);
    disp(Bias);
    
%     [Scale, Bias] = CalibAccel(measuredAccel);
%     disp(Scale);
%     disp(Bias);
    
    calData = struct('Bias', Bias', 'Scale', Scale);
    calDataChar = savejson('', calData);
    
    fileID = fopen(strcat('Calibration/accelCalData', names{i}, 'Range', num2str(accelFS), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
    
%     disp(names{i});
%     disp('Before: After');
    oldMagn = zeros(nSamples, 1);
    newMagn = zeros(nSamples, 1);
    
    for j = 1:nSamples
        ax = measuredAccel(j, 1);
        ay = measuredAccel(j, 2);
        az = measuredAccel(j, 3);
        
        x_c = ax - Bias(1);
        y_c = ay - Bias(2);
        z_c = az - Bias(3);

        ax_c = Scale(1, 1) * x_c + Scale(1, 2) * y_c + Scale(1, 3) * z_c;
        ay_c = Scale(2, 1) * x_c + Scale(2, 2) * y_c + Scale(2, 3) * z_c;
        az_c = Scale(3, 1) * x_c + Scale(3, 2) * y_c + Scale(3, 3) * z_c;
        
%         fprintf('[%1.3f %1.3f %1.3f]: [%1.3f %1.3f %1.3f]\n', ax, ay, az, ax_c, ay_c, az_c);
        
        oldMagn(j) = sqrt(ax^2 + ay^2 + az^2);
        newMagn(j) = sqrt(ax_c^2 + ay_c^2 + az_c^2);
        
    end
    
    if isPlottingOldNewMagn
        figure('NumberTitle', 'off', 'Name', strcat('OldNewMagn ', names{i}));
        hold on;
        plot(oldMagn, '-r.');
        plot(newMagn, '-g.');
        legend('oldMagnitude', 'newMagnitude');
        grid on;
    end
    
    disp(names{i});
    fprintf('Old magnitude: %f\n', mean(oldMagn));
    fprintf('New magnitude: %f\n', mean(newMagn));
    
end;
