function [residuals] = AccelCostFunction(E, accel)

residuals = zeros(size(accel, 1), 1);
bias = E(7:9);
scale = [E(1) E(2) E(3);
         E(2) E(4) E(5);
         E(3) E(5) E(6)];
for i = 1:size(accel, 1)
    calAccel = scale * (accel(i, :) - bias)';
    residuals(i,1) = 1 - sqrt(calAccel(1)^2 + calAccel(2)^2 + calAccel(3)^2);
end

end

