addpath('jsonlab');
addpath('Utils');
addpath('Logs');
addpath('../Koltushi');
clear;
close all;
clc;

isCalMag = true;
isCalAccel = true;
isFiltered = true;
DoAdaptiveGain = true;
DoBiasEstimation = true;

isPlottingAccel = false;
isPlottingGyro = false;
isPlottingMag = false;

isPlottingDeviceQuat = false;
isPlottingDiffQuat = false;
isPlottingEulerAngles = true;

rawData = loadjson('quatDiffZ.json');

data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, 'isPlottingAccel', ...
    isPlottingAccel, 'isPlottingGyro', isPlottingGyro, 'isPlottingMag', isPlottingMag, 'isCalAccel', isCalAccel);

quatIMU = GetQuaternions(data, rawData.Config.GyroDataRate, 'isPlottingQuat', isPlottingDeviceQuat, ...
    'DoAdaptiveGain', DoAdaptiveGain, 'DoBiasEstimation', DoBiasEstimation);

quatDiff = quatmultiply(quatIMU(:, :, 2), quatconj(quatIMU(:, :, 1)));

if isPlottingDiffQuat
    figure('NumberTitle', 'off', 'Name', 'QuaternionDiff');
    hold on;
    quatSec = data.tacc ./ power(10, 6);
    plot(quatSec, quatDiff(:, 1), '-k.');
    plot(quatSec, quatDiff(:, 2), '-r.');
    plot(quatSec, quatDiff(:, 3), '-g.');
    plot(quatSec, quatDiff(:, 4), '-b.');
    title('QuaternionDiff');
    legend('q0', 'q1', 'q2', 'q3');
end

if isPlottingEulerAngles
    len = length(data.tacc);
    zAngle = zeros(len, 1);
    yAngle = zeros(len, 1);
    xAngle = zeros(len, 1);
    for i = 1:len
        [zAngle(i), yAngle(i), xAngle(i)] = quat2angle(quatDiff(i, :));
    end

    figure('NumberTitle', 'off', 'Name', 'Euler angles');
    hold on;
    plot(xAngle, '-r.');
    plot(yAngle, '-g.');
    plot(zAngle, '-b.');
    xlabel('seconds');
    title('Euler angles');
    legend('x', 'y', 'z');
end

BoxAnimation(rawData.Config.GyroDataRate, quatDiff);