addpath('Utils');
clear;
close all;
clc;

isFiltered = true;
isCalMag = true;
isCalAccel = true;

angle = 180;
qESinit = quatnormalize([cosd(angle/2), -sind(angle/2) * [1 1 1]]);
qSEinit = quatconj(qESinit);

angle = 180;
qEB = quatnormalize([cosd(angle/2), -sind(angle/2) * [0 0 1]]);
angle = -90;
temp = quatnormalize([cosd(angle/2), -sind(angle/2) * [0 1 0]]);
qEB = quatmultiply(qEB, temp);
qSB = quatmultiply(qEB, qSEinit);

forward = quatrotate(qEB, [1 0 0]);
qEBr = rotationTo([1 0 0], forward);
qSBr = quatmultiply(qEBr, qSEinit);

angle = 90;
diff = quatnormalize([cosd(angle/2), -sind(angle/2) * [0 1 0]]);
qES = quatmultiply(qESinit, diff);

qEB1 = quatmultiply(qSB, qES);
qEBr1 = quatmultiply(qSBr, qES);

diff = quatmultiply(quatconj(qEB1), qEBr1);
disp(quatrotate(qEB1, [1 0 0]));
disp(quatrotate(qEBr1, [1 0 0]));