close all;
clear;
%clc;
addpath('jsonlab');
addpath('Logs');
addpath('Utils');

isPlottingMag = true;
isPlottingMagCal = true;

fileName = 'calibrationMagNew.json';
rawData = loadjson(fileName);
magSens = rawData.Config.MagSens;

data = ProcessRawData(rawData);

names = SortNames(fieldnames(data));
nDevice = length(names);

calibrationData = struct();
for i = 1:nDevice
    currentDevice = data.(names{i});
    
    % Medfilt for eliminate impulse noise
    mx = medfilt1(currentDevice.mx, 3);
    my = medfilt1(currentDevice.my, 3);
    mz = medfilt1(currentDevice.mz, 3);
    
%     mx = currentDevice.mx;
%     my = currentDevice.my;
%     mz = currentDevice.mz;
%     
%     mag = medfilt3([mx; my; mz]);
%     
%     mx = mag(1, :);
%     my = mag(2, :);
%     mz = mag(3, :);
    
    if isPlottingMag
        figure('NumberTitle', 'off', 'Name', strcat('MagnetometerXYZ', names{i}));
        grid on;
        scatter3(mx(:), my(:), mz(:), 4, [0 0 1], 'filled');
        axis equal;
    end
    
    minX = min(mx);
    maxX = max(mx);
    minY = min(my);
    maxY = max(my);
    minZ = min(mz);
    maxZ = max(mz);
    
    Bias = [(minX + maxX) / 2, (minY + maxY) / 2, (minZ + maxZ) / 2];
    
    radX = (maxX - minX) / 2;
    radY = (maxY - minY) / 2;
    radZ = (maxZ - minZ) / 2;

    avgRad = (radX + radY + radZ) / 3;
    
    Sens = diag([avgRad / radX, avgRad / radY, avgRad / radZ]);
    
    
    disp(names{i});
    disp(Bias);
    disp(Sens);
    
    x_c = mx - Bias(1);
    y_c = my - Bias(2);
    z_c = mz - Bias(3);

    x_cal = Sens(1, 1) * x_c;
    y_cal = Sens(2, 2) * y_c;
    z_cal = Sens(3, 3) * z_c;
    
    if isPlottingMagCal
        figure('NumberTitle', 'off', 'Name', strcat('MagnetometerXYZ', names{i}));
        grid on;
        scatter3(x_cal(:), y_cal(:), z_cal(:), 4, [1 0 0], 'filled');
        axis equal;
    end
    calData = struct('Bias', Bias', 'Scale', Sens);
    calDataChar = savejson('', calData);
    
    fileID = fopen(strcat('Logs/magCalData', names{i}, 'Sens', num2str(magSens), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
end