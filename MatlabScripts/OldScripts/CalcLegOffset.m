addpath('jsonlab');
addpath('Utils');
addpath('Logs');
clear;
close all;

isFiltered = true;
isCalAccel = true;

isPlottingAccel = false;
isPlottingGyro = false;

rawData = loadjson('leg3DOffset.json');
data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isPlottingAccel', ...
    isPlottingAccel, 'isPlottingGyro', isPlottingGyro, 'isCalAccel', isCalAccel, 'isMean', true);

dt = 1 / rawData.Config.GyroDataRate;
jointAngles  = CalcLegAngles(data, dt);

names = SortNames(fieldnames(data));
nDevice = length(names);
Rgs = zeros(3, 3, nDevice);
for i = 1:nDevice
    Rgs(:, :, i) = rotz(jointAngles(i));
end

gbAngles = [-90, 0, 0, 0];
Rgb = zeros(3, 3, nDevice);
for i = 1:nDevice
    Rgb(:, :, i) = rotz(gbAngles(i));
end

Rsb = zeros(3, 3, nDevice);
for i = 1:nDevice
    Rsb(:, :, i) = Rgs(:, :, i)' * Rgb(:, :, i);
    [z, ~, ~] = dcm2angle(Rsb(:, :, i));
    disp(['Offset angle for ', names{i}, ': ' num2str(rad2deg(z))]);
end

save('LegOffset.mat', 'Rsb');