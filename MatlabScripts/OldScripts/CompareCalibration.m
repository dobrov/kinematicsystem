close all;
clear;
clc;
nFiles = 2;
maxDevice = 5;
fileNames = cell(nFiles, 1);
fileNames{1} = 'accelCalib2G.mat';
fileNames{2} = 'accelCalib2G0.mat';

Sx = zeros(nFiles, maxDevice);
Sy = zeros(nFiles, maxDevice); 
Sz = zeros(nFiles, maxDevice);
Sxy = zeros(nFiles, maxDevice);
Sxz = zeros(nFiles, maxDevice);
Syz = zeros(nFiles, maxDevice);

Bx = zeros(nFiles, maxDevice);
By = zeros(nFiles, maxDevice);
Bz = zeros(nFiles, maxDevice);

for i = 1:nFiles
    load(fileNames{i}, 'calibrationData');
    names = fieldnames(calibrationData);
    nDevice = length(names);
    for j = 1:nDevice
        curDevice = calibrationData.(names{j});
        Sens = curDevice.Sens;
        Bias = curDevice.Bias;
        Sx(i, j) = Sens(1, 1);
        Sy(i, j) = Sens(2, 2);
        Sz(i, j) = Sens(3, 3);
        
        Sxy(i, j) = Sens(1, 2);
        Sxz(i, j) = Sens(1, 3);
        Syz(i, j) = Sens(2, 3);
        
        Bx(i, j) = Bias(1);
        By(i, j) = Bias(2);
        Bz(i, j) = Bias(3);
    end
end


