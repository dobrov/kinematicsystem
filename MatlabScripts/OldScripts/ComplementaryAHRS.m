close all;
clear;
clc;
addpath('jsonlab');
addpath('Logs');
addpath('ComplementaryFilter');
data = loadjson('zxyrot.json');

isPlottingMag = false;
isPlottingAccel = false;
isPlottingGyro = false;
isPlottingResAccel = true;
isPlottingResMag = true;
isPlottingQuat = false;
isBoxAnimation = true;

isCalibrateMag = false;
isFiltered = true;

gyroSens = data.Config.GyroSens;
accSens = data.Config.AccelSens;
magSens = data.Config.MagSens;

names = fieldnames(data.Data);

currentDevice = data.Data.(names{1});

ax = currentDevice.ax .* accSens; 
ay = currentDevice.ay .* accSens;  	 
az = currentDevice.az .* accSens;

gx = deg2rad((-currentDevice.gy) .* gyroSens);
gy = deg2rad((-currentDevice.gx) .* gyroSens);
gz = deg2rad((-currentDevice.gz) .* gyroSens);

mx = currentDevice.mx;
my = currentDevice.my;
mz = currentDevice.mz;

if isCalibrateMag
    calData = loadjson(strcat('magCalData', names{1}, '.json'));
    MagBias = calData.MagBias;
    MagSens = calData.MagSens;
    mx_c = mx - MagBias(1);
    my_c = my - MagBias(2);
    mz_c = mz - MagBias(3);

    mx_cal = MagSens(1, 1) * mx_c + MagSens(1, 2) * my_c + MagSens(1, 3) * mz_c;
    my_cal = MagSens(2, 1) * mx_c + MagSens(2, 2) * my_c + MagSens(2, 3) * mz_c;
    mz_cal = MagSens(3, 1) * mx_c + MagSens(3, 2) * my_c + MagSens(3, 3) * mz_c;
else
    mx_cal = mx;
    my_cal = my;
    mz_cal = mz;
end

if isFiltered
    bOrder = 3;
    bCutOff = 3;
    [b, a] = butter(bOrder, 2*bCutOff/data.Config.AccelDataRate, 'low');
    ax = filtfilt(b, a, ax);
    ay = filtfilt(b, a, ay);
    az = filtfilt(b, a, az);
    bOrder = 2;
    [b, a] = butter(bOrder, 2*bCutOff/data.Config.MagDataRate, 'low');
    mx_cal = filtfilt(b, a, mx_cal);
    my_cal = filtfilt(b, a, my_cal);
    mz_cal = filtfilt(b, a, mz_cal);
end

if (isPlottingAccel)
    figure('NumberTitle', 'off', 'Name', strcat('Accelerometer', names{1}));
    hold on;
    tAccSec = currentDevice.tacc ./ power(10, 6);
    plot(tAccSec, ax, '-r.');
    plot(tAccSec, ay, '-g.');
    plot(tAccSec, az, '-b.');
    plot(tAccSec, sqrt(az .^ 2 + ay .^ 2 + ax .^ 2), '-k.');
    xlabel('seconds');
    ylabel('g');
    title('Accelerometer');
    legend('X', 'Y', 'Z');
end

if (isPlottingMag)
    figure('NumberTitle', 'off', 'Name', strcat('Magnetometer', names{1}));
    hold on;
    tMagSec = currentDevice.tmag ./ power(10, 6);
    plot(tMagSec, mx_cal, '-r.');
    plot(tMagSec, my_cal, '-g.');
    plot(tMagSec, mz_cal, '-b.');
    plot(tMagSec, sqrt(mz_cal .^ 2 + mx_cal .^ 2 + my_cal .^ 2), '-k.');        
    xlabel('seconds');
    title('Magnetometer');
    legend('X', 'Y', 'Z');
end

if (isPlottingGyro)
    figure('NumberTitle', 'off', 'Name', strcat('Gyro', names{1}));
    hold on;
    tGyroSec = currentDevice.tgyro ./ power(10, 6);
    plot(tGyroSec, gx, '-r.');
    plot(tGyroSec, gy, '-g.');
    plot(tGyroSec, gz, '-b.');
    xlabel('seconds');
    title('Gyro');
    legend('X', 'Y', 'Z');
end

AHRS = ComplementaryFilter(data.Config.GyroDataRate);
period = 1000000 / data.Config.GyroDataRate;
gyroSize = numel(currentDevice.tgyro);
accelSize = numel(currentDevice.tacc);
magSize = numel(currentDevice.tmag);
gI = 1;
aI = 1;
mI = 1;

currentTime = max([currentDevice.tgyro(1) currentDevice.tmag(1) ...
                   currentDevice.tacc(1)]);
if currentTime == currentDevice.tacc(1)
    while currentTime ~= currentDevice.tgyro(gI)
        gI = gI + 1;
    end
    while currentTime ~= currentDevice.tmag(mI)
        mI = mI + 1;
    end
elseif currentTime == currentDevice.tgyro(1)
    while currentTime ~= currentDevice.tacc(aI)
        aI = aI + 1;
    end
    while currentTime ~= currentDevice.tmag(mI)
        mI = mI + 1;
    end
else
    while currentTime ~= currentDevice.tgyro(gI)
        gI = gI + 1;
    end
    while currentTime ~= currentDevice.tacc(aI)
        aI = aI + 1;
    end
end

AHRS.UpdateMARG([gx(gI) gy(gI) gz(gI)], [ax(aI) ay(aI) az(aI)], ...
                [mx_cal(mI) my_cal(mI) mz_cal(mI)]);
quatTime = currentTime;
quat = AHRS.Quaternion;
currentTime = currentTime + period;
mI = mI + 1;
aI = aI + 1;
gI = gI + 1;
%Worked only if accel and gyro have same freq and mag has freq / 2 ^ x
%Interpolated data
while gI <= gyroSize
    if mI <= magSize && currentDevice.tmag(mI) == currentTime
        AHRS.UpdateMARG([gx(gI) gy(gI) gz(gI)], [ax(aI) ay(aI) az(aI)], ...
            [mx_cal(mI) my_cal(mI) mz_cal(mI)]);
        mI = mI + 1;
    else
        if aI <= accelSize
            AHRS.UpdateIMU([gx(gI) gy(gI) gz(gI)], [ax(aI) ay(aI) az(aI)]);
        end
    end
    quatTime = [quatTime currentTime];
    quat = [quat; AHRS.Quaternion];
    aI = aI + 1;
    gI = gI + 1;
    currentTime = currentTime + period;
end

if isPlottingQuat
    figure('NumberTitle', 'off', 'Name', strcat('Quaternion', names{1}));
    hold on;
    quatSec = quatTime ./ power(10, 6);
    plot(quatSec, quat(:, 1), '-k.');
    plot(quatSec, quat(:, 2), '-r.');
    plot(quatSec, quat(:, 3), '-g.');
    plot(quatSec, quat(:, 4), '-b.');
    title('Quaternion');
    legend('q0', 'q1', 'q2', 'q3');
end

qconj = zeros(size(quat));
for i=1:size(quat, 1)
    qconj(i, :) = quatconj(quat(i, :));
end

a_res = [];
m_res = [];
mI = 1;
aI = 1;
while currentDevice.tacc(aI) ~= quatTime(1)
    aI = aI + 1;
end
while currentDevice.tmag(mI) ~= quatTime(1)
    mI = mI + 1;
end

for i = 1:numel(quatTime)
    if mI <= magSize && quatTime(i) == currentDevice.tmag(mI)
        m_res = [m_res; quatrotate(quat(i, :), [mx_cal(mI) my_cal(mI) mz_cal(mI)])];
        mI = mI + 1;
    end
    a_res = [a_res; quatrotate(quat(i, :), [ax(aI) ay(aI) az(aI)])];
    aI = aI + 1;
end

if isPlottingResAccel
    figure('NumberTitle', 'off', 'Name', strcat('AccelerometerRes', names{1}));
    hold on;
    plot(a_res(:, 1), '-r.');
    plot(a_res(:, 2), '-g.');
    plot(a_res(:, 3), '-b.');
    title('Accelerometer');
    legend('X', 'Y', 'Z');
end

if isPlottingResMag
    figure('NumberTitle', 'off', 'Name', strcat('MagnetometerRes', names{1}));
    hold on;
    plot(m_res(:, 1), '-r.');
    plot(m_res(:, 2), '-g.');
    plot(m_res(:, 3), '-b.');
    title('Magnetometer');
    legend('X', 'Y', 'Z');
end

if isBoxAnimation
    BoxAnimation(data.Config.GyroDataRate, quat);
end