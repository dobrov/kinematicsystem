addpath('jsonlab');
addpath('Utils');
addpath('Logs');
clear;
close all;
clc;

isCalMag = true;
isCalAccel = true;
isFiltered = true;
isPlottingCalAccel = false;
isPlottingGyro = false;
isPlottingCalMag = false;
isPlottingRawQuat = false;
isComparePointX = true;
isComparePointY = true;
isComparePointZ = true;
isPlottingQGB = true;
isPlottingGBAngles = false;

rawData = loadjson('offset-90y.json');
freq = rawData.Config.GyroDataRate;
data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, 'isPlottingCalAccel', ...
    isPlottingCalAccel, 'isPlottingGyro', isPlottingGyro, 'isPlottingCalMag', isPlottingCalMag, ...
    'isCalAccel', isCalAccel);

len = length(data.tgyro);
names = SortNames(fieldnames(data));
nDevice = length(names);

qES = GetQuaternions(data, freq, 'isPlottingQuat', isPlottingRawQuat);
load('OffsetRigid.mat'); % qSB

angle = 130;
qGE = [cosd(angle/2), -sind(angle/2) * [0 0 1]];

qGB = zeros(size(qES));
for i = 1:nDevice
    temp = quatmultiply(qSB(i, :), qES(:, :, i));
    qGB(:, :, i) = quatmultiply(temp, qGE);
end

L = 10;
point = zeros(len, 3, nDevice);
for i = 1:nDevice
    point(:, :, i) = quatrotate(qGB(:, :, i), [L 0 0]);
end

% BoxAnimation(freq, qGB(:, :, 1));
possibleColors = cell(7, 1);
possibleColors{1} = '-r.';
possibleColors{2} = '-g.';
possibleColors{3} = '-b.';
possibleColors{4} = '-k.';
possibleColors{5} = '-m.';
possibleColors{6} = '-y.';
possibleColors{7} = '-c.';

if isComparePointX
    figure('NumberTitle', 'off', 'Name', 'PointX');
    hold on;
    grid minor;
    for i = 1:nDevice
        plot(point(:, 1, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('PointX');
    legend(names{:});
end

if isComparePointY
    figure('NumberTitle', 'off', 'Name', 'PointY');
    hold on;
    grid minor;
    for i = 1:nDevice
        plot(point(:, 2, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('PointY');
    legend(names{:});
end

if isComparePointZ
    figure('NumberTitle', 'off', 'Name', 'PointZ');
    hold on;
    grid minor;
    for i = 1:nDevice
        plot(point(:, 3, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('PointZ');
    legend(names{:});
end

if isPlottingQGB
    tSec = data.tgyro ./ power(10, 6);
    for i = 1:nDevice
        figure('NumberTitle', 'off', 'Name', strcat('qGB_', num2str(i)));
        hold on;
        grid minor;
        plot(tSec, qGB(:, 1, i), '-k.');
        plot(tSec, qGB(:, 2, i), '-r.');
        plot(tSec, qGB(:, 3, i), '-g.');
        plot(tSec, qGB(:, 4, i), '-b.');
        title('Quaternion');
        legend('q0', 'q1', 'q2', 'q3');
        %BoxAnimation(100, qEB(:, :, i));
    end
end

device = 1;
LegAnimation(freq, point(:, :, device)', point(:, :, device)', point(:, :, device)', point(:, :, device)');