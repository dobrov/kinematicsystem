addpath('../Koltushi');
addpath('ginputc');
addpath('jsonlab');
addpath('Utils');
addpath('Logs');
close all;
clear;
clc;
warning('off', 'Images:initSize:adjustingMag');

offsetName = '2015-11-06 17.04.08.jpg';
horLen = 27.5;

isFiltered = true;
isCalAccel = true;
isPlottingAccel = false;

%% Calc angle from photo
bitmap = imread(offsetName);
hFig = figure('Toolbar','none', 'Menubar','none');
hIm = imshow(bitmap);
[x, y] = ginputc(7, 'ShowPoints', true, 'Color', 'r', 'LineWidth', 2, 'ConnectPoints', false);
hold on;
line(x(1:2), y(1:2), 'Color', [1 0 0], 'LineWidth', 2, 'Marker','o',...
    'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor', [0,0,0], 'LineStyle', '--');
kinChain = line(x(3:end), y(3:end), 'Color', [1 0 0], 'LineWidth', 2, 'Marker','o',...
    'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor',[0,0,0]);
hor = [(x(2) - x(1)) (y(2) - y(1))];
normHor = norm(hor);
scale = normHor / horLen; % per cm
diff = (y(2) - y(1)) / (x(2) - x(1));

% degrees
relativeAngles = zeros(1, 4);
% length (cm);
segmentLength = zeros(1, 4);
prevSeg = hor;
prevNorm = normHor;
for i = 3:length(x) - 1
    X1 = x(i);
    Y1 = y(i);
    X0 = x(1);
    X2 = x(2);
    Y0 = y(i) - diff * (X1 - x(1));
    Y2 = y(i) + diff * (x(2) - X1);    
    seg = [(x(i + 1) - x(i)) (y(i + 1) - y(i))];
    
    normSeg = norm(seg);
    line([X1 X2], [Y1 Y2], 'LineStyle', '--', 'Color', [1 0 0], 'LineWidth', 2 ...
    ,'Marker','o', 'MarkerEdgeColor', [0 0 0], 'MarkerFaceColor',[0,0,0]);
    crossProd = cross([seg 0], [prevSeg 0]);
    dotProd = dot(prevSeg, seg);
    relativeAngles(i - 2) =  sign(crossProd(3)) * acosd(dotProd / (prevNorm * normSeg));
    segmentLength(i - 2) = normSeg / scale;
    prevSeg = seg;
    prevNorm = normSeg;
    fprintf('segment number %d - angle: %f(degrees), len: %f(cm)\n', i - 2, relativeAngles(i - 2), segmentLength(i - 2));
end

%% read IMU data
relativeAngles(1) = 180 + relativeAngles(1);
%segmentLength = [13.5, 13.5, 8, 3.5];
rawData = loadjson('legOffsetPhoto.json');
data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isMean', true, 'isCalAccel', isCalAccel);

names = SortNames(fieldnames(data));

dt = 1 / rawData.Config.GyroDataRate;
jointAngles  = CalcLegAngles(data, dt);

nDevice = length(names);
Rgs = zeros(3, 3, nDevice);
for i = 1:nDevice
    Rgs(:, :, i) = rotz(jointAngles(i));
end

gbAngles = -relativeAngles;
Rgb = zeros(3, 3, nDevice);
for i = 1:nDevice
    Rgb(:, :, i) = rotz(gbAngles(i));
end

Rsb = zeros(3, 3, nDevice);
for i = 1:nDevice
    Rsb(:, :, i) = Rgs(:, :, i)' * Rgb(:, :, i);
    [z, ~, ~] = dcm2angle(Rsb(:, :, i));
    disp(['Offset angle for ', names{i}, ': ' num2str(rad2deg(z))]);
end

save('LegOffset.mat', 'Rsb', 'segmentLength');
