addpath('jsonlab');
addpath('Utils');
addpath('Logs');
clear;
%close all;

isFiltered = true;
isCalMag = true;
isCalAccel = true;
DoAdaptiveGain = true;
DoBiasEstimation = true;

isPlottingAccel = false;
isPlottingGyro = false;
isPlottingMag = false;
isCompareAccelMagn = false;
rawData = loadjson('twoSegmentOffset.json');
data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, 'isPlottingAccel', ...
    isPlottingAccel, 'isPlottingGyro', isPlottingGyro, 'isPlottingMag', isPlottingMag, 'isMean', true, ...
    'isCalAccel', isCalAccel, 'isCompareAccelMagn', isCompareAccelMagn);

names = SortNames(fieldnames(data));
nDevice = length(names);

quatIMU = GetQuaternions(data, rawData.Config.GyroDataRate, 'DoAdaptiveGain', DoAdaptiveGain, 'DoBiasEstimation', DoBiasEstimation);

% Q (global body) = Q (sensor body) * Q (global sensor)
% Q (sensor body) = Q (global body) / Q (global sensor)
% Q (global body) = Q (global sensor) * Q (sensor body) 

quatGS = zeros(nDevice, 4);

for i = 1:nDevice
    quatGS(i, :) = quatIMU(:, :, i);
end


%g = [0 1 0];


angle = -90;
quatGB = [cosd(angle/2), -sind(angle/2) * [0 0 1]];

quatSB = quatmultiply(quatconj(quatGS), quatGB);
disp(quatSB);

quatResGB = quatmultiply(quatGS, quatSB);

for i = 1:nDevice
    [z, ~, ~] = quat2angle(quatSB(i,:));
    disp(['Offset angle for ', names{i}, ': ' num2str(rad2deg(z))]);
end
save('Leg3DOffset.mat', 'quatSB');