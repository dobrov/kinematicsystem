addpath('jsonlab');
addpath('Utils');
addpath('Logs');
clear;
%close all;
clc;

isFiltered = true;
isCalAccel = true;

isPlottingAccel = false;
isPlottingGyro = false;
isPlottingJointAngles = true;
isShowingAnimation = true;
isVideo = false;

rawData = loadjson('Leg3D.json');
load('LegOffset.mat');
if exist('segmentLength','var')
    L = segmentLength;
else
    L = [13.5, 13.5, 8, 3.5]; % centimeters
end

data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isPlottingAccel', ...
    isPlottingAccel, 'isPlottingGyro', isPlottingGyro, 'isCalAccel', isCalAccel);

dt = 1 / rawData.Config.GyroDataRate;
freq = rawData.Config.GyroDataRate;
jointAngles = CalcLegAngles(data, dt);
len = length(data.tacc);
names = SortNames(fieldnames(data));
nDevice = length(names);

Rgb = zeros(3, 3, len, nDevice);

for i = 1:nDevice
    for j = 1:len
        Rgb(:, :, j, i) = rotz(jointAngles(i, j)) * Rsb(:, :, i);
    end
end

ForwardKinematics(freq, Rgb, L, 'isPlottingJointAnglesZ', isPlottingJointAngles, ...
    'isShowingAnimation', isShowingAnimation, 'isMirror', true, 'isPlotLine', true);
