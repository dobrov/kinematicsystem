addpath('jsonlab');
addpath('Utils');
addpath('Logs');

clear;
close all;
clc;

isComparePoint1X = false;
isComparePoint1Y = false;
isComparePoint1Z = false;
isComparePoint2X = true;
isComparePoint2Y = true;
isComparePoint2Z = true;

isPlottingSegment1Angles = false;
isPlottingSegment2Angles = true;
isPlottingCalMag = false;
isPlottingMag = false;
isPlottingCalAccel = false;
isPlottingAccel = false;
isPlottingGyro = false;
isPlottingRawQuat = false;
isPlottingQuatDiff = false;
isPlottingSegmentQuat = false;

isCalMag = true;
isCalAccel = true;
isFiltered = true;
DoAdaptiveGain = true;
DoBiasEstimation = true;

rawData = loadjson('twoSegment2.json');
load('Leg3DOffset.mat');
if exist('segmentLength','var')
    L = segmentLength;
else
    L = [13.5, 13.5]; % centimeters
end

data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isPlottingAccel', ...
    isPlottingAccel, 'isPlottingGyro', isPlottingGyro, 'isPlottingMag', isPlottingMag, 'isCalMag', isCalMag, ...
    'isPlottingCalMag', isPlottingCalMag, 'isCalAccel', isCalAccel, 'isPlottingCalAccel', isPlottingCalAccel);

quatGS = GetQuaternions(data, rawData.Config.GyroDataRate, 'DoAdaptiveGain', DoAdaptiveGain, ...
    'DoBiasEstimation', DoBiasEstimation, 'isPlottingQuat', isPlottingRawQuat);
names = SortNames(fieldnames(data));
nDevice = length(names);
len = size(quatGS, 1);

quatGB = zeros(size(quatGS));
for i = 1:nDevice
    quatGB(:, :, i) = quatmultiply(quatGS(:, :, i), quatSB(i, :));
end

possibleColors = cell(7, 1);
possibleColors{1} = '-r.';
possibleColors{2} = '-g.';
possibleColors{3} = '-b.';
possibleColors{4} = '-k.';
possibleColors{5} = '-m.';
possibleColors{6} = '-y.';
possibleColors{7} = '-c.';

firstSegment = [1; 2];
secondSegment = [3; 4];
nPerm1 = numel(firstSegment);
nPerm2 = numel(meshgrid(firstSegment, secondSegment));  % n of cartesian product
point1 = zeros(len, 3, nPerm1);
legnd1 = cell(nPerm1, 1);
point2 = zeros(len, 3, nPerm2);
legnd2 = cell(nPerm2, 1);

for i = 1:nPerm1
    point1(:, :, i) = quatrotate(quatGB(:, :, firstSegment(i)), [L(1) 0 0]);
    
    legnd1{i} = ['Device', num2str(firstSegment(i))];
    for j = 1:nPerm2/nPerm1
        point2(:, :, (i - 1) * nPerm2/nPerm1 + j) = point1(:, :, i) + ...
            quatrotate(quatGB(:, :, secondSegment(j)), [L(2) 0 0]);
        
        legnd2{(i - 1) * nPerm2/nPerm1 + j} = ['Devices', num2str(firstSegment(i)), ...
            'and', num2str(secondSegment(j))];
    end
end

if isComparePoint1X
    figure('NumberTitle', 'off', 'Name', 'Point1X');
    hold on;
    grid minor;
    for i = 1:nPerm1
        plot(point1(:, 1, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('Point1X');
    legend(legnd1{:});
end

if isComparePoint1Y
    figure('NumberTitle', 'off', 'Name', 'Point1Y');
    hold on;
    grid minor;
    for i = 1:nPerm1
        plot(point1(:, 2, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('Point1Y');
    legend(legnd1{:});
end

if isComparePoint1Z
    figure('NumberTitle', 'off', 'Name', 'Point1Z');
    hold on;
    grid minor;
    for i = 1:nPerm1
        plot(point1(:, 3, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('Point1Z');
    legend(legnd1{:});
end

if isComparePoint2X
    figure('NumberTitle', 'off', 'Name', 'Point2X');
    hold on;
    grid minor;
    for i = 1:nPerm2
        plot(point2(:, 1, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('Point2X');
    legend(legnd2{:});
end

if isComparePoint2Y
    figure('NumberTitle', 'off', 'Name', 'Point2Y');
    hold on;
    grid minor;
    for i = 1:nPerm2
        plot(point2(:, 2, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('Point2Y');
    legend(legnd2{:});
end

if isComparePoint2Z
    figure('NumberTitle', 'off', 'Name', 'Point2Z');
    hold on;
    grid minor;
    for i = 1:nPerm2
        plot(point2(:, 3, i), possibleColors{mod(i, length(possibleColors)) + 1});
    end
    title('Point2Z');
    legend(legnd2{:});
end

if isPlottingSegmentQuat
    tSec = data.tgyro ./ power(10, 6);
    for i = 1:nDevice
        figure('NumberTitle', 'off', 'Name', strcat('SegmentQuat', num2str(i)));
        hold on;
        grid minor;
        plot(tSec, quatGB(:, 1, i), '-k.');
        plot(tSec, quatGB(:, 2, i), '-r.');
        plot(tSec, quatGB(:, 3, i), '-g.');
        plot(tSec, quatGB(:, 4, i), '-b.');
        title('Quaternion');
        legend('q0', 'q1', 'q2', 'q3');
        %BoxAnimation(100, quatGB(:, :, i));
    end
end

if isPlottingQuatDiff
    tSec = data.tgyro ./ power(10, 6);
    firstDiff = quatmultiply(quatconj(quatGB(:, :, firstSegment(2))), quatGB(:, :, firstSegment(1)));
    secondDiff = quatmultiply(quatconj(quatGB(:, :, secondSegment(2))), quatGB(:, :, secondSegment(1)));
    figure('NumberTitle', 'off', 'Name', strcat('FirstDiff', num2str(i)));
    hold on;
    grid minor;
    plot(tSec, firstDiff(:, 1), '-k.');
    plot(tSec, firstDiff(:, 2), '-r.');
    plot(tSec, firstDiff(:, 3), '-g.');
    plot(tSec, firstDiff(:, 4), '-b.');
    title('Quaternion');
    legend('q0', 'q1', 'q2', 'q3');
    
    figure('NumberTitle', 'off', 'Name', strcat('SecondDiff', num2str(i)));
    hold on;
    grid minor;
    plot(tSec, secondDiff(:, 1), '-k.');
    plot(tSec, secondDiff(:, 2), '-r.');
    plot(tSec, secondDiff(:, 3), '-g.');
    plot(tSec, secondDiff(:, 4), '-b.');
    title('Quaternion');
    legend('q0', 'q1', 'q2', 'q3');
end

if isPlottingSegment1Angles
    nDeviceOnSeg = length(firstSegment);
    legnd = cell(nDeviceOnSeg * 3, 1);
    zAngle = zeros(len, nDeviceOnSeg);
    yAngle = zeros(len, nDeviceOnSeg);
    xAngle = zeros(len, nDeviceOnSeg);
    
    for i = 1:nDeviceOnSeg
        for j = 1:len
            [zAngle(j, i), yAngle(j, i), xAngle(j, i)] = quat2angle(quatGB(j, :, firstSegment(i)));
        end
        legnd{3 * i  - 2} = ['Device', num2str(firstSegment(i)), 'X'];
        legnd{3 * i  - 1} = ['Device', num2str(firstSegment(i)), 'Y'];
        legnd{3 * i} = ['Device', num2str(firstSegment(i)), 'Z'];
    end
    
    figure('NumberTitle', 'off', 'Name', 'Segment1Angles');
    hold on;
    grid minor;
    title('Segment1Angles');
    for i = 1:nDeviceOnSeg
        plot(rad2deg(xAngle(:, i)), possibleColors{mod(3 * i - 2, length(possibleColors)) + 1});
        plot(rad2deg(yAngle(:, i)), possibleColors{mod(3 * i - 1, length(possibleColors)) + 1});
        plot(rad2deg(zAngle(:, i)), possibleColors{mod(3 * i, length(possibleColors)) + 1});
    end
    legend(legnd{:});
end

if isPlottingSegment2Angles
    nDeviceOnSeg = length(secondSegment);
    legnd = cell(nDeviceOnSeg * 3, 1);
    zAngle = zeros(len, nDeviceOnSeg);
    yAngle = zeros(len, nDeviceOnSeg);
    xAngle = zeros(len, nDeviceOnSeg);
    
    for i = 1:nDeviceOnSeg
        for j = 1:len
            [zAngle(j, i), yAngle(j, i), xAngle(j, i)] = quat2angle(quatGB(j, :, secondSegment(i)));
        end
        legnd{3 * i  - 2} = ['Device', num2str(secondSegment(i)), 'X'];
        legnd{3 * i  - 1} = ['Device', num2str(secondSegment(i)), 'Y'];
        legnd{3 * i} = ['Device', num2str(secondSegment(i)), 'Z'];
    end
    
    figure('NumberTitle', 'off', 'Name', 'Segment2Angles');
    hold on;
    grid minor;
    title('Segment2Angles');
    for i = 1:nDeviceOnSeg
        plot(rad2deg(xAngle(:, i)), possibleColors{mod(3 * i - 2, length(possibleColors)) + 1});
        plot(rad2deg(yAngle(:, i)), possibleColors{mod(3 * i - 1, length(possibleColors)) + 1});
        plot(rad2deg(zAngle(:, i)), possibleColors{mod(3 * i, length(possibleColors)) + 1});
    end
    legend(legnd{:});
end
