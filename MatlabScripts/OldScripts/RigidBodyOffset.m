addpath('jsonlab');
addpath('Utils');
addpath('Logs');
clear;
%close all;
%clc;

isFiltered = true;
isCalMag = true;
isCalAccel = true;
isPlottingAccel = false;
isPlottingGyro = false;
isPlottingMag = false;
isPlottingRawQuat = false;

rawData = loadjson('global180z90y.json');
data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, 'isCalAccel', isCalAccel, 'isPlottingAccel', ...
    isPlottingAccel, 'isPlottingGyro', isPlottingGyro, 'isPlottingMag', isPlottingMag, 'isMean', true);

names = SortNames(fieldnames(data));
nDevice = length(names);

qES = GetQuaternions(data, rawData.Config.GyroDataRate, 'isPlottingQuat', isPlottingRawQuat);

angle = 130;
qGE = [cosd(angle/2), -sind(angle/2) * [0 0 1]];
qEG = quatconj(qGE);

angle = 180;
qGB = [cosd(angle/2), -sind(angle/2) * [0 0 1]];

angle = 90;
temp = [cosd(angle/2), -sind(angle/2) * [0 1 0]];
qGB = quatmultiply(qGB, temp);
%disp(qGB);

forward = quatrotate(qGB, [1 0 0]);

qGBres = rotationTo([1 0 0], forward);

forward = quatrotate(qGBres, [1 0 0]);

qSB = zeros(nDevice, 4);
for i = 1:nDevice
    qSE = quatconj(qES(1, :, i));
    temp = quatmultiply(qGB, qEG);
    %temp = quatmultiply(qGBres, qEG);
    qSB(i, :) = quatmultiply(temp, qSE);
end

% for i = 1:nDevice
%     temp = quatmultiply(qSB(i, :), qES(1, :, i));
%     qGB = quatmultiply(temp, qGE);
%     disp(qGB);
% end

save('OffsetRigid.mat', 'qSB');