addpath('jsonlab');
addpath('Utils');
addpath('Logs');
clear;
close all;
clc;

isFiltered = true;
isCalMag = true;
isCalAccel = true;

rawData = loadjson('globalTest90z.json');
data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, 'isCalAccel', isCalAccel, 'isMean', true);
qES1 = GetQuaternions(data, rawData.Config.GyroDataRate);

names = SortNames(fieldnames(data));
nDevice = length(names);

rawData = loadjson('globalTest-90y.json');
data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, 'isCalAccel', isCalAccel, 'isMean', true);
qES2 = GetQuaternions(data, rawData.Config.GyroDataRate);

% rawData = loadjson('offset180z-90x.json');
% data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, 'isCalAccel', isCalAccel, 'isMean', true);
% qES2 = GetQuaternions(data, rawData.Config.GyroDataRate);
% 
% rawData = loadjson('offset-90y.json');
% data = ProcessRawData(rawData, 'isFiltered', isFiltered, 'isCalMag', isCalMag, 'isCalAccel', isCalAccel, 'isMean', true);
% qES3 = GetQuaternions(data, rawData.Config.GyroDataRate);
% 
% % diff = quatdivide(q1, q2),  q2 = quatmultiply(q1, quatconj(diff))
% 
% % diff = conj(q2) * q1, q2 = q1 * conj(diff) 
% % quatdivide(q1, q2) = quatmultiply(quatconj(q2), q1)
% 
curQuat = qES2;
diff = zeros(nDevice, 4);

for i = 1:nDevice
    diff(i, :) = quatdivide(curQuat(:, :, i), qES1(:, :, i));
    BoxAnimation(100, diff(i, :));
end

% qESres = quatmultiply(qES1, diff);
% 
% disp(curQuat);
% disp(qESres);
% diff = quatmultiply(quatconj(qES3), qES);
% 
% % Qeb = Qes * Qsb
% % Qsb = quatdivide(Qeb, Qes)
% % Qsb = quatmultiply(quatconj(Qes), Qeb);
% % Qbs = quatdivide(Qes, Qeb)
% % Qbs = quatmultiply(Qbe, Qes)
% BoxAnimation(100, curQuat);
% BoxAnimation(100, qESres);
% % [z, y, x] = quat2angle(diff);
% % disp(rad2deg(z));
% % disp(rad2deg(y));
% % disp(rad2deg(x));
% % disp(diff);
