%close all;

shift = 1000;
delta = 0.01; % cm
lineX = [10 -7 -7 10 10];
lineY = [-11.1 -11.1 7.8 7.8 -11.1];

[x, y] = meshgrid(-7:delta:10, -11.1);
groundTruth = [fliplr(x)' y'];
[x, y] = meshgrid(-7, -11.1:delta:7.8);
groundTruth = [groundTruth; [x y]];
[x, y] = meshgrid(-7:delta:10, 7.8);
groundTruth = [groundTruth; [x' y']];
[x, y] = meshgrid(10, -11.1:delta:7.8);
groundTruth = [groundTruth; [x fliplr(y')']];

groundTruth = circshift(groundTruth, [shift 0]);
len = size(groundTruth, 1);
angle = 20;
for i = 1:len
    temp = rotz(angle) * [groundTruth(i, :), 0]';
    groundTruth(i, :) = temp(1:2);
end

figure('NumberTitle', 'off', 'Name', 'Figure');
hold on;
grid minor;
plot(groundTruth(:, 1), groundTruth(:, 2));

figure('NumberTitle', 'off', 'Name', 'Points');
hold on;
grid minor;
plot(groundTruth(:, 1), 'r.-');
plot(groundTruth(:, 2), 'g.-');
legend('X', 'Y');

figure('NumberTitle', 'off', 'Name', 'Diff');
hold on;
grid minor;
plot(diff(groundTruth(:, 1)), 'r.-');
plot(diff(groundTruth(:, 2)), 'g.-');
legend('X', 'Y');

