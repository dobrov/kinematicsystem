close all;
clear;
clc;
addpath('jsonlab');
addpath('ComplementaryFilter');
data = loadjson('test26.json');

isCalibrateMag = true;

gyroSens = data.Config.GyroSens;
accSens = data.Config.AccelSens;

names = fieldnames(data.Data);

currentDevice = data.Data.(names{1});

ax = currentDevice.ax .* accSens; 
ay = currentDevice.ay .* accSens;  	 
az = currentDevice.az .* accSens;

gx = deg2rad((-currentDevice.gy) .* gyroSens);
gy = deg2rad((-currentDevice.gx) .* gyroSens);
gz = deg2rad((-currentDevice.gz) .* gyroSens);

mx = currentDevice.mx;
my = currentDevice.my;
mz = currentDevice.mz;

if isCalibrateMag
    magCal = loadjson(strcat('magCalData', names{1}, '.json'));
    MagBias = magCal.MagBias;
    MagSens = magCal.MagSens;
    
    mx_c = mx - MagBias(1);
    my_c = my - MagBias(2);
    mz_c = mz - MagBias(3);

    mx_cal = MagSens(1, 1) * mx_c + MagSens(1, 2) * my_c + MagSens(1, 3) * mz_c;
    my_cal = MagSens(2, 1) * mx_c + MagSens(2, 2) * my_c + MagSens(2, 3) * mz_c;
    mz_cal = MagSens(3, 1) * mx_c + MagSens(3, 2) * my_c + MagSens(3, 3) * mz_c;
else
    mx_cal = mx;
    my_cal = my;
    mz_cal = mz;
end

AHRS = ComplementaryFilter(data.Config.GyroDataRate);
period = 1000000 / data.Config.GyroDataRate;
gyroSize = numel(currentDevice.tgyro);
accelSize = numel(currentDevice.tacc);
magSize = numel(currentDevice.tmag);
gI = 1;
aI = 1;
mI = 1;

currentTime = max([currentDevice.tgyro(1) currentDevice.tmag(1) ...
                   currentDevice.tacc(1)]);
if currentTime == currentDevice.tacc(1)
    while currentTime ~= currentDevice.tgyro(gI)
        gI = gI + 1;
    end
    while currentTime ~= currentDevice.tmag(mI)
        mI = mI + 1;
    end
elseif currentTime == currentDevice.tgyro(1)
    while currentTime ~= currentDevice.tacc(aI)
        aI = aI + 1;
    end
    while currentTime ~= currentDevice.tmag(mI)
        mI = mI + 1;
    end
else
    while currentTime ~= currentDevice.tgyro(gI)
        gI = gI + 1;
    end
    while currentTime ~= currentDevice.tacc(aI)
        aI = aI + 1;
    end
end

AHRS.UpdateMARG([gx(gI) gy(gI) gz(gI)], [ax(aI) ay(aI) az(aI)], ...
                [mx_cal(mI) my_cal(mI) mz_cal(mI)]);
quatTime = currentTime;
quat = AHRS.Quaternion;
currentTime = currentTime + period;
mI = mI + 1;
aI = aI + 1;
gI = gI + 1;
%Worked only if accel and gyro have same freq and mag has freq / 2 ^ x
%Interpolated data
while gI <= gyroSize
    if mI <= magSize && currentDevice.tmag(mI) == currentTime
        AHRS.UpdateMARG([gx(gI) gy(gI) gz(gI)], [ax(aI) ay(aI) az(aI)], ...
            [mx_cal(mI) my_cal(mI) mz_cal(mI)]);
        mI = mI + 1;
    else
        if aI <= accelSize
            AHRS.UpdateIMU([gx(gI) gy(gI) gz(gI)], [ax(aI) ay(aI) az(aI)]);
        end
    end
    quatTime = [quatTime currentTime];
    quat = [quat; AHRS.Quaternion];
    aI = aI + 1;
    gI = gI + 1;
    currentTime = currentTime + period;
end

qtQuat = currentDevice.quaternion;
qtTime = currentDevice.tquat;

figure('NumberTitle', 'off', 'Name', strcat('Quaternion', names{1}));
hold on;
quatSec = quatTime ./ power(10, 6);
qtTimeSec = qtTime ./ power(10, 6);

plot(quatSec, quat(:, 1), '--k');
plot(quatSec, quat(:, 2), '--r');
plot(quatSec, quat(:, 3), '--g');
plot(quatSec, quat(:, 4), '--b');
plot(qtTimeSec, qtQuat(:, 1), 'k');
plot(qtTimeSec, qtQuat(:, 2), 'r');
plot(qtTimeSec, qtQuat(:, 3), 'g');
plot(qtTimeSec, qtQuat(:, 4), 'b');

title('Quaternion');
legend('q0', 'q1', 'q2', 'q3', 'q0qt', 'q1qt', 'q2qt', 'q3qt');

a_res = [];
m_res = [];
mI = 1;
aI = 1;
while currentDevice.tacc(aI) ~= quatTime(1)
    aI = aI + 1;
end
while currentDevice.tmag(mI) ~= quatTime(1)
    mI = mI + 1;
end

for i = 1:numel(quatTime) - 1
    if mI <= magSize && quatTime(i) == currentDevice.tmag(mI)
        m_res = [m_res; quatrotate(quat(i, :), [mx_cal(mI) my_cal(mI) mz_cal(mI)])];
        mI = mI + 1;
    end
    a_res = [a_res; quatrotate(quat(i, :), [ax(aI) ay(aI) az(aI)])];
    aI = aI + 1;
end

a_res_q = [];
m_res_q = [];
mI = 1;
aI = 1;
while currentDevice.tacc(aI) ~= qtTime(1)
    aI = aI + 1;
end
while currentDevice.tmag(mI) ~= qtTime(1)
    mI = mI + 1;
end

for i = 1:numel(qtTime) - 1
    if mI <= magSize && quatTime(i) == currentDevice.tmag(mI)
        m_res_q = [m_res_q; quatrotate(qtQuat(i, :), [mx_cal(mI) my_cal(mI) mz_cal(mI)])];
        mI = mI + 1;
    end
    a_res_q = [a_res_q; quatrotate(qtQuat(i, :), [ax(aI) ay(aI) az(aI)])];
    aI = aI + 1;
end

figure('NumberTitle', 'off', 'Name', strcat('AccelerometerRes', names{1}));
hold on;
plot(a_res(:, 1), '-r.');
plot(a_res(:, 2), '-g.');
plot(a_res(:, 3), '-b.');
title('Accelerometer');
legend('X', 'Y', 'Z');

figure('NumberTitle', 'off', 'Name', strcat('MagnetometerRes', names{1}));
hold on;
plot(m_res(:, 1), '-r.');
plot(m_res(:, 2), '-g.');
plot(m_res(:, 3), '-b.');
title('Magnetometer');
legend('X', 'Y', 'Z');

figure('NumberTitle', 'off', 'Name', strcat('AccelerometerResQt', names{1}));
hold on;
plot(a_res_q(:, 1), '-r.');
plot(a_res_q(:, 2), '-g.');
plot(a_res_q(:, 3), '-b.');
title('Accelerometer');
legend('X', 'Y', 'Z');

figure('NumberTitle', 'off', 'Name', strcat('MagnetometerResQt', names{1}));
hold on;
plot(m_res_q(:, 1), '-r.');
plot(m_res_q(:, 2), '-g.');
plot(m_res_q(:, 3), '-b.');
title('Magnetometer');
legend('X', 'Y', 'Z');

%BoxAnimation(data.Config.GyroDataRate, qtQuat);