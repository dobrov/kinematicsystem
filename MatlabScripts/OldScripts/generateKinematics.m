addpath('Utils');

clear;
%close all;
%clc;

fig = figure('NumberTitle', 'off', 'Name', 'Leg Animation');
lighting phong;
set(gcf, 'Renderer', 'zbuffer');
set(gca,'SortMethod','depth');
hold on;

axis equal;

q = zeros(4, 4);
qJoint = zeros(size(q, 1) - 1, 4);

%first
angle = 60;
ax = [0 1 0];
q(1, :) = [cosd(angle/2), -sind(angle/2) * (ax / norm(ax))];

angle = -70;
ax = [1 0 0];
q(1, :) = quatmultiply(q(1, :), [cosd(angle/2), -sind(angle/2) * (ax / norm(ax))]);

%second
angle = 120;
ax = [0 1 0];
q(2, :) = [cosd(angle/2), -sind(angle/2) * (ax / norm(ax))];

%third
angle = 60;
ax = [0 1 0];
q(3, :) = [cosd(angle/2), -sind(angle/2) * (ax / norm(ax))];

%fourth
angle = 120;
ax = [0 1 0];
q(4, :) = [cosd(angle/2), -sind(angle/2) * (ax / norm(ax))];

qRotated = zeros(size(q));
qRotatedJoint = zeros(size(qJoint));
for i = 1:size(qRotated, 1)
    rotatedVector = quatrotate(q(i, :), [1 0 0]);
    qRotated(i, :) = rotationTo([1 0 0], rotatedVector);
    disp(q(i, :));
    disp(qRotated(i, :));
%     if i >= 2
%         qJoint(i - 1, :) = quatmultiply(q(i, :), quatconj(q(i - 1, :)));
%         disp(quat2eul(qJoint(i - 1, :)));
%         qRotatedJoint(i - 1, :) = quatmultiply(qRotated(i, :), quatconj(qRotated(i - 1, :)));
%         disp(quat2eul(qRotatedJoint(i - 1, :)));
%     end
end

L = [5, 5, 5, 5];

colors = [0.8 0 0; 0 0.8 0; 0 0 0.8; 0.8 0 0.8];
point = zeros(5, 3);
point(1, :) = [0 0 0];
scatter3(point(1, 1), point(1, 2), point(1, 3), 'ko', 'filled');
for i = 2:5    
    point(i, :) = point(i - 1, :) + quatrotate(q(i - 1, :), [L(i - 1) 0 0]); 
    line([point(i - 1, 1) point(i, 1)], [point(i - 1, 2) point(i, 2)], ...
        [point(i - 1, 3) point(i, 3)], 'Color', colors(i - 1, :), 'LineWidth', 2);
    scatter3(point(i, 1), point(i, 2), point(i, 3), 'ko', 'filled');
    midPoint = point(i - 1, :) + (point(i, :) - point(i - 1, :)) / 2;
    xPoint = quatrotate(qRotated(i - 1, :), [1 0 0]);
    yPoint = quatrotate(qRotated(i - 1, :), [0 1 0]);
    zPoint = quatrotate(qRotated(i - 1, :), [0 0 1]);
    line([midPoint(1), midPoint(1) + xPoint(1)], [midPoint(2), midPoint(2) + xPoint(2)],...
        [midPoint(3), midPoint(3) + xPoint(3)], 'Color', [1 0 0], 'LineWidth', 3);
    line([midPoint(1), midPoint(1) + yPoint(1)], [midPoint(2), midPoint(2) + yPoint(2)],...
        [midPoint(3), midPoint(3) + yPoint(3)], 'Color', [0 1 0], 'LineWidth', 3);
    line([midPoint(1), midPoint(1) + zPoint(1)], [midPoint(2), midPoint(2) + zPoint(2)],...
        [midPoint(3), midPoint(3) + zPoint(3)], 'Color', [0 0 1], 'LineWidth', 3);
end

xLim = get(gca,'xlim');
yLim = get(gca,'ylim');
zLim = get(gca,'zlim');

xRange = xLim(2) - xLim(1);
yRange = yLim(2) - yLim(1);
zRange = zLim(2) - zLim(1);
ox = xLim(2) - xRange * 0.15;
oy = yLim(2) - yRange * 0.15;
oz = zLim(2) - zRange * 0.15;

line([ox, ox + xRange * 0.1], [oy, oy], [oz, oz], 'Color', [1, 0, 0], 'LineWidth', 3);
line([ox, ox], [oy, oy + yRange * 0.1], [oz, oz], 'Color', [0, 1, 0], 'LineWidth', 3);
line([ox, ox], [oy, oy], [oz, oz + zRange * 0.1], 'Color', [0, 0, 1], 'LineWidth', 3);

scatter3(ox, oy, oz, 'MarkerFaceColor', [0 0 0]);
scatter3(ox + xRange * 0.1, oy, oz, 'MarkerFaceColor', [1 0 0]);
scatter3(ox, oy + yRange * 0.1, oz, 'MarkerFaceColor', [0 1 0]);
scatter3(ox, oy, oz + zRange * 0.1, 'MarkerFaceColor', [0 0 1]);

% set(gca,'xtick',[]);
% set(gca,'xticklabel',[]);
% set(gca,'ytick',[]);
% set(gca,'yticklabel',[]);
% set(gca,'ztick',[]);
% set(gca,'zticklabel',[]);
