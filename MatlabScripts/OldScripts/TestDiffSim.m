% angle = 90;
% qEB1 = [cosd(angle/2), -sind(angle/2) * [0 0 1]];
% 
% angle = 270;
% % change axis from z
% qES1 = [cosd(angle/2), -sind(angle/2) * [0 0 0]];
% 
% angle = 180;
% % change axis from z
% diff = [cosd(angle/2), -sind(angle/2) * [0 0 0]];
% 
% qEB2 = quatmultiply(qEB1, diff);
% qES2 = quatmultiply(qES1, diff);
% 
% qSE1 = quatconj(qES1);
% qSE2 = quatconj(qES2);

% qSB1 = quatmultiply(qSE1, qEB1);
% 
% disp(qSB1);
% qSB2 = quatmultiply(qSE2, qEB2);
% qSB21 = quatmultiply(quatmultiply(quatconj(diff), qSE1), quatmultiply(qEB1, diff));
% disp(qSB21);
% disp(qSB2);

% qSB11 = quatmultiply(qEB1, qSE1);
% disp(qSB11);
% qSB21 = quatmultiply(qEB2, qSE2);
% disp(qSB21);

% mesh grid axis todo

% expand to all angles

perm = [0 0 1; 0 1 0; 0 1 1; 1 0 0; 1 0 1; 1 1 0; 1 1 1];
for i = 1:length(perm)
    perm(i, :) = perm(i, :) / norm(perm(i, :));
end
angles = 1:20:360;
for i = 1:length(perm)
    for j = 1:length(perm)
        for k = 1:length(perm)
            for m = 1:length(angles)
                for n = 1:length(angles)
                    for u = 1:length(angles)
                        qSB = quatnormalize([cosd(angles(m)/2), -sind(angles(m)/2) * perm(i, :)]);
                        qES = quatnormalize([cosd(angles(n)/2), -sind(angles(n)/2) * perm(j, :)]);
                        qGE = quatnormalize([cosd(angles(u)/2), -sind(angles(u)/2) * perm(k, :)]);
                        q1 = quatmultiply(quatmultiply(qSB, qES), qGE);
                        
                        qEG = quatconj(qGE);
                        qSE = quatconj(qES);
                        qBS = quatconj(qSB);
                        q2 = quatconj(quatmultiply(quatmultiply(qEG, qSE), qBS));
                        
                        if ~IsEqual(q1, q2)
                            disp('doesnt same');
                            fprintf('q1: %f %f %f %f\n', q1);
                            fprintf('q2: %f %f %f %f\n', q2);
                            fprintf('EB axis: %d %d %d\n', perm(i, :));
                            fprintf('ES axis: %d %d %d\n', perm(j, :));
                            fprintf('diff axis: %d %d %d\n\n', perm(k, :));
                        end
                    end
                end
            end
%             angle = 90;
%             qEB1 = quatnormalize([cosd(angle/2), -sind(angle/2) * perm(i, :)]);
% 
%             angle = 270;
%             qES1 = quatnormalize([cosd(angle/2), -sind(angle/2) * perm(j, :)]);
% 
%             angle = 180;
%             diff = quatnormalize([cosd(angle/2), -sind(angle/2) * perm(k, :)]);
%             
%             qEB2 = quatmultiply(qEB1, diff);
%             qES2 = quatmultiply(qES1, diff);
%             qSE1 = quatconj(qES1);
%             qSE2 = quatconj(qES2);
%                        
%             if IsEqual(quatmultiply(qSE1, qEB1),quatmultiply(qSE2, qEB2))
%                 disp('same');
%                 fprintf('EB axis: %f %f %f\n', perm(i, :));
%                 fprintf('ES axis: %f %f %f\n', perm(j, :));
%                 fprintf('diff axis: %f %f %f\n\n', perm(k, :));
%             end
        end
    end
end