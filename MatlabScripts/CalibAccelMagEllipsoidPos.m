close all;
clear;
%clc;
addpath('jsonlab');
addpath('Logs');
addpath('Utils');

fileName = '705calib.json';
rawData = loadjson(fileName);

isGyroBiasCal = true;

accelDataRate = rawData.Config.AccelDataRate;
magDataRate = rawData.Config.MagDataRate;
gyroDataRate = rawData.Config.GyroDataRate;

if accelDataRate ~= gyroDataRate
    disp('Accel data rate should be same as gyro data rate');
end

data = ProcessRawData(rawData);

accelFS = data.Config.AccelFullScale;
magFS = data.Config.MagFullScale;
gyroFS = data.Config.GyroFullScale;

staticPos = FindStaticPos(data, 'isAccel', true, 'isGyro', true, ...
    'isMag', true, 'isGyroBetween', true, 'isPlottingSteadyState', false);

names = SortNames(fieldnames(data));
nDevice = length(names);

for i = 1:nDevice
    ax = staticPos.(names{i}).Accel.x;
    ay = staticPos.(names{i}).Accel.y;
    az = staticPos.(names{i}).Accel.z;
        
    [~, BiasAccel] = CalEllipsoid([ax' ay' az']);    
    calAccel = ApplyCalibration(eye(3), BiasAccel, [ax' ay' az']);
    [ScaleAccel, ~] = CalPosition(calAccel, [0 0 1; 0 0 -1; 0 1 0; 0 -1 0; 1 0 0; -1 0 0]);
    calAccel = ApplyCalibration(ScaleAccel, BiasAccel, [ax' ay' az']);
    
    calData = struct('Bias', BiasAccel, 'Scale', ScaleAccel);
    calDataChar = savejson('', calData);
    fileID = fopen(strcat('Calibration/accelCalData', names{i}, 'Range', num2str(accelFS), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
    
    mx = data.(names{i}).mx;
    my = data.(names{i}).my;
    mz = data.(names{i}).mz;
    [ScaleMag, BiasMag] = CalEllipsoid([mx' my' mz']);

    mx = staticPos.(names{i}).Mag.x;
    my = staticPos.(names{i}).Mag.y;
    mz = staticPos.(names{i}).Mag.z;
    calMag = ApplyCalibration(ScaleMag, BiasMag, [mx' my' mz']);
    
    chosenPoint = 1;
    trueMag = calMag(chosenPoint, :);
    %trueMag = trueMag / norm(trueMag);
    truePoints = GetMag24Points([trueMag(1) trueMag(2) trueMag(3)]);
    [ScaleMag1, ~] = CalPosition(calMag, truePoints);
    ScaleMag = ScaleMag1 * ScaleMag;
    calMag = ApplyCalibration(ScaleMag, BiasMag, [mx' my' mz']);
    
    calData = struct('Bias', BiasMag, 'Scale', ScaleMag);
    calDataChar = savejson('', calData);
    fileID = fopen(strcat('Calibration/magCalData', names{i}, 'Range', ...
        num2str(data.Config.MagFullScale), '.json'), 'w');
    fprintf(fileID,'%s', calDataChar);
    fclose(fileID);
    
    gx = staticPos.(names{i}).Gyro.x;
    gy = staticPos.(names{i}).Gyro.y;
    gz = staticPos.(names{i}).Gyro.z;
    
    BiasGyro = [mean(gx), mean(gy), mean(gz)];
    
    if isGyroBiasCal
        calData = struct('Bias', BiasGyro, 'Scale', eye(3));
    else
        ScaleGyro = CalGyroAqua(calAccel, calMag, BiasGyro, ...
        staticPos.(names{i}).GyroBetween, data.Config.GyroDataRate);
        calData = struct('Bias', mean(BiasGyro), 'Scale', ScaleGyro);
    end
    
    calDataChar = savejson('', calData);
    fileID = fopen(strcat('Calibration/gyroCalData', names{i}, 'Range', num2str(gyroFS), '.json'), 'w');
    fprintf(fileID, '%s', calDataChar);
    fclose(fileID);
end    