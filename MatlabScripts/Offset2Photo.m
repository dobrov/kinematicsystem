addpath('Utils');
addpath('Offset');

%close all;
clear;
clc;
warning('off', 'Images:initSize:adjustingMag');

nJoints = 4;
len = 9.75;

leftImage = './InclineTest/left2.JPG';
backImage = './InclineTest/back2.JPG';

joints = zeros(nJoints, 3);

leftImageHandle = figure('NumberTitle', 'off', 'Name', 'XZ - Left image');
bitmapFirst = imread(leftImage);
imshow(bitmapFirst);
tightfig;
% scale
[x, z] = ginput_zoom(2);
leftScale = norm([(x(2) - x(1)) (z(2) - z(1))]) / len;
originX = x(1);
originZ = z(1);

rotVector = [(x(2) - x(1)) (z(2) - z(1))] / norm([(x(2) - x(1)) (z(2) - z(1))]);
rotAxis = cross([rotVector 0], [1 0 0]);
rotAngle = sign(rotAxis(3)) * acosd(dot(rotVector, [1 0]));
if abs(rotAngle) > 90
    rotAngle = rotAngle - sign(rotAngle) * 180;
end

% joints 
[x, z] = ginput_zoom(nJoints);

for i = 1:nJoints
    joints(i, 1) = (originX - x(i)) / leftScale;
    joints(i, 3) = (originZ - z(i)) / leftScale;
    joints(i, :) = roty(-rotAngle) * joints(i, :)'; 
end

backImageHandle = figure('NumberTitle', 'off', 'Name', 'YZ - Back image');
bitmapSecond = imread(backImage);
imshow(bitmapSecond);
tightfig;

% scale
[y, z] = ginput_zoom(2);
backScale = norm([(y(2) - y(1)) (z(2) - z(1))]) / len;

originY = y(1);
originZ = z(1);

rotVector = [(y(2) - y(1)) (z(2) - z(1))] / norm([(y(2) - y(1)) (z(2) - z(1))]);
rotAxis = cross([rotVector 0], [1 0 0]);
rotAngle = sign(rotAxis(3)) * acosd(dot(rotVector, [1 0]));
if abs(rotAngle) > 90
    rotAngle = rotAngle - sign(rotAngle) * 180;
end

[y, z] = ginput_zoom(nJoints);

tJoint = zeros(3, 1);
for i = 1:nJoints
    tJoint(2) = (originY - y(i)) / backScale;
    tJoint(3) = (originZ - z(i)) / backScale;
    tJoint = rotx(rotAngle) * tJoint;
    joints(i, 2) = tJoint(2);
end

segmentsFVectors = zeros(nJoints - 1, 3);
segmentsLength = zeros(nJoints - 1, 1);
segmentsQuat = zeros(nJoints - 1, 4);
for i = 1:nJoints - 1
    segmentsFVectors(i, :) = joints(i + 1, :) - joints(i, :);
    segmentsLength(i) = norm(segmentsFVectors(i, :));
    segmentsFVectors(i, :) = segmentsFVectors(i, :) / segmentsLength(i);
    segmentsQuat(i, :) = rotationTo([1 0 0], segmentsFVectors(i, :));
end
LHL = struct('Quat', segmentsQuat, 'Length', segmentsLength);
save(strcat('Offset/offset.mat'), 'LHL');

fig = figure('NumberTitle', 'off', 'Name', 'Leg Animation');
set(gcf, 'Renderer', 'zbuffer');
set(gca, 'SortMethod', 'depth');
hold on;

colors = [0.8 0 0; 0 0.8 0; 0 0 0.8; 0.8 0 0.8];
point = zeros(nJoints, 3);
point(1, :) = [0 0 0];
for i = 2:nJoints
    point(i, :) = point(i - 1, :) + quatrotate(segmentsQuat(i - 1, :), [segmentsLength(i - 1) 0 0]);
    line([point(i - 1, 1) point(i, 1)], [point(i - 1, 2) point(i, 2)], ...
        [point(i - 1, 3) point(i, 3)], 'Color', colors(i - 1, :), 'LineWidth', 2);
    scatter3(point(i, 1), point(i, 2), point(i, 3), 'ko', 'filled');
    midPoint = point(i - 1, :) + (point(i, :) - point(i - 1, :)) / 2;
    xPoint = quatrotate(segmentsQuat(i - 1, :), [1 0 0]);
    yPoint = quatrotate(segmentsQuat(i - 1, :), [0 1 0]);
    zPoint = quatrotate(segmentsQuat(i - 1, :), [0 0 1]);
    line([midPoint(1), midPoint(1) + xPoint(1)], [midPoint(2), midPoint(2) + xPoint(2)],...
        [midPoint(3), midPoint(3) + xPoint(3)], 'Color', [1 0 0], 'LineWidth', 3);
    line([midPoint(1), midPoint(1) + yPoint(1)], [midPoint(2), midPoint(2) + yPoint(2)],...
        [midPoint(3), midPoint(3) + yPoint(3)], 'Color', [0 1 0], 'LineWidth', 3);
    line([midPoint(1), midPoint(1) + zPoint(1)], [midPoint(2), midPoint(2) + zPoint(2)],...
        [midPoint(3), midPoint(3) + zPoint(3)], 'Color', [0 0 1], 'LineWidth', 3);
end
view([-180 0]);
axis tight;
axis equal;