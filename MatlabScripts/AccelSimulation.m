addpath('Utils');
clear;

len = 20;

gFrameVec = @(v) GetFrameVector(rotx(5) * roty(5), v);
dipAngle = 70;
g = gFrameVec([0 0 1]);
l = gFrameVec((roty(dipAngle) *[0 0 1]')');

rotQuats = zeros(len, 4);
rotQuats(1, :) = [1 0 0 0];
rotQuats(2, :) = quatmultiply([cosd(180/2), -sind(180/2) * gFrameVec([0 1 0])], rotQuats(1, :));
rotQuats(3, :) = quatmultiply([cosd(90/2), -sind(90/2) * gFrameVec([-1 0 0])], rotQuats(2, :));
rotQuats(4, :) = quatmultiply([cosd(180/2), -sind(180/2) * gFrameVec([1 0 0])], rotQuats(3, :));
rotQuats(5, :) = quatmultiply([cosd(90/2), -sind(90/2) * gFrameVec([0 -1 0])], rotQuats(4, :));
rotQuats(6, :) = quatmultiply([cosd(180/2), -sind(180/2) * gFrameVec([1 0 0])], rotQuats(5, :));
rotQuats(7:end, :) = GenerateRotQuats(len - 6);

accel = quatrotate(rotQuats, repmat(g, len, 1));
mag = quatrotate(rotQuats, repmat(l, len, 1));

sMag = [1 0.2 0.5;
        0.2 1 0.3;
        0.3 -0.1 1];        
bMag = [0.5; 0.1; -1];

magSensor = zeros(len, 3);
for i = 1:len
    magSensor(i, :) = (sMag \ mag(i, :)' + bMag)';
end
%magSensor = magSensor + 0.01 * randn(size(magSensor));

sAccel = [0.9342    0.1026    0.1;
          0.226    0.7987    0.0439;
          0.15    0.0439    1.1880];
bAccel = [0.05; -0.05; 0.01];

accelSensor = zeros(len, 3);
for i = 1:len
    accelSensor(i, :) = (sAccel \ accel(i, :)' + bAccel)';
end
%accelSensor = accelSensor + 0.01 * randn(size(accelSensor));

[sAccelRes, bAccelRes] = CalEllipsoid(accelSensor);

accelRes = zeros(size(accelSensor, 1), 3);
for i = 1:size(accelRes, 1)
    accelRes(i, :) = (sAccelRes * (accelSensor(i, :) - bAccelRes)')';
end

E = [1, 0, 0, 0];
values = GetAccel6Points(accelRes(1, :));
ObjFun = @(E) AccelPosCostFunction(E, accelRes(1:6, :), values);
options = optimset('MaxFunEvals', 150000, 'MaxIter', 6000, 'TolFun', 10^(-10));

[E, rsnorm] = lsqnonlin(ObjFun, E, [], [], options);
sAccelRes1 = quatnormalize(reshape(E(1:4), 1, 4));

sAccelRes = quat2rotm(quatconj(sAccelRes1)) * sAccelRes;

accelMagn = zeros(size(accelSensor, 1), 1);
for i = 1:size(accelMagn,1)
    accelRes(i, :) = (sAccelRes * (accelSensor(i, :) - bAccelRes)')';
    accelMagn(i) = norm(accelRes(i, :));
end

[sMagRes, bMagRes] = CalEllipsoid(magSensor);
magRes = zeros(size(magSensor));
for i = 1:size(magRes, 1)
    magRes(i, :) = (sMagRes * (magSensor(i, :) - bMagRes)')';
end

E = [1, 0, 0, 0];
ObjFun = @(E) RotMagCostFunction(E, accelRes, magRes);
options = optimset('MaxFunEvals', 150000, 'MaxIter', 6000, 'TolFun', 10^(-10));

[E, rsnorm] = lsqnonlin(ObjFun, E, [], [], options);
sMagRes1 = quatnormalize(reshape(E(1:4), 1, 4));
sMagRes = quat2rotm(quatconj(sMagRes1)) * sMagRes;

for i = 1:size(accelMagn,1)
    magRes(i, :) = (sMagRes * (magSensor(i, :) - bMagRes)')';
end

disp(sAccelRes);
disp(sAccel);

disp(bAccelRes);
disp(bAccel');

PlotSphere(roty(70)* [0 0 1]', sAccel, bAccel, sAccelRes, bAccelRes);
PlotSphere(roty(70)* [0 0 1]', sMag, bMag, sMagRes, bMagRes);

figure('NumberTitle', 'off', 'Name', 'Accel');
hold on;
plot(accel(:, 1), '-r');
plot(accel(:, 2), '-g');
plot(accel(:, 3), '-b');
plot(accelRes(:, 1), '--r');
plot(accelRes(:, 2), '--g');
plot(accelRes(:, 3), '--b');
title('Accel');
legend('x', 'y', 'z', 'xr', 'yr', 'zr');
hold off;

figure('NumberTitle', 'off', 'Name', 'Mag');
hold on;
plot(mag(:, 1), '-r');
plot(mag(:, 2), '-g');
plot(mag(:, 3), '-b');
plot(magRes(:, 1), '--r');
plot(magRes(:, 2), '--g');
plot(magRes(:, 3), '--b');
title('Mag');
legend('x', 'y', 'z', 'xr', 'yr', 'zr');
hold off;