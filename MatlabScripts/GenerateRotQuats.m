function rotQuats = GenerateRotQuats(len)

theta = acos(2 * rand(len, 1) - 1);
phi = 2 * pi .* rand(len, 1);
psi = 2 * pi .* rand(len, 1);

sinTheta = sin(theta);
rotQuats = axang2quat([sinTheta .* cos(phi) sinTheta .* sin(phi) cos(theta) psi]);

end