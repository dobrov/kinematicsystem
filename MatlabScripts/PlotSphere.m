function PlotSphere(sensorVector, realScale, realBias, calcScale, calcBias)

circleX = zeros(360, 3);
for i = 1:360
    circleX(i, :) = rotx(i) * sensorVector;
end

sensorVector = rotz(90) * sensorVector;
circleY = zeros(360, 3);
for i = 1:360
    circleY(i, :) = roty(i) * sensorVector;
end

sensorVector = rotx(90) * sensorVector;
circleZ = zeros(360, 3);
for i = 1:360
    circleZ(i, :) = rotz(i) * sensorVector;
end

for i = 1:360
    circleX(i, :) = calcScale * (realScale \ circleX(i, :)');
    circleY(i, :) = calcScale * (realScale \ circleY(i, :)');
    circleZ(i, :) = calcScale * (realScale \ circleZ(i, :)');   
end

lineX = [0 0 0; 1.1 0 0];
lineY = [0 0 0; 0 1.1 0];
lineZ = [0 0 0; 0 0 1.1];

rotQuats = GenerateRotQuats(1000);
sData = quatrotate(rotQuats, repmat(sensorVector', 1000, 1));

figure('NumberTitle', 'off', 'Name', 'Sphere');
grid on;
hold on;
scatter3(sData(:, 1), sData(:, 2), sData(:, 3), 4, [0.5 0.5 0.5], 'filled');
line(circleX(:, 1), circleX(:, 2), circleX(:, 3), 'Color', [1 0 0], 'LineWidth', 2);
line(lineX(:, 1), lineX(:, 2), lineX(:, 3), 'Color', [1 0 0], 'LineWidth', 2);
line(circleY(:, 1), circleY(:, 2), circleY(:, 3), 'Color', [0 1 0], 'LineWidth', 2);
line(lineY(:, 1), lineY(:, 2), lineY(:, 3), 'Color', [0 1 0], 'LineWidth', 2);
line(circleZ(:, 1), circleZ(:, 2), circleZ(:, 3), 'Color', [0 0 1], 'LineWidth', 2);
line(lineZ(:, 1), lineZ(:, 2), lineZ(:, 3), 'Color', [0 0 1], 'LineWidth', 2);
axis equal;
view(-50, 20);

end