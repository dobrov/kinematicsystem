%close all;
addpath('Utils');
g = [0; 0; 1];
dipAngle = 70;
l = (roty(dipAngle) * g);

sigmaMag = 0.1;
sigmaAccel = 0.1;
sigmaGyro = 0.005;
covU = diag([sigmaAccel sigmaAccel sigmaAccel, sigmaMag sigmaMag sigmaMag]);
covG = diag([sigmaGyro sigmaGyro sigmaGyro]);
m = zeros(3, 1080);
a = zeros(3, 1080);
w = zeros(3, 1080);

for i = 1:360
    a(:, i) = roty(i) * g;
    m(:, i) = roty(i) * l;
    w(:, i) = [0 pi/180 0];
end
for i = 1:360
    a(:, i + 360) = roty(i) * g;
    m(:, i + 360) = roty(i) * l;
    w(:, i + 360) = [0 pi/180 0];
end

for i = 1:360
    a(:, i + 720) = rotz(i) * g;
    m(:, i + 720) = rotz(i) * l;
    w(:, i + 720) = [0 0 pi/180];
end
aNoised = a + sigmaAccel * randn(size(a));
mNoised = m + sigmaMag * randn(size(a));
wNoised = w + sigmaGyro * randn(size(a));
qMeas = zeros(size(a) + [1 0]);
qState = zeros(size(a) + [1 0]);
ares = zeros(size(a));
mres = zeros(size(a));

state = [1; 0; 0; 0];
prevMeas = [0; 0; 0; 0];
P = diag([1000, 1000, 1000, 1000]);
AHRS = ComplementaryFilter(1);
for i = 1:1080
    [meas, J] = AquaMeasurement(aNoised(:, i), mNoised(:, i), prevMeas);
    R = J * covU * J';
    %R = diag([sigmaMag, sigmaMag, sigmaMag, sigmaMag]);
    screw = [0 -wNoised(3, i) wNoised(2, i); 
             wNoised(3, i) 0 -wNoised(1, i);
             -wNoised(2, i) wNoised(1, i) 0];
    omega = [0, wNoised(:, i)';
             -wNoised(:, i), -screw];
    F = eye(4)- 1/2 * omega;
    predicted = F * state;
    predicted = predicted / norm(predicted);
    ksi = [state(2) state(3) state(4);
       -state(1) -state(4) -state(3);
       state(3) -state(1) -state(2);
       -state(3) state(2) -state(1)];
%     ksi = [predicted(2) predicted(3) predicted(4);
%            -predicted(1) -predicted(4) -predicted(3);
%            predicted(3) -predicted(1) -predicted(2);
%            -predicted(3) predicted(2) -predicted(1)];
       
    Q = 1/4 * ksi * covG * ksi';
    Ppred = F * P * F' + Q;
    residual = meas' - predicted;
    S = Ppred + R;
    K = Ppred/S;
    
    state = predicted + K * residual;
    state = state / norm(state);
    P = (eye(4) - K) * Ppred;
    qState(:, i) = state;
    AHRS.UpdateMARG(rad2deg(-wNoised(:, i)'), aNoised(:, i)', mNoised(:, i)');
    qMeas(:, i) = meas;
    prevMeas = meas';
end

figure('NumberTitle', 'off', 'Name', 'Quat measured');
hold on;
% plot(qState(1, :), '-k.');
% plot(qState(2, :), '-r.');
% plot(qState(3, :), '-g.');
% plot(qState(4, :), '-b.');
plot(qMeas(1, :), '-k.');
plot(qMeas(2, :), '-r.');
plot(qMeas(3, :), '-g.');
plot(qMeas(4, :), '-b.');
title('Quat Measured');
hold off;

% figure('NumberTitle', 'off', 'Name', 'Accel');
% hold on;
% plot(a(1, :), '-r.');
% plot(a(2, :), '-g.');
% plot(a(3, :), '-b.');
% plot(ares(1, :), '-m.');
% plot(ares(2, :), '-k.');
% plot(ares(3, :), '-y.');
% title('Accel');
% legend('ax', 'ay', 'az', 'axr', 'ayr', 'azr');
% hold off;
% 
% figure('NumberTitle', 'off', 'Name', 'Mag');
% hold on;
% plot(m(1, :), '-r.');
% plot(m(2, :), '-g.');
% plot(m(3, :), '-b.');
% plot(mres(1, :), '-m.');
% plot(mres(2, :), '-k.');
% plot(mres(3, :), '-y.');
% title('Mag');
% legend('mx', 'my', 'mz', 'mxr', 'myr', 'mzr');
% hold off;
