function [residuals] = RotCostFunction(E, accel, mag)
sAccel = reshape(E(1:4), 1, 4);
sMag = reshape(E(5:end), 1, 4);

sAccel = quatnormalize(sAccel);
sMag = quatnormalize(sMag);

dotProduct = zeros(length(accel), 1);
%disp('---------------');
for i = 1:length(accel)
    calAccel = quatrotate(sAccel, accel(i, :) / norm(accel(i, :)));
    calMag = quatrotate(sMag, mag(i, :) / norm(mag(i, :)));   
    dotProduct(i) = dot(calAccel, calMag);
    %resQuats = AquaMeasurement(calAccel, calMag);
%     disp(quatrotate(resQuats, calMag));
%     disp(quatrotate(resQuats, calAccel)); 
end

posComb = nchoosek(1:length(accel), 2);
residuals = zeros(length(posComb), 1);
for i = 1:length(posComb)
    residuals(i) = abs(dotProduct(posComb(i, 1)) - dotProduct(posComb(i, 2)));
end
end

